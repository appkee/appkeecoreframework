//
//  ButtonView.swift
//  AppkeeCore
//
//  Created by Radek Zmeškal on 08/10/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeButtonView: UIView {

    //MARK: - Outlets
    @IBOutlet weak var button: UIButton!
    
    var handler: () -> Void = { () in }
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
    }
        
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

     //MARK: - Actions
    @IBAction func selectionButtonTap(_ sender: Any) {
        self.handler()
    }
       
    // MARK: - Functions
    func configure(with title: String, colorSettings: AppkeeColorSettings?, handler: @escaping () -> Void) {
        button.setTitle(title, for: .normal)
        self.handler = handler
        
        if let colorSettings = colorSettings {
            button.backgroundColor = colorSettings.contentText
            button.setTitleColor(colorSettings.content, for: .normal)
        }
    }
}

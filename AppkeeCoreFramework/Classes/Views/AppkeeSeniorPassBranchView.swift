//
//  AppkeeSeniorPassBranchView.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/27/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeSeniorPassBranchTableViewCell: UITableViewCell {

    @IBOutlet weak var containerLabel: UIView!

    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var addressTitleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!

    @IBOutlet weak var discountTitleLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!



    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }


    
}

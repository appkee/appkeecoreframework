//
//  AppkeeSelectionView.swift
//  AppkeeCore
//
//  Created by Radek Zmeškal on 03/10/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeSelectionView: UIView {

    //MARK: - Properties
    
    private lazy var pickerView: PickerView = {
        let picker = PickerView()
        picker.handler = { [weak self] (item) in
            guard let self = self else { return }
            
            self.inputTextField.text = item.name
            self.handler(item)
        }
        return picker
    }()
    
    //MARK: - Outlet
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var inputTextField: UITextField! {
        didSet {
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 5))
            inputTextField.leftView = paddingView
            inputTextField.leftViewMode = .always
            inputTextField.inputView = pickerView
            inputTextField.tintColor = .clear
        }
    }
    
//    @IBOutlet weak var selectionButton: UIButton! {
//        didSet {
//            //translate
//            selectionButton.setTitle(TR_FORM_SELECTION, for: .normal)
//        }
//    }
    
    var handler: (AppkeeFieldItem) -> Void = { (item) in }
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK: - Actions
//    @IBAction func selectionButtonTap(_ sender: Any) {
//        self.handler()
//    }
    
    // MARK: - Functions
    func configure(with title: String, items: [AppkeeFieldItem], colorSettings: AppkeeColorSettings?, handler: @escaping (AppkeeFieldItem) -> Void) {
        titleLabel.text = title
        self.handler = handler
        self.pickerView.colorSettings = colorSettings
        self.pickerView.inputs = items
        if let item = items.first {
            self.inputTextField.text = item.name
            self.handler(item)
        }
        
        if let colorSettings = colorSettings {
//            self.pickerView.backgroundColor
            self.pickerView.tintColor = colorSettings.contentText
            
            
            titleLabel.textColor = colorSettings.contentText
            
            inputTextField.textColor = colorSettings.contentText
            
            inputTextField.layer.borderWidth = 1
            inputTextField.layer.borderColor = colorSettings.contentText.cgColor
//            selectionButton.setTitleColor(colorSettings.contentText, for: .normal)
        }
    } 
    
    func updateSelection(title: String) {
//        selectionButton.setTitle(title, for: .normal)
        inputTextField.text = title
    }
}

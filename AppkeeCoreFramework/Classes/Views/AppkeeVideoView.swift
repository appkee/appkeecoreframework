//
//  AppkeeVideoView.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 27/09/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit
import YoutubePlayer_in_WKWebView

class AppkeeVideoView: UIView {

    //MARK: - Outlet
    @IBOutlet private weak var playerView: WKYTPlayerView!
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Functions
    func configure(with link: String) {
        
        let playerVars = ["playsinline": 1]
        
        playerView.load(withVideoId: link, playerVars: playerVars)
    }
}

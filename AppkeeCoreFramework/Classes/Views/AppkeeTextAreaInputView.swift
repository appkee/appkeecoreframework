//
//  AppkeeTextAreaInputView.swift
//  AppkeeCore
//
//  Created by Radek Zmeškal on 03/10/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeTextAreaInputView: UIView {

    //MARK: - Outlet
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var textView: UITextView! {
        didSet {
            textView.delegate = self
            textView.layer.borderWidth = 1.0
            textView.layer.borderColor = UIColor.black.cgColor
            textView.textColor = textColor
            
            textView.textContainerInset = UIEdgeInsets(top: 5, left: 3, bottom: 5, right: 3)
        }
    }
    
    //MARK: - Properties
    var placeholder: String? = nil
    var textColor: UIColor = UIColor.lightGray
    var handler: (String) -> Void = { (value) in }
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()                
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Functions
    func configure(with title: String, colorSettings: AppkeeColorSettings?, handler: @escaping (String) -> Void) {
//        placeholder = title
//        textView.text = placeholder
        titleLabel.text = title
        self.handler = handler
        
        if let colorSettings = colorSettings {
            textColor = colorSettings.contentText
            
            titleLabel.textColor = colorSettings.contentText
            titleLabel.tintColor = colorSettings.contentText
            
            textView.layer.borderWidth = 1
            textView.layer.borderColor = colorSettings.contentText.cgColor
        }
    }
}
    
extension AppkeeTextAreaInputView: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        handler(textView.text ?? "")
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = textColor
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeholder
            textView.textColor = UIColor.lightGray
        }
    }
}





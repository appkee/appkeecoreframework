//
//  AppkeeCheckboxesView.swift
//  AppkeeCore
//
//  Created by Radek Zmeškal on 03/10/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeCheckboxesView:  UIView {

   //MARK: - Outlet
   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var stackView: UIStackView!
   
   var handler: (Int, Bool) -> Void = { (id, value) in }
   
   // MARK: - Lifecycle
   override init(frame: CGRect) {
       super.init(frame: frame)
       self.loadNib()
   }

   required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
   }
   
   //MARK: - Actions
   
   // MARK: - Functions
   func configure(with title: String, items:[AppkeeFieldItem], colorSettings: AppkeeColorSettings?, handler: @escaping (Int, Bool) -> Void) {
        titleLabel.text = title
        self.handler = handler
       
        for item in items {
            let checkBoxView = AppkeeCheckboxView(frame: .zero)
            checkBoxView.configure(with: item.name ?? "", colorSettings: colorSettings) { (value) in
                handler(item.id, value)
            }
            
            stackView.addArrangedSubview(checkBoxView)
        }
       
        if let colorSettings = colorSettings {
            titleLabel.textColor = colorSettings.contentText
        }
   }
}

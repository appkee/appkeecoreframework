//
//  AppkeeBannerView.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 12/13/20.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AppkeeBannerView: UIView {

    @IBOutlet weak var separatorImageView: UIImageView!

    @IBOutlet weak var closeTopButton: UIButton!

    @IBOutlet weak var closeBottomButton: UIButton!

    @IBOutlet weak var bannerView: GADBannerView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    var delegate: AppkeeAdvertisementViewDelegate?

    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
    }

     //MARK: - Actions
    @IBAction func closeButtonTap(_ sender: Any) {
        self.delegate?.close()
    }

    // MARK: - Functions
    func configure(with adUnitID: String, position: AppkeeAdvertisement.Position, colorSettings: AppkeeColorSettings) {
        separatorImageView.backgroundColor = colorSettings.header

        closeTopButton.tintColor = colorSettings.headerText
        closeBottomButton.tintColor = colorSettings.headerText

        switch position {
        case .top:
            closeTopButton.isHidden = true
            closeBottomButton.isHidden = false
        case .bottom:
            closeTopButton.isHidden = false
            closeBottomButton.isHidden = true
        }

        self.bannerView.adUnitID = adUnitID
        self.bannerView.delegate = self
        self.bannerView.load(GADRequest())
        self.bannerView.rootViewController = UIApplication.shared.windows.first!.rootViewController
    }
}

extension AppkeeBannerView: GADBannerViewDelegate {

    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("recieced banner")
    }

    func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error) {
        print("\(error)")
    }
}

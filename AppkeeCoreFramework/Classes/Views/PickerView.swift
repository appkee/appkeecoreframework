//
//  PickerView.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 18/02/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import UIKit

final class PickerView: UIPickerView {
    
    // MARK: - Types
    
    // MARK: - Properties
    var colorSettings: AppkeeColorSettings?
    var inputs: [AppkeeFieldItem] = []
    var handler: ((AppkeeFieldItem) -> Void)?
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonSetup()
    }
    
    // MARK: - Setup
    
    private func commonSetup() {
        // Set picker view delegate and data source
        delegate = self
        dataSource = self
    }
}

// MARK: - UIPickerViewDataSource

extension PickerView: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return inputs.count
    }
}

// MARK: - UIPickerViewDelegate

extension PickerView: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        handler?(inputs[row])
    }
    
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        return inputs[row].name
//    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var attributes: [NSAttributedString.Key: Any] = [:]
        if let colorSettings = self.colorSettings {
            attributes[NSAttributedString.Key.foregroundColor] = colorSettings.contentText
        }
        
        return NSAttributedString(string: inputs[row].name ?? "", attributes: attributes)
    }
}

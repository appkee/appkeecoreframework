//
//  AppkeeTitleView.swift
//  AppkeeCore
//
//  Created by Radek Zmeškal on 08/10/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeTitleView: UIView {

    //MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var separatorImageView: UIImageView!
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
    }
        
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

    // MARK: - Functions
    func configure(with title: String, colorSettings: AppkeeColorSettings?) {
        titleLabel.text = title
        
        if let colorSettings = colorSettings {
            titleLabel.textColor = colorSettings.contentText
            separatorImageView.backgroundColor = colorSettings.contentText
        }
    }
}

//
//  AdvertisementView.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 26/01/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import UIKit

protocol AppkeeAdvertisementViewDelegate: class {
    func close()
    func open()
}

class AppkeeAdvertisementView: UIView {

    //MARK: - Outlets
    @IBOutlet weak var separatorImageView: UIImageView!
    
    @IBOutlet weak var openButton: UIButton!
    
    @IBOutlet weak var closeTopButton: UIButton!
    
    @IBOutlet weak var closeBottomButton: UIButton!
        
    @IBOutlet weak var imageView: UIImageView! 
    
    var delegate: AppkeeAdvertisementViewDelegate?
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
    }
        
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

     //MARK: - Actions
    @IBAction func closeButtonTap(_ sender: Any) {
        self.delegate?.close()
    }
    
    @IBAction func openButtonTap(_ sender: Any) {
        self.delegate?.open()
    }
    
    // MARK: - Functions
    func configure(with image: UIImage, position: AppkeeAdvertisement.Position, colorSettings: AppkeeColorSettings) {
//        self.layer.borderColor = colorSettings.header.cgColor
//        self.layer.borderWidth = 1.0
        
        separatorImageView.backgroundColor = colorSettings.header
        
        closeTopButton.tintColor = colorSettings.headerText
        closeBottomButton.tintColor = colorSettings.headerText
        
        self.imageView.image = image
        
        switch position {
        case .top:
            closeTopButton.isHidden = true
            closeBottomButton.isHidden = false
        case .bottom:
            closeTopButton.isHidden = false
            closeBottomButton.isHidden = true
        }
    }
}

//
//  AppkeeTextInputView.swift
//  AppkeeCore
//
//  Created by Radek Zmeškal on 03/10/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeTextInputView: UIView {

    //MARK: - Outlet
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var textField: UITextField! {
        didSet {
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 5))
            textField.leftView = paddingView
            textField.leftViewMode = .always
        }
    }
    
    var handler: (String) -> Void = { (value) in }
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
        
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Functions
    func configure(with title: String, numeric: Bool = false, colorSettings: AppkeeColorSettings?, handler: @escaping (String) -> Void) {
        titleLabel.text = title
//        textField.placeholder = title
        self.handler = handler
        
        if numeric {
            textField.keyboardType = .numbersAndPunctuation
        }
        
        if let colorSettings = colorSettings {
            titleLabel.textColor = colorSettings.contentText
            
            textField.textColor = colorSettings.contentText
            textField.tintColor = colorSettings.contentText
            
            textField.layer.borderWidth = 1
            textField.layer.borderColor = colorSettings.contentText.cgColor
        }
    }

    //MARK: - Actions
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        handler(textField.text ?? "")
    }
}


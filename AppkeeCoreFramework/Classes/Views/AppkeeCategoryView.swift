//
//  AppkeCategoryView.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/25/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeCategoryView: UIView {

    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var valueSwitch: UISwitch!

    var handler: (Bool) -> Void = { _ in }

    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // MARK: - Functions
    func configure(with title: String, id: Int, colorSettings: AppkeeColorSettings?, handler: @escaping (Bool) -> Void) {
        self.handler = handler

        self.titleLabel.text = title
        self.valueSwitch.isOn = false

        if let colorSettings = colorSettings {
            self.valueSwitch.onTintColor = colorSettings.header
        }
    }

    //MARK: - Actions
    @IBAction func buttonTap(_ sender: UISwitch) {
        self.handler(sender.isOn)
    }
    
}

//
//  AppkeeTextView.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 26/09/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit
import WebKit

protocol AppkeeTextViewDelegate {
    func openLink(link: String, name: String);
    func openDeeplink(params: String);
}

class AppkeeTextView: UIView {
    
    //MARK: - Outlets
    private var webView: WKWebView?
    
    var delegate: AppkeeTextViewDelegate?
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // MARK: - Functions
    func configure(with content: String?, colorSettings: AppkeeColorSettings?) {
        let webView = WKWebViewSizeToFit(frame: self.frame)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.scrollView.isScrollEnabled = false
        webView.scrollView.bounces = false
        webView.sizeToFit()
        webView.isOpaque = false
        webView.sizeToFitDelegate = self
//        webView.configuration.dataDetectorTypes = [
//            WKDataDetectorTypes.phoneNumber,
////            WKDataDetectorTypes.address
//            WKDataDetectorTypes.calendarEvent,
//            WKDataDetectorTypes.trackingNumber,
//            WKDataDetectorTypes.flightNumber,
//            WKDataDetectorTypes.lookupSuggestion
//        ];
//        webView.navigationDelegate = self
        
        webView.isUserInteractionEnabled = true
        
        self.addSubview(webView)
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(4)-[webView]-(4)-|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["webView" : webView]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(20)-[webView]-(20)-|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["webView" : webView]))
                
        if let content = content {
            webView.loadHTMLString(content.toHtml(colorSettings: colorSettings), baseURL: nil)
        }
        
        self.webView = webView
    }
}

extension AppkeeTextView: WKWebViewSizeToFitDelegate {

    func sizeUpdated() {
            
    }
    
    func openLink(link: String, name: String) {
        delegate?.openLink(link: link, name: name)
    }

    func openDeeplink(params: String) {
        delegate?.openDeeplink(params: params)
    }

}

//extension TextView: WKNavigationDelegate {
//
//    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        self.superview?.layoutSubviews()
//    }
//}

//
//  AppkeeImageView.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 26/09/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeImageView: UIView {
    
    //MARK: - Outlets
    @IBOutlet weak var imageImageView: UIImageViewSizeToFit!
    
    @IBOutlet weak var button: UIButton!
    
    //MARK: - Properties
    var handler: () -> Void = { () in }
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
    }
        
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

    // MARK: - Functions
    func configure(with image: UIImage, handler: @escaping () -> Void) {
        imageImageView.image = image
        self.handler = handler
    }

    func configure(with image: UIImage) {
        imageImageView.image = image
    }
    
    @IBAction func buttonTap(_ sender: Any) {
        handler()
    }
}

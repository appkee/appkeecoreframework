//
//  AppkeePhotoCaptureViewTableViewCell.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/6/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeePhotoCaptureView: UIView {
    //MARK: - Outlet

    @IBOutlet weak var imageView: UIView!

    @IBOutlet weak var imageImageView: UIImageView!

    @IBOutlet weak var photoView: UIView!

    @IBOutlet weak var photoImageView: UIImageView!

    @IBOutlet weak var buttonView: UIView!

    @IBOutlet weak var takePhotoButton: UIButton!

    var handler: () -> Void = { () in }

    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // MARK: - Functions
    func configure(with image: UIImage, id: Int, colorSettings: AppkeeColorSettings?, handler: @escaping () -> Void) {
        self.handler = handler

        imageImageView.image = image

        takePhotoButton.setTitle(translations.photoPage.TAKE_PHOTO, for: .normal)

        if let colorSettings = colorSettings {
            takePhotoButton.backgroundColor = colorSettings.contentText
            takePhotoButton.setTitleColor(colorSettings.content, for: .normal)
        }
    }

    func setPhoto(image: UIImage) {
        self.photoImageView.image = image
        self.photoView.isHidden = false
    }

    //MARK: - Actions
    @IBAction func buttonTap(_ sender: Any) {
        self.handler()
    }
}

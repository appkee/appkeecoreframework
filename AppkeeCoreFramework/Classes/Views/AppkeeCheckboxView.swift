//
//  AppkeeCheckboxView.swift
//  AppkeeCore
//
//  Created by Radek Zmeškal on 03/10/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeCheckboxView: UIView {

    //MARK: - Outlet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkBox: UISwitch!
    
    var handler: (Bool) -> Void  = { (value) in }
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK: - Actions
    @IBAction func switchChange(_ sender: UISwitch) {
        handler(sender.isOn)
    }
    
    // MARK: - Functions
    func configure(with title: String, isOn: Bool = true, colorSettings: AppkeeColorSettings?, handler: @escaping (Bool) -> Void) {
        self.titleLabel.text = title
        self.checkBox.isOn = isOn
        self.handler = handler
        
        if let colorSettings = colorSettings {
            titleLabel.textColor = colorSettings.contentText
            checkBox.tintColor = colorSettings.contentText
            checkBox.onTintColor = colorSettings.contentText
        }
    }
}

//
//  AppkeeBannerViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 1/31/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import UIKit

protocol AppkeeBannerViewControllerDelegate: class {
    func openLink(link: String)
    func openDeeplink(params: String)
}

class AppkeeBannerViewController: UIViewController {

//    enum Type {
//        case single
//        case adMob
//        case banners
//    }

    private let topStackView = UIStackView()
    private let bottomStackView = UIStackView()

    private let containerViewController = UIView()


    private var banners: [AppkeeAdvertisement] = []
    private var bannerPosition: Int = -1

    private var timer: Timer? = nil

    private var viewController: UIViewController? = nil
    private var bannerView: AppkeeAdvertisementView? = nil

    private let graphicManager: AppkeeGraphicManager

    weak var delegate: AppkeeBannerViewControllerDelegate?

    init( graphicManager: AppkeeGraphicManager) {
        self.graphicManager = graphicManager

        super.init(nibName: nil, bundle: nil)

        if let contentColor = graphicManager.colorsSettings?.content {
            self.view.backgroundColor = contentColor
        }

        self.containerViewController.backgroundColor = .clear
        self.topStackView.backgroundColor = .clear
        self.bottomStackView.backgroundColor = .clear

        view.addSubview(self.topStackView)
        view.addSubview(self.containerViewController)
        view.addSubview(self.bottomStackView)

        self.containerViewController.translatesAutoresizingMaskIntoConstraints = false
        self.topStackView.translatesAutoresizingMaskIntoConstraints = false
        self.bottomStackView.translatesAutoresizingMaskIntoConstraints = false

        self.topStackView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        self.topStackView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        self.topStackView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true

        self.containerViewController.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        self.containerViewController.topAnchor.constraint(equalTo: self.topStackView.bottomAnchor, constant: 0).isActive = true
        self.containerViewController.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true

        self.bottomStackView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        self.bottomStackView.topAnchor.constraint(equalTo: self.containerViewController.bottomAnchor, constant: 0).isActive = true
        self.bottomStackView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        self.bottomStackView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true

        self.topStackView.distribution = .equalSpacing
        self.topStackView.axis = .vertical
        self.topStackView.spacing = 0

        self.bottomStackView.distribution = .equalSpacing
        self.bottomStackView.axis = .vertical
        self.bottomStackView.spacing = 0

        self.topStackView.addArrangedSubview(UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0)))
        self.bottomStackView.addArrangedSubview(UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0)))
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewController?.viewWillAppear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewController?.viewDidAppear(animated)
        self.containerViewController.layoutIfNeeded()
    }

    func setViewController(_ controller: UIViewController) {

        self.viewController = controller

        self.navigationItem.rightBarButtonItems = controller.navigationItem.rightBarButtonItems
        self.title = controller.title

        controller.view.translatesAutoresizingMaskIntoConstraints = false
        containerViewController.addSubview(controller.view)

        controller.view.leftAnchor.constraint(equalTo: self.containerViewController.leftAnchor, constant: 0).isActive = true
        controller.view.topAnchor.constraint(equalTo: self.containerViewController.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        controller.view.rightAnchor.constraint(equalTo: self.containerViewController.rightAnchor, constant: 0).isActive = true
        controller.view.bottomAnchor.constraint(equalTo: self.containerViewController.bottomAnchor, constant: 0).isActive = true

//        self.addChild(controller)
    }

    func removeBanners() {
        for subview in topStackView.arrangedSubviews {
            if subview.isKind(of: AppkeeAdvertisementView.self) {
                topStackView.removeArrangedSubview(subview)
                subview.removeFromSuperview()
            }

            if subview.isKind(of: AppkeeBannerView.self) {
                topStackView.removeArrangedSubview(subview)
                subview.removeFromSuperview()
            }
        }

        for subview in bottomStackView.arrangedSubviews {
            if subview.isKind(of: AppkeeAdvertisementView.self) {
                bottomStackView.removeArrangedSubview(subview)
                subview.removeFromSuperview()
            }

            if subview.isKind(of: AppkeeBannerView.self) {
                bottomStackView.removeArrangedSubview(subview)
                subview.removeFromSuperview()
            }
        }
    }

    func setBanners( banners: [AppkeeAdvertisement]) {
        self.removeBanners()

        self.banners = banners

        if banners.count > 1, !UserDefaults.bannersOff() {
            self.addBanners(banners)
            return
        }

        if banners.count == 1 {
            let banner = banners[0]

            if banner.visibility, !UserDefaults.containsAdvertisement(advertisementId: banner.id) {
                // find visible adMob
                if banner.adUnitID != nil {
                    self.addAbMob(banner)
                } else {
                    self.addBanner(banner)
                }
            }
        }
    }

    func addAbMob(_ banner: AppkeeAdvertisement) {
        guard let colorSettings = graphicManager.colorsSettings, let adUnitID = banner.adUnitID else {
            return
        }

        let bannerView = AppkeeBannerView(frame: .zero)
        bannerView.delegate = self

        let height = self.view.frame.size.width / 5.0

        bannerView.heightAnchor.constraint(equalToConstant: height).isActive = true

        switch banner.position {
        case .bottom:
            self.bottomStackView.addArrangedSubview(bannerView)
        case .top:
            self.topStackView.addArrangedSubview(bannerView)
        }

        self.bannerPosition = 0

        bannerView.layoutIfNeeded()

        bannerView.configure(with: adUnitID, position: banner.position, colorSettings: colorSettings)
    }

    func addBanner(_ banner: AppkeeAdvertisement) {
        guard let colorsSettings = graphicManager.colorsSettings, let image = graphicManager.getImage(name: banner.image) else {
            return
        }

        let bannerView = AppkeeAdvertisementView(frame: .zero)
        bannerView.delegate = self

        let height = self.view.frame.size.width / 5.0

        bannerView.heightAnchor.constraint(equalToConstant: height).isActive = true

        switch banner.position {
        case .bottom:
            self.bottomStackView.addArrangedSubview(bannerView)
        case .top:
            self.topStackView.addArrangedSubview(bannerView)
        }

        self.bannerPosition = 0

        bannerView.layoutIfNeeded()

        bannerView.configure(with: image, position: banner.position, colorSettings: colorsSettings)
    }

    func addBanners(_ banners: [AppkeeAdvertisement]) {

        let bannerView = AppkeeAdvertisementView(frame: .zero)
        bannerView.delegate = self

        let height = self.view.frame.size.width / 5.0

        bannerView.heightAnchor.constraint(equalToConstant: height).isActive = true

        switch banners[0].position {
        case .bottom:
            self.bottomStackView.addArrangedSubview(bannerView)
        case .top:
            self.topStackView.addArrangedSubview(bannerView)
        }

        self.bannerView = bannerView

        self.bannerPosition = 0

        refreshBanner()
    }

    @objc private func refreshBanner() {
        guard let colorsSettings = graphicManager.colorsSettings else {
            return
        }

        self.bannerPosition += 1

        if self.bannerPosition >= self.banners.count {
            self.bannerPosition = 0
        }
//
//        guard let image = graphicManager.getImage(name: banner.image)  else {
//
//        }


        let banner = banners[self.bannerPosition]

        if let image = graphicManager.getImage(name: banner.image) {
            bannerView?.configure(with: image, position: banner.position, colorSettings: colorsSettings)
        }

        self.timer = Timer.scheduledTimer(timeInterval: Double(banner.duration ?? 5), target: self, selector: #selector(self.refreshBanner), userInfo: nil, repeats: false)
    }
}

extension AppkeeBannerViewController: AppkeeAdvertisementViewDelegate {
    func close() {
        if banners.count > 1 {
            UserDefaults.bannersAdd()

            timer?.invalidate()
        } else {
            let banner = self.banners[self.bannerPosition]

            UserDefaults.addAdvertisement(advertisementId: banner.id)
        }

        self.removeBanners()
    }

    func open() {
        let banner = self.banners[self.bannerPosition]

        if let link = banner.url {
            let deeplink = link.replacingOccurrences(of: "http://", with: "")
            if deeplink.hasPrefix("[\(AppkeeConstants.deeplinkSection)") {
                delegate?.openDeeplink(params: deeplink)
                return
            }
            delegate?.openLink(link: link)
        }
    }
}


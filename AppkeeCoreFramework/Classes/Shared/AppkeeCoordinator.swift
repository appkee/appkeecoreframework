//
//  Coordinator.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 05/05/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

enum AppkeeCoordinatorRoutes {
    case loader
    case menu
    case customLoader
    case login
    case pass(passType: PassType)
    case photoReport
    case pin(pinCode: String)
    case notificationAreas
    case loginRegister
}

public protocol AppkeeCoordinator: class {
    var childrens: [AppkeeCoordinator] { get set }
    var dependencies: AppkeeFullDependencies { get set }
    func start(root: Bool)
//    func start(with routeType: DeepLinkRouteType, params: [DeepLinkParams]?)
}

protocol AppkeeCoordinatorDelegate: class {
    func navigate(to: AppkeeCoordinatorRoutes)
    func newApp()
}

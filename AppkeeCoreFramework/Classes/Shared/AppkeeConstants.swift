//
//  Constants.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 06/05/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

public final class AppkeeConstants {

    static let accessToken = "2a6c67e8c0df82c3551aceb1b8d44e114ded28a5"
    static let appCode = "567"
//    static let appCode = "95"
    
    public static let appkeeColor = UIColor(red: 0.0, green: 152.0/255.0, blue: 215.0/255.0, alpha: 1.0)
    public static let loginColor = UIColor(red: 244.0/255.0, green: 89.0/255.0, blue: 10.0/255.0, alpha: 1.0)
            
    static let baseImageUrl = "https://appkee-manager.cz/"
    static let iconsImageURL = "https://appkee-manager.cz/images/icons/"
    
    static let voucherImageURL = "ic_voucher.png"
    static let settingsImageURL = "ic_app_settings.png"
    static let emptyImageURL = "ic_question_circle_o.png"

    static let deeplinkSection = "section_id"
    static let deeplinkArticle = "article_id"

    enum UserDefs {
        static let vouchers = "UserManager-Key-Vouchers"
        static let advertsements = "UserManager-Key-Advertisements"
        static let banners = "UserManager-Key-Banners"
        static let appCode = "UserManager-Key-AppCode"
        static let languageCode = "UserManager-Key-LanguageCode"
        static let programFavourites = "UserManager-Key-ProgramFavourites"
        static let newLogin = "UserManager-Key-FirstLogin"
        static let webViewCookies = "UserManager-Key-Cookies"
        static let webViewLogged = "UserManager-Key-WebViewLogged"
        static let webViewFBLogged = "UserManager-Key-WebViewLoggedFB"

        static let pageEmail = "UserManager-Key-PageEmail"
        static let pagePassword = "UserManager-Key-PagePassword"
        static let userIdApple = "UserManager-Key-UserIdApple"

        static let pageUser = "UserManager-Key-PageUser"
        static let dialogUser = "UserManager-Key-DialogUser"

        static let favorites = "UserManager-Key-Favorites"

        static let pinCode = "UserManager-Key-PinCode"
        static let pinCodeSection = "UserManager-Key-PinCodeSection"

        static let introPageLogin = "UserManager-Key-IntroPageLogin"
        static let seniorPassLogin = "UserManager-Key-SeniorPassLogin"
        static let familyPassLogin = "UserManager-Key-FamilyPassLogin"

        static let seniorPassCategories = "UserManager-Key-seniorPassCategories"
        static let seniorPassCategoriesUpdate = "UserManager-Key-seniorPassCategoriesUpdate"

        static let familyPassCategories = "UserManager-Key-familyPassCategories"
        static let familyPassCategoriesUpdate = "UserManager-Key-familyPassCategoriesUpdate"

        static let seniorPassBranches = "UserManager-Key-seniorPassBranches"
        static let seniorPassBranchesUpdate = "UserManager-Key-seniorPassBranchesUpdate"

        static let familyPassBranches = "UserManager-Key-familyPassBranches"
        static let familyPassBranchesUpdate = "UserManager-Key-familyPassBranchesUpdate"

        static let seniorPassIZSBranches = "UserManager-Key-seniorPassIZSBranches"
        static let seniorPassIZSBranchesUpdate = "UserManager-Key-seniorPassIZSBranchesUpdate"

        static let notificationRegions = "UserManager-Key-notificationAreas"

        static let searchTooltip = "UserManager-Key-SearchTooltip"

    }

    static let photoReportSalt = 1000
}

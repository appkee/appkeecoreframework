//
//  AppkeeWKWebViewSizeToFit.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 26/09/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit
import WebKit

protocol WKWebViewSizeToFitDelegate {
    func sizeUpdated();
    func openLink(link: String, name: String);
    func openDeeplink(params: String);
}

class WKWebViewSizeToFit: WKWebView {
    
    private var height: CGFloat = 0.0
    var sizeToFitDelegate: WKWebViewSizeToFitDelegate?

    init(frame: CGRect) {
        let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
        let userScript = WKUserScript(source: jscript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        let wkUController = WKUserContentController()
        wkUController.addUserScript(userScript)
        let wkWebConfig = WKWebViewConfiguration()
        wkWebConfig.userContentController = wkUController
        
        super.init(frame: frame, configuration: wkWebConfig)
        self.navigationDelegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override var intrinsicContentSize: CGSize {
//        print(">>\(self.description) - \(self.scrollView.contentSize)")
        return CGSize(width: self.scrollView.contentSize.width, height: height)
    }
}

extension WKWebViewSizeToFit: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("document.documentElement.scrollHeight", completionHandler: { (value, _) in
            self.height = value as! CGFloat
            webView.invalidateIntrinsicContentSize()
            
            self.sizeToFitDelegate?.sizeUpdated()
        })        
    }        
        
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: ((WKNavigationActionPolicy) -> Void)) {
        switch navigationAction.navigationType {
        case .other:
            decisionHandler(.allow)
            return
        case .linkActivated:
            guard let url = navigationAction.request.url else {
                decisionHandler(.cancel)
                return
            }

            let deeplink = url.lastPathComponent
            if deeplink.hasPrefix("[\(AppkeeConstants.deeplinkSection)") {
                sizeToFitDelegate?.openDeeplink(params: deeplink)
            } else if let urlString = url.absoluteString.removingPercentEncoding {
                if urlString.starts(with: "http") {
                    var name = url.lastPathComponent.capitalizingFirstLetter().replacingOccurrences(of: "/", with: "")
                    if name.isEmpty {
                        name = url.host ?? ""
                    }

                    sizeToFitDelegate?.openLink(link: urlString, name: name)
                } else if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
        default:
            break
        }
       
        decisionHandler(.cancel)
    }    
}

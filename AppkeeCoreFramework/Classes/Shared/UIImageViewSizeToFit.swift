//
//  UIImageViewSizeToFit.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 26/09/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

class UIImageViewSizeToFit: UIImageView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.invalidateIntrinsicContentSize()
    }

    override var intrinsicContentSize: CGSize {

        if let myImage = self.image {
            self.superview?.layoutIfNeeded()
            
            let myImageWidth = myImage.size.width
            let myImageHeight = myImage.size.height
            let myViewWidth = self.frame.size.width

            let ratio = myImageHeight/myImageWidth
            let scaledHeight = myViewWidth * ratio
            
            return CGSize(width: myViewWidth, height: scaledHeight)
        }
        return CGSize(width: -1.0, height: -1.0)
    }
}

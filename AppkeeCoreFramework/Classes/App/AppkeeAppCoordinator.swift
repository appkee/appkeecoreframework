//
//  AppCoordinator.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 05/05/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FirebaseMessaging
import FirebaseCrashlytics
import FirebaseCore
import NotificationCenter
import GoogleMobileAds

public protocol AppkeeDelegate {
    func close()
}

protocol AppkeeAppCoordinatorDelegate {
    func reload()
    func close()
    func languageSelection()
    func accountDelete()
    func notificationSettings()
    func logout()
    func passLogout()
}

public class AppkeeAppCoordinator: AppkeeCoordinator {
    let window: UIWindow
    let application: UIApplication
    let launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    
    public var childrens: [AppkeeCoordinator] = []
    var children: AppkeeCoordinator?
    public var dependencies: AppkeeFullDependencies
        
    public var delegate: AppkeeDelegate?
    private var passType: PassType?
    
    
    // MARK: - Init
    public init() {
        window = UIWindow()
        application = UIApplication()
        launchOptions = nil
        dependencies = AppkeeDependencyManager.shared
    }
    
    public init(window: UIWindow,
         application: UIApplication,
         launchOptions: [UIApplication.LaunchOptionsKey: Any]?,
         dependencies: AppkeeFullDependencies = AppkeeDependencyManager.shared) {
        self.window = window
        self.application = application
        self.launchOptions = launchOptions
        self.dependencies = dependencies
    }
    
    public func start(root: Bool = false) {
        if  let path        = Bundle.main.path(forResource: "Preferences", ofType: "plist"),
            let xml         = FileManager.default.contents(atPath: path),
            let preferences = try? PropertyListDecoder().decode(AppkeeAppPreferences.self, from: xml)
        {
            if let appCode = preferences.appCode {
                dependencies.configManager.appCode = appCode
            } else if let languages = preferences.languages {
                if let appCode = UserDefaults.getLanguage()?.appCode {
                    dependencies.configManager.appCode = appCode
                } else {
                    self.showLangugeSelection(languages: languages)
                    
                    // Finally make the window key and visible.
                    window.makeKeyAndVisible()
                    return
                }
            }
        }
        
        // Perform initial application seutp.
        setupAfterLaunch()
        
        // Start your first flow here. For example, this is the
        // ideal place for deciding if you should show login or main flows.
        showLoader()
        
        // Finally make the window key and visible.
        window.makeKeyAndVisible()
    }
    
    public func start(appCode: String, appMenu: Bool = false) {
        dependencies.configManager.appCode = appCode
        dependencies.configManager.appMenu = appMenu
        
        // Perform initial application seutp.
        setupAfterLaunch()
        
        // Start your first flow here. For example, this is the
        // ideal place for deciding if you should show login or main flows.
        showLoader()
        
        // Finally make the window key and visible.
        window.makeKeyAndVisible()
    }
    
    // MARK: - Flows -
    
    
    // MARK: - Additional Setup -
    
    func setupAfterLaunch() {
        // Perform initial app setup after launch like analytics, integrations and more.
        
        IQKeyboardManager.shared.enable = true
        
        // Use Firebase library to configure APIs
        
        if FirebaseApp.app() == nil {
            FirebaseApp.configure()
        }
    
        Messaging.messaging().isAutoInitEnabled = true

        
//        if let colorSettings = dependencies.graphicManager.colorsSettings {
////            IQKeyboardManager.shared.toolbarTintColor = colorSettings.contentText
//            IQKeyboardManager.shared.shouldToolbarUsesTextFieldTintColor = true
//        }
    }

    private func updateNotifications() {
        let appDescription = dependencies.appDescriptionManager.readAppDescription()

        if (appDescription?.regionNotificationsEnabled ?? false), !(appDescription?.notificationRegions?.isEmpty ?? true) {
            let areasIds = UserDefaults.notificationRegionsID ?? []

            for region in appDescription?.notificationRegions ?? [] {
                let topic = "/topics/apk-\(dependencies.configManager.appCode)-\(region.regionId)"

                if areasIds.contains(region.id) {
                    Messaging.messaging().subscribe(toTopic: topic) { error in
                        print("Subscribed to topic: \(topic)")
                    }
                } else {
                    Messaging.messaging().unsubscribe(fromTopic:topic) { error in
                        print("Unsubscribed to topic: \(topic)")
                    }
                }
            }

            let topic = "/topics/apk-\(dependencies.configManager.appCode)"
            if (UserDefaults.notificationRegionsID ?? []).isEmpty {
                Messaging.messaging().unsubscribe(fromTopic:topic) { error in
                    print("Unsubscribed to topic: \(topic)")
                }
            } else {
                Messaging.messaging().subscribe(toTopic: topic) { error in
                    print("Subscribed to topic: \(topic)")
                }
            }
        } else {
            let topic = "/topics/apk-\(dependencies.configManager.appCode)"

            Messaging.messaging().subscribe(toTopic: topic) { error in
                print("Subscribed to topic: \(topic)")
            }
        }

        if let region = UserDefaults.seniorPassLogin?.region {
            let topic = "/topics/apk-\(dependencies.configManager.appCode)-\(region)"

            Messaging.messaging().subscribe(toTopic: topic) { error in
                print("Subscribed to topic: \(topic)")
            }
        }

        if let region = UserDefaults.familyPassLogin?.region {
            let topic = "/topics/apk-\(dependencies.configManager.appCode)-\(region)"

            Messaging.messaging().subscribe(toTopic: topic) { error in
                print("Subscribed to topic: \(topic)")
            }
        }
    }

    private func unsubscribeNotifications() {

        let topic = "/topics/apk-\(dependencies.configManager.appCode)"
        Messaging.messaging().subscribe(toTopic: topic) { error in
            print("Subscribed to topic: \(topic)")
        }

//        for region in appDescription?.notificationRegions ?? [] {
//            let topic = "/topics/apk-\(dependencies.configManager.appCode)-\(region.regionId)"
//
//            if areasIds.contains(region.id) {
//                Messaging.messaging().subscribe(toTopic: topic) { error in
//                    print("Subscribed to topic: \(topic)")
//                }
//            } else {
//                Messaging.messaging().unsubscribe(fromTopic:topic) { error in
//                    print("Unsubscribed to topic: \(topic)")
//                }
//            }
//        }


        if let region = UserDefaults.seniorPassLogin?.region {
            let topic = "/topics/apk-\(dependencies.configManager.appCode)-\(region)"

            Messaging.messaging().unsubscribe(fromTopic:topic) { error in
                print("Unsubscribed to topic: \(topic)")
            }
        }

        if let region = UserDefaults.familyPassLogin?.region {
            let topic = "/topics/apk-\(dependencies.configManager.appCode)-\(region)"

            Messaging.messaging().unsubscribe(fromTopic:topic) { error in
                print("Unsubscribed to topic: \(topic)")
            }
        }
    }
}

private extension AppkeeAppCoordinator {

    func showMenu() {
        updateNotifications()

        if dependencies.appDescriptionManager.readAppDescription()?.adMobEnabled ?? false {
            GADMobileAds.sharedInstance().start(completionHandler: nil)
        }

        switch dependencies.appDescriptionManager.readAppDescription()?.menuType {
        case 0:
            showSideMenu()
        case 1:
            showTableMenu()
        case 2:
            showCollectionMenu()
        default:
            break
        }

        getNotifications()
    }
    
    func showLangugeSelection(languages: [AppkeeAppPreferenceLanguage]) {
        // Create your child coordinator here, add it as a child and start it.
        // Make sure you set the root view controller of the window.
        
        let coordinator = AppkeeLanguageSelectionCoordinator(window: window, dependencies: dependencies)
        coordinator.delegate = self
        children = coordinator
        coordinator.start(languages: languages)
    }

    func showLoginPin(pinCode: String) {
        // Create your child coordinator here, add it as a child and start it.
        // Make sure you set the root view controller of the window.

        let coordinator = AppkeeLoginPinCoordinator(window: window, dependencies: dependencies, pinCode: pinCode)
        coordinator.delegate = self
        children = coordinator
        coordinator.start()
    }
    
    func showLoader() {
        // Create your child coordinator here, add it as a child and start it.
        // Make sure you set the root view controller of the window.
        
        let coordinator = AppkeeLoaderCoordinator(window: window, dependencies: dependencies)
        coordinator.delegate = self
        children = coordinator
        coordinator.start()
    }
    
    func showCustomLoader() {
        // Create your child coordinator here, add it as a child and start it.
        // Make sure you set the root view controller of the window.
        
        let coordinator = AppkeeCustomLoaderCoordinator(window: window, dependencies: dependencies)
        coordinator.delegate = self
        children = coordinator
        coordinator.start()
    }

    func showLogin() {
        // Create your child coordinator here, add it as a child and start it.
        // Make sure you set the root view controller of the window.

        let coordinator = AppkeeLoginCoordinator(window: window, dependencies: dependencies)
        coordinator.delegate = self
        children = coordinator
        coordinator.start()
    }

    func showPass(passType: PassType) {
        self.passType = passType

        switch passType {
        case .senior:
            showSenioPass()
        case .family:
            showFamillyPass()
        }
    }

    func showSenioPass() {
        if let validTo = UserDefaults.seniorPassLogin?.validTo, let date = validTo.getDate() {
            if date > Date() {
                self.navigate(to: .menu)
                return
            }
        }

        let coordinator = AppkeePassLoginCoordinator(window: window, dependencies: dependencies, passType: .senior)
        coordinator.delegate = self
        children = coordinator
        coordinator.start()
    }

    func showFamillyPass() {
        if let validTo = UserDefaults.familyPassLogin?.validTo, let date = validTo.getDate() {
            if date > Date() {
                self.navigate(to: .menu)
                return
            }
        }

        let coordinator = AppkeePassLoginCoordinator(window: window, dependencies: dependencies, passType: .family)
        coordinator.delegate = self
        children = coordinator
        coordinator.start()
    }

    func showNotificationAreas() {
        let coordinator = AppkeNotificationRegionsCoordinator(window: window, dependencies: dependencies, passType: passType)
        coordinator.delegate = self
        children = coordinator
        coordinator.start()
    }

    func showLoginRegister() {
        if let _ = UserDefaults.pageUser {
            self.navigate(to: .menu)
            return
        }

        let coordinator = AppkeeLoginRegisterCoordinator(window: window, dependencies: dependencies)
        coordinator.delegate = self
        children = coordinator
        coordinator.start()
    }
    
    func showTableMenu() {
        let coordinator = AppkeeTableMenuCoordinator(window: window, dependencies: dependencies)
        coordinator.delegate = self
        children = coordinator
        coordinator.start()
    }
    
    func showSideMenu() {
        let coordinator = AppkeeSideMenuCoordinator(window: window, dependencies: dependencies)
        coordinator.delegate = self
        children = coordinator
        coordinator.start()
    }

    func showCollectionMenu() {
        let coordinator = AppkeeCollectionMenuCoordinator(window: window, dependencies: dependencies)
        coordinator.delegate = self
        children = coordinator
        coordinator.start()
    }
}

extension AppkeeAppCoordinator: AppkeeCoordinatorDelegate {
    func newApp() {
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        self.application.applicationIconBadgeNumber = -1
    }

    func navigate(to route: AppkeeCoordinatorRoutes) {
        switch route {
        case .loader:
            showLoader()
        case .menu:
            showMenu()
        case .customLoader:
            showCustomLoader()
        case .login:
            showLogin()
        case .photoReport:
            showMenu()
        case .pin(let pinCode):
            showLoginPin(pinCode: pinCode)
        case .notificationAreas:
            showNotificationAreas()
        case .loginRegister:
            showLoginRegister()
        case .pass(let type):
            showPass(passType: type)
        }
    }
}

extension AppkeeAppCoordinator: AppkeeAppCoordinatorDelegate {
    func passLogout() {
        if let region = UserDefaults.seniorPassLogin?.region {
            let topic = "/topics/apk-\(dependencies.configManager.appCode)-\(region)"

            Messaging.messaging().unsubscribe(fromTopic: topic) { error in
                print("Unsubscribed to topic: \(topic)")
            }
        }

        if let region = UserDefaults.familyPassLogin?.region {
            let topic = "/topics/apk-\(dependencies.configManager.appCode)-\(region)"

            Messaging.messaging().unsubscribe(fromTopic: topic) { error in
                print("Unsubscribed to topic: \(topic)")
            }
        }

        UserDefaults.seniorPassLogin = nil
        UserDefaults.familyPassLogin = nil

        if let passType = self.passType {
            showPass(passType: passType)
        }
    }

    func logout() {
        UserDefaults.pageUser = nil

        let appDescription = self.dependencies.appDescriptionManager.readAppDescription()
        if let _ = appDescription?.customIntroPage {
            showLoginRegister()
        } else {
            showMenu()
        }
    }

    func languageSelection() {
        let alert = UIAlertController(title: translations.settings.LANGUAGE_CONTINUE, message: nil, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: translations.common.YES, style: .default, handler: { (_) in
            let topic = "/topics/apk-\(self.dependencies.configManager.appCode)"
            Messaging.messaging().unsubscribe(fromTopic: topic)

            let appDescription = self.dependencies.appDescriptionManager.readAppDescription()

            if (appDescription?.regionNotificationsEnabled ?? false), !(appDescription?.notificationRegions?.isEmpty ?? true) {
                for region in appDescription?.notificationRegions ?? [] {
                    let topic = "/topics/apk-\(self.dependencies.configManager.appCode)-\(region.regionId)"

                    Messaging.messaging().unsubscribe(fromTopic:topic) { error in
                        print("Unsubscribed to topic: \(topic)")
                    }
                }
            }

            UserDefaults.setLanguage(language: nil)
            self.start()
        }))
        alert.addAction(UIAlertAction(title: translations.common.NO, style: .cancel, handler: nil))
        
        window.rootViewController?.present(alert, animated: true, completion: nil)
    }

    func accountDelete() {
        self.showLoginRegister()
    }

    func notificationSettings() {
        self.showNotificationAreas()
    }

    func reload() {
        showLoader()
    }
    
    func close() {
        delegate?.close()
    }
}

extension AppkeeAppCoordinator: AppkeeLanguageSelectionCoordinatorDelegate {
    func openApp(language: AppkeeAppPreferenceLanguage) {
        UserDefaults.setLanguage(language: language)

        self.start(appCode: language.appCode)
    }
}

public extension AppkeeAppCoordinator {
    func getNotifications() {
        DispatchQueue.main.async {
            Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] (timer) in
                guard let self = self else {
                    timer.invalidate()
                    return
                }

                if !((self.children is AppkeeSideMenuCoordinator) || (self.children is AppkeeTableMenuCoordinator) || (self.children is AppkeeCollectionMenuCoordinator)) {
                    return
                }

                UNUserNotificationCenter.current().getDeliveredNotifications { [weak self] (notifications) in
                    guard let self = self else {
                        timer.invalidate()
                        return
                    }

                    if notifications.count == 0 {
                        DispatchQueue.main.async {
                            self.application.applicationIconBadgeNumber = -1
                        }
                        timer.invalidate()
                        return
                    }

                    for notification in notifications {
                        let userInfo = notification.request.content.userInfo
                        let title = notification.request.content.title
                        let description = notification.request.content.body

                        if let action = userInfo["action"] as? String {
                            switch action {
                            case "link":
                                if let link = userInfo["link"] as? String {
                                    self.showNotificationLink(title: title, description: description, link: link)
                                }
                            case "section":
                                if let deeplink = userInfo["link"] as? String {
                                    self.showNotificationDeeplink(title: title, description: description, deeplink: deeplink)
                                }
                            case "dialog":
                                self.showNotification(title: title, description: description)
                            default:
                                continue
                            }
                        } else {
                            self.showNotification(title: title, description: description)
                        }

                        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
                        self.application.applicationIconBadgeNumber = -1
                        timer.invalidate()
                    }
                }
            }
        }
    }

    func showNotification(title: String, description: String) {
        let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: translations.common.OK, style: .default, handler: nil))

            if self.children is AppkeeSideMenuCoordinator || self.children is AppkeeTableMenuCoordinator || self.children is AppkeeCollectionMenuCoordinator {
                DispatchQueue.main.async {
                    UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
                }
            }
    }

    func showNotificationLink(title: String, description: String, link: String) {
        let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: translations.common.OK, style: .default, handler: nil))

        alert.addAction(UIAlertAction(title: translations.common.OPEN, style: .default, handler: { action in
            self.openLink(link: link)
        }))

        if let _ = self.children as? AppkeeCoordinator {
            DispatchQueue.main.async {
                UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
            }
        }
    }

    func showNotificationDeeplink(title: String, description: String, deeplink: String) {
        let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: translations.common.OK, style: .default, handler: nil))

        alert.addAction(UIAlertAction(title: translations.common.OPEN, style: .default, handler: { action in
            self.openDeeplinkLink(deeplik: deeplink)
        }))

        if let _ = self.children as? AppkeeCoordinator {
            DispatchQueue.main.async {
                UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
            }
        }
    }

    func openLink(link: String) {
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] (timer) in
            guard let self = self else {
                timer.invalidate()
                return
            }

            if let coordinator = self.children as? AppkeeSideMenuCoordinator {
                coordinator.openLink(link: link)

                timer.invalidate()
            }

            if let coordinator = self.children as? AppkeeTableMenuCoordinator {
                coordinator.openLink(link: link)

                timer.invalidate()
            }

            if let coordinator = self.children as? AppkeeCollectionMenuCoordinator {
                coordinator.openLink(link: link)

                timer.invalidate()
            }
        }
    }

    func openDeeplinkLink(deeplik: String) {
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] (timer) in
            guard let self = self else {
                timer.invalidate()
                return
            }

            if let coordinator = self.children as? AppkeeSideMenuCoordinator {
                coordinator.openDeeplink(params: deeplik)

                timer.invalidate()
            }

            if let coordinator = self.children as? AppkeeTableMenuCoordinator {
                coordinator.openDeeplink(params: deeplik)

                timer.invalidate()
            }

            if let coordinator = self.children as? AppkeeCollectionMenuCoordinator {
                coordinator.openDeeplink(params: deeplik)

                timer.invalidate()
            }
        }
    }

    func parseNotification(userInfo: [String: Any]) {
        if let aps = userInfo["aps"] as? [String: Any] {
            if let alert = aps["alert"] as? [String: Any] {
                if let title = alert["title"] as? String, let description = alert["body"] as? String {
                    if let action = userInfo["action"] as? String {
                        switch action {
                        case "link":
                            if let link = userInfo["link"] as? String {
                                Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] (timer) in
                                    guard let self = self else {
                                        timer.invalidate()
                                        return
                                    }

                                    if !((self.children is AppkeeSideMenuCoordinator) || (self.children is AppkeeTableMenuCoordinator) || (self.children is AppkeeCollectionMenuCoordinator)) {
                                        return
                                    }

                                    self.showNotificationLink(title: title, description: description, link: link)
                                    timer.invalidate()
                                }
                            }
                        case "section":
                            if let deeplink = userInfo["link"] as? String {
                                Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] (timer) in
                                    guard let self = self else {
                                        timer.invalidate()
                                        return
                                    }

                                    if !((self.children is AppkeeSideMenuCoordinator) || (self.children is AppkeeTableMenuCoordinator) || (self.children is AppkeeCollectionMenuCoordinator)) {
                                        return
                                    }

                                    self.showNotificationDeeplink(title: title, description: description, deeplink: deeplink)
                                    timer.invalidate()
                                }
                            }
                        case "dialog":
                            Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] (timer) in
                                guard let self = self else {
                                    timer.invalidate()
                                    return
                                }

                                if !((self.children is AppkeeSideMenuCoordinator) || (self.children is AppkeeTableMenuCoordinator) || (self.children is AppkeeCollectionMenuCoordinator)) {
                                    return
                                }

                                self.showNotification(title: title, description: description)
                                timer.invalidate()
                            }
                        default:
                            Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] (timer) in
                                guard let self = self else {
                                    timer.invalidate()
                                    return
                                }

                                if !((self.children is AppkeeSideMenuCoordinator) || (self.children is AppkeeTableMenuCoordinator) || (self.children is AppkeeCollectionMenuCoordinator)) {
                                    return
                                }

                                self.showNotification(title: title, description: description)
                                timer.invalidate()
                            }
                        }
                    }

                    UNUserNotificationCenter.current().removeAllDeliveredNotifications()
                    self.application.applicationIconBadgeNumber = -1
                }
            }
        }
    }
    
    func setDeviceToken(deviceToken: Data) {
#if DEBUG
        Messaging.messaging().setAPNSToken(deviceToken, type: .sandbox)
#else
        Messaging.messaging().setAPNSToken(deviceToken, type: .prod)
#endif
    }
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

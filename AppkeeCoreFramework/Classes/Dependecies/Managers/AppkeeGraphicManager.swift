//
//  GraphicManager.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 05/05/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import KRProgressHUD

protocol AppkeeGraphicManagerDelegate: class {
    
}

public final class AppkeeGraphicManager: NSObject {
    
    weak var delegate: AppkeeGraphicManagerDelegate?
    
    var colorsSettings: AppkeeColorSettings?
    
    var imageLinks: [String: URL] = [:]
    
    var appName: String = ""
    var appIcon: String?
    
//    init() {
//        
//    }
    
    func generateColors(appDesription: AppkeeAppDescription) {
        
        let headerColor = UIColor( appDesription.headerColor)
        let headerTextColor = UIColor( appDesription.headerTextColor)
        let menuColor = UIColor( appDesription.menuColor)
        let menuTextColor = UIColor( appDesription.menuTextColor)
        let contentColor = UIColor( appDesription.contentColor)
        let contentTextColor = UIColor( appDesription.contentTextColor)
        let contentTextColorHEX = appDesription.contentTextColor

        let loginColor: UIColor
        if let color = appDesription.loginColor {
            loginColor = UIColor(color)
        } else {
            loginColor = AppkeeConstants.appkeeColor
        }

        let registerColor: UIColor
        if let color = appDesription.registerColor {
            registerColor = UIColor(color)
        } else {
            registerColor = AppkeeConstants.appkeeColor
        }
        
        colorsSettings = AppkeeColorSettings(header: headerColor,
                                             headerText: headerTextColor,
                                             menu: menuColor,
                                             menuText: menuTextColor,
                                             content: contentColor,
                                             contentText: contentTextColor,
                                             contentTextHEX: contentTextColorHEX,
                                             loginColor: loginColor,
                                             registerColor: registerColor,
                                             css: appDesription.css)
        
        KRProgressHUD
            .set(style: .custom(background: menuColor, text: menuTextColor, icon: menuTextColor))
            .set(activityIndicatorViewColors: [menuTextColor])
            .set(maskType: .clear)
    }
    
    func downloadImages(appDesription: AppkeeAppDescription, appCode: String) {
     
        appName = appCode
        
        var imageArray: [String] = []
        
        if let logo = appDesription.logo, !logo.isEmpty {
            if let name = logo.split(separator: "/").last {
                appIcon = String(name)
            }
            
            imageArray.append(logo)
        }
        
        if let bgImage = appDesription.bgImage, !bgImage.isEmpty {
            imageArray.append(bgImage)
        }

        if let loadingImage = appDesription.loadingImage, !loadingImage.isEmpty {
            imageArray.append(loadingImage)
        }
        
        if let menuHeaderImage = appDesription.menuHeaderImage, !menuHeaderImage.isEmpty {
            imageArray.append(menuHeaderImage)
        }        
        
        if appDesription.vouchersUse {
            imageArray.append(AppkeeConstants.voucherImageURL)
        }

        for section in appDesription.sections {
            if let icon = section.icon, !icon.isEmpty {
                imageArray.append(icon)
            }
            
            for image in section.images ?? [] {
                if let url = image.content, !url.isEmpty {
                    imageArray.append(url)
                }
            }
            
            if let image = section.advertisement?.image, !image.isEmpty {
                imageArray.append(image)
            }
        }

        if let image = appDesription.advertisement?.image, !image.isEmpty {
            imageArray.append(image)
        }

        for banner in appDesription.banners ?? [] {
            if let image = banner.image, !image.isEmpty {
                imageArray.append(image)
            }
        }
        
        imageArray.append(AppkeeConstants.emptyImageURL)
        imageArray.append(AppkeeConstants.settingsImageURL)
        imageArray.append(AppkeeConstants.voucherImageURL)
        
        //Warning???
        
        AppkeeStorage.createFolder(folder: appName, in: .documents)
        
//        var savedImages: [String] = []
        
        for image in imageArray {
        
            guard let url = createFullUrlPath(path: image) else {
                Analytics.logEvent("Create path for image failed", parameters: ["path" : image])
                
                continue
            }

            // skip if image exists
            if let _ = Bundle.main.path(forResource: url.lastPathComponent, ofType: "") {
                continue
            }
            
            if !AppkeeStorage.fileExists(folder: appName, fileName: url.lastPathComponent, in: .documents) {
                
                do {
                    let data = try Data(contentsOf: url.absoluteURL)
                    
                    AppkeeStorage.saveImage(folder: appName, fileName: url.lastPathComponent, data: data, in: .documents)
                    
//                    print("Saved \(url.lastPathComponent)")
                    
                } catch {
                    Analytics.logEvent("Saving image failed", parameters: ["path" : image, "error": error.localizedDescription])
                    
                    print("Saved failed \(url.lastPathComponent) \n \(url.absoluteString)")
                    
                    continue
                }
            }
            
//            savedImages.append(image)
        }
    }
    
    private func createFullUrlPath(path: String) -> URL? {
        
        if path.hasPrefix("app-data") {
//            if let url = (path.replacingOccurrences(of: "..", with: Constants.baseImageUrl)).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
//                return URL(fileURLWithPath: url)
//            }
            
            return URL(string: AppkeeConstants.baseImageUrl.appending(path))
        } else {
//            if let url = (Constants.fullImageURL.appending(path)).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
//                return URL(string: url)
//            }
            
            return URL(string: AppkeeConstants.iconsImageURL.appending(path))
        }
//        return nil
    }
    
    public func getImage(name: String?, placeholder: Bool = true) -> UIImage? {
        guard let name = name?.split(separator: "/").last else {
            if let appIcon = self.appIcon, placeholder {
                return AppkeeStorage.retrieveImage(folder: appName, fileName: appIcon, from: .documents)
            }
            return nil
        }
        
        return AppkeeStorage.retrieveImage(folder: appName, fileName: String(name), from: .documents)
    }


    public func getScaledImage(name: String?, placeholder: Bool = true, size: CGFloat = 100.0) -> UIImage? {

        var imageSource: CGImageSource?
        if let name = name?.split(separator: "/").last {
            imageSource = AppkeeStorage.retrieveImageSource(folder: appName, fileName: String(name), from: .documents)
        } else {
            if let appIcon = self.appIcon, placeholder {
                imageSource = AppkeeStorage.retrieveImageSource(folder: appName, fileName: appIcon, from: .documents)
            }
        }

        if let imageSource = imageSource {
            // Calculate the desired dimension
            let maxDimensionInPixels = size * UIScreen.main.scale

            // Perform downsampling
            let downsampleOptions = [
                kCGImageSourceCreateThumbnailFromImageAlways: true,
                kCGImageSourceShouldCacheImmediately: true,
                kCGImageSourceCreateThumbnailWithTransform: true,
                kCGImageSourceThumbnailMaxPixelSize: maxDimensionInPixels
            ] as CFDictionary
            guard let downsampledImage = CGImageSourceCreateThumbnailAtIndex(imageSource, 0, downsampleOptions) else {
                return nil
            }

            // Return the downsampled image as UIImage
            return UIImage(cgImage: downsampledImage)
        }

        return nil
    }

    
    
    public func imageLink(name: String?) -> String? {
        if let name = name {
            return AppkeeConstants.baseImageUrl + name
        }
        return nil
    }
}

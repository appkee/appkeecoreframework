//
//  AppDescriptionManager.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 05/05/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import FirebaseAnalytics
import UIKit

protocol AppkeeAppDescriptionManagerDelegate: class {
        
}

public final class AppkeeAppDescriptionManager: NSObject {
    
    weak var delegate: AppkeeAppDescriptionManagerDelegate?
    
    private let dataFileBudle = "dataFile.txt"
    private let dataFile = "dataFile.json"
    
    override init() {
        super.init()
        
        initDataFile()
    }
    
    func readAppDescription() -> AppkeeAppDescription? {                
        let appDescription = AppkeeStorage.retrieve(dataFile, from: .documents, as: AppkeeAppDescription.self)
        return appDescription
    }
    
    func saveAppDescription(appDescription: AppkeeAppDescription) {
        let filteredSections = appDescription.sections

        var appDescription = appDescription
        var sections: [AppkeeSection] = []


        // add banners
        if let banners = appDescription.banners?.filter( { $0.visibility } ), !banners.isEmpty {
            for section in filteredSections {
                var section = section
                section.banners = banners

                if section.icon == nil || (section.icon?.isEmpty ?? true) {
                    section.icon = AppkeeConstants.emptyImageURL
                }

                sections.append(section)
            }
            // adMob
        } else if appDescription.adMobEnabled, let iosAdUnitId = appDescription.iosAdUnitId {
            let advertisement = AppkeeAdvertisement(id: -1, position: appDescription.advertisement?.position ?? .bottom, adUnitID: iosAdUnitId, visibility: true)

            for section in filteredSections {
                var section = section

                section.banners = [advertisement]

                if section.icon == nil || (section.icon?.isEmpty ?? true) {
                    section.icon = AppkeeConstants.emptyImageURL
                }

                sections.append(section)
            }
        } else {
            // global advertisment
            if let advertisement = appDescription.advertisement, advertisement.visibility {
                for section in filteredSections {
                    var section = section
                    if let advertisementSection = section.advertisement {
                        if advertisementSection.visibility {
                            section.banners = [advertisementSection]
                        } else {
                            section.banners = [advertisement]
                        }
                    } else {
                        section.banners = [advertisement]
                    }

                    if section.icon == nil || (section.icon?.isEmpty ?? true) {
                        section.icon = AppkeeConstants.emptyImageURL
                    }

                    sections.append(section)
                }
            } else {
                for section in filteredSections {
                    var section = section

                    if let advertisement = section.advertisement {
                        section.banners = [advertisement]
                    } else {
                        section.banners = nil
                    }

                    if section.icon == nil || (section.icon?.isEmpty ?? true) {
                        section.icon = AppkeeConstants.emptyImageURL
                    }

                    sections.append(section)
                }
            }
        }

        if appDescription.vouchersUse {
            let section = AppkeeSection(voucher: translations.voucher.TITLE)
            sections.append(section)
        }


//        if appDescription.favoritesType != nil {
//            var name: String = ""
//            switch appDescription.favoritesType {
//                case .article:
//                    name = translations.favorites.TITLE_ARTICLE
//                case .recipe:
//                    name = translations.favorites.TITLE_RECIPE
//                default:
//                    break
//            }
//
//            let section = AppkeeSection(favories: name)
//            sections.append(section)
//        }

        if let user = appDescription.appUser {
            UserDefaults.pageUser = user
        } else {
            UserDefaults.pageUser = nil
        }

//        if UserDefaults.getLanguage() != nil
//            || (appDescription.regionNotificationsEnabled && !(appDescription.notificationRegions?.isEmpty ?? true))
//            || UserDefaults.pageUser != nil
//        {
//            let section = AppkeeSection(settings: translations.settings.TITLE)
//            sections.append(section)
//        }

        appDescription.sections = sections
        
//        print("\(appDescription)")
        
        AppkeeStorage.store(appDescription, to: .documents, as: dataFile)
    }
    
    private func initDataFile() {
        // init configat first start
        if !AppkeeStorage.fileExists(dataFile, in: .documents) {
            if let resourcePath = Bundle.main.path(forResource: dataFileBudle, ofType: nil) {
                do {
                    let url = URL(fileURLWithPath: resourcePath)
                    let jsonData = try Data(contentsOf: url)
                    let decoder = JSONDecoder()
//                    decoder.dateDecodingStrategy = .iso8601
                    
                    let appResponse = try decoder.decode(AppkeeAppResponse.self, from: jsonData)
                    
                    if let appDescription = appResponse.data {
                        AppkeeStorage.store(appDescription, to: .documents, as: dataFile)
                    } else {
                        Analytics.logEvent("Reading of app config", parameters: ["dataFile": dataFile, "error": "No response"])
                    }
                }
                catch {
                    Analytics.logEvent("Saving of app config", parameters: ["dataFile": dataFile, "error": error.localizedDescription])
                }
            }
        }
    }
}

//
//  AppkeeConfigManager.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 03/02/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

protocol AppkeeConfigManagerDelegate: class {
        
}

public final class AppkeeConfigManager: NSObject {
    
    private var code: String?
        
    var appCode: String {
        get {
            return code ?? AppkeeConstants.appCode
        }
        set {
            code = newValue
        }
    }
    var adUnitID: String?
    
    var appDebug: Bool = false
    var appMenu: Bool = false

//    override init() {
//        super.init()
//
//
//    }
    
    
    
}

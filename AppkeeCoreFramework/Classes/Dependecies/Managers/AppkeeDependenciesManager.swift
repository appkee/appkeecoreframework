//
//  AppkeeDependenciesManager.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 05/05/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

public protocol HasRepository {
    var repository: AppkeeRepository { get }
}

public protocol HasGraphicManager {
    var graphicManager: AppkeeGraphicManager { get }
}

public protocol HasAppDescriptionManager {
    var appDescriptionManager: AppkeeAppDescriptionManager { get }
}

public protocol HaConfigManager {
    var configManager: AppkeeConfigManager { get }
}

public typealias AppkeeFullDependencies =
    HasRepository
    & HasGraphicManager
    & HasAppDescriptionManager
    & HaConfigManager

public final class AppkeeDependencyManager: AppkeeFullDependencies {
    
    // MARK: - Static Properties
    public static let shared = AppkeeDependencyManager()
    
    // MARK: - Properties
    public let repository: AppkeeRepository
    
    public let graphicManager: AppkeeGraphicManager
    public let appDescriptionManager: AppkeeAppDescriptionManager
    public let configManager: AppkeeConfigManager
    
    // MARK: - Init
    private init() {
        // Setup repository
        let repository = AppkeeServerRepository()
        self.repository = repository
        
        self.graphicManager = AppkeeGraphicManager()
        self.appDescriptionManager = AppkeeAppDescriptionManager()
        self.configManager = AppkeeConfigManager()
    }
}

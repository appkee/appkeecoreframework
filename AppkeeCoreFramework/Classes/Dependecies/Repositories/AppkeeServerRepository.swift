//
//  AppkeeServerRepository.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 27/12/2018.
//  Copyright © 2018 Radek Zmeskal. All rights reserved.
//

import Alamofire
import SwiftyJSON

// swiftlint:disable file_length
public final class AppkeeServerRepository {
    
    // MARK: - Properties
    
    let baseUrlString = "https://appkee-manager.cz"
    
    let staticHeaders = [
        "Appkee-Api-Client": "ios",
        "Appkee-Access-Token": AppkeeConstants.accessToken,
        "Appkee-Api-Client-Version": "1.2.0",
        "Accept": "application/json",
        "Accept-Encoding": "gzip, deflate",
        "Content-Type": "multipart/form-data",
        "Cookie": "nette-samesite=1"

    ]
    
    let sessionManager: SessionManager
    let requestsQueue: DispatchQueue = DispatchQueue.global(qos: .default)
    
    // MARK: - Init
    
    public init() {
        // get the default headers
        var headers = Alamofire.SessionManager.defaultHTTPHeaders
        // add your custom header
        headers["Appkee-Api-Client"] = "ios"
        headers["Appkee-Access-Token"] = AppkeeConstants.accessToken
        headers["Appkee-Api-Client-Version"] = "1.2.0"
        headers["Accept"] = "application/json"
//        headers["Cookie"] = "nette-samesite=1"
        headers["Content-Type"] = "application/json"
        
        
        // create a custom session configuration
        let configuration = URLSessionConfiguration.default
        // add the headers
//        configuration.httpShouldSetCookies = false
        configuration.httpAdditionalHeaders = headers
        configuration.timeoutIntervalForRequest = 30.0
        
        // create a session manager with the configuration
        sessionManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    /// Basic perform function to perform requests on the request queue.
    ///
    /// - Parameters:
    ///   - path: The endpoint path. `baseUrlString` is added in front of the path.
    ///   - method: The HTTP method you want to use, ie. GET, POST.
    ///   - parameters: Any custom parameters if any.
    ///   - encoding: Optionally provide custom encoding.
    ///   - headers: The headers you wish to add to your request.
    ///   - hasBaseUrl: Indicates if the base url should be added to the request
    ///   - action: Action to execute with the request on the request queue after it was created. Typically
    ///             perform the request and parse the response, eg. `responseCodable`.
    func perform(_ path: String, method: Alamofire.HTTPMethod = .get, parameters: Parameters? = nil,
                 encoding: ParameterEncoding = URLEncoding.default, headers: HTTPHeaders? = nil,
                 hasBaseUrl: Bool = true, action: @escaping ((Alamofire.DataRequest) -> Void)) {
        requestsQueue.async {
            let baseUrl = hasBaseUrl ? self.baseUrlString : ""
            let request = self.sessionManager.request(baseUrl + path,
                                                      method: method, parameters: parameters,
                                                      encoding: encoding, headers: headers)
            action(request)
        }
    }
}


extension AppkeeServerRepository: AppRepository {
    public func appData(appCode: String, loggedIn: Bool, appUserId: Int?, handler: @escaping (DataResponse<AppkeeAppResponse>) -> Void) {
        var params: [String: Any] = [
            "id": appCode,
            "loggedIn": loggedIn
        ]

        if let appUserId = appUserId {
            params["appUserId"] = appUserId
        }

        perform("/app/app-data", method: .get, parameters: params, encoding: URLEncoding.default) {
//            $0.responseJSON(completionHandler: { (json) in
//                print(">>> \(json)")
//            })
            $0.responseCodable(completion: handler)
        }
    }
}

extension AppkeeServerRepository: RSSRepository {
    public func getRSS(link: String, handler: @escaping (DataResponse<RSSFeed>) -> Void) {
        Alamofire.request(link).responseRSS() { response in
            handler(response)
        }
    }
}

extension AppkeeServerRepository: FormRepository {
    public func sendForm(sectionId: Int, message: String, handler: @escaping (DataResponse<AppkeeFormResponse>) -> Void) {
        perform("/app/send-form", method: .post, parameters: ["section-id": sectionId, "message": message], encoding: URLEncoding.default) {
            $0.responseCodable(completion: handler)
        }
    }
}

extension AppkeeServerRepository: VoucherRepository {
    public func getVouchers(appCode: String, handler: @escaping (DataResponse<AppkeeVouchersResponse>) -> Void) {
        perform("/app/vouchers", method: .get, parameters: ["appId": appCode], encoding: URLEncoding.default) {
            $0.responseCodable(completion: handler)
        }
    }
    
    public func useVouchers(idCoupon: Int, confirmCode: String?, handler: @escaping (DataResponse<AppkeeUseVoucherResponse>) -> Void) {
        perform("/app/use-voucher", method: .post, parameters: ["id": idCoupon, "confirm-code": confirmCode ?? ""], encoding: URLEncoding.httpBody) {
            $0.responseCodable(completion: handler)
        }
    }
}

extension AppkeeServerRepository: PageLoginRepository {
    public func pageRegister(email: String, password: String, confirmPass: String, name: String, phone: String, address: String, appCode: String, handler: @escaping (DataResponse<AppkeePageLoginResponse>) -> Void) {
        let params = [
            "email": email,
            "password": password,
            "password-again": confirmPass,
            "name": name,
            "phone": phone,
            "address": address,
            "confirm": true,
            "app-code": appCode,
            "email-to-admin": true
        ] as [String : Any]

        perform("/user/register", method: .post, parameters: params, encoding: URLEncoding.default) {
            $0.responseString { (str) in
                print(">>>\(str)")
            }
            $0.responseCodable(completion: handler)
        }
    }

    public func pageLogin(email: String, password: String, appCode: String, handler: @escaping (DataResponse<AppkeePageLoginResponse>) -> Void) {
        perform("/user/login", method: .post, parameters: ["email": email, "password": password, "app-code": appCode], encoding: URLEncoding.default) {
            $0.responseCodable(completion: handler)
        }
    }

    public func seniorPassLogin(key: String, appCode: String, handler: @escaping (DataResponse<AppkeeSeniorPassResponse>) -> Void) {
        perform("/api/verify", method: .post, parameters: ["key": key, "app-code": appCode], encoding: URLEncoding.default) {
            $0.responseString { (str) in
                print(">>>\(str)")
                print(">>>")
            }
            $0.responseCodable(completion: handler)
        }
    }
    
    public func familyPassLogin(key: String, appCode: String, handler: @escaping (DataResponse<AppkeeFamilyPassResponse>) -> Void) {
        perform("/api/verify", method: .post, parameters: ["key": key, "app-code": appCode], encoding: URLEncoding.default) {
            $0.responseString { (str) in
                print(">>>\(str)")
                print(">>>")
            }
            $0.responseCodable(completion: handler)
        }
    }

    public func deleteAccount(appCode: String, email: String, password: String, handler: @escaping (DataResponse<AppkeeDeleteAccountResponse>) -> Void) {
        perform("/user/delete", method: .post, parameters: ["email": email, "password": password, "app-code": appCode], encoding: URLEncoding.default) {
            $0.responseCodable(completion: handler)
            $0.responseString { (str) in
                print(">>>\(str)")
            }
        }
    }

    public func forgotPass(email: String, appCode: String, handler: @escaping (DataResponse<AppkeePageForgotPassResponse>) -> Void) {
        perform("/user/forgotten-password", method: .post, parameters: ["email": email, "app-code": appCode], encoding: URLEncoding.default) {
            $0.responseCodable(completion: handler)
            $0.responseString { (str) in
                print(">>>\(str)")
            }
        }
    }
}

extension AppkeeServerRepository: SenioPassRepository {
    public func seniorPassBranches(regionID: Int, handler: @escaping (DataResponse<AppkeeSeniorPassBranchesResponse>) -> Void) {
        perform("/api/senior-pas-branches", method: .get, parameters: ["regionId": regionID], encoding: URLEncoding.default) {
            $0.responseCodable(completion: handler)
        }
    }

    public func seniorPassCategories(handler: @escaping (DataResponse<AppkeeSeniorPassInfoResponse>) -> Void) {
        perform("/api/senior-pas-info", method: .get, parameters: [:], encoding: URLEncoding.default) {
            $0.responseCodable(completion: handler)
        }
    }

    public func familyPassBranches(regionID: Int, handler: @escaping (DataResponse<AppkeeFamilyPassBranchesResponse>) -> Void) {
        perform("/api/rodinne-pasy-branches", method: .get, parameters: ["regionId": regionID], encoding: URLEncoding.default) {
            $0.responseCodable(completion: handler)
            $0.responseString { (str) in
                print(">>>\(str)")
            }
        }
    }

    public func familyPassCategories(handler: @escaping (DataResponse<AppkeeSeniorPassInfoResponse>) -> Void) {
        perform("/api/rodinne-pasy-info", method: .get, parameters: [:], encoding: URLEncoding.default) {
            $0.responseString { (str) in
                print(">>>\(str)")
            }
            $0.responseCodable(completion: handler)
        }
    }

    public func seniorPassIZS(regionID: Int, handler: @escaping (DataResponse<AppkeeSeniorPassIZSResponse>) -> Void) {
        perform("/api/senior-pas-izs", method: .get, parameters: ["regionId": regionID], encoding: URLEncoding.default) {
            $0.responseCodable(completion: handler)
            $0.responseString { (str) in
                print(">>>\(str)")
            }
        }
    }
}

extension AppkeeServerRepository: LoginRepository {
    public func login(email: String, password: String, handler: @escaping (DataResponse<AppkeeLoginResponse>) -> Void) {
        perform("/user/app-list", method: .get, parameters: ["email": email, "password": password], encoding: URLEncoding.default) {
            $0.responseCodable(completion: handler)
        }
    }
    
    public func newApp(appCode: String, handler: @escaping (DataResponse<String>) -> Void) {
        perform("/statistics/new-app-install", method: .post, parameters: ["app-code": appCode], encoding: URLEncoding.default) {
            $0.validate(statusCode: [200])
            $0.responseString(completionHandler: handler)
        }
    }
}

//
//  Repository.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 05/05/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import Alamofire

public typealias AppkeeRepository = AppRepository & RSSRepository & FormRepository & VoucherRepository & PageLoginRepository & LoginRepository & SenioPassRepository

public protocol AppRepository {
    func appData(appCode: String, loggedIn: Bool, appUserId: Int?, handler: @escaping (DataResponse<AppkeeAppResponse>) -> Void)
}


public protocol RSSRepository {
    func getRSS(link: String, handler: @escaping (DataResponse<RSSFeed>) -> Void)
}

public protocol FormRepository {
    func sendForm(sectionId: Int, message: String, handler: @escaping (DataResponse<AppkeeFormResponse>) -> Void)
}

public protocol VoucherRepository {
    func getVouchers(appCode: String, handler: @escaping (DataResponse<AppkeeVouchersResponse>) -> Void)
    func useVouchers(idCoupon: Int, confirmCode: String?, handler: @escaping (DataResponse<AppkeeUseVoucherResponse>) -> Void)
}

public protocol PageLoginRepository {
    func pageLogin(email: String, password: String, appCode: String, handler: @escaping (DataResponse<AppkeePageLoginResponse>) -> Void)
    func pageRegister(email: String,  password: String, confirmPass: String, name: String, phone: String, address: String, appCode: String, handler: @escaping (DataResponse<AppkeePageLoginResponse>) -> Void)
    func seniorPassLogin(key: String, appCode: String, handler: @escaping (DataResponse<AppkeeSeniorPassResponse>) -> Void)
    func familyPassLogin(key: String, appCode: String, handler: @escaping (DataResponse<AppkeeFamilyPassResponse>) -> Void)

    func deleteAccount(appCode: String, email: String, password: String, handler: @escaping (DataResponse<AppkeeDeleteAccountResponse>) -> Void)
    func forgotPass(email: String, appCode: String, handler: @escaping (DataResponse<AppkeePageForgotPassResponse>) -> Void)
}

public protocol SenioPassRepository {
    func seniorPassBranches(regionID: Int, handler: @escaping (DataResponse<AppkeeSeniorPassBranchesResponse>) -> Void)
    func seniorPassCategories(handler: @escaping (DataResponse<AppkeeSeniorPassInfoResponse>) -> Void)
    func familyPassBranches(regionID: Int, handler: @escaping (DataResponse<AppkeeFamilyPassBranchesResponse>) -> Void)
    func familyPassCategories(handler: @escaping (DataResponse<AppkeeSeniorPassInfoResponse>) -> Void)

    func seniorPassIZS(regionID: Int, handler: @escaping (DataResponse<AppkeeSeniorPassIZSResponse>) -> Void)
}

public protocol LoginRepository {
    func login(email: String, password: String, handler: @escaping (DataResponse<AppkeeLoginResponse>) -> Void)
    func newApp(appCode: String, handler: @escaping (DataResponse<String>) -> Void)
}

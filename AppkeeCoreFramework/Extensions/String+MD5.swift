//
//  String+MD5.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 4/23/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import CommonCrypto

extension String {
    func MD5() -> String? {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        var digest = [UInt8](repeating: 0, count: length)

        if let d = self.data(using: String.Encoding.utf8) {
            _ = d.withUnsafeBytes { (body: UnsafePointer<UInt8>) in
                CC_MD5(body, CC_LONG(d.count), &digest)
            }
        }

        return (0..<length).reduce("") {
            $0 + String(format: "%02x", digest[$1])
        }
    }

    func hash() -> String? {
        return (AppkeeConstants.accessToken + "-" + self).MD5()
    }
}

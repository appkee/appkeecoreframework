//
//  DAte+Formatter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/20/22.
//  Copyright © 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

extension Date {
    func getYear() -> Int {
        let components = Calendar.current.dateComponents([.year], from: self)

        return components.year ?? 0
    }
}

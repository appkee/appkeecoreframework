//
//  UITableView+Empty.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 14/03/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import UIKit

extension UITableView {

    func setEmptyMessage(_ message: String, textColor: UIColor) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = textColor
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.sizeToFit()

        messageLabel.isHidden = true
        self.backgroundView = messageLabel
    }
    
    func setEmptyMessage(_ message: NSAttributedString, textColor: UIColor) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.attributedText = message
        messageLabel.textColor = textColor
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.sizeToFit()

        messageLabel.isHidden = true
        self.backgroundView = messageLabel
    }

    func showEmptyMessage() {
        self.backgroundView?.isHidden = false
    }
    
    func hideEmptyMessage() {
        self.backgroundView?.isHidden = true
    }
}


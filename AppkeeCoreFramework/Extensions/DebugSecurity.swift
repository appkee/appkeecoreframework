//
//  DebugSecurity.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 3/4/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

#if !DEBUG

public func print(_ items: Any..., separator: String = " ", terminator: String = "\n") { }
public func debugPrint(_ items: Any..., separator: String = " ", terminator: String = " \n") { }
public func print<Target: TextOutputStream>(_ items: Any..., separator: String = " ", terminator: String = " \n", to output: inout Target) { }
public func debugPrint<Target: TextOutputStream>(_ items: Any..., separator: String = " ", terminator: String = " \n", to output: inout Target) { }
public func NSLog(_ format: String, _ args: CVarArg...) { }

#endif

//
//  Cells+Dequeue.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 26/09/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit


public extension UITableViewCell {
    static var reuseId: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: reuseId, bundle: Bundle(for: self))
    }
}

public extension UICollectionReusableView {
    static var reuseId: String {
        return String(describing: self)
    }

    static var nib: UINib {
        return UINib(nibName: reuseId, bundle: Bundle(for: self))
    }
}

public extension UITableViewHeaderFooterView {
    static var reuseId: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: reuseId, bundle: Bundle(for: self))
    }
}

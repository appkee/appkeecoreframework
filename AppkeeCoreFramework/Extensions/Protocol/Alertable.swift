//
//  Alertable.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 19/01/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import UIKit

public protocol Alertable { }
public extension Alertable where Self: UIViewController {
    
    private var viewController: UIViewController {
        return navigationController ?? self
    }
 
    func showAlert(withTitle title: String,
                   message: String? = nil,
                   okTitle: String,
                   cancelTitle: String? = nil,
                   handler: (() -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: okTitle, style: .default) { _ in
            handler?()
        }
        alertController.addAction(okAction)

        if let cancelTitle = cancelTitle {
            let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel)
            alertController.addAction(cancelAction)
        }

        DispatchQueue.main.async {
            self.viewController.present(alertController, animated: true)
        }
    }
    
    func showAlertYesNo(withTitle title: String,
                        message: String,
                        handler: ((Bool) -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: translations.common.YES, style: .default) { _ in
            handler?(true)
        }
        alertController.addAction(okAction)
        
        let cancelAction = UIAlertAction(title: translations.common.NO, style: .cancel) { _ in
            handler?(false)
        }
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async {
            self.viewController.present(alertController, animated: true)
        }
    }
    
    func showErrorAlert(withMessage message: String, hasSettingsButton: Bool = false, handler: (() -> Void)? = nil) {
        // Currently we have no way of seeing the error code so we have to match the message to get a custom title
        let alertController = UIAlertController(title: translations.error.TITLE, message: message, preferredStyle: .alert)

        let okAction = UIAlertAction(title: translations.common.OK, style: .cancel)
        alertController.addAction(okAction)

        DispatchQueue.main.async {
            self.viewController.present(alertController, animated: true) {
                handler?()
            }
        }
    }
    
    func showAlert(withTitle title: String,
                   message: String?,
                   handler: @escaping () -> Void) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: translations.common.OK, style: .cancel) { _ in
            alertController.dismiss(animated: true) {
                handler()
            }
        }
        alertController.addAction(okAction)
        
        DispatchQueue.main.async {
            self.viewController.present(alertController, animated: true)
        }
    }
    
    func showInputAlert(withTitle title: String,
                   message: String? = nil,
                   okTitle: String,
                   cancelTitle: String? = nil,
                   handler: @escaping ((String?) -> Void)) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertController.addTextField { (textField) in
            
        }
        
        
        let okAction = UIAlertAction(title: okTitle, style: .default) { _ in
            guard let textField = alertController.textFields?.first else {
                return
            }
            
            handler(textField.text)
        }
        alertController.addAction(okAction)
                
        if let cancelTitle = cancelTitle {
             alertController.addAction(UIAlertAction(title: cancelTitle, style: .cancel))
        }

        DispatchQueue.main.async {
            self.viewController.present(alertController, animated: true)
        }
    }

    func showSecureInputAlert(withTitle title: String,
                   placeholderMessage: String? = nil,
                   okTitle: String,
                   cancelTitle: String? = nil,
                   handler: @escaping ((String?) -> Void)) {

        let alertController = UIAlertController(title: title, message: nil, preferredStyle: .alert)

        alertController.addTextField { (textField) in
            textField.isSecureTextEntry = true
            textField.placeholder = placeholderMessage
        }


        let okAction = UIAlertAction(title: okTitle, style: .default) { _ in
            guard let textField = alertController.textFields?.first else {
                return
            }

            handler(textField.text)
        }
        alertController.addAction(okAction)

        if let cancelTitle = cancelTitle {
             alertController.addAction(UIAlertAction(title: cancelTitle, style: .cancel))
        }

        DispatchQueue.main.async {
            self.viewController.present(alertController, animated: true)
        }
    }

    func showPinAlert(pinCode: String, handler: @escaping () -> Void) {
        let alert = UIAlertController(title: translations.pinSection.TITLE, message: nil, preferredStyle: .alert)

        alert.addTextField { (textField) in
            textField.keyboardType = .numberPad
            textField.isSecureTextEntry = true
            textField.textAlignment = .center
        }

        alert.addAction(UIAlertAction(title: translations.common.OK, style: .default, handler:{ (UIAlertAction) in
            alert.dismiss(animated: true) {
                if let textField = alert.textFields?[0] {
                    if pinCode == textField.text?.hash() {
                        let alert2 = UIAlertController(title: translations.pinSection.SUCCESS_TITLE, message: nil, preferredStyle: .alert)
                        alert2.addAction(UIKit.UIAlertAction(title: translations.common.OK, style: .cancel, handler: {_ in
                            handler()
                        }))

                        self.present(alert2, animated: true, completion: nil)
                        return
                    }
                }

                let alert2 = UIAlertController(title: translations.pinSection.ERROR_TITLE, message: translations.pinSection.ERROR_MESSAGE, preferredStyle: .alert)
                alert2.addAction(UIKit.UIAlertAction(title: translations.common.OK, style: .cancel, handler: nil))

                self.present(alert2, animated: true, completion: nil)
            }
        }))

        //translate
        alert.addAction(UIAlertAction(title: translations.common.CANCEL, style: .cancel, handler: nil))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
//    func showCustomAlert(withTitle title: String,
//                         message: String,
//                         alertTitle1: String,
//                         alertTitle2: String,
//                         handler: ((Bool) -> Void)? = nil) {
//
//        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
//
//        let alertAction1 = UIAlertAction(title: alertTitle1, style: .default) { _ in
//            handler?(true)
//        }
//        let alertAction2 = UIAlertAction(title: alertTitle2, style: .cancel) { _ in
//            handler?(false)
//        }
//
//        let isRTL = (DependencyManager.shared.translatableManager.currentLanguage as? Language)?.direction != .rightToLeft
//
//        if isRTL {
//            alertController.addAction(alertAction1)
//            alertController.addAction(alertAction2)
//        } else {
//            alertController.addAction(alertAction2)
//            alertController.addAction(alertAction1)
//        }
//
//        DispatchQueue.main.async {
//            self.viewController.present(alertController, animated: true)
//        }
//    }
}

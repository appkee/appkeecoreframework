//
//  Progressable.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 20/01/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import UIKit
import KRProgressHUD

public protocol Progressable {}
public extension Progressable where Self: UIViewController {

    func showProgress(title: String = translations.common.LOADING) {
        let _ = KRProgressHUD.showOn(self)
        KRProgressHUD.show(withMessage: title)
    }

    func hideProgress() {
        KRProgressHUD.dismiss()
    }
    
}

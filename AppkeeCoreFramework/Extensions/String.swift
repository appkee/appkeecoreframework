//
//  String.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 23/09/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

extension String {
    
//    func removeDiacritics() -> String {
//        let mutableString = NSMutableString(string: self) as CFMutableString
//        CFStringTransform(mutableString, nil, kCFStringTransformStripDiacritics, false)
//
//        return (mutableString as String).replacingOccurrences(of: " ", with: "_")
//    }


    func unaccent() -> String {
        return self.folding(options: .diacriticInsensitive, locale: .current)

    }

    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }

    func removeDiacritics() -> String {
        let mutableString = NSMutableString(string: self) as CFMutableString
        CFStringTransform(mutableString, nil, kCFStringTransformStripDiacritics, false)

        return (mutableString as String).replacingOccurrences(of: " ", with: "_")
    }

}

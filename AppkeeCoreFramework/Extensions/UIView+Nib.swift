//
//  UIView+Nib.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 26/09/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

extension UIView {
    @discardableResult
    func loadNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        guard
            let nibName = NSStringFromClass(type(of: self)).components(separatedBy: ".").last,
            let views = bundle.loadNibNamed(nibName, owner: self, options: nil),
            let contentView = views.first as? UIView
            else {
                return nil
        }

        addSubview(contentView)
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView.frame = self.bounds

        return contentView
    }
}

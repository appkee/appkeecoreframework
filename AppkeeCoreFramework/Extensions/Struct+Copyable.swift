//
//  Struct+Copyable.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 12/03/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

protocol Copyable
{
  init(other: Self)
}

extension Copyable
{
  func copy() -> Self
  {
    return Self.init(other: self)
  }
}

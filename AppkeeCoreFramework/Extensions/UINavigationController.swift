//
//  UINavigationController.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 07/05/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func addHeader(navigationBar: UINavigationBar, title: String?, image: UIImage?, colorSettings: AppkeeColorSettings?) {
        
        let targetHeight = navigationBar.frame.size.height - 10;
        
//        let targetWidth = navigationBar.frame.size.width - 80;
        
        let view = UIStackView()
        view.distribution = .equalSpacing
        view.spacing = 6.0
        
        if let image = image {
            let scale = image.size.width/image.size.height
            
            let width = scale * targetHeight;
            
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: targetHeight))
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
            
            imageView.translatesAutoresizingMaskIntoConstraints = false
            
            NSLayoutConstraint(item: imageView,
                               attribute: NSLayoutConstraint.Attribute.width,
                               relatedBy: NSLayoutConstraint.Relation.equal,
                               toItem: nil,
                               attribute: NSLayoutConstraint.Attribute.notAnAttribute,
                               multiplier: 1,
                               constant: width).isActive = true
            
            
            view.addArrangedSubview(imageView)
        }
        
        if let title = title {
            let label = UILabel()
            
            label.text = title
//            label.textColor = self.color
            label.adjustsFontSizeToFitWidth = true
            label.minimumScaleFactor = 0.5
            label.textAlignment = .center
            label.textColor = colorSettings?.headerText
            
            view.addArrangedSubview(label)
        }
        
        self.navigationItem.titleView = view
    }
}

extension UINavigationController {

    func popToRootViewController(animated: Bool = true, completion: @escaping () -> Void) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        popToRootViewController(animated: animated)
        CATransaction.commit()
    }

//    func pushViewController(_ viewController: UIViewController, animated: Bool = true, completion: @escaping () -> Void) {
//        CATransaction.begin()
//        CATransaction.setCompletionBlock(completion)
//        pushViewController(viewController, animated: animated)
//        CATransaction.commit()
//    }
}

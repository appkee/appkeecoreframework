//
//  String+Localization.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 13/01/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

public struct TranslationCommon {
    let OK = "common.ok".localized
    let CANCEL = "common.cancel".localized
    let YES = "common.yes".localized
    let NO = "common.no".localized
    let USE = "common.use".localized
    let OPEN = "common.open".localized
    public let LOADING = "common.loading".localized
    
    let TABLE_PULL_TO_REFRESH = "table.pull_to_refresh".localized
    let TABLE_FAVOURITES_NO_DATA = "table.favourites.empty".localized
}

public struct TranslationLink {
    let OPEN = "link.open".localized
    let ERROR_TITLE = "link.error_title".localized
    let BUTTON = "link.button".localized
}

public struct TranslationMaps {
    let OPEN = "maps.open".localized
    let APPLE = "maps.apple".localized
    let GOOGLE = "maps.google".localized
}

public struct TranslationForm {
    let SELECTION = "form.selection".localized
    let SENDING = "form.sending".localized
    let CONSENT = "form.consent".localized
    let SEND = "form.send".localized
    let ERROR = "form.error".localized
}

public struct TranslationVouchers {
    let TITLE = "vouchers.title".localized
    let DETAIL_TITLE = "vouchers.detail_title".localized
    let USED_TITLE = "vouchers.used_title".localized
    let USED_MESSAGE = "vouchers.used_message".localized
    let USE_TITLE = "vouchers.use_title".localized
    let USE_MESSAGE = "vouchers.use_message".localized
    let VALID = "vouchers.date".localized
    let NO_DATA = "vouchers.no_data".localized
}

public struct TranslationPhotoReporter {
    let TITLE = "photoReporter.title".localized

    let IMAGE_DATE = "photoReporter.image_date".localized
    let IMAGE_LOCATION = "photoReporter.image_location".localized
    let IMAGE_ADDRESS = "photoReporter.image_address".localized
    let IMAGE_DESCRIPTION = "photoReporter.image_description".localized

    let INPUT_DESCRIPTION = "photoReporter.input_description".localized
    let INPUT_ADDRESS = "photoReporter.input_address".localized
    let INPUT_CONFIRM = "photoReporter.input_confirm".localized

    let SAVED = "photoReporter.saved".localized

    let ERROR_PHOTO = "photoReporter.error.photo".localized
    let ERROR_DESCRIPTION = "photoReporter.error.description".localized
    let ERROR_ADDRESS = "photoReporter.error.address".localized
    let ERROR_SAVE = "photoReporter.error.save".localized
}

public struct TranslationRss {
    let NO_DATA = "rss.no_data".localized
}

public struct TranslationQRCode {
    let RESCAN = "qrCodeReader.rescan".localized    
}

public struct TranslationSettings {
    let TITLE = "settings.title".localized
    let LANGUAGE_CONTINUE = "settings.language_continue".localized
    let CURRENT_LANGUAGE = "settings.current_language".localized
    let CHANGE_LANGUAGE = "settings.change_language".localized
    let HEADER_LANGUAGE = "settings.header_language".localized
    let HEADER_NOTIFICATIONS = "settings.header_notification".localized
    let CHANGE_NOTIFICATIONS = "settings.change_notification".localized

    let HEADER_ACCOUNT = "settings.account_setting".localized
    let ACCOUNT_DELETION_TEXT = "settings.account_setting_title".localized
    let ACCOUNT_DELETION_TITLE = "settings.account_deletion_title".localized
    let ACCOUNT_DELETION_DELETE = "settings.account_deletion_delete".localized
    let ACCOUNT_DELETION_PASSWORD = "settings.account_deletion_enter_password".localized
    let ACCOUNT_DELETION_ERROR_TITLE = "settings.account_deletion_error_title".localized
    let ACCOUNT_DELETION_ERROR_MESSAGE = "settings.account_deletion_error_message".localized
    let ACCOUNT_DELETION_SUCCESS = "settings.account_deletion_success".localized
}

public struct TranslationDebug {
    let TITLE = "debug.title".localized
    let MENU = "debug.menu".localized
    let RELOAD = "debug.reload".localized
}

public struct TranslationError {
    let TITLE = "error.title".localized
    let WRONG_CODE = "error.wrong_code".localized
    let WRONG_VOUCHER_ID = "error.wrong_voucher_id".localized
    let UNKNOWN = "error.unknown".localized
}

public struct PushWarningError {
    let TITLE = "push_warning.title".localized
    let DESCRIPTION = "push_warning.description".localized
    let ITEM_NOTIFICATION = "push_warning.item_notification".localized
    let ITEM_SETTINGS = "push_warning.item_settings".localized
    let ITEM_SWITCH = "push_warning.item_switch".localized
    
    let BUTTON_SETIINGS = "push_warning.button_settings".localized
    let BUTTON_CLOSE = "push_warning.button_close".localized
}

public struct Login {
    let LOGIN = "login.login".localized
    let SIGNIN = "login.signin".localized
}

public struct LoginPage {
    let TITLE = "loginPage.title".localized
    let EMAIL = "loginPage.email".localized
    let PASSWORD = "loginPage.password".localized
    let LOGIN = "loginPage.login".localized
    let ERROR_TITLE = "loginPage.error_title".localized
    let ERROR_GENERAL = "loginPage.general_error".localized
    let ERROR_CONECTION = "loginPage.connection_error".localized
    let ERROR_NETWORK = "loginPage.network_error".localized
    let ERROR_INVALID_EMAIL = "loginPage.invalid_email".localized
    let NO_LOGIN = "loginPage.nologin".localized
    let REGISTER = "loginPage.register".localized
    let DIALOG = "loginPage.dialog".localized

    let ERROR_FB_LOGIN = "loginPage.fb_login".localized
    let ERROR_FB_NO_EMAIL = "loginPage.fb_no_email".localized

    let NO_ACCESS = "loginPage.no_access".localized

    let FORGOT_PASS = "loginPage.forgotemPass".localized
    let SEND = "loginPage.send".localized
}

public struct RegisterPage {
    let DESCRIPTION = "registerPage.description".localized
    let EMAIL = "registerPage.email".localized
    let NAME = "registerPage.name".localized
    let TEL = "registerPage.tel".localized
    let ADDRESS = "registerPage.address".localized
    let PASSWORD = "registerPage.password".localized
    let CONFIRM_PASS = "registerPage.confirmPass".localized
    let PRIVACY = "registerPage.privacy".localized
    let BUTTON = "registerPage.button".localized

    let ALERT_TITLE = "registerPage.success_dialog_title".localized
    let ALERT_TEXT = "registerPage.success_dialog_text".localized
    let ALERT_TEXT2 = "registerPage.success_dialog_text2".localized

    let ERROR_NO_EMAIL = "registerPage.missing_email".localized
    let ERROR_INVALID_EMAIL = "registerPage.invalid_email".localized
    let ERROR_MISSING_PASSWORD = "registerPage.missing_password".localized
    let ERROR_PASS_NOT_SAME = "missing_passwords_not_same.name".localized
    let ERROR_INVALID_PASS = "registerPage.invalid_password".localized
    let ERROR_MISSING_NAME = "registerPage.missing_name".localized
    let ERROR_MISSING_PHONE = "registerPage.missing_phone".localized
}

public struct LogoutPage {
    let ALERT_TITLE = "logoutPage.alert".localized
}

public struct Favorites {
    let TITLE_RECIPE = "favorites.recipe.title".localized
    let TITLE_ARTICLE = "favorites.article.title".localized

    let RECIPE_NO_DATA = "favorites.recipe.no_data".localized
    let ARTICLE_NO_DATA = "favorites.articles.no_data".localized

    let RECIPE_ADDED = "favorites.recipe.added".localized
    let RECIPE_REMOVED = "favorites.recipe.removed".localized

    let ARTICLE_ADDED = "favorites.articles.added".localized
    let ARTICLE_REMOVED = "favorites.articles.removed".localized
}

public struct PhotoPage {
    let TAKE_PHOTO = "photoPage.takePhoto".localized
    let SEND = "photoPage.send".localized
    let SUBJECT = "photoPage.subject".localized
    let SEND_ERROR = "photoPage.error".localized
}

public struct PINLogin {
    let TITLE = "pin.login.title".localized
    let ERROR_TITLE = "pin.login.error.title".localized
    let ERROR_MESSAGE = "pin.login.error.message".localized
}

public struct PINSection {
    let TITLE = "pin.section.title".localized
    let ERROR_TITLE = "pin.section.error.title".localized
    let ERROR_MESSAGE = "pin.section.error.message".localized
    let SUCCESS_TITLE = "pin.section.success.title".localized
}

public struct PassBranch {
    let LOGIN_CARD_ID = "login.card_id".localized
    let LOGIN_CARD_ID_PLACEHOLDER = "login.card_id.placeholder".localized
    let LOGIN_MISSING_CARD_ID = "login.missing_card_id".localized
    let LOGIN_REGISTER = "login.register".localized

    let LOGIN_CONTACT_PHONE_INFO = "login.contact.phone_info".localized
    let LOGIN_CONTACT_EMAIL = "login.contact.email".localized
    let LOGIN_CONTACT_PHONE = "login.contact.phone".localized

    let BRANCHES_TITLE = "branch.title".localized
    let BRANCHES_SEARCH = "branches.search".localized
    let BRANCHES_SEARCH_TITLE = "branches.search.title".localized
    let BRANCHES_REGION_TITLE = "branches.region.title".localized
    let BRANCHES_DISTRICT_TITLE = "branches.district.title".localized
    let BRANCHES_DISTRICT_ALL = "branches.district.all".localized
    let BRANCHES_CATEGORIES_TITLE = "branches.categories.title".localized
    let BRANCHES_NO_ITEMS = "branches.no_items".localized
    let BRANCHES_ERROR = "branches.error".localized

    let DISTANCE_DEFAULT_TEXT = "distance.default_text".localized
    let DISTANCE_TEXT_SEARCHING = "distance.text_searching".localized
    let DISTANCE_TEXT_FOUND = "distance.text_found".localized

//    "location.permission_disabled" = "Pro zobrazení nejbližších slev si prosím povolte určování dle polohy";
//    "location.off" = "Pro zobrazení nejbližších slev si prosím zapněte určování dle polohy";

    let BRANCH_NAVIGATE = "branch.navigate".localized
    let BRANCH_ADDRESS = "branch.address".localized
    let BRANCH_CONTACT = "branch.contact".localized
    let BRANCH_PHONE = "branch.phone".localized
    let BRANCH_EMAIL = "branch.email".localized
    let BRANCH_WEB = "branch.web".localized
    let BRANCH_DISCOUNTS = "branch.discounts".localized
    let BRANCH_DISTANCE = "branch.distance".localized
    let BRANCH_DESCRIPTION = "branch.description".localized

    let BRANCH_KIDS_ONE = "branch.kids.one".localized
    let BRANCH_KIDS_MANY = "branch.kids.many".localized

    let BRANCH_IZS_TITLE = "branch.izs.title".localized
    let BRANCH_IZS_PHONE = "branch.izs.phone".localized
    let BRANCH_IZS_EMAIL = "branch.izs.email".localized
    let BRANCH_IZS_DISTANCE = "branch.izs.distance".localized

    let CARD_FAMILY = "card.family".localized
}

public struct NotificationAreas {
    let TITLE = "notificationAreas.title".localized
    let ALL_AREAS = "notificationAreas.all".localized
    let BUTTON = "notificationAreas.button".localized
}

public struct Search {
    let AUTHORS = "search.authors".localized
}

public struct Translation {
    public var common = TranslationCommon()
    public var link = TranslationLink()
    public var map = TranslationMaps()
    public var form = TranslationForm()
    public var voucher = TranslationVouchers()
    public var photoReporter = TranslationPhotoReporter()
    public var rss = TranslationRss()
    public var qrCode = TranslationQRCode()
    public var settings = TranslationSettings()
    public var debug = TranslationDebug()
    public var error = TranslationError()
    public var pushWarning = PushWarningError()
    public var login = Login()
    public var loginPage = LoginPage()
    public var registePage = RegisterPage()
    public var logoutPage = LogoutPage()
    public var favorites = Favorites()
    public var photoPage = PhotoPage()
    public var pinLogin = PINLogin()
    public var pinSection = PINSection()
    public var seniorPass = PassBranch()
    public var notificationAreas = NotificationAreas()
    public var search = Search()
    
    mutating func reload() {
        common = TranslationCommon()
        link = TranslationLink()
        map = TranslationMaps()
        form = TranslationForm()
        voucher = TranslationVouchers()
        photoReporter = TranslationPhotoReporter()
        rss = TranslationRss()
        qrCode = TranslationQRCode()
        settings = TranslationSettings()
        debug = TranslationDebug()
        error = TranslationError()
        pushWarning = PushWarningError()
        login = Login()
        loginPage = LoginPage()
        registePage = RegisterPage()
        logoutPage = LogoutPage()
        favorites = Favorites()
        photoPage = PhotoPage()
        pinLogin = PINLogin()
        pinSection = PINSection()
        seniorPass = PassBranch()
        notificationAreas = NotificationAreas()
        search = Search()
    }
}

public var translations = Translation()
    

/// Localization

public extension String {
    
    var localized: String {
        if let languageCode = UserDefaults.getLanguageCode(),
            let path = Bundle(for: AppkeeConfigManager.self).path(forResource: languageCode, ofType: "lproj"),
            let languageBundle = Bundle (path: path) {
                return languageBundle.localizedString(forKey: self, value: "", table: nil)
        }

        if let selectedLanguage = UserDefaults.getLanguage()?.language,
            let path = Bundle(for: AppkeeConfigManager.self).path(forResource: selectedLanguage, ofType: "lproj"),
            let languageBundle = Bundle (path: path) {
                return languageBundle.localizedString(forKey: self, value: "", table: nil)
        }

        
        if let path = Bundle(for: AppkeeConfigManager.self).path(forResource: "cs", ofType: "lproj"),
            let languageBundle = Bundle (path: path) {
                return languageBundle.localizedString(forKey: self, value: "", table: nil)
        }
        
        let bundle = Bundle(for: AppkeeConfigManager.self)
        return bundle.localizedString(forKey: self, value: "", table: nil)
    }
  
//    func localized(_ args: [CVarArg]) -> String {
//        return localized(args)
//    }
  
    func localized(_ args: CVarArg...) -> String {
        return String(format: localized, args)
    }
}

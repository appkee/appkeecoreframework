//
//  URL+params.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 12/31/20.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

extension URL {
    func valueOf(_ queryParamaterName: String) -> String? {
        guard let url = URLComponents(string: self.absoluteString) else { return nil }
        return url.queryItems?.first(where: { $0.name == queryParamaterName })?.value
    }
}

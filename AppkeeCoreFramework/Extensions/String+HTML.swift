//
//  UILabel+HTML.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 27/09/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func toHtml() -> NSAttributedString {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data,
                                          options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html,
                                                    NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }

    func toHtml(colorSettings: AppkeeColorSettings?) -> String {
        var style: String
        if let colorSettings = colorSettings {
            if let css = colorSettings.css, !css.isEmpty {
                style = String(format: "<style>* { font-family: Helvetica Neue; margin-left:0;margin-right:0;padding-left:0;padding-right:0;} td{padding:5px;} table{width:100%%!important;} strong{color:inherit;}   a { color: %@;} %@ </style>", arguments: [
                    colorSettings.contentTextHEX,
                    css
                ])
            } else {
                style = String(format: "<style>* { font-family: Helvetica Neue; margin-left:0;margin-right:0;padding-left:0;padding-right:0;} td{padding:5px;} table{width:100%%!important;} strong{color:inherit;}   a { color: %@;}  </style>", arguments: [
                    colorSettings.contentTextHEX
                ])
            }
            let html = String(format: "<html> <body style=\"background-color:transparent; color:%@ \"> %@ %@  </body></html>", arguments: [
                    colorSettings.contentTextHEX,
                    style,
                    self
            ])
            
            return html
        } else {
            return self
        }
    }
    
    func stripOutHtml() -> String? {
//        do {
//            guard let data = self.data(using: .unicode) else {
//                return nil
//            }
//            let attributed = try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
//            return attributed.string
//        } catch {
//            return nil
//        }

        let str = self.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
        return str.replacingOccurrences(of: "&[^;]+;", with: "", options: .regularExpression, range: nil)
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
      return prefix(1).uppercased() + self.lowercased().dropFirst()
    }

    mutating func capitalizeFirstLetter() {
      self = self.capitalizingFirstLetter()
    }
}

extension String {
    func boolValueFromString() -> Bool {
        return NSString(string: self).boolValue
    }
}

//
//  Date+String.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 25/01/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

extension String {
        
    func formatedDate() -> String {
        let dateFormatter = DateFormatter()
//        dateFormatter.timeZone = .current
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        if let date = dateFormatter.date(from: self) {
            dateFormatter.dateFormat = "dd.MM.yyyy"
            
            return dateFormatter.string(from: date)
        } else {
            return ""
        }
    }

    func getDate() -> Date? {
        let dateFormatter = DateFormatter()
//        dateFormatter.timeZone = .current
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"

        return dateFormatter.date(from: self)
    }
}

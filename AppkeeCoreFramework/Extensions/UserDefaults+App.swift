//
//  UserDefaults+App.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 20/01/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

extension UserDefaults {
        
    class var Vouchers: [Int] {
        let array = UserDefaults.standard.array(forKey: AppkeeConstants.UserDefs.vouchers) as? [Int] ?? []
        return array
    }
    
    class func containsVoucher(voucherId: Int) -> Bool {
        let array = UserDefaults.standard.array(forKey: AppkeeConstants.UserDefs.vouchers) as? [Int] ?? []
        return array.contains(voucherId)
    }
        
    class func addVoucher(voucherId: Int) {
        var array = UserDefaults.standard.array(forKey: AppkeeConstants.UserDefs.vouchers) as? [Int] ?? []
        if !array.contains(voucherId) {
            array.append(voucherId)
            UserDefaults.standard.set(array, forKey: AppkeeConstants.UserDefs.vouchers)
        }
    }

    class func containsAdvertisement(advertisementId: Int) -> Bool {
        let array = UserDefaults.standard.array(forKey: AppkeeConstants.UserDefs.advertsements) as? [Int] ?? []
        return array.contains(advertisementId)
    }
        
    class func addAdvertisement(advertisementId: Int) {
        var array = UserDefaults.standard.array(forKey: AppkeeConstants.UserDefs.advertsements) as? [Int] ?? []
        if !array.contains(advertisementId) {
            array.append(advertisementId)
            UserDefaults.standard.set(array, forKey: AppkeeConstants.UserDefs.advertsements)
        }
    }
    
    class func clearAdvertisement() {
        UserDefaults.standard.set(nil, forKey: AppkeeConstants.UserDefs.advertsements)
    }
    

    class func bannersOff() -> Bool {
        return UserDefaults.standard.bool(forKey: AppkeeConstants.UserDefs.banners)
    }

    class func bannersAdd() {
        UserDefaults.standard.set(true, forKey: AppkeeConstants.UserDefs.banners)
    }

    class func bannersClean() {
        UserDefaults.standard.set(false, forKey: AppkeeConstants.UserDefs.banners)
    }

    class var newLogin: Bool {
        get {
            let fistLogin = !UserDefaults.standard.bool(forKey: AppkeeConstants.UserDefs.newLogin)
            return fistLogin
        }
        set {
            UserDefaults.standard.set(!newValue, forKey: AppkeeConstants.UserDefs.newLogin)
        }
    }
    
    class func containsProgramFavourite(programId: Int) -> Bool {
        let array = UserDefaults.standard.array(forKey: AppkeeConstants.UserDefs.programFavourites) as? [Int] ?? []
        return array.contains(programId)
    }
    
    class func addProgramFavourite(programId: Int) {
        var array = UserDefaults.standard.array(forKey: AppkeeConstants.UserDefs.programFavourites) as? [Int] ?? []
        if !array.contains(programId) {
            array.append(programId)
            UserDefaults.standard.set(array, forKey: AppkeeConstants.UserDefs.programFavourites)
        }
    }
    
    class func removeProgramFavourite(programId: Int) {
        var array = UserDefaults.standard.array(forKey: AppkeeConstants.UserDefs.programFavourites) as? [Int] ?? []
        if let index = array.firstIndex(of: programId) {
            array.remove(at: index)
            UserDefaults.standard.set(array, forKey: AppkeeConstants.UserDefs.programFavourites)
        }
    }

    class func setLanguageCode(code: String) {
        if let data = UserDefaults.standard.value(forKey:AppkeeConstants.UserDefs.appCode) as? Data,
           let code = try? PropertyListDecoder().decode(AppkeeAppPreferenceLanguage.self, from: data) {
            return
        }

        UserDefaults.standard.setValue(code, forKey: AppkeeConstants.UserDefs.languageCode)
    }

    class func getLanguageCode() -> String? {
        return UserDefaults.standard.string(forKey: AppkeeConstants.UserDefs.languageCode)
    }

    
    class func setLanguage(language: AppkeeAppPreferenceLanguage?) {
        UserDefaults.standard.setValue(nil, forKey: AppkeeConstants.UserDefs.languageCode)
        UserDefaults.standard.set(try? PropertyListEncoder().encode(language), forKey: AppkeeConstants.UserDefs.appCode)
                
        translations.reload()
    }
    
    class func getLanguage() -> AppkeeAppPreferenceLanguage? {
        if let data = UserDefaults.standard.value(forKey:AppkeeConstants.UserDefs.appCode) as? Data {
            return try? PropertyListDecoder().decode(AppkeeAppPreferenceLanguage.self, from: data)
        }
        
        return nil
    }
    
    class var isWebViewLogged: Bool {
        get {
            let logged = UserDefaults.standard.bool(forKey: AppkeeConstants.UserDefs.webViewLogged)
            return logged
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppkeeConstants.UserDefs.webViewLogged)
        }
    }

    class var pageEmail: String? {
        get {
            let pageEmail = UserDefaults.standard.string(forKey: AppkeeConstants.UserDefs.pageEmail)
            return pageEmail
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppkeeConstants.UserDefs.pageEmail)
        }
    }

    class var pagePassword: String? {
        get {
            let pagePassword = UserDefaults.standard.string(forKey: AppkeeConstants.UserDefs.pagePassword)
            return pagePassword
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppkeeConstants.UserDefs.pagePassword)
        }
    }

    class var userIdApple: String? {
        get {
            let userIdApple = UserDefaults.standard.string(forKey: AppkeeConstants.UserDefs.userIdApple)
            return userIdApple
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppkeeConstants.UserDefs.userIdApple)
        }
    }

    class func addFavorite(articleId: Int) {
        var array = UserDefaults.standard.array(forKey: AppkeeConstants.UserDefs.favorites) as? [Int] ?? []
        if !array.contains(articleId) {
            array.append(articleId)
            UserDefaults.standard.set(array, forKey: AppkeeConstants.UserDefs.favorites)
        }
    }

    class func removeFavorite(articleId: Int) {
        var array = UserDefaults.standard.array(forKey: AppkeeConstants.UserDefs.favorites) as? [Int] ?? []
        if let index = array.firstIndex(of: articleId) {
            array.remove(at: index)
            UserDefaults.standard.set(array, forKey: AppkeeConstants.UserDefs.favorites)
        }
    }

    class func favorites() -> [Int] {
        return UserDefaults.standard.array(forKey: AppkeeConstants.UserDefs.favorites) as? [Int] ?? []
    }

    class func containsFavourite(articleId: Int) -> Bool {
        let array = UserDefaults.standard.array(forKey: AppkeeConstants.UserDefs.favorites) as? [Int] ?? []
        return array.contains(articleId)
    }

    class func addPinCode(pinCode: String) {
        UserDefaults.standard.set(pinCode, forKey: AppkeeConstants.UserDefs.pinCode)
    }

    class func pinCode() -> String? {
        return UserDefaults.standard.string(forKey: AppkeeConstants.UserDefs.pinCode)
    }

    class func addSectionPinCode(sectionID: Int, pinCode: String) {
        var dictionary = UserDefaults.standard.object(forKey: AppkeeConstants.UserDefs.pinCodeSection) as? [Int : String] ?? [:]
        dictionary[sectionID] = pinCode
        let data = NSKeyedArchiver.archivedData(withRootObject: dictionary)
        UserDefaults.standard.set(data, forKey: AppkeeConstants.UserDefs.pinCodeSection)
    }

    class func sectionPinCode(sectionID: Int) -> String? {
        if let data = UserDefaults.standard.data(forKey: AppkeeConstants.UserDefs.pinCodeSection) {
            let dictionary = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Int : String] ?? [:]
            return dictionary[sectionID]
        }

        return nil
    }

//    class var introPageLogin: String? {
//        get {
//            let pageEmail = UserDefaults.standard.string(forKey: AppkeeConstants.UserDefs.introPageLogin)
//            return pageEmail
//        }
//        set {
//            UserDefaults.standard.set(newValue, forKey: AppkeeConstants.UserDefs.introPageLogin)
//        }
//    }

    class var seniorPassLogin: AppkeeSeniorPass? {
        get {
            if let data = UserDefaults.standard.value(forKey:AppkeeConstants.UserDefs.seniorPassLogin) as? Data {
                return try? PropertyListDecoder().decode(AppkeeSeniorPass.self, from: data)
            }

            return nil
        }
        set {
            UserDefaults.standard.set(try? PropertyListEncoder().encode(newValue), forKey: AppkeeConstants.UserDefs.seniorPassLogin)
        }
    }
    
    class var familyPassLogin: AppkeeFamilyPass? {
        get {
            if let data = UserDefaults.standard.value(forKey:AppkeeConstants.UserDefs.familyPassLogin) as? Data {
                return try? PropertyListDecoder().decode(AppkeeFamilyPass.self, from: data)
            }

            return nil
        }
        set {
            UserDefaults.standard.set(try? PropertyListEncoder().encode(newValue), forKey: AppkeeConstants.UserDefs.familyPassLogin)
        }
    }

    class var seniorPassCategories: AppkeeSeniorPassInfo? {
        get {
            if let data = UserDefaults.standard.value(forKey:AppkeeConstants.UserDefs.seniorPassCategories) as? Data {
                return try? PropertyListDecoder().decode(AppkeeSeniorPassInfo.self, from: data)
            }
            return nil
        }
        set {
            UserDefaults.standard.set(try? PropertyListEncoder().encode(newValue), forKey: AppkeeConstants.UserDefs.seniorPassCategories)
        }
    }

    class var seniorPassCategoriesDate: Date? {
        get {
            return UserDefaults.standard.value(forKey:AppkeeConstants.UserDefs.seniorPassCategoriesUpdate) as? Date
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppkeeConstants.UserDefs.seniorPassCategoriesUpdate)
        }
    }

    class var familyPassCategories: AppkeeSeniorPassInfo? {
        get {
            if let data = UserDefaults.standard.value(forKey:AppkeeConstants.UserDefs.familyPassCategories) as? Data {
                return try? PropertyListDecoder().decode(AppkeeSeniorPassInfo.self, from: data)
            }
            return nil
        }
        set {
            UserDefaults.standard.set(try? PropertyListEncoder().encode(newValue), forKey: AppkeeConstants.UserDefs.familyPassCategories)
        }
    }

    class var familyPassCategoriesDate: Date? {
        get {
            return UserDefaults.standard.value(forKey:AppkeeConstants.UserDefs.familyPassCategoriesUpdate) as? Date
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppkeeConstants.UserDefs.familyPassCategoriesUpdate)
        }
    }

    class var seniorPassBranches: [Int: [AppkeeSeniorPassBranch]]? {
        get {
            if let data = UserDefaults.standard.value(forKey:AppkeeConstants.UserDefs.seniorPassBranches) as? Data {
                return try? PropertyListDecoder().decode([Int: [AppkeeSeniorPassBranch]].self, from: data)
            }
            return nil
        }
        set {
            UserDefaults.standard.set(try? PropertyListEncoder().encode(newValue), forKey: AppkeeConstants.UserDefs.seniorPassBranches)
        }
    }

    class var seniorPassBranchesDate: Date? {
        get {
            return UserDefaults.standard.value(forKey:AppkeeConstants.UserDefs.seniorPassBranchesUpdate) as? Date
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppkeeConstants.UserDefs.seniorPassBranchesUpdate)
        }
    }

    class var familyPassBranches: [Int: [AppkeeFamilyPassBranch]]? {
        get {
            if let data = UserDefaults.standard.value(forKey:AppkeeConstants.UserDefs.familyPassBranches) as? Data {
                return try? PropertyListDecoder().decode([Int: [AppkeeFamilyPassBranch]].self, from: data)
            }
            return nil
        }
        set {
            UserDefaults.standard.set(try? PropertyListEncoder().encode(newValue), forKey: AppkeeConstants.UserDefs.familyPassBranches)
        }
    }

    class var familyPassBranchesDate: Date? {
        get {
            return UserDefaults.standard.value(forKey:AppkeeConstants.UserDefs.familyPassBranchesUpdate) as? Date
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppkeeConstants.UserDefs.familyPassBranchesUpdate)
        }
    }

    class var seniorPassIZSBranches: AppkeeSeniorPassIZSList? {
        get {
            if let data = UserDefaults.standard.value(forKey:AppkeeConstants.UserDefs.seniorPassIZSBranches) as? Data {
                return try? PropertyListDecoder().decode(AppkeeSeniorPassIZSList.self, from: data)
            }
            return nil
        }
        set {
            UserDefaults.standard.set(try? PropertyListEncoder().encode(newValue), forKey: AppkeeConstants.UserDefs.seniorPassIZSBranches)
        }
    }

    class var seniorPassIZSDate: Date? {
        get {
            return UserDefaults.standard.value(forKey:AppkeeConstants.UserDefs.seniorPassIZSBranchesUpdate) as? Date
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppkeeConstants.UserDefs.seniorPassIZSBranchesUpdate)
        }
    }

    class var notificationRegionsID: [Int]? {
        get {
            if let data = UserDefaults.standard.value(forKey:AppkeeConstants.UserDefs.notificationRegions) as? Data {
                return try? PropertyListDecoder().decode([Int].self, from: data)
            }
            return nil
        }
        set {
            UserDefaults.standard.set(try? PropertyListEncoder().encode(newValue), forKey: AppkeeConstants.UserDefs.notificationRegions)
        }
    }

    class var pageUser: AppkeeUser? {
        get {
            if let data = UserDefaults.standard.value(forKey:AppkeeConstants.UserDefs.pageUser) as? Data {
                return try? PropertyListDecoder().decode(AppkeeUser.self, from: data)
            }
            return nil
        }
        set {
            UserDefaults.standard.set(try? PropertyListEncoder().encode(newValue), forKey: AppkeeConstants.UserDefs.pageUser)
        }
    }

//    class var dialogUser: AppkeeUser? {
//        get {
//            if let data = UserDefaults.standard.value(forKey:AppkeeConstants.UserDefs.dialogUser) as? Data {
//                return try? PropertyListDecoder().decode(AppkeeUser.self, from: data)
//            }
//            return nil
//        }
//        set {
//            UserDefaults.standard.set(try? PropertyListEncoder().encode(newValue), forKey: AppkeeConstants.UserDefs.dialogUser)
//        }
//    }

    class var searchTooltip: Int {
        get {
            let searchTooltip = UserDefaults.standard.integer(forKey: AppkeeConstants.UserDefs.searchTooltip)
            return searchTooltip
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppkeeConstants.UserDefs.searchTooltip)
        }
    }
}

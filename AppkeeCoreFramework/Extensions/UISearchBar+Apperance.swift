//
//  UISearchBar+Apperance.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 7/7/20.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

extension UISearchBar {

     var textColor: UIColor? {
         get {
            if let textField = self.value(forKey: "searchField") as? UITextField  {
                 return textField.textColor
             } else {
                 return nil
             }
         }

         set (newValue) {
            if let textField = self.value(forKey: "searchField") as? UITextField  {
                 textField.textColor = newValue
             }
         }
     }
    
    var textBackgroundColor: UIColor? {
        get {
           if let textField = self.value(forKey: "searchField") as? UITextField  {
                return textField.backgroundColor
            } else {
                return nil
            }
        }

        set (newValue) {
           if let textField = self.value(forKey: "searchField") as? UITextField  {
                textField.backgroundColor = newValue
            }
        }
    }
}

//
//  Alamofire+Codable.swift
//  CareemNOW
//
//  Created by Dominik Ringler on 22/08/2018.
//  Copyright © 2018 CareemNOW. All rights reserved.
//

import Alamofire

enum NetworkError: Int, LocalizedError {
    case notFound = 404
    case serverError = 500
    case timeOut = -1001
    case noResponse = -2102
    
    var localizedDescription: String {
        switch self {
        case .notFound, .serverError:
            return "Error not found"
        case .timeOut, .noResponse:
            return "Error connection"
        }
    }
    
    var errorDescription: String? {
        return localizedDescription
    }
}

extension Alamofire.DataRequest {
    
    static var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        return decoder
    }()
    
    func responseCodable<T: Codable>(completion: @escaping (DataResponse<T>) -> Void) {
        validate().responseData { response in
            switch response.result {
                
            case .success(let data):
                do {
                    let decodedData = try DataRequest.decoder.decode(T.self, from: data)
                    completion(DataResponse(request: self.request, response: self.response, data: data, result: .success(decodedData)))
                } catch {
                    print(">> \(error)")
                    completion(DataResponse(request: self.request, response: self.response, data: data, result: .failure(error)))
                }
                
            case .failure(let error):
                var formattedError = error
                if
                    case let AFError.responseValidationFailed(reason: reason) = error,
                    case let .unacceptableStatusCode(code: code) = reason,
                    let networkError = NetworkError(rawValue: code) {
                    formattedError = networkError
                } else if let networkError = NetworkError(rawValue: (error as NSError).code) {
                    formattedError = networkError
                }
                completion(DataResponse(request: self.request, response: self.response,
                                        data: response.data, result: .failure(formattedError)))
            }
        }
    }
}


//extension Data {
//    
//    static var decoder: JSONDecoder = {
//        let decoder = JSONDecoder()
////        decoder.keyDecodingStrategy = .convertFromSnakeCase
//        decoder.dateDecodingStrategy = .iso8601
//        return decoder
//    }()
//    
//    func responseCodable<T: Codable>(completion: @escaping (DataResponse<T>) -> Void) {
//        validate().responseData { response in
//            switch response.result {
//                
//            case .success(let data):
//                do {
//                    let decodedData = try DataRequest.decoder.decode(T.self, from: data)
//                    completion(DataResponse(request: self.request, response: self.response, data: data, result: .success(decodedData)))
//                } catch {
//                    completion(DataResponse(request: self.request, response: self.response, data: data, result: .failure(error)))
//                }
//                
//            case .failure(let error):
//                var formattedError = error
//                if
//                    case let AFError.responseValidationFailed(reason: reason) = error,
//                    case let .unacceptableStatusCode(code: code) = reason,
//                    let networkError = NetworkError(rawValue: code) {
//                    formattedError = networkError
//                } else if let networkError = NetworkError(rawValue: (error as NSError).code) {
//                    formattedError = networkError
//                }
//                completion(DataResponse(request: self.request, response: self.response,
//                                        data: response.data, result: .failure(formattedError)))
//            }
//        }
//    }
//}

//
//  UIlabel+HTML.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 27/09/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

extension UILabel {
    func setHtml(from string: String, async: Bool = true) {
        let fontColor = textColor.toHexString()
        let fontSize = font?.pointSize ?? 12.0
        let fontName = font?.fontName ?? ""
        let textAlign: String

        switch textAlignment {
        case .center:
            textAlign = "center"
        case .left:
            textAlign = "left"
        case .right:
            textAlign = "right"
        case .natural:
            textAlign = "natural"
        case .justified:
            textAlign = "justify"

        @unknown default:
            textAlign = "left"
        }

        let styleHtml =
        """
        <style type='text/css'>
        a { text-decoration: none }
        body{font-family: '\(fontName)'; font-size:\(fontSize)px; color:\(fontColor);text-align: \(textAlign);}
        </style>
        """
        if async {
            DispatchQueue.main.async {
                let attributeText = (string + styleHtml).toHtml()
                self.attributedText = attributeText
            }
        } else {
            let attributeText = (string + styleHtml).toHtml()
            self.attributedText = attributeText
        }
    }
}

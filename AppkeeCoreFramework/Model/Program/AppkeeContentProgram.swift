//
//  AppkeeContent2Program.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 07/03/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

struct AppkeeContentProgram: Codable {
    enum ContentType: String, Codable {
        case detail = "detail"
        case pause = "break"
        case empty = "empty"
    }
    
    let id: Int
    let title: String
    let type: ContentType
    let content: String?
    let time: String
    let info: String?
    let color: String?
    let textColor: String?
    let order: Int
    
    var room: String?

    let authors: [String]

    var dateTime: Date {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "HH:mm:ss"

        let array = time.split(separator: " ")
        return dateFormatter.date(from: String(array[0])) ?? Date()
    }
}

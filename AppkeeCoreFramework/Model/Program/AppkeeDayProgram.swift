//
//  AppkeeProgram.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 07/03/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

struct AppkeeDayProgram: Codable {
    let name: String
    var content: [AppkeeRoomProgram]
    let order: Int
}

//
//  File.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 07/03/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

struct AppkeeProgramSection: Codable {
    let id: Int
    let sectionId: Int
    let program: [AppkeeDayProgram]
    let order: Int
    let articleId: Int
}

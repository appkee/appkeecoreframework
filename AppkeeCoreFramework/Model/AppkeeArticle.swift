//
//  AppkeeArticle.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 23/09/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeArticleContentType: String, Codable {
    case page = "page"
    case link = "link"
    case program = "program"
    case myProgram = "myProgram"
    case articles = "articles"
}

struct AppkeeArticle: Codable {
    let id: Int
    let sectionId: Int
    let customId: Int
    let name: String?
    let link: String?
    let type: AppkeeArticleContentType
    let order: Int
    let visibility: Bool
    let articles: [AppkeeSubArticle]
}


struct AppkeeSubArticle: Codable {
    let id: Int
    let sectionId: Int
    let articleId: Int
    let customId: Int
    let name: String?
    let link: String?
    let type: AppkeeArticleContentType
    let order: Int
    let visibility: Bool
}

//
//  AppkeeConfig.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 07/02/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

struct AppkeeAppPreferenceLanguage: Codable {
    let appCode:String
    let image:String
    let language: String
    let title: String
}

struct AppkeeAppPreferences: Codable {
    var appCode: String?
    var language: String?
    var languages: [AppkeeAppPreferenceLanguage]?
    var pinCode: String?
}

//
//  AppkeeSeniorPassIZSResponse.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/2/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

public struct AppkeeSeniorPassIZSResponse: Codable {
    let error: String?
    let success: Bool
    let data: AppkeeSeniorPassIZSList?
}

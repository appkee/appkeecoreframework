//
//  AppkeeVoucherResponse.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 14/01/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

public struct AppkeeVouchersResponse: Codable {
    let error: String?
    let success: Bool
    let data: AppkeeVouchersDescription?
}

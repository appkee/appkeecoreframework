//
//  AppkeeVoucher.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 14/01/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

public struct AppkeeVoucher: Codable {
    let id: Int
    let name: String
    let description: String?
    let image: String?
    let validTo: String
}

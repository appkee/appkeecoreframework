//
//  AppkeeUseVoucherResponse.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 17/01/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

public struct AppkeeUseVoucherResponse: Codable {
    public let error: String?
    public let success: Bool
//    let data:
}

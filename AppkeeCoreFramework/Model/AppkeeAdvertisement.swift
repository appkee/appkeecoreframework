//
//  AppkeeAdvertisement.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 26/01/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

struct AppkeeAdvertisement: Codable {
    enum Position: String, Codable {
        case bottom = "bottom"
        case top = "top"
    }
    
    let id: Int
    let image: String?
    let url: String?
    let position: Position
    var visibility: Bool
    let duration: Int?


    var adUnitID: String?

    init(id: Int, position: Position, adUnitID: String, visibility: Bool) {
        self.id = id
        self.position = position
        self.adUnitID = adUnitID
        self.visibility = visibility

        self.duration = nil
        self.image = nil
        self.url = nil
    }
}

//
//  AppkeeContent.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 27/09/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

struct AppkeeContent: Codable {
    let id: Int
    let sectionId: Int
    let content: String?    
    let order: Int
    let clickable: Bool
    let articleId: Int
    let link: String?
}


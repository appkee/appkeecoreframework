//
//  AppkeeAppResponse.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 23/09/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

public struct AppkeeAppResponse: Codable {
    let error: String?
    let success: Bool
    let data: AppkeeAppDescription?
}

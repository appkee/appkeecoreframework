//
//  AppkeeSenioPass.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 5/25/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

struct AppkeeSeniorPass: Codable {
    let number: String
    let region: Int
    let regionName: String
    let firstname: String
    let surname: String
    let birthYear: Int
    let validTo: String
}

struct AppkeeFamilyPass: Codable {
    let number: String
    let region: String
    let regionName: String
    let familyName: String
    let kidsYears: [Int]
    let validTo: String
}


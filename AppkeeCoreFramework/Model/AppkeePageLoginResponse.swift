//
//  AppkeePageLoginResponse.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 1/24/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

public struct AppkeePageLoginResponse: Codable {
    public let error: String?
    public let success: Bool
    public let data: AppkeeUser?
}

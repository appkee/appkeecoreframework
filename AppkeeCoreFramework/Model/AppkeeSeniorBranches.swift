//
//  AppkeeSeniorBranch.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/22/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

struct AppkeeSeniorPassBranchesArray: Codable {
    let branches: [AppkeeSeniorPassBranch]
}

struct AppkeeFamilyPassBranchesArray: Codable {
    let branches: [AppkeeFamilyPassBranch]
}


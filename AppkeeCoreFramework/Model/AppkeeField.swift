//
//  AppkeeField.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 27/12/2018.
//  Copyright © 2018 Radek Zmeskal. All rights reserved.
//

import Foundation

struct AppkeeField: Codable {
    enum AppkeeFieldType: String, Codable {
        case title = "title"
        case text = "text"
        case textArea = "textarea"
        case number = "number"
        case select = "select"
        case checkbox = "checkbox"
    }
    
    let id: Int
    let sectionId: Int
    let name: String?
    let type: AppkeeFieldType
    let order: Int
    let required: Bool
}


                                                       

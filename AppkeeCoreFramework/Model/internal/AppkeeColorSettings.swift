//
//  AppkeeColorSettings.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 05/05/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

struct AppkeeColorSettings {
    
    let vouchersText = UIColor.black
    let voucherBackground = UIColor.white
    let voucherDescription = UIColor.gray
    let voucherButton = UIColor.red
    
    let header: UIColor
    let headerText: UIColor
    let menu: UIColor
    let menuText: UIColor
    let content: UIColor
    let contentText: UIColor
    let contentTextHEX: String
    let loginColor: UIColor

    let registerColor: UIColor

    let css: String?
}

//
//  AppkeeSeniorPassInfoResponse.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/25/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

public struct AppkeeSeniorPassInfoResponse: Codable {
    let error: String?
    let success: Bool
    let data: AppkeeSeniorPassInfo?
}


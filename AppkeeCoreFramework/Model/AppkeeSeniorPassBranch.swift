//
//  AppkeeSeniorBranch.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/22/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

struct AppkeeSeniorPassLocation: Codable {
    let lat: Double
    let lon: Double
//    let location: CLLocation

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let lat = try? values.decode(Double.self, forKey: .lat) {
            self.lat = lat
        } else if let lat = try? values.decode(String.self, forKey: .lat) {
            self.lat = Double(lat) ?? 0.0
        } else {
            self.lat = 0.0
        }

        if let lon = try? values.decode(Double.self, forKey: .lon) {
            self.lon = lon
        } else if let lon = try? values.decode(String.self, forKey: .lon) {
            self.lon = Double(lon) ?? 0.0
        } else {
            self.lon = 0.0
        }

//        location = CLLocation(latitude: lat, longitude: lon)
    }
}

struct AppkeeSeniorPassDisscount: Codable {
    let description: String
    let value: Float?
    let since: String?
    let until: String?
}

struct AppkeeSeniorPassProvider: Codable {
    let id: Int
    let name: String
    let web: String?
    let description: String
    let level: String
    let logo: String?
}

struct AppkeeSeniorPassBranchObj: Codable {
    let id: Int
    let name: String
    let address: String
    let city: String
    let psc: Int
    let district: Int
    let districtName: String
    let region: Int
    let phone: String
    let email: String?
    let description: String
    let category: Int
    let web: String?
    let isEshop: Bool
    let level: String

    let location: AppkeeSeniorPassLocation
    let discounts: [AppkeeSeniorPassDisscount]
    let provider: AppkeeSeniorPassProvider

    var distance: Double?
}


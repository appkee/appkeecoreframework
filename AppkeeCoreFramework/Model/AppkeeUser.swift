//
//  AppkeeUser.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/4/22.
//  Copyright © 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

public struct AppkeeUser: Codable {
    let id: Int
    let email: String
    let name: String?
    let phone: String?
    let address: String?
    let active: Bool
}

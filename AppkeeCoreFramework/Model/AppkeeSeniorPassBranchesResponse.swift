//
//  AppkeeSeniorBranchesResponse.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/22/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

public struct AppkeeSeniorPassBranchesResponse: Codable {
    let error: String?
    let success: Bool
    let data: AppkeeSeniorPassBranchesArray?
}

public struct AppkeeFamilyPassBranchesResponse: Codable {
    let error: String?
    let success: Bool
    let data: AppkeeFamilyPassBranchesArray?
}


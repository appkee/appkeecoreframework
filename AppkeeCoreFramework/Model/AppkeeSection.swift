//
//  AppkeeSection.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 27/12/2018.
//  Copyright © 2018 Radek Zmeskal. All rights reserved.
//

import Foundation

struct AppkeeSection: Codable {
    enum SectionType: String, Codable {
        case page = "page"
        case articles = "articles"
        case gallery = "gallery"
        case rss = "rss"
        case link = "link"
        case gps = "gps"
        case form = "form"
        case custom = "custom"
        case voucher = "voucher"
        case photoReporter = "photoReporter"
        case qr = "qr"
        case settings = "settings"
        case favorites = "favorites"
    }
    
//    enum OrderType: Int, Codable {
//        case titleOnly = 3
//        case withoutDescription = 2
//        case withoutImage = 1
//        case withImage = 0
//    }
    
    let id: Int
    let name: String
    let type: SectionType
    var icon: String?
    let order: Int
    let hideImages: Int
    let link: String?
    let loggedInLink: String?
    let offlineLink: String?
    let gdprText: String?

    let externalLink: Bool
    let rssLink: String?
    
    let articles: [AppkeeArticle]?
    let texts: [AppkeeContent]?
    let images: [AppkeeContent]?
    let videos: [AppkeeContent]?
    let fields: [AppkeeField]
    let fieldItems: [AppkeeFieldItem]
    let programs: [AppkeeProgramSection]
    
    let email: String?
    let confirm: String?
    let latitude: String?
    let longitude: String?
    let code: String?
    let pinCode: String?
    let regionId: Int?

    let onlyLoggedIn: Bool
    let onlyUnloggedIn: Bool?

    let color: String?

    let css: String?

    let visibility: Bool

    let favorites: Bool

    let dialog: String?

    var advertisement: AppkeeAdvertisement?

    var banners: [AppkeeAdvertisement]?
    
    init(voucher: String) {
        self.id = -10
        self.name = voucher
        self.type = .voucher
        self.icon = AppkeeConstants.voucherImageURL
        self.order = INTPTR_MAX - 2
        self.hideImages = 0
        self.link = nil
        self.loggedInLink = nil
        self.offlineLink = nil
        self.externalLink = false
        self.rssLink = nil
        self.css = nil
        self.gdprText = nil

        self.articles = nil
        self.texts = nil
        self.images = nil
        self.videos = nil
        self.fields = []
        self.fieldItems = []
        self.programs = []
        
        self.email = nil
        self.confirm = nil
        self.latitude = nil
        self.longitude = nil
        self.code = nil
        self.pinCode = nil
        self.regionId = nil

        self.onlyLoggedIn = false
        self.onlyUnloggedIn = false

        self.color = nil

        self.favorites = false

        self.visibility = true
        
        self.advertisement = nil

        self.dialog = nil
    }
    
    init(settings: String) {
        self.id = -11
        self.name = settings
        self.type = .settings
        self.icon = AppkeeConstants.settingsImageURL
        self.order = INTPTR_MAX - 2
        self.hideImages = 0
        self.link = nil
        self.loggedInLink = nil
        self.offlineLink = nil
        self.externalLink = false
        self.rssLink = nil
        self.css = nil
        self.gdprText = nil

        self.articles = nil
        self.texts = nil
        self.images = nil
        self.videos = nil
        self.fields = []
        self.fieldItems = []
        self.programs = []
        
        self.email = nil
        self.confirm = nil
        self.latitude = nil
        self.longitude = nil
        self.code = nil
        self.pinCode = nil
        self.regionId = nil

        self.onlyLoggedIn = false
        self.onlyUnloggedIn = false

        self.color = nil

        self.favorites = false

        self.visibility = true
        
        self.advertisement = nil

        self.dialog = nil

    }
}

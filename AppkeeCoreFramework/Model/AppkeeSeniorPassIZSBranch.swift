//
//  File.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/3/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

struct AppkeeSeniorPassIZSBranch: Codable {
    let id: Int
    let regionId: Int
    let type: String
    let name: String
    let address: String
    let city: String
    let psc: String
    let lat: Double
    let lon: Double
    let phone: String
    let email: String

    var distance: Double?
}

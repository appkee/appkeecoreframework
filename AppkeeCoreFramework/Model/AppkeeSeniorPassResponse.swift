//
//  AppkeeSeniorPassResponse.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 5/25/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

public struct AppkeeSeniorPassResponse: Codable {
    let error: String?
    let success: Bool
    let data: AppkeeSeniorPass?
}

public struct AppkeeFamilyPassResponse: Codable {
    let error: String?
    let success: Bool
    let data: AppkeeFamilyPass?
}

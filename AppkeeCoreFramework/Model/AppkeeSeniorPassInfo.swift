//
//  AppkeeSeniorPassInfo.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/25/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

struct AppkeeSeniorPassInfoRegion: Codable {
    let id: Int
    let name: String

    let districts: [AppkeeSeniorPassInfoCategory]

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        do {
            let id = try container.decode(Int.self, forKey: .id)
            self.id = id
        } catch {
            let id = try container.decode(String.self, forKey: .id)
            self.id = Int(id) ?? 0
        }

        self.name = try container.decode(String.self, forKey: .name)
        self.districts = try container.decode([AppkeeSeniorPassInfoCategory].self, forKey: .districts)
    }
}

struct AppkeeSeniorPassInfoCategory: Codable {
    let id: Int
    let name: String

    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        do {
            let id = try container.decode(Int.self, forKey: .id)
            self.id = id
        } catch {
            let id = try container.decode(String.self, forKey: .id)
            self.id = Int(id) ?? 0
        }

        self.name = try container.decode(String.self, forKey: .name)
    }
}

struct AppkeeSeniorPassInfo: Codable {
    let regions: [AppkeeSeniorPassInfoRegion]
    let categories: [AppkeeSeniorPassInfoCategory]
}


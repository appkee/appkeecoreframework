//
//  DeleteAccountResponse.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 3/9/22.
//  Copyright © 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

public struct AppkeeDeleteAccountResponse: Codable {
    let error: String?
    let success: Bool
}

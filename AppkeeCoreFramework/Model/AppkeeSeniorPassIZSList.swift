//
//  AppkeeSeniorPassIZSList.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/2/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

struct AppkeeSeniorPassIZSList: Codable {
    let izs: [AppkeeSeniorPassIZSBranch]
}

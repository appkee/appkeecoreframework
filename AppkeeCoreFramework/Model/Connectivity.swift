//
//  Connectivity.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 1/19/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}

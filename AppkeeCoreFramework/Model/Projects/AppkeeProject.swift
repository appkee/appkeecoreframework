//
//  Project.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 02/02/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

public struct AppkeeProject: Codable {
    public let id: Int
    public let name: String
    public let usersApp: Bool
    public let published: Bool
}

//
//  AppkeeProjects.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 02/02/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

public struct AppkeeProjects: Codable {
    public let apps: [AppkeeProject]
}

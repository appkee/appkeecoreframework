//
//  AppkeeSeniorBranch.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/22/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

struct AppkeeSeniorPassLocation: Codable {
    let lat: Double
    let lon: Double
//    let location: CLLocation

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let lat = try? values.decode(Double.self, forKey: .lat) {
            self.lat = lat
        } else if let lat = try? values.decode(String.self, forKey: .lat) {
            self.lat = Double(lat) ?? 0.0
        } else {
            self.lat = 0.0
        }

        if let lon = try? values.decode(Double.self, forKey: .lon) {
            self.lon = lon
        } else if let lon = try? values.decode(String.self, forKey: .lon) {
            self.lon = Double(lon) ?? 0.0
        } else {
            self.lon = 0.0
        }

//        location = CLLocation(latitude: lat, longitude: lon)
    }
}

struct AppkeeSeniorPassDisscount: Codable {
    let description: String
    let value: Float?
    let since: String?
    let until: String?
}

//struct AppkeeSeniorPassProvider: Codable {
//    let id: Int
//    let name: String
//    let web: String?
//    let description: String
//    let level: String
//    let logo: String?
//}

struct AppkeeSeniorPassBranch: Codable {
    let id: Int
    let name: String
    let address: String
    let city: String
    let psc: Int
    let district: Int
    let districtName: String
    let region: Int
    let phone: String
    let email: String?
    let description: String
    let category: Int
    let web: String?
    let isEshop: Bool
    let level: String

    let location: AppkeeSeniorPassLocation?
    let discounts: [AppkeeSeniorPassDisscount]
//    let provider: AppkeeSeniorPassProvider

    var distance: Double?

//    init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//
//        do {
//            let id = try container.decode(Int.self, forKey: .id)
//            self.id = id
//        } catch {
//            let id = try container.decode(String.self, forKey: .id)
//            self.id = Int(id) ?? 0
//        }
//
//        self.name = try container.decode(String.self, forKey: .name)
//        self.address = try container.decode(String.self, forKey: .address)
//        self.city = try container.decode(String.self, forKey: .city)
//        self.psc = try container.decode(Int.self, forKey: .psc)
//        self.district = try container.decode(Int.self, forKey: .district)
//        self.districtName = try container.decode(String.self, forKey: .districtName)
//        self.region = try container.decode(Int.self, forKey: .region)
//        self.phone = try container.decode(String.self, forKey: .phone)
//        self.email = try container.decode(String.self, forKey: .email)
//        self.description = try container.decode(String.self, forKey: .description)
//        self.category = try container.decode(Int.self, forKey: .category)
//        self.web = try container.decode(String.self, forKey: .web)
//        self.isEshop = try container.decode(Bool.self, forKey: .isEshop)
//        self.level = try container.decode(String.self, forKey: .level)
//
//
//        self.location = try container.decode(AppkeeSeniorPassLocation.self, forKey: .location)
//        self.discounts = try container.decode([AppkeeSeniorPassDisscount].self, forKey: .discounts)
//        self.provider = try container.decode(AppkeeSeniorPassProvider.self, forKey: .provider)
//    }
}


struct AppkeeFamilyPassBranch: Codable {
    let id: String
    let street_one: String
    let street_two: String?
    let city: String
    let post_code: String
    let email: String?
    let phone_number: String
    let web: String?
    let description: String?
    let text_sale: String
    let percent_sale: String

    let district_id: Int
    let category_id: Int
    let company_name: String

    let lat: Double
    let lng: Double

    var distance: Double?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case street_one = "street_one"
        case street_two = "street_two"
        case city = "city"
        case post_code = "post_code"
        case email = "email"
        case phone_number = "phone_number"
        case web = "web"
        case description = "description"
        case text_sale = "text_sale"
        case percent_sale = "percent_sale"
        case district_id = "district_id"
        case category_id = "category_id"
        case company_name = "company_name"
        case lat = "lat"
        case lng = "lng"

        case attributes = "attributes"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try container.decode(String.self, forKey: .id)
        self.district_id = Int(try container.decode(String.self, forKey: .district_id)) ?? -1
        self.category_id = Int(try container.decode(String.self, forKey: .category_id)) ?? -1
        self.company_name = try container.decode(String.self, forKey: .company_name)

        let attributesContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .attributes)

        self.street_one = try attributesContainer.decode(String.self, forKey: .street_one)
        self.street_two = try attributesContainer.decode(String?.self, forKey: .street_two)
        self.city = try attributesContainer.decode(String.self, forKey: .city)
        self.post_code = try attributesContainer.decode(String.self, forKey: .post_code)
        self.email = try attributesContainer.decode(String?.self, forKey: .email)
        self.phone_number = try attributesContainer.decode(String.self, forKey: .phone_number)
        self.web = try attributesContainer.decode(String?.self, forKey: .web)
        self.description = try attributesContainer.decode(String?.self, forKey: .description)
        self.text_sale = try attributesContainer.decode(String.self, forKey: .text_sale)
        self.percent_sale = try attributesContainer.decode(String?.self, forKey: .percent_sale) ?? ""
        self.lat = try attributesContainer.decode(Double.self, forKey: .lat)
        self.lng = try attributesContainer.decode(Double.self, forKey: .lng)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(district_id, forKey: .district_id)
        try container.encode(category_id, forKey: .category_id)

        var attributesContainer = container.nestedContainer(keyedBy: CodingKeys.self, forKey: .attributes)
        try attributesContainer.encode(street_one, forKey: .street_one)
        try attributesContainer.encode(street_two, forKey: .street_two)
        try attributesContainer.encode(city, forKey: .city)
        try attributesContainer.encode(post_code, forKey: .post_code)
        try attributesContainer.encode(email, forKey: .email)
        try attributesContainer.encode(phone_number, forKey: .phone_number)
        try attributesContainer.encode(web, forKey: .web)
        try attributesContainer.encode(description, forKey: .description)
        try attributesContainer.encode(text_sale, forKey: .text_sale)
        try attributesContainer.encode(percent_sale, forKey: .percent_sale)
        try attributesContainer.encode(lat, forKey: .lat)
        try attributesContainer.encode(lng, forKey: .lng)
    }
}

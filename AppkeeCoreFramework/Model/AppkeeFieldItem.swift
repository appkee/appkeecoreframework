//
//  FieldItem.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 27/12/2018.
//  Copyright © 2018 Radek Zmeskal. All rights reserved.
//

import Foundation

struct AppkeeFieldItem: Codable {
    let id: Int
    let fieldId: Int
    let name: String?
    let order: Int
}


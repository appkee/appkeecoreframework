//
//  FormResponse.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 04/02/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

public struct AppkeeFormResponse: Codable {
    let error: String?
    let success: Bool
}

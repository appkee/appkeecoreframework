//
//  AppkeePageForgotPassResponse.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/20/22.
//  Copyright © 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

public struct AppkeePageForgotPassResponse: Codable {
    public let error: String?
    public let success: Bool
    public let data: AppkeeMessageResponse?
}


//
//  AppkeeNotificationArea.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 12/29/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

struct AppkeeNotificationArea: Codable {
    let id: Int
    let regionId: Int
    let regionName: String
}

//
//  AppkeeAppDescription.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 27/12/2018.
//  Copyright © 2018 Radek Zmeskal. All rights reserved.
//

import Foundation

public struct AppkeeAppDescription: Codable {
    enum FavoriteType: String, Codable {
        case article
        case recipe
    }

    let name: String
    let language: String
    let menuType: Int
    let menuColumns: Int
    let headerColor: String
    let headerTextColor: String
    let menuColor: String
    let menuTextColor: String
    let contentColor: String
    let contentTextColor: String
    let logo: String?
    let onlyLogo: Bool
    let loadingImage: String?
    let searchUse: Bool
    let photoReporterUse: Bool
    let photoReporterEmail: String?
    let css: String?
    var sections: [AppkeeSection]
    let customIntroPage: String?
    
    let vouchersUse: Bool
    
    var advertisement: AppkeeAdvertisement?
    
    let bgImage: String?
    let menuHeaderImage: String?

    let adMobEnabled: Bool
    let iosAdUnitId: String?

    let loginPage: Bool
    let loginText: String?
    let loginColor: String?
    let registerColor: String?
    let loginMethods: [String]?
    let accountDeletionEnabled: Bool?

    let favoritesType: FavoriteType?
    let favoritesColor: String?

    let banners: [AppkeeAdvertisement]?

    let photoReportApp: Bool
    let photoReportAppEmails: [String]?

    let pinCode: String?

    let regionNotificationsEnabled: Bool
    let notificationRegions: [AppkeeNotificationArea]?

    let appUser: AppkeeUser?
}

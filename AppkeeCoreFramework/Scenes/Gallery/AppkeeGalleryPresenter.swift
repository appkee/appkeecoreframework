//
//  AppkeeGalleryPresenter.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 30/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeGalleryPresenter {
    // MARK: - Properties
    private let section: AppkeeSection
    private let images: [AppkeeContent]
    private let graphicManager: AppkeeGraphicManager
        
    let interactor: AppkeeGalleryInteractorInput
    weak var coordinator: AppkeeGalleryCoordinatorInput?
    weak var output: AppkeeGalleryPresenterOutput?

    // MARK: - Init
    init(interactor: AppkeeGalleryInteractorInput, coordinator: AppkeeGalleryCoordinatorInput, graphicManager: AppkeeGraphicManager, section: AppkeeSection) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.section = section
        self.graphicManager = graphicManager
        
        self.images = section.images?.sorted(by: { $0.order < $1.order }) ?? []
    }
}

// MARK: - User Events -

extension AppkeeGalleryPresenter: AppkeeGalleryPresenterInput {
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfItems(in section: Int) -> Int {
        return images.count
    }
        
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }
    }

    func handle(_ action: AppkeeGallery.Action) {
        switch action {
        case .navigate(let index):
            let image = images[index]
            if image.clickable {
                coordinator?.navigate(to: .detail(index: index, images: images.filter({ $0.clickable })))
            }
        }
    }
    
    func configure(_ item: AppkeeGalleryCollectionViewCellDelegate, at indexPath: IndexPath) {
        let image = section.images?[indexPath.row]

        let icon = graphicManager.getScaledImage(name: image?.content, size: 200)

        item.configure(with: icon, colorSettings: graphicManager.colorsSettings)
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeGalleryPresenter: AppkeeGalleryInteractorOutput {

}

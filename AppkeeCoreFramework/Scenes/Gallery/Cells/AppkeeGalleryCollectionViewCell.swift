//
//  AppkeeGalleryCollectionViewCell.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 30/09/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

protocol AppkeeGalleryCollectionViewCellDelegate: class {
    func configure(with icon:UIImage?, colorSettings: AppkeeColorSettings?)
}

class AppkeeGalleryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageImageView: UIImageView!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return layoutAttributes
    }
}

extension AppkeeGalleryCollectionViewCell: AppkeeGalleryCollectionViewCellDelegate {
 
    func configure(with icon:UIImage?, colorSettings: AppkeeColorSettings?) {
        if let colorSettings = colorSettings {
//            contentView.backgroundColor = colorSettings.contentTextColor
        }
        
        imageImageView.layer.masksToBounds = true
        
        imageImageView.image = icon
    }
}


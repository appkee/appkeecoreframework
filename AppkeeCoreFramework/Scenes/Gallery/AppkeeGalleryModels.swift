//
//  AppkeeGalleryModels.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 30/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeGallery {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case navigate(index: Int)
    }

    enum Route {
        case detail(index: Int, images: [AppkeeContent])
        case openAdvertisementLink(link: String)
        case openDeeplink(params: String)
    }
}

extension AppkeeGallery.Request {
    
}

extension AppkeeGallery.Response {

}

extension AppkeeGallery.DisplayData {
    
}

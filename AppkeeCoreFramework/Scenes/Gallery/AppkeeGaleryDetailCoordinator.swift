//
//  AppkeeGaleryDetailCoordinator.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 25/10/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit
import SKPhotoBrowser

class AppkeeGalleryDetaildCoordinator: AppkeeCoordinator {
        
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator]
    var dependencies: AppkeeFullDependencies
    var images: [SKPhoto] = []
//    weak var delegate: GalleryCoordinatorDelegate?
    
    var delegate: AppkeeSideMenuCoordinatorDelegate?

    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, images:[AppkeeContent]) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.childrens = []
        
        for image in images {
            if let imageContent = dependencies.graphicManager.getImage(name: image.content) {
                let photo = SKPhoto.photoWithImage(imageContent)
                self.images.append(photo)
            }
        }
    }
    
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, image: UIImage) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.childrens = []
        
        let photo = SKPhoto.photoWithImage(image)
        self.images = [photo]
    }
    
    func start(root: Bool = false) {
        if images.count == 0 {
            return
        }
        
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(0)
        navigationController.topViewController?.present(browser, animated: true, completion: {})
    }

    func start(index : Int) {
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(index)
        navigationController.topViewController?.present(browser, animated: true, completion: {})
    }
        
}

//// MARK: - Navigation Callbacks
//// PRESENTER -> COORDINATOR
//extension GalleryDetaildCoordinator: GalleryItemsDataSource {
//    func itemCount() -> Int {
//        return images.count
//    }
//
//    func provideGalleryItem(_ index: Int) -> GalleryItem {
//        let item = images[index]
//        let image = dependencies.graphicManager.getImage(name: item.content)
//        
//        let galleryItem = GalleryItem.image { $0(image) }
//                
//        return galleryItem
//    }
//}

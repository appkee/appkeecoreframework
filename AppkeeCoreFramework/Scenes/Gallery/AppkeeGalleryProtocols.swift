//
//  AppkeeGalleryProtocols.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 30/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol GalleryCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeGalleryCoordinatorInput: class {
 
    func navigate(to route: AppkeeGallery.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeGalleryInteractorInput {
    // func perform(_ request: Gallery.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeGalleryInteractorOutput: class {
    // func present(_ response: Gallery.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeGalleryPresenterInput {
    
    var numberOfSections: Int { get }
    func numberOfItems(in section: Int) -> Int
    
    func viewCreated()
    func handle(_ action: AppkeeGallery.Action)
    
    func configure(_ item: AppkeeGalleryCollectionViewCellDelegate, at indexPath: IndexPath)
}

// PRESENTER -> VIEW
protocol AppkeeGalleryPresenterOutput: class {
    // func display(_ displayModel: Gallery.DisplayData.Work)
    func setupUI(colorSettings: AppkeeColorSettings)
}

//
//  AppkeeGalleryViewController.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 30/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

let imagesInLine: CGFloat = 3.0

class AppkeeGalleryViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.register(AppkeeGalleryCollectionViewCell.nib, forCellWithReuseIdentifier: AppkeeGalleryCollectionViewCell.reuseId)
        }
    }

    // MARK: - Properties
    private var presenter: AppkeeGalleryPresenterInput!
    
    var width: CGFloat = 0.0

    // MARK: - Init
    class func instantiate(with presenter: AppkeeGalleryPresenterInput) -> AppkeeGalleryViewController {
        let name = "\(AppkeeGalleryViewController.self)"
        let bundle = Bundle(for: AppkeeGalleryViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeGalleryViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
        
        width = (self.collectionView.frame.width / imagesInLine) - 2
        
        self.collectionView?.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    

    // MARK: - Callbacks -

}

extension AppkeeGalleryViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return presenter.numberOfSections
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfItems(in: section)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AppkeeGalleryCollectionViewCell.reuseId, for: indexPath) as! AppkeeGalleryCollectionViewCell
        presenter.configure(cell, at: indexPath)
        return cell
    }
}

extension AppkeeGalleryViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.handle(.navigate(index: indexPath.row))
    }
}

extension AppkeeGalleryViewController: UICollectionViewDelegateFlowLayout {
     func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = width //* 3.0/4.0

        return CGSize(width: width, height: height)
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeGalleryViewController: AppkeeGalleryPresenterOutput {
    func setupUI(colorSettings: AppkeeColorSettings) {
        view.backgroundColor = colorSettings.content
    }    
}

//
//  SeniorPassIZSInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/2/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import FirebaseAnalytics
import Alamofire

class AppkeeSeniorPassIZSInteractor {
    // MARK: - Properties
    weak var output: AppkeeSeniorPassIZSInteractorOutput?
    private let repository: AppkeeRepository
    private let graphicManager: AppkeeGraphicManager

    // MARK: - Init
    init(repository: AppkeeRepository, graphicManager: AppkeeGraphicManager) {
        self.repository = repository
        self.graphicManager = graphicManager
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeSeniorPassIZSInteractor: AppkeeSeniorPassIZSInteractorInput {
    func perform(_ request: AppkeeSeniorPassIZS.Request.Search) {
        repository.seniorPassIZS(regionID: request.regionID) { [weak self] (response) in
            guard let self = self else { return }

            switch response.result {
            case .success(let response):
                if response.success, let data = response.data {
                    self.output?.present(AppkeeSeniorPassIZS.Response.Search(data: data))
                } else {
                    self.output?.present(AppkeeSeniorPassIZS.Response.Error(message: response.error ?? translations.error.UNKNOWN))
                }
                return
            case .failure(let error):
                self.output?.present(AppkeeSeniorPassIZS.Response.Error(message: error.localizedDescription))

                Analytics.logEvent("Loading IZS failed", parameters: ["regionID": request.regionID, "error": error.localizedDescription])
            }
        }
    }
}

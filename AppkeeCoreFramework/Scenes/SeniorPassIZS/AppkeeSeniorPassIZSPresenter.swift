//
//  SeniorPassIZSPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/2/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class AppkeeSeniorPassIZSPresenter: NSObject {
    // MARK: - Properties
    private let props: AppkeePageProps
    private let locationManager = CLLocationManager()
    private let graphicManager: AppkeeGraphicManager

    let interactor: AppkeeSeniorPassIZSInteractorInput
    weak var coordinator: AppkeeSeniorPassIZSCoordinatorInput?
    weak var output: AppkeeSeniorPassIZSPresenterOutput?

    private let contentIds: [Int]

    // MARK: - Init
    init(interactor: AppkeeSeniorPassIZSInteractorInput, coordinator: AppkeeSeniorPassIZSCoordinatorInput, graphicManager: AppkeeGraphicManager, props: AppkeePageProps) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.props = props
        self.graphicManager = graphicManager
        

        contentIds = (props.texts + props.images).sorted (by: { $0.order < $1.order }).map({$0.id })
    }
}

// MARK: - User Events -

extension AppkeeSeniorPassIZSPresenter: AppkeeSeniorPassIZSPresenterInput {
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }

        for id in contentIds {
            if let text = props.texts.first(where: {$0.id == id }) {
                addText(text: text.content)
                continue
            }

            if let image = props.images.first(where: {$0.id == id }) {
                addImage(name: image.content, id: id, clickable: image.clickable, link: image.link)
                continue
            }
        }

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.delegate = self

        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation

            self.updateLocalizationStatus()
        }
    }

    func handle(_ action: AppkeeSeniorPassIZS.Action) {
        switch action {
        case .search:
            output?.display(AppkeeSeniorPassIZS.DisplayData.Loading(isFinished: false))

            if let date = UserDefaults.seniorPassIZSDate, Calendar.current.isDate(date, inSameDayAs: Date()), let branches = UserDefaults.seniorPassIZSBranches {
                self.processData(data: branches.izs)
                return
            }

            let regionID = UserDefaults.seniorPassLogin?.region ?? 0

            self.interactor.perform(AppkeeSeniorPassIZS.Request.Search(regionID: regionID))
        }
    }

    private func addText(text: String?) {
        if let text = text {
            output?.addText(text, colorSettings: graphicManager.colorsSettings)
        }
    }

    private func addImage(name: String?, id: Int, clickable: Bool, link: String?) {
        if let image = graphicManager.getImage(name: name) {
            output?.addImage(image)
        }
    }

    private func updateLocalizationStatus() {
        switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                output?.display(AppkeeSeniorPassIZS.DisplayData.Location(text: translations.seniorPass.DISTANCE_DEFAULT_TEXT))
            case .authorizedAlways, .authorizedWhenInUse:
                output?.display(AppkeeSeniorPassIZS.DisplayData.Location(text: translations.seniorPass.DISTANCE_TEXT_FOUND))
            @unknown default:
            break
        }
    }
}

extension AppkeeSeniorPassIZSPresenter: CLLocationManagerDelegate {
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        self.updateLocalizationStatus()
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeSeniorPassIZSPresenter: AppkeeSeniorPassIZSInteractorOutput {
    func getLocation() -> (CLLocationCoordinate2D?) {

        guard let location = locationManager.location else {
            return nil
        }

        let locValue:CLLocationCoordinate2D = location.coordinate

        return (locValue)
    }

    private func processData(data: [AppkeeSeniorPassIZSBranch]) {
        var updatedBranches = data

        if let regionID = UserDefaults.seniorPassLogin?.region {
            updatedBranches = updatedBranches.filter { $0.regionId == regionID }
        }

        if let location = getLocation() {
            let coordinate = CLLocation(latitude: location.latitude, longitude: location.longitude)

            updatedBranches = updatedBranches.map{
                var branch = $0
//                if let lat = branch.lat, let lon = branch.location?.lon {
                    branch.distance = CLLocation(latitude: branch.lat, longitude: branch.lon).distance(from: coordinate)
//                }
                return branch
            }

            updatedBranches = updatedBranches.sorted { $0.distance ?? 0.0 < $1.distance ?? 0.0 }
        }

        output?.display(AppkeeSeniorPassIZS.DisplayData.Loading(isFinished: true))

        self.coordinator?.navigate(to: .showResults(data: updatedBranches))
    }

    func present(_ response: AppkeeSeniorPassIZS.Response.Search) {
        UserDefaults.seniorPassIZSBranches = response.data
        UserDefaults.seniorPassIZSDate = Date()

        self.processData(data: response.data.izs)
    }

    func present(_ response: AppkeeSeniorPassIZS.Response.Error) {
        output?.display(AppkeeSeniorPassIZS.DisplayData.Error(message: response.message))
    }
}

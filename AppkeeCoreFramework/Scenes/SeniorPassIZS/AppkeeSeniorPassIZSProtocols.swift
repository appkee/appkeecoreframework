//
//  SeniorPassIZSProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/2/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol SeniorPassIZSCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeSeniorPassIZSCoordinatorInput: class {
    func navigate(to route: AppkeeSeniorPassIZS.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeSeniorPassIZSInteractorInput {
    // func perform(_ request: SeniorPassIZS.Request.Work)
    func perform(_ request: AppkeeSeniorPassIZS.Request.Search)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeSeniorPassIZSInteractorOutput: class {
    // func present(_ response: SeniorPassIZS.Response.Work)

    func present(_ response: AppkeeSeniorPassIZS.Response.Search)
    func present(_ response: AppkeeSeniorPassIZS.Response.Error)

}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeSeniorPassIZSPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeeSeniorPassIZS.Action)
}

// PRESENTER -> VIEW
protocol AppkeeSeniorPassIZSPresenterOutput: class {
    func addText(_ text: String, colorSettings: AppkeeColorSettings?)
    func addImage(_ image: UIImage)

    func setupUI(colorSettings: AppkeeColorSettings)

    func display(_ displayModel: AppkeeSeniorPassIZS.DisplayData.Location)
    func display(_ displayModel: AppkeeSeniorPassIZS.DisplayData.Loading)
    func display(_ displayModel: AppkeeSeniorPassIZS.DisplayData.Error)
}

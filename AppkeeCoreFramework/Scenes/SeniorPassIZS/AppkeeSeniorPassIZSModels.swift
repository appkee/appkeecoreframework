//
//  SeniorPassIZSModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/2/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeSeniorPassIZS {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case search
    }

    enum Route {
        case openAdvertisementLink(link: String)
        case openDeeplink(params: String)
        case showResults(data: [AppkeeSeniorPassIZSBranch])
    }
}

extension AppkeeSeniorPassIZS.Request {
    struct Search {
        let regionID: Int
    }
}

extension AppkeeSeniorPassIZS.Response {
    struct Search {
        let data: AppkeeSeniorPassIZSList
    }

    struct Loading {
        let isFinished: Bool
    }

    struct Error {
        let message: String
    }
}

extension AppkeeSeniorPassIZS.DisplayData {
    struct Location {
        let text: String
    }
    
    struct Error {
        let message: String
    }

    struct Loading {
        let isFinished: Bool
    }
}

//
//  SeniorPassIZSViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/2/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeSeniorPassIZSViewController: UIViewController {
    // MARK: - Outlets

    @IBOutlet weak var stackView: UIStackView!

    @IBOutlet weak var locationLabel: UILabel! {
        didSet {
            locationLabel.text = translations.seniorPass.DISTANCE_DEFAULT_TEXT
        }
    }

    @IBOutlet weak var searchButton: UIButton! {
        didSet {
            searchButton.setTitle(translations.seniorPass.BRANCHES_SEARCH.uppercased(), for: .normal)
        }
    }


    // MARK: - Properties
    private var presenter: AppkeeSeniorPassIZSPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeSeniorPassIZSPresenterInput) -> AppkeeSeniorPassIZSViewController {
        let name = "\(AppkeeSeniorPassIZSViewController.self)"
        let bundle = Bundle(for: AppkeeSeniorPassIZSViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeSeniorPassIZSViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
    }

    // MARK: - Callbacks -
    @IBAction func searchButtonTap(_ sender: Any) {
        self.presenter.handle(.search)
    }

}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeSeniorPassIZSViewController: AppkeeSeniorPassIZSPresenterOutput, Alertable, Progressable {
    func addText(_ text: String, colorSettings: AppkeeColorSettings?) {
        let textView = AppkeeTextView(frame: CGRect(x: 0, y: 0, width: self.stackView.frame.width, height: 50))
        textView.configure(with: text, colorSettings: colorSettings)

        stackView.addArrangedSubview(textView)
    }

    func addImage(_ image: UIImage) {
        let imageView = AppkeeImageView(frame: .zero)
        imageView.configure(with: image)

        stackView.addArrangedSubview(imageView)
   }

    func setupUI(colorSettings: AppkeeColorSettings) {
        self.searchButton.setTitleColor(.white, for: .normal)
        self.searchButton.backgroundColor = colorSettings.loginColor
        
        self.view.backgroundColor = colorSettings.content
    }

    func display(_ displayModel: AppkeeSeniorPassIZS.DisplayData.Location) {
        self.locationLabel.text = displayModel.text
    }

    func display(_ displayModel: AppkeeSeniorPassIZS.DisplayData.Loading) {
        if displayModel.isFinished {
            self.hideProgress()
        } else {
            self.showProgress()
        }
    }

    func display(_ displayModel: AppkeeSeniorPassIZS.DisplayData.Error) {
        hideProgress()

        showErrorAlert(withMessage: displayModel.message)
    }
}

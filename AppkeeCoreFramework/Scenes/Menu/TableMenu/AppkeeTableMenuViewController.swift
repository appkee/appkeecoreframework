//
//  AppkeeTableMenuViewController.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 04/05/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeTableMenuViewController: UIViewController {
        
    // MARK: - Outlets
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.register(AppkeeTableMenuTableViewCell.nib, forCellReuseIdentifier: AppkeeTableMenuTableViewCell.reuseId)
            tableView.register(AppkeeTableMenuHeaderView.nib, forHeaderFooterViewReuseIdentifier: AppkeeTableMenuHeaderView.reuseId)
            tableView.sectionHeaderHeight = UITableView.automaticDimension
            tableView.tableHeaderView = UIView()
            tableView.tableFooterView = UIView()
            tableView.estimatedSectionHeaderHeight = 100
            tableView.estimatedRowHeight = 60
            tableView.separatorStyle = .none
        }
    }
    
    // MARK: - Properties
    private var presenter: AppkeeTableMenuPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeTableMenuPresenterInput) -> AppkeeTableMenuViewController {
        let name = "\(AppkeeTableMenuViewController.self)"
        let bundle = Bundle(for: AppkeeTableMenuViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeTableMenuViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
        
        self.title = ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }

    // MARK: - Callbacks -

}

extension AppkeeTableMenuViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.numberOfItems(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AppkeeTableMenuTableViewCell.reuseId, for: indexPath) as! AppkeeTableMenuTableViewCell
       presenter.configure(cell, at: indexPath)
       return cell
    }        
}

extension AppkeeTableMenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.handle(.navigate(indexPath.row))
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView( withIdentifier: AppkeeTableMenuHeaderView.reuseId) as! AppkeeTableMenuHeaderView
        presenter.configure(header)
        return header
    }
}


// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeTableMenuViewController: AppkeeTableMenuPresenterOutput {
    func setupUI(colorSettings: AppkeeColorSettings, image: UIImage?) {
        self.view.backgroundColor = colorSettings.content
        
        if let image = image {
            let imageView = UIImageView(image: image)
            imageView.contentMode = .scaleAspectFill
            self.tableView.backgroundView = imageView
        }
    }
}

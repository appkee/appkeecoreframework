//
//  AppkeeTableMenuProtocols.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 04/05/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol TableMenuCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeTableMenuCoordinatorInput: class {
    func navigate(to route: AppkeeTableMenu.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeTableMenuInteractorInput {
    // func perform(_ request: TableMenu.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeTableMenuInteractorOutput: class {
    // func present(_ response: TableMenu.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeTableMenuPresenterInput {
    var numberOfSections: Int { get }
    func numberOfItems(in section: Int) -> Int
    
    func viewCreated()
    func handle(_ action: AppkeeTableMenu.Action)
    
    func configure(_ item: AppkeeTableMenuTableViewCellDelegate, at indexPath: IndexPath)
    func configure(_ item: AppkeeTableMenuHeaderViewDelegate)
}

// PRESENTER -> VIEW
protocol AppkeeTableMenuPresenterOutput: class {
    // func display(_ displayModel: TableMenu.DisplayData.Work)
    func setupUI(colorSettings: AppkeeColorSettings, image: UIImage?)
}

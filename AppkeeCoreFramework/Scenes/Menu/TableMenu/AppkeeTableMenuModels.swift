//
//  AppkeeTableMenuModels.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 04/05/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeTableMenu {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case navigate(Int)
    }

    enum Route {
        case open(AppkeeSection)
        case camera(String)
        case search
    }
}

extension AppkeeTableMenu.Request {

}

extension AppkeeTableMenu.Response {

}

extension AppkeeTableMenu.DisplayData {
    
}

//
//  AppkeeTableMenuPresenter.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 04/05/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeTableMenuPresenter {
    // MARK: - Properties
    
    let interactor: AppkeeTableMenuInteractorInput
    private let appDescription: AppkeeAppDescription
    private let graphicManager: AppkeeGraphicManager
    weak var coordinator: AppkeeTableMenuCoordinatorInput?
    weak var output: AppkeeTableMenuPresenterOutput?

    // MARK: - Init
    init(appDesription: AppkeeAppDescription, graphicManager: AppkeeGraphicManager, interactor: AppkeeTableMenuInteractorInput, coordinator: AppkeeTableMenuCoordinatorInput) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.appDescription = appDesription
        self.graphicManager = graphicManager
    }
}

// MARK: - User Events -

extension AppkeeTableMenuPresenter: AppkeeTableMenuPresenterInput {
    
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfItems(in section: Int) -> Int {
       return appDescription.sections.count
    }
    
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings, image: graphicManager.getImage(name: appDescription.bgImage))
        }
    }

    func handle(_ action: AppkeeTableMenu.Action) {
        switch action {
        case .navigate(let index):
            let section =  appDescription.sections[index]
            
            if section.externalLink {
                // Show alert
                
            } else {
                coordinator?.navigate(to: .open(section))
            }
        }
    }
    
    func configure(_ item: AppkeeTableMenuTableViewCellDelegate, at indexPath: IndexPath) {
        let section = appDescription.sections[indexPath.row]
        
        let icon = graphicManager.getImage(name: section.icon)
        
        item.configure(with: icon, title: section.name, colorSettings: graphicManager.colorsSettings)
    }
    
    func configure(_ item: AppkeeTableMenuHeaderViewDelegate) {
        var handlerEmail: (() -> Void)?
        var handlerSearch: (() -> Void)?
        if let email = self.appDescription.photoReporterUse ? self.appDescription.photoReporterEmail : nil {
            handlerEmail = {
                self.coordinator?.navigate(to: .camera(email))
            }
        }
        
        if self.appDescription.searchUse {
            handlerSearch = {
                self.coordinator?.navigate(to: .search)
            }
        }
        
        item.configure(with: graphicManager.getImage(name: appDescription.menuHeaderImage),
                       colorSettings: graphicManager.colorsSettings,
                       handlerCamera: handlerEmail,
                       handlerSearch: handlerSearch)
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeTableMenuPresenter: AppkeeTableMenuInteractorOutput {

}


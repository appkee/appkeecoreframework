//
//  AppkeeTableMenuInteractor.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 04/05/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeTableMenuInteractor {
    // MARK: - Properties
    weak var output: AppkeeTableMenuInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeTableMenuInteractor: AppkeeTableMenuInteractorInput {
}

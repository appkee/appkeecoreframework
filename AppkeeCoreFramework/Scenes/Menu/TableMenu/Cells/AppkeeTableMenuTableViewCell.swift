//
//  AppkeeTableMenuTableViewCell.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 04/05/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

protocol AppkeeTableMenuTableViewCellDelegate: class {
    func configure(with icon:UIImage?, title: String, colorSettings: AppkeeColorSettings?)
}

class AppkeeTableMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var icoImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension AppkeeTableMenuTableViewCell: AppkeeTableMenuTableViewCellDelegate {
 
    func configure(with icon:UIImage?, title: String, colorSettings: AppkeeColorSettings?) {
        icoImageView.image = icon?.withRenderingMode(.alwaysTemplate)
        titleLabel.text = title
        
        if let colorSettings = colorSettings {
            titleLabel.textColor = colorSettings.menuText
            icoImageView.tintColor = colorSettings.menuText
        }
    }
}

//
//  AppkeeTableMenuHeaderView.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 5/17/20.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import UIKit


protocol AppkeeTableMenuHeaderViewDelegate: class {
    func configure(with icon:UIImage?, colorSettings: AppkeeColorSettings?)
    func configure(with icon:UIImage?, colorSettings: AppkeeColorSettings?, handlerCamera: (() -> Void)?, handlerSearch: (() -> Void)?)
}

class AppkeeTableMenuHeaderView: UITableViewHeaderFooterView {
    
//    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    
    var handlerCamera: () -> Void = { () in }
    var handlerSearch: () -> Void = { () in }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
             
    }
    
    @IBAction func cameraButtonTap(_ sender: Any) {
        self.handlerCamera()
    }
    
    @IBAction func searchButtonTap(_ sender: Any) {
        self.handlerSearch()
    }    
}

extension AppkeeTableMenuHeaderView: AppkeeTableMenuHeaderViewDelegate {
    
    func configure(with icon:UIImage?, colorSettings: AppkeeColorSettings?) {
            if let colorSettings = colorSettings {
                contentView.backgroundColor = .clear
            }
            
            iconImageView.image = icon
            cameraButton.isHidden = true
            searchButton.isHidden = true
        }
    
    func configure(with icon: UIImage?, colorSettings: AppkeeColorSettings?, handlerCamera: (() -> Void)?, handlerSearch: (() -> Void)?) {
        if let colorSettings = colorSettings {
//            nameLabel.textColor = colorSettings.menuText
            contentView.backgroundColor = .clear
//            containerView.backgroundColor = colorSettings.header
            cameraButton.tintColor = colorSettings.menuText
            searchButton.tintColor = colorSettings.menuText
        }
        
        cameraButton.isHidden = true
        searchButton.isHidden = true
        
        if let handlerCamera = handlerCamera {
            if let image = UIImage(named: "cameraMenu", in: Bundle(for: AppkeeTableMenuHeaderView.self), compatibleWith: nil) {
                cameraButton.setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
            }
            
            cameraButton.isHidden = false
            self.handlerCamera = handlerCamera
        }
        
        if let handlerSearch = handlerSearch {
            if let image = UIImage(named: "search", in: Bundle(for: AppkeeTableMenuHeaderView.self), compatibleWith: nil) {
                searchButton.setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
            }
            
            searchButton.isHidden = false
            self.handlerSearch = handlerSearch
        }                        
        
        iconImageView.image = icon
    }
}

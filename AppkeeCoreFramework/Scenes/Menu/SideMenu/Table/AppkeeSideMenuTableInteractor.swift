//
//  AppkeeSideMenuTableInteractor.swift
//  AppkeeCore
//
//  Created by Radek Zmeškal on 08/10/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeSideMenuTableInteractor {
    // MARK: - Properties
    weak var output: AppkeeSideMenuTableInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeSideMenuTableInteractor: AppkeeSideMenuTableInteractorInput {
}

//
//  AppkeeSideMenuTableProtocols.swift
//  AppkeeCore
//
//  Created by Radek Zmeškal on 08/10/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeSideMenuTableInteractorInput {
    // func perform(_ request: SideMenuTable.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeSideMenuTableInteractorOutput: class {
    // func present(_ response: SideMenuTable.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeSideMenuTablePresenterInput {
    var numberOfSections: Int { get }
    func numberOfItems(in section: Int) -> Int

    func currentIndex() -> Int
    
    func viewCreated()
    func handle(_ action: AppkeeSideMenuTable.Action)
    
    func configure(_ tableView: UITableView)
    func configure(_ item: AppkeeSideMenuTableViewCellDelegate, at indexPath: IndexPath)
    func configure(_ item: AppkeeSideMenuHeaderViewDelegate)
}

// PRESENTER -> VIEW
protocol AppkeeSideMenuTablePresenterOutput: class {
    // func display(_ displayModel: SideMenuTable.DisplayData.Work)
    func setupUI(colorSettings: AppkeeColorSettings)
    func selectIndex(indexPath: IndexPath)
}

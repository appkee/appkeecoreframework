//
//  AppkeeSideMenuTableViewController.swift
//  AppkeeCore
//
//  Created by Radek Zmeškal on 08/10/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeSideMenuTableViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.register(AppkeeSideMenuTableViewCell.nib, forCellReuseIdentifier: AppkeeSideMenuTableViewCell.reuseId)
            tableView.register(AppkeeSideMenuHeaderView.nib, forHeaderFooterViewReuseIdentifier: AppkeeSideMenuHeaderView.reuseId)
            tableView.estimatedRowHeight = 60
            tableView.separatorStyle = .singleLine
            tableView.layer.cornerRadius = 8.0
            tableView.estimatedSectionHeaderHeight = 60.0
            tableView.sectionHeaderHeight = UITableView.automaticDimension
            tableView.tableFooterView = UIView()
            presenter.configure(tableView)
        }
    }
    
    // MARK: - Properties
    private var presenter: AppkeeSideMenuTablePresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeSideMenuTablePresenterInput) -> AppkeeSideMenuTableViewController {
        let name = "\(AppkeeSideMenuTableViewController.self)"
        let bundle = Bundle(for: AppkeeSideMenuTableViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeSideMenuTableViewController
        vc.presenter = presenter
//        vc.transitioningDelegate = vc
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
        
        let blurEffect = UIBlurEffect(style: .regular)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        
//        let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
//        let vibrancyView = UIVisualEffectView(effect: vibrancyEffect)
//        vibrancyView.translatesAutoresizingMaskIntoConstraints = false
//
//        blurEffectView.contentView.addSubview(vibrancyView)
        
//        self.view.insertSubview(blurEffectView, at: 0)
        
        let tapGesture = UITapGestureRecognizer(target: self,
                         action: #selector(hideMenu))
        tapGesture.delegate = self
        view.addGestureRecognizer(tapGesture)
        
        if presenter.numberOfItems(in: 0) > 0 {
            let index = IndexPath(item: presenter.currentIndex(), section: 0)
            tableView.selectRow(at: index, animated: false, scrollPosition: .none)
        }
    }

    // MARK: - Callbacks -

    @objc func hideMenu() {
        presenter.handle(.hide)
    }
}

extension AppkeeSideMenuTableViewController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let point = gestureRecognizer.location(in: tableView)
        let indexPath = tableView.indexPathForRow(at: point)
        return indexPath == nil
    }
}

extension AppkeeSideMenuTableViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.numberOfItems(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: AppkeeSideMenuTableViewCell.reuseId, for: indexPath) as! AppkeeSideMenuTableViewCell
       presenter.configure(cell, at: indexPath)
       return cell
    }
}

extension AppkeeSideMenuTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView( withIdentifier: AppkeeSideMenuHeaderView.reuseId) as! AppkeeSideMenuHeaderView
        presenter.configure(header)
        return header
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 60
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.handle(.navigate(indexPath.row))
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeSideMenuTableViewController: AppkeeSideMenuTablePresenterOutput {
    func setupUI(colorSettings: AppkeeColorSettings) {
        tableView.backgroundColor = colorSettings.menu
        
//        self.view.backgroundColor = colorSettings.headerColor.withAlphaComponent(0.75)
    }
    
    func selectIndex(indexPath: IndexPath) {
        if let tableView = tableView {
            tableView.selectRow(at: indexPath, animated: true, scrollPosition: .top)
        }
    }
}

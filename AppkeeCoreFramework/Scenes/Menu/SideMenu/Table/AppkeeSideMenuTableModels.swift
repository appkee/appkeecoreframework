//
//  AppkeeSideMenuTableModels.swift
//  AppkeeCore
//
//  Created by Radek Zmeškal on 08/10/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeSideMenuTable {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case navigate(Int)
        case hide
        case focusSection(sectionId: Int, pinCode: String?)
        case focusIndex(index: Int, pinCode: String?)
    }

    enum Route {

    }
}

extension AppkeeSideMenuTable.Request {

}

extension AppkeeSideMenuTable.Response {

}

extension AppkeeSideMenuTable.DisplayData {
    
}

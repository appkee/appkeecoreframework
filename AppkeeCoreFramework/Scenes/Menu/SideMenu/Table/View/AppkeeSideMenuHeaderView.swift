//
//  AppkeeSideMenuHeaderView.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 23/10/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

protocol AppkeeSideMenuHeaderViewDelegate: class {
    func configure(with icon:UIImage?, title: String?, region: String?, colorSettings: AppkeeColorSettings?)
}

class AppkeeSideMenuHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var regionLabel: UILabel!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
             
    }
}

extension AppkeeSideMenuHeaderView: AppkeeSideMenuHeaderViewDelegate {

    func configure(with icon:UIImage?, title: String?, region: String?, colorSettings: AppkeeColorSettings?) {
        if let colorSettings = colorSettings {
            nameLabel.textColor = colorSettings.headerText
            regionLabel.textColor = colorSettings.headerText
            self.contentView.backgroundColor = colorSettings.header
            self.backgroundColor = .clear
            containerView.backgroundColor = colorSettings.header
        }
        
        iconImageView.image = icon
        nameLabel.text = title
        regionLabel.text = region
        
        if let title = title, !title.isEmpty {
            nameLabel.isHidden = false
        } else {
            nameLabel.isHidden = true
        }
        
    }
}

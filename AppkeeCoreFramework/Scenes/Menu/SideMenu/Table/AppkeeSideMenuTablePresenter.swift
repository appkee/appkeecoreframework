//
//  AppkeeSideMenuTablePresenter.swift
//  AppkeeCore
//
//  Created by Radek Zmeškal on 08/10/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeeSideMenuTablePresenter {
    // MARK: - Properties
    private let sections: [AppkeeSection]
    private let visibleSections: [AppkeeSection]
    private let graphicManager: AppkeeGraphicManager
    private var selectedIndex = 0
    private let title: String?
    private let image: UIImage?
    
    let interactor: AppkeeSideMenuTableInteractorInput
    weak var output: AppkeeSideMenuTablePresenterOutput?
    
    // Your custom coordinator
    weak var coordinator: AppkeeSideMenuCoordinatorInput?

    // MARK: - Init
    init(interactor: AppkeeSideMenuTableInteractorInput,
         coordinator: AppkeeSideMenuCoordinatorInput,
         graphicManager: AppkeeGraphicManager,
         sections: [AppkeeSection],
         title: String?,
         image: UIImage?) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.sections = sections
        self.graphicManager = graphicManager
        self.title = title
        self.image = image

        self.visibleSections = sections.filter { $0.visibility }
    }
}

// MARK: - User Events -

extension AppkeeSideMenuTablePresenter: AppkeeSideMenuTablePresenterInput {
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfItems(in section: Int) -> Int {
       return visibleSections.count
    }

    func currentIndex() -> Int {
        return self.selectedIndex
    }
    
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }
        
        let indexPath = IndexPath(row: selectedIndex, section: 0)
        output?.selectIndex(indexPath: indexPath)
    }

    func handle(_ action: AppkeeSideMenuTable.Action) {
        switch action {
        case .navigate(let index):
            let section = visibleSections[index]

            if let pinCode = section.pinCode {
                let pinCodeSection = UserDefaults.sectionPinCode(sectionID: section.id)
                if pinCode != pinCodeSection {
                    coordinator?.navigate(to: .pin(pinCode: pinCode, index: index))

                    let indexPath = IndexPath(row: selectedIndex, section: 0)
                    output?.selectIndex(indexPath: indexPath)
                    return
                }
            }

            if section.type == .custom, section.code == nil {
                let indexPath = IndexPath(row: selectedIndex, section: 0)
                output?.selectIndex(indexPath: indexPath)
                
                return
            }
            
            if let link = section.link, section.type == .link, section.externalLink {
                coordinator?.navigate(to: .openLink(link))
                
                let indexPath = IndexPath(row: selectedIndex, section: 0)
                output?.selectIndex(indexPath: indexPath)
                return
            }
            
            if let latitude = section.latitude, let longitude = section.longitude, section.type == .gps {
                coordinator?.navigate(to: .openGPS(latitude, longitude))
                
                let indexPath = IndexPath(row: selectedIndex, section: 0)
                output?.selectIndex(indexPath: indexPath)
                return
            }

            if !(section.dialog?.isEmpty ?? true) {
                coordinator?.navigate(to: .open(section))

                if UserDefaults.pageUser == nil {
                    let indexPath = IndexPath(row: selectedIndex, section: 0)
                    output?.selectIndex(indexPath: indexPath)
                }
                return
            }
            
            selectedIndex = index
            coordinator?.navigate(to: .open(section))
        case .hide:
            coordinator?.navigate(to: .hideMenu)
        case let .focusIndex(index, pinCode):
            let indexPath = IndexPath(row: index, section: 0)
            output?.selectIndex(indexPath: indexPath)

            let section = visibleSections[index]
            coordinator?.navigate(to: .open(section))

            selectedIndex = index

            if let pinCode = pinCode {
                UserDefaults.addSectionPinCode(sectionID: section.id, pinCode: pinCode)
            }
        case let .focusSection(sectionId, pinCode):
            if let index = self.visibleSections.firstIndex(where: { $0.id == sectionId }) {
                self.handle(.focusIndex(index: index, pinCode: pinCode))
            }
        }
    }
    
    func configure(_ item: AppkeeSideMenuTableViewCellDelegate, at indexPath: IndexPath) {
        let section = visibleSections[indexPath.row]
        
        let icon = graphicManager.getImage(name: section.icon, placeholder: false)

        var selectedColor: UIColor? = nil
        if let color = section.color {
            selectedColor = UIColor(color)
        }

        item.configure(with: icon, title: section.name, selectedColor: selectedColor, colorSettings: graphicManager.colorsSettings)
    }
    
    func configure(_ item: AppkeeSideMenuHeaderViewDelegate) {
        let region = UserDefaults.seniorPassLogin?.regionName ?? UserDefaults.familyPassLogin?.regionName

        item.configure(with: image, title: title, region: region, colorSettings: graphicManager.colorsSettings)
    }
    
    func configure(_ tableView: UITableView) {
        tableView.separatorColor = graphicManager.colorsSettings?.headerText
    }

    func setSelectedIndex(index: Int) {
        selectedIndex = index
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeSideMenuTablePresenter: AppkeeSideMenuTableInteractorOutput {

}

//
//  AppkeeSideMenuTableViewCell.swift
//  AppkeeCore
//
//  Created by Radek Zmeškal on 08/10/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

protocol AppkeeSideMenuTableViewCellDelegate: class {
    func configure(with icon:UIImage?, title: String, selectedColor: UIColor?, colorSettings: AppkeeColorSettings?)
}

class AppkeeSideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var icoImageView: UIImageView!
        
    @IBOutlet weak var titleLabel: UILabel!
    
    var normalColor: UIColor?
    var selectedColor: UIColor?
        
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            icoImageView.tintColor = selectedColor
        } else {
            icoImageView.tintColor = normalColor
        }
    }
}

extension AppkeeSideMenuTableViewCell: AppkeeSideMenuTableViewCellDelegate {
 
    func configure(with icon:UIImage?, title: String, selectedColor: UIColor?, colorSettings: AppkeeColorSettings?) {
        icoImageView.image = icon?.withRenderingMode(.alwaysTemplate)
        titleLabel.text = title
        
        if let colorSettings = colorSettings {
            if let selectedColor = selectedColor {
                self.normalColor = selectedColor
                self.selectedColor = selectedColor

                titleLabel.textColor = selectedColor
                titleLabel.highlightedTextColor = selectedColor
                icoImageView.tintColor = selectedColor

                let selectedBackgroundView = UIView()
                selectedBackgroundView.backgroundColor = colorSettings.menuText.withAlphaComponent(0.5)
                self.selectedBackgroundView = selectedBackgroundView

                return
            }

            self.normalColor = colorSettings.menuText
            self.selectedColor = colorSettings.menu
            
            titleLabel.textColor = colorSettings.menuText
            titleLabel.highlightedTextColor = colorSettings.menu
            icoImageView.tintColor = normalColor
            
            let selectedBackgroundView = UIView()
            selectedBackgroundView.backgroundColor = colorSettings.menuText.withAlphaComponent(0.5)
            self.selectedBackgroundView = selectedBackgroundView
        }
    }
}

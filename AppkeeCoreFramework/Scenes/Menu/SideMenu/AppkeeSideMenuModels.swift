//
//  SideMenuModels.swift
//  AppkeeCore
//
//  Created by Radek Zmeškal on 08/10/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeSideMenu {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {

    }

    enum Route {
        case open(AppkeeSection)
        case openLink(String)
        case openGPS(String, String)
        case pin(pinCode: String, index: Int)
        case hideMenu
    }
}

extension AppkeeSideMenu.Request {

}

extension AppkeeSideMenu.Response {

}

extension AppkeeSideMenu.DisplayData {
    
}

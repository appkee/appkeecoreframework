//
//  SideMenuCoordinator.swift
//  AppkeeCore
//
//  Created by Radek Zmeškal on 08/10/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

protocol AppkeeSideMenuCoordinatorDelegate {
    func openMenu()
    func openDebugMenu()
    func openPhotoReporter(email: String)
    func openLanguageSelection()
    func openNotificationSettings()
    func openAccountDelete()
    func openSearch()
    func openDeeplink(params: String)
    func openLink(link: String)
    func openGPS(lat: String, lon: String)
    func logout()
    func passLogout()
    func reload()
}

class AppkeeSideMenuCoordinator: AppkeeCoordinator {
    var dependencies: AppkeeFullDependencies
        
    // MARK: - Properties
    private let navigationController: UINavigationController
    private let appDescription: AppkeeAppDescription?
    private let email: String?
    private let search: Bool
    private lazy var slideInTransitioningDelegate = SlideInPresentationManager()
    
    private let window: UIWindow
        
    var childrens: [AppkeeCoordinator] = []
//    weak var delegate: TableMenuCoordinatorDelegate?
    var sideMenuController: AppkeeSideMenuTableViewController?
    var sideMenuPresenter: AppkeeSideMenuTablePresenter?
    
    var delegate: AppkeeAppCoordinatorDelegate?

    // MARK: - Init
    init(window: UIWindow, dependencies: AppkeeFullDependencies) {
        self.dependencies = dependencies
        self.window = window
        
        self.navigationController = UINavigationController()
        self.navigationController.view.backgroundColor = .white
        
        self.navigationController.navigationBar.isHidden = true
                        
        self.appDescription = dependencies.appDescriptionManager.readAppDescription()
        
        self.email = (self.appDescription?.photoReporterUse ?? false) ? self.appDescription?.photoReporterEmail : nil
        self.search = appDescription?.searchUse ?? false
    }

    func start(root: Bool = false) {
        if let colorsSettings = dependencies.graphicManager.colorsSettings {

            if #available(iOS 15, *) {
                let appearance = UINavigationBarAppearance()
                appearance.configureWithOpaqueBackground()
                appearance.backgroundColor = colorsSettings.header
                appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: colorsSettings.headerText]
                UINavigationBar.appearance().standardAppearance = appearance
                UINavigationBar.appearance().scrollEdgeAppearance = appearance
                UINavigationBar.appearance().tintColor = colorsSettings.headerText
            } else {
                self.navigationController.navigationBar.barTintColor = colorsSettings.header
                self.navigationController.navigationBar.tintColor = colorsSettings.headerText
                self.navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: colorsSettings.headerText]
            }

            self.navigationController.navigationBar.isHidden = false
        }
        
        window.rootViewController = navigationController
        
        guard let appDescription = appDescription else {
            return
        }

        var sections = appDescription.sections
        if let seniorPassLogin = UserDefaults.seniorPassLogin {
            sections = appDescription.sections.filter({ (section) -> Bool in
                if let regionId = section.regionId {
                    return regionId == seniorPassLogin.region
                }
                return true
            })
        }
        if let familyPassLogin = UserDefaults.familyPassLogin {
            sections = appDescription.sections.filter({ (section) -> Bool in
                if let regionId = section.regionId {
                    return regionId == Int(familyPassLogin.region)
                }
                return true
            })
        }

        if let _ = UserDefaults.pageUser {
            sections = sections.filter { !($0.onlyUnloggedIn ?? false) }
        } else {
            sections = sections.filter { !$0.onlyLoggedIn}
        }

        if UserDefaults.getLanguage() != nil
            || (appDescription.regionNotificationsEnabled && !(appDescription.notificationRegions?.isEmpty ?? true))
            || UserDefaults.pageUser != nil
        {
            let section = AppkeeSection(settings: translations.settings.TITLE)
            sections.append(section)
        }

        let tableInteractor = AppkeeSideMenuTableInteractor()
        let tablePresenter = AppkeeSideMenuTablePresenter(interactor: tableInteractor,
                                                    coordinator: self,
                                                    graphicManager: dependencies.graphicManager,
                                                    sections: sections,
                                                    title: appDescription.onlyLogo ? nil : appDescription.name,
                                                    image: dependencies.graphicManager.getImage(name: appDescription.logo))

        let tableVC = AppkeeSideMenuTableViewController.instantiate(with: tablePresenter)

        tableInteractor.output = tablePresenter
        tablePresenter.output = tableVC

        sideMenuController = tableVC
        sideMenuPresenter = tablePresenter

        for (index, section) in sections.enumerated()  {
           if section.pinCode == nil {
                tablePresenter.setSelectedIndex(index: index)
                self.navigate(to: .open(section))
                break
           }
       }

        if !dependencies.configManager.appDebug {
            self.testPushNotifications()
        }
    }
}
// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeSideMenuCoordinator: AppkeeSideMenuCoordinatorInput {    
    
    func navigate(to route: AppkeeSideMenu.Route) {
        switch route {
        case .open(let section):
            switch section.dialog {
            case "login-register":
                if let user = UserDefaults.pageUser {
                    if !user.active {
                        showLoginFailedDialog()
                        return
                    }
                } else {
                    showLoginDialog(section: section)
                    return
                }
            default:
                break
            }


            navigationController.dismiss(animated: true, completion: nil)
            switch section.type {
            case .page:
                showPage(section: section)
            case .articles:
                showArticles(section: section)
            case .gallery:
                showGallery(section: section)
            case .link:
                showLink(section: section)
            case .rss:
                showRss(section: section)
            case .gps:
                break
            case .form:
                showForm(section: section)
            case .voucher:
                showVouchers(section: section)
            case .photoReporter:
//                showPhotoReporter(section: section)
                break
            case .qr:
                showQRCodeReader(section: section)
            case .settings:
                showSettings(section: section, accountDeletionEnabled: appDescription?.accountDeletionEnabled)
            case .custom:
                switch section.code {
                case "seniorpas-card":
                    showSeniorPass(section: section)
                case "rodinnepasy-card":
                    showFamilyPass(section: section)
                case "seniorpas-branches":
                    showSeniorPassBranches(section: section)
                case "rodinnepasy-branches":
                    showFamillyPasBranches(section: section)
                case "seniorpas-izs":
                    showSeniorPassIZS(section: section)
                case "vipsikora-member":
                    showVipSikoraMember(section: section)
                default:
                    return
                }
                break;
            case .favorites:
                showFavorites(section: section)
            }
        case let .openLink(link):
            showOpenAlert(link: link)
        case let .openGPS(lat, lon):
            showGPS(latitude: lat, longtitude: lon)
        case let .pin(pinCode, index):
            showPinAlert(pinCode: pinCode) {
                self.sideMenuPresenter?.handle(.focusIndex(index: index, pinCode: pinCode))
            }
        case .hideMenu:
            navigationController.dismiss(animated: true, completion: nil)
        }
    }
}

extension AppkeeSideMenuCoordinator: AppkeeSideMenuCoordinatorDelegate {
    func reload() {
        self.delegate?.reload()
    }

    func openAccountDelete() {
        delegate?.accountDelete()
    }

    func passLogout() {
        self.showPassLogoutAlert()
    }
    
    func logout() {
        self.showLogoutAlert()
    }

    func openGPS(lat: String, lon: String) {
        self.navigate(to: .openGPS(lat, lon))
    }

    func openSearch() {
        self.showSearch()
    }
    
    func openLanguageSelection() {
        delegate?.languageSelection()
    }

    func openNotificationSettings() {
        delegate?.notificationSettings()
    }
    
    func openPhotoReporter(email: String) {
        self.showPhotoReporter(email: email)
    }
    
    func openDebugMenu() {
        let alert = UIAlertController(title: translations.debug.TITLE, message: nil, preferredStyle: .actionSheet)
        
        if dependencies.configManager.appMenu {
            alert.addAction(UIAlertAction(title: translations.debug.MENU, style: .default, handler:{ (UIAlertAction) in
                self.delegate?.close()
            }))
            
            alert.addAction(UIAlertAction(title: translations.debug.RELOAD, style: .default, handler:{ (UIAlertAction) in
                self.delegate?.reload()
            }))
        }
        
        alert.addAction(UIAlertAction(title: translations.common.CANCEL, style: .cancel, handler: nil))
        
        self.navigationController.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func openMenu() {
        if let sideMenu = sideMenuController {
//            sideMenu.modalPresentationStyle = .custom
//            sideMenu.modalTransitionStyle = .flipHorizontal
                        
            slideInTransitioningDelegate.direction = .left
            
            if let colorsSettings = dependencies.graphicManager.colorsSettings {
                slideInTransitioningDelegate.backgroundColor = colorsSettings.content.withAlphaComponent(0.75)
            }
            
            slideInTransitioningDelegate.disableCompactHeight = true
            sideMenu.transitioningDelegate = slideInTransitioningDelegate
            sideMenu.modalPresentationStyle = .custom
            
            navigationController.present(sideMenu, animated: true, completion: nil)
        }
    }
    
    private func testPushNotifications() {
        let current = UNUserNotificationCenter.current()

        current.getNotificationSettings(completionHandler: { (settings) in
            if settings.authorizationStatus == .denied {
                DispatchQueue.main.async {
                    self.showPushWarning()
                }
            }
        })
    }

    func openDeeplink(params: String) {
        let str = params.replacingOccurrences(of: "[", with: "").replacingOccurrences(of: "]", with: "")
        guard let url = URL(string: "test.cz?" + str) else {
            return
        }

        let section = url.valueOf(AppkeeConstants.deeplinkSection)
        let article = url.valueOf(AppkeeConstants.deeplinkArticle)

        if let section = section, let sectionID = Int(section) {
            let articleID: Int? = article != nil ? Int(article ?? "") : nil
            self.selectItem(sectionID: sectionID, articleID: articleID)
        }
    }

    func openLink(link: String) {
        self.showNotificationLink(link: link)
    }
}

extension AppkeeSideMenuCoordinator {
    private func selectItem(sectionID: Int,  articleID: Int?) {

        guard let sections = self.appDescription?.sections else {
            return
        }

        switch sectionID {
            case -20:
                showRegister()
                return
        default:
            break
        }

        for (index, section) in sections.enumerated()  {
            if section.id == sectionID {
                if !section.visibility, let articleID = articleID {
                    self.showArticle(section: section, articleID: articleID)
                    return
                }

                if let pinCode = section.pinCode {
                    let pinCodeSection = UserDefaults.sectionPinCode(sectionID: section.id)
                    if pinCode != pinCodeSection {
                        showPinAlert(pinCode: pinCode) { [weak self] in
                            UserDefaults.addSectionPinCode(sectionID: section.id, pinCode: pinCode)

                            self?.sideMenuPresenter?.handle(.focusSection(sectionId: sectionID, pinCode: pinCode))
                            if let articleID = articleID {
                                if let coordinator = self?.childrens.last as? AppkeeArticlesCoordinator {
                                    coordinator.navigate(to: .openArticle(articleID))
                                }
                            }
                        }
                        return
                    }
                }

                self.sideMenuPresenter?.handle(.focusSection(sectionId: sectionID, pinCode: nil))
                break;
            }
        }
        if let articleID = articleID {
            if let coordinator = childrens.last as? AppkeeArticlesCoordinator {
                coordinator.navigate(to: .openArticle(articleID))
            }
        }
    }

    func showLoginDialog(section: AppkeeSection) {
        self.navigationController.dismiss(animated: true) { [self] in
            let coordinator = AppkeeLoginDialogCoordinator(navigationController: navigationController, dependencies: self.dependencies, appDescription: appDescription, section: section)
            self.childrens.append(coordinator)
            coordinator.delegate = self
            coordinator.start()
        }
    }

    func showLoginFailedDialog() {
        navigationController.dismiss(animated: true, completion: {
            let alert = UIAlertController(title: translations.loginPage.ERROR_TITLE, message: translations.loginPage.NO_ACCESS, preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: translations.common.OK, style: .cancel, handler: nil))

            self.navigationController.present(alert, animated: true, completion: {
                print("completion block")
            })
        })
    }

    private func showPage(section: AppkeeSection) {
        let props = AppkeePageProps(articleId: section.id, title: section.name, texts: section.texts ?? [], images: section.images ?? [], videos: section.videos ?? [], favorites: false, favoritesType: nil, banners: section.banners)
        
        let coordinator = AppkeePageCoordinator(navigationController: navigationController,
                                          dependencies: dependencies,
                                          props: props,
                                          email: email,
                                          search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start(root: true)
    }

    private func showArticle(section: AppkeeSection, articleID: Int) {
        guard let article = section.articles?.first(where: { $0.id == articleID }) else {
            return
        }

        switch article.type {
        case .page:
            let texts = section.texts?.filter({ $0.articleId == article.id }) ?? []
            let images = section.images?.filter({ $0.articleId == article.id }) ?? []
            let videos = section.videos?.filter({ $0.articleId == article.id }) ?? []

            let props = AppkeePageProps(articleId: -1, title: article.name ?? "", texts: texts, images: images, videos: videos, favorites: false, favoritesType: nil, banners: nil)

            let coordinator = AppkeePageCoordinator(navigationController: navigationController,
                                              dependencies: dependencies,
                                              props: props,
                                              email: email,
                                              search: search)
            childrens.append(coordinator)
            coordinator.delegate = self
            coordinator.start(root: false)
        default:
            break
        }
    }
    
    private func showArticles(section: AppkeeSection) {

        let recipements: [String]? = self.appDescription?.photoReportApp ?? false ? self.appDescription?.photoReportAppEmails : nil

        let coordinator = AppkeeArticlesCoordinator(navigationController: navigationController,
                                          dependencies: dependencies,
                                          section: section,
                                          favoritesType: self.appDescription?.favoritesType,
                                          recipients: recipements,
                                          email: email,
                                          search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start(root: true)
    }
    
    private func showGallery(section: AppkeeSection) {
        let coordinator = AppkeeGalleryCoordinator(navigationController: navigationController,
                                          dependencies: dependencies,
                                          section: section,
                                          email: email,
                                          search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start(root: true)
    }
    
    private func showLink(section: AppkeeSection) {
        let link = (UserDefaults.isWebViewLogged ? section.loggedInLink ?? section.link : section.link) ?? ""
                
        let props = AppkeeLinkProps(title: section.name, link: link, offlineLink: section.offlineLink, banners: section.banners, css: section.css)
        let coordinator = AppkeeLinkCoordinator(navigationController: navigationController,
                                          dependencies: dependencies,
                                          props: props,
                                          email: email,
                                          search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start(root: true)
    }

    private func showNotificationLink(link: String) {
        var title = link
        if let url = URL(string: link), let host = url.host {
            title = host
        }

        let props = AppkeeLinkProps(title: title, link: link, offlineLink: nil, banners: [], css: nil)
        let coordinator = AppkeeLinkCoordinator(navigationController: navigationController,
                                          dependencies: dependencies,
                                          props: props,
                                          email: email,
                                          search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }
    
    private func showRss(section: AppkeeSection) {
        let coordinator = AppkeeRSSCoordinator(navigationController: navigationController,
                                          dependencies: dependencies,
                                          section: section,
                                          email: email,
                                          search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start(root: true)
    }
    
    private func showForm(section: AppkeeSection) {
        let coordinator = AppkeeFormCoordinator(navigationController: navigationController,
                                          dependencies: dependencies,
                                          section: section,
                                          email: email,
                                          search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start(root: true)
    }
    
    private func showOpenAlert(link: String) {
        navigationController.dismiss(animated: true, completion: {
            let alert = UIAlertController(title: translations.link.OPEN, message: nil, preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: translations.common.YES, style: .default , handler:{ (UIAlertAction) in
                if let url = URL(string: link) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }))
            
            //translate
            alert.addAction(UIAlertAction(title: translations.common.NO, style: .cancel, handler: nil))

            self.navigationController.present(alert, animated: true, completion: {
                print("completion block")
            })
        })
    }

    private func showLogoutAlert() {
        navigationController.dismiss(animated: true, completion: {
            let alert = UIAlertController(title: translations.logoutPage.ALERT_TITLE, message: nil, preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: translations.common.YES, style: .default , handler: { (UIAlertAction) in
                self.delegate?.logout()
            }))

            //translate
            alert.addAction(UIAlertAction(title: translations.common.NO, style: .cancel, handler: nil))

            self.navigationController.present(alert, animated: true, completion: {
                print("completion block")
            })
        })
    }

    private func showPassLogoutAlert() {
        navigationController.dismiss(animated: true, completion: {
            let alert = UIAlertController(title: translations.logoutPage.ALERT_TITLE, message: nil, preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: translations.common.YES, style: .default , handler: { (UIAlertAction) in
                self.delegate?.passLogout()
            }))

            //translate
            alert.addAction(UIAlertAction(title: translations.common.NO, style: .cancel, handler: nil))

            self.navigationController.present(alert, animated: true, completion: {
                print("completion block")
            })
        })
    }

    private func showPinAlert(pinCode: String, handler: @escaping () -> Void) {
        navigationController.dismiss(animated: true, completion: {
            let alert = UIAlertController(title: translations.pinSection.TITLE, message: nil, preferredStyle: .alert)

            alert.addTextField { (textField) in
                textField.keyboardType = .numberPad
                textField.isSecureTextEntry = true
                textField.textAlignment = .center
            }

            alert.addAction(UIAlertAction(title: translations.common.OK, style: .default, handler:{ (UIAlertAction) in
                alert.dismiss(animated: true) {
                    if let textField = alert.textFields?[0] {
                        if pinCode == textField.text?.hash() {
                            let alert2 = UIAlertController(title: translations.pinSection.SUCCESS_TITLE, message: nil, preferredStyle: .alert)
                            alert2.addAction(UIKit.UIAlertAction(title: translations.common.OK, style: .cancel, handler: {_ in 
                                handler()
                            }))

                            self.navigationController.present(alert2, animated: true, completion: nil)
                            return
                        }
                    }

                    let alert2 = UIAlertController(title: translations.pinSection.ERROR_TITLE, message: translations.pinSection.ERROR_MESSAGE, preferredStyle: .alert)
                    alert2.addAction(UIKit.UIAlertAction(title: translations.common.OK, style: .cancel, handler: nil))

                    self.navigationController.present(alert2, animated: true, completion: nil)
                }
            }))

            //translate
            alert.addAction(UIAlertAction(title: translations.common.CANCEL, style: .cancel, handler: nil))

            self.navigationController.present(alert, animated: true, completion: {
                print("completion block")
            })
        })
    }
    
    private func showGPS(latitude: String, longtitude: String) {
        let showGPS = {
            let alert = UIAlertController(title: translations.map.OPEN, message: nil, preferredStyle: .actionSheet)

            if let url = URL(string: "http://maps.apple.com/"), UIApplication.shared.canOpenURL(url) {
                alert.addAction(UIAlertAction(title: translations.map.APPLE, style: .default , handler:{ (UIAlertAction) in
                    let link = String(format: "http://maps.apple.com?daddr=%.7f,%.7f&saddr=&dirflg=d", arguments: [Double(latitude) ?? 0.0, Double(longtitude) ?? 0.0])
                    if let url = URL(string: link) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }))
            }

            if let url = URL(string: "comgooglemaps://"), UIApplication.shared.canOpenURL(url) {
                alert.addAction(UIAlertAction(title: translations.map.GOOGLE, style: .default , handler:{ (UIAlertAction) in
                    let link = String(format: "comgooglemaps://?saddr=&daddr=%.7f,%.7f&directionsmode=driving", arguments: [Double(latitude) ?? 0.0, Double(longtitude) ?? 0.0])
                    if let url = URL(string: link) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }))
            }

            alert.addAction(UIAlertAction(title: translations.common.CANCEL, style: .cancel, handler: nil))

            self.navigationController.present(alert, animated: true, completion: {
                print("completion block")
            })
        }

        if navigationController.presentedViewController?.isKind(of: AppkeeSideMenuTableViewController.self) ?? false {
            navigationController.dismiss(animated: true, completion: {
                showGPS()
            })
        } else {
            showGPS()
        }

    }
    
    private func showVouchers(section: AppkeeSection) {
        let coordinator = AppkeeVouchersCoordinator(navigationController: navigationController,
                                          dependencies: dependencies,
                                          section: section,
                                          email: email,
                                          search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start(root: true)
    }
    
    private func showPhotoReporter(email: String) {
        let coordinator = AppkeePhotoReportCoordinator(navigationController: navigationController,
                                                       dependencies: dependencies,
                                                       email: email)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start(root: true)
    }
    
    private func showQRCodeReader(section: AppkeeSection) {
        let coordinator = AppkeeQRCodeReaderCoordinator(navigationController: navigationController,
                                                       dependencies: dependencies,
                                                       section: section,
                                                       email: email,
                                                       search: search)
        
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start(root: true)
    }
    
    private func showSettings(section: AppkeeSection, accountDeletionEnabled: Bool?) {
        let coordinator = AppkeeSettingsCoordinator(navigationController: navigationController,
                                                       dependencies: dependencies,
                                                       section: section,
                                                       email: email,
                                                    accountDeletionEnabled: accountDeletionEnabled)
        
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start(root: true)
    }
    
    private func showPushWarning() {
        let coordinator = AppkeePushWarningCoordinator(navigationController: navigationController,
                                                       dependencies: dependencies)
        
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }
    
    private func showSearch() {
        let coordinator = AppkeeSearchCoordinator(navigationController: navigationController,
                                                  dependencies: dependencies,
                                                  email: email,
                                                  sections: appDescription?.sections ?? [])
        childrens.append(coordinator)
//        coordinator.delegate = self
        coordinator.start(root: true)
    }

    private func showFavorites(section: AppkeeSection) {
        let coordinator = AppkeeFavoritesCoordinator(navigationController: navigationController,
                                                     dependencies: dependencies,
                                                     section: section,
                                                     sections: appDescription?.sections ?? [],
                                                     favoritesType: appDescription?.favoritesType ?? .article,
                                                     email: email)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start(root: true)
    }

    private func showSeniorPass(section: AppkeeSection) {
        let coordinator = AppkeeSeniorPassCardCoordinator(navigationController: navigationController,
                                                          dependencies: dependencies,
                                                          section: section,
                                                          email: email,
                                                          search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start(root: true)
    }

    private func showFamilyPass(section: AppkeeSection) {
        let coordinator = AppkeeFamilyPassCardCoordinator(navigationController: navigationController,
                                                          dependencies: dependencies,
                                                          section: section,
                                                          email: email,
                                                          search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start(root: true)
    }

    private func showSeniorPassBranches(section: AppkeeSection) {
        let coordinator = AppkeePassBenefitsCoordinator(navigationController: navigationController,
                                                              dependencies: dependencies,
                                                              section: section,
                                                              passType: .senior,
                                                              email: email,
                                                              search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start(root: true)
    }

    private func showFamillyPasBranches(section: AppkeeSection) {
        let coordinator = AppkeePassBenefitsCoordinator(navigationController: navigationController,
                                                              dependencies: dependencies,
                                                              section: section,
                                                              passType: .family,
                                                              email: email,
                                                              search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start(root: true)
    }

    private func showSeniorPassIZS(section: AppkeeSection) {
        let coordinator = AppkeeSeniorPassIZSCoordinator(navigationController: navigationController,
                                                              dependencies: dependencies,
                                                              section: section,
                                                              email: email,
                                                              search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start(root: true)
    }

    private func showVipSikoraMember(section: AppkeeSection) {
        let coordinator = AppkeeVipSikoraMemberCoordinator(navigationController: navigationController,
                                                              dependencies: dependencies,
                                                              section: section,
                                                              email: email,
                                                              search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start(root: true)
    }

    func showRegister() {
        let coordinator = AppkeeLoginRegisterRegisterCoordinator(navigationController: navigationController,
                                                                 dependencies: dependencies,
                                                                 style: .standart)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start(root: false)
    }

}

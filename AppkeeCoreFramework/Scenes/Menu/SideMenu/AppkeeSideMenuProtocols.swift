//
//  AppkeeSideMenuProtocols.swift
//  AppkeeCore
//
//  Created by Radek Zmeškal on 08/10/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol SideMenuCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeSideMenuCoordinatorInput: class {
    func navigate(to route: AppkeeSideMenu.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeSideMenuInteractorInput {
    // func perform(_ request: SideMenu.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeSideMenuInteractorOutput: class {
    // func present(_ response: SideMenu.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeSideMenuPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeeSideMenu.Action)
}

// PRESENTER -> VIEW
protocol AppkeeSideMenuPresenterOutput: class {
    // func display(_ displayModel: SideMenu.DisplayData.Work)
}

//
//  AppkeeCoolectionMenuPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/10/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeCollectionMenuPresenter {
    // MARK: - Properties
    let interactor: AppkeeCollectionMenuInteractorInput
    private let appDescription: AppkeeAppDescription
    private let graphicManager: AppkeeGraphicManager
    private let sections: [AppkeeSection]
    private let visibleSections: [AppkeeSection]
    weak var coordinator: AppkeeCollectionMenuCoordinatorInput?
    weak var output: AppkeeCoolectionMenuPresenterOutput?

    // MARK: - Init
    init(appDesription: AppkeeAppDescription, graphicManager: AppkeeGraphicManager, interactor: AppkeeCollectionMenuInteractorInput, coordinator: AppkeeCollectionMenuCoordinatorInput) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.appDescription = appDesription
        self.graphicManager = graphicManager
        self.sections = appDesription.sections

        var visibleSections: [AppkeeSection] = []
        if let _ = UserDefaults.pageUser {
            visibleSections = sections.filter { $0.visibility }.filter { !($0.onlyUnloggedIn ?? false) }
        } else {
            visibleSections = sections.filter { $0.visibility }.filter { !$0.onlyLoggedIn }
        }

        let count = (appDesription.menuColumns * appDesription.menuColumns)
        if visibleSections.count > count {
            visibleSections = Array(visibleSections[...count])
        }

        self.visibleSections = visibleSections
    }
}

// MARK: - User Events -

extension AppkeeCollectionMenuPresenter: AppkeeCollectionMenuPresenterInput {
    func configure(_ item: AppkeeCollectionMenuCellDelegate, at indexPath: IndexPath) {
        let section = self.visibleSections[indexPath.row]

        let icon = graphicManager.getImage(name: section.icon)

        item.configure(with: icon, title: section.name, colorSettings: graphicManager.colorsSettings)
    }

    var numberOfSections: Int {
        return 1
    }

    var menuColumns: Int {
        return appDescription.menuColumns
    }

    func numberOfItems(in section: Int) -> Int {
       return visibleSections.count
    }

    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings, image: graphicManager.getImage(name: appDescription.logo))
        }
    }

    func handle(_ action: AppkeeCollectionMenu.Action) {
        switch action {
        case .navigate(let index):
            let section =  self.visibleSections[index]

            if section.externalLink {
                // Show alert

            } else {
                coordinator?.navigate(to: .open(section))
            }
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeCollectionMenuPresenter: AppkeeCollectionMenuInteractorOutput {

}

//
//  AppkeeCoolectionMenuModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/10/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeCollectionMenu {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case navigate(Int)
    }

    enum Route {
        case open(AppkeeSection)
        case camera(String)
        case search
    }
}

extension AppkeeCollectionMenu.Request {

}

extension AppkeeCollectionMenu.Response {

}

extension AppkeeCollectionMenu.DisplayData {
    
}

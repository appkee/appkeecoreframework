//
//  AppkeeCoolectionMenuInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/10/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeCollectionMenuInteractor {
    // MARK: - Properties
    weak var output: AppkeeCollectionMenuInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeCollectionMenuInteractor: AppkeeCollectionMenuInteractorInput {
}

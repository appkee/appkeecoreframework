//
//  AppkeeCoolectionMenuViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/10/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeCollectionMenuViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!

    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.register(AppkeeCollectionMenuCell.nib, forCellWithReuseIdentifier: AppkeeCollectionMenuCell.reuseId)

//            let flowLayout = UICollectionViewFlowLayout()
//            flowLayout.scrollDirection = .horizontal
//            flowLayout.minimumLineSpacing = 0
//            flowLayout.minimumInteritemSpacing = 0
//            flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//            collectionView.collectionViewLayout = flowLayout
        }
    }

    // MARK: - Properties
    private var presenter: AppkeeCollectionMenuPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeCollectionMenuPresenterInput) -> AppkeeCollectionMenuViewController {
        let name = "\(AppkeeCollectionMenuViewController.self)"
        let bundle = Bundle(for: AppkeeCollectionMenuViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeCollectionMenuViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }

    // MARK: - Callbacks -
}

extension AppkeeCollectionMenuViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfItems(in: section)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AppkeeCollectionMenuCell.reuseId, for: indexPath) as! AppkeeCollectionMenuCell
        presenter.configure(cell, at: indexPath)
        return cell
    }

}

extension AppkeeCollectionMenuViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.size.width - (CGFloat(presenter.menuColumns + 1)*10))/CGFloat(presenter.menuColumns) - 1
        let height = (collectionView.frame.size.height - (CGFloat(presenter.menuColumns + 1)*10))/CGFloat(presenter.menuColumns) - 1
            return CGSize(width: width, height: height)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.handle(.navigate(indexPath.row))
    }
}

extension AppkeeCollectionMenuViewController {

}



// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeCollectionMenuViewController: AppkeeCoolectionMenuPresenterOutput {
    func setupUI(colorSettings: AppkeeColorSettings, image: UIImage?) {
        self.view.backgroundColor = colorSettings.content

        self.logoImageView.image = image
    }
}

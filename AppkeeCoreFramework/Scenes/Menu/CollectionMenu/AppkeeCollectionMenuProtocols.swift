//
//  AppkeeCoolectionMenuProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/10/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeeCoolectionMenuCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeCollectionMenuCoordinatorInput: class {
    func navigate(to route: AppkeeCollectionMenu.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeCollectionMenuInteractorInput {
    // func perform(_ request: AppkeeCoolectionMenu.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeCollectionMenuInteractorOutput: class {
    // func present(_ response: AppkeeCoolectionMenu.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeCollectionMenuPresenterInput {
    var numberOfSections: Int { get }
    func numberOfItems(in section: Int) -> Int
    var menuColumns: Int { get }

    func viewCreated()
    func handle(_ action: AppkeeCollectionMenu.Action)

    func configure(_ item: AppkeeCollectionMenuCellDelegate, at indexPath: IndexPath)
}

// PRESENTER -> VIEW
protocol AppkeeCoolectionMenuPresenterOutput: class {
    // func display(_ displayModel: AppkeeCoolectionMenu.DisplayData.Work)
    func setupUI(colorSettings: AppkeeColorSettings, image: UIImage?)
}

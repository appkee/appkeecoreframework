//
//  AppkeeCoolectionMenuCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/10/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeeCollectionMenuCoordinator: AppkeeCoordinator {
    // MARK: - Properties
    var dependencies: AppkeeFullDependencies

    // MARK: - Properties
    private let navigationController: UINavigationController
    private let appDescription: AppkeeAppDescription?
    private let email: String?
    private let search: Bool

    private let window: UIWindow

    var childrens: [AppkeeCoordinator] = []
//    weak var delegate: TableMenuCoordinatorDelegate?

    var delegate: AppkeeAppCoordinatorDelegate?

    // MARK: - Init
    init(window: UIWindow, dependencies: AppkeeFullDependencies) {
        self.dependencies = dependencies
        self.window = window

        self.navigationController = UINavigationController()
        self.navigationController.navigationBar.isHidden = true

        self.appDescription = dependencies.appDescriptionManager.readAppDescription()

        self.email = (self.appDescription?.photoReporterUse ?? false) ? self.appDescription?.photoReporterEmail : nil
        self.search = appDescription?.searchUse ?? false
    }

    func start(root: Bool = false) {

        if let colorsSettings = dependencies.graphicManager.colorsSettings {
            self.navigationController.navigationBar.barTintColor = colorsSettings.header
            self.navigationController.navigationBar.tintColor = colorsSettings.headerText
            self.navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: colorsSettings.headerText]
        }

        guard let appDesription = dependencies.appDescriptionManager.readAppDescription() else {
            return
        }

        let interactor = AppkeeCollectionMenuInteractor()
        let presenter = AppkeeCollectionMenuPresenter(appDesription: appDesription,
                                           graphicManager: dependencies.graphicManager,
                                           interactor: interactor,
                                           coordinator: self)
        let vc = AppkeeCollectionMenuViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        navigationController.setViewControllers([vc], animated: false)

        window.rootViewController = navigationController
    }
}
// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeCollectionMenuCoordinator: AppkeeCollectionMenuCoordinatorInput {
    func navigate(to route: AppkeeCollectionMenu.Route) {
        switch route {
        case .open(let section):
            switch section.type {
            case .page:
                showPage(section: section)
            case .articles:
                showArticles(section: section)
            case .gallery:
                showGallery(section: section)
            case .link:
                showLink(section: section)
            case .rss:
                showRss(section: section)
            case .gps:
                break
            case .form:
                showForm(section: section)
            case .voucher:
                showVouchers(section: section)
            case .photoReporter:
                break
            case .qr:
                showQRCodeReader(section: section)
            case .settings:
                showSettings(section: section, accountDeletionEnabled: appDescription?.accountDeletionEnabled)
            case .custom:
                break
            case .favorites:
                showFavorites(section: section)
            }
        case .camera(let email):
            showPhotoReporter(email: email)
        case .search:
            openSearch()
        }
    }
}

extension AppkeeCollectionMenuCoordinator {
    private func selectItem(sectionID: Int,  articleID: Int?) {
        guard let sections = self.appDescription?.sections else {
            return
        }

        for (index, section) in sections.enumerated()  {
            if section.id == sectionID {
                if !section.visibility, let articleID = articleID {
                    DispatchQueue.main.async {
                        self.showArticle(section: section, articleID: articleID)
                    }
                    return
                }

                if let pinCode = section.pinCode {
                    let pinCodeSection = UserDefaults.sectionPinCode(sectionID: section.id)
                    if pinCode != pinCodeSection {
                        showPinAlert(pinCode: pinCode) { [weak self] in
                            UserDefaults.addSectionPinCode(sectionID: section.id, pinCode: pinCode)

                            DispatchQueue.main.async {
                                self?.navigate(to: .open(section))
                                if let articleID = articleID {
                                    if let coordinator = self?.childrens.last as? AppkeeArticlesCoordinator {
                                        coordinator.navigate(to: .openArticle(articleID))
                                    }
                                }
                            }
                        }
                        return
                    }
                }

                DispatchQueue.main.async {
                    self.navigate(to: .open(section))
                }
                break;
            }
        }
        if let articleID = articleID {
            if let coordinator = childrens.last as? AppkeeArticlesCoordinator {
                coordinator.navigate(to: .openArticle(articleID))
            }
        }

//        if let section = self.appDescription?.sections.first(where: { $0.id == sectionID}) {
//          self.navigate(to: .open(section))
//        }
//
//        if let articleID = articleID {
//            if let coordinator = childrens.last as? AppkeeArticlesCoordinator {
//                coordinator.navigate(to: .openArticle(articleID))
//            }
//        }
    }

    private func showPage(section: AppkeeSection) {
//        let props = AppkeePageProps(articleId: section.id, title: section.name, texts: section.texts ?? [], images: section.images ?? [], videos: section.videos ?? [], favorites: false, favoritesType: nil, advertisement: section.advertisement)

        let props = AppkeePageProps(articleId: section.id, title: section.name, texts: section.texts ?? [], images: section.images ?? [], videos: section.videos ?? [], favorites: false, favoritesType: nil, banners: nil)

        let coordinator = AppkeePageCoordinator(navigationController: navigationController,
                                          dependencies: dependencies,
                                          props: props,
                                          email: email,
                                          search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }

    private func showArticle(section: AppkeeSection, articleID: Int) {
        guard let article = section.articles?.first(where: { $0.id == articleID }) else {
            return
        }

        switch article.type {
        case .page:
            let texts = section.texts?.filter({ $0.articleId == article.id }) ?? []
            let images = section.images?.filter({ $0.articleId == article.id }) ?? []
            let videos = section.videos?.filter({ $0.articleId == article.id }) ?? []

            let props = AppkeePageProps(articleId: -1, title: article.name ?? "", texts: texts, images: images, videos: videos, favorites: false, favoritesType: nil, banners: nil)

            let coordinator = AppkeePageCoordinator(navigationController: navigationController,
                                              dependencies: dependencies,
                                              props: props,
                                              email: email,
                                              search: search)
            childrens.append(coordinator)
            coordinator.delegate = self
            coordinator.start()
        default:
            break
        }
    }

    private func showArticles(section: AppkeeSection) {
        let recipements: [String]? = self.appDescription?.photoReportApp ?? false ? self.appDescription?.photoReportAppEmails : nil

        let coordinator = AppkeeArticlesCoordinator(navigationController: navigationController,
                                          dependencies: dependencies,
                                          section: section,
                                          favoritesType: self.appDescription?.favoritesType,
                                          recipients: recipements,
                                          email: email,
                                          search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }

    private func showGallery(section: AppkeeSection) {
        let coordinator = AppkeeGalleryCoordinator(navigationController: navigationController,
                                          dependencies: dependencies,
                                          section: section,
                                          email: email, search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }

    private func showLink(section: AppkeeSection) {
        let props = AppkeeLinkProps(title: section.name, link: section.link ?? "", offlineLink: section.offlineLink, banners: section.banners, css: section.css)
        let coordinator = AppkeeLinkCoordinator(navigationController: navigationController,
                                          dependencies: dependencies,
                                          props: props,
                                          email: email,
                                          search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }

    private func showNotificationLink(link: String) {
        var title = link
        if let url = URL(string: link), let host = url.host {
            title = host
        }

        let props = AppkeeLinkProps(title: title, link: link, offlineLink: nil, banners: [], css: nil)
        let coordinator = AppkeeLinkCoordinator(navigationController: navigationController,
                                          dependencies: dependencies,
                                          props: props,
                                          email: email,
                                          search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }

    private func showRss(section: AppkeeSection) {
        let coordinator = AppkeeRSSCoordinator(navigationController: navigationController,
                                          dependencies: dependencies,
                                          section: section,
                                          email: email,
                                          search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }

    private func showForm(section: AppkeeSection) {
        let coordinator = AppkeeFormCoordinator(navigationController: navigationController,
                                          dependencies: dependencies,
                                          section: section,
                                          email: email,
                                          search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }

    private func showVouchers(section: AppkeeSection) {
        let coordinator = AppkeeVouchersCoordinator(navigationController: navigationController,
                                          dependencies: dependencies,
                                          section: section,
                                          email: email,
                                          search: search)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }

    private func showQRCodeReader(section: AppkeeSection) {
        let coordinator = AppkeeQRCodeReaderCoordinator(navigationController: navigationController,
                                                       dependencies: dependencies,
                                                       section: section,
                                                       email: email,
                                                       search: search)

        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }

    private func showSettings(section: AppkeeSection, accountDeletionEnabled: Bool?) {
        let coordinator = AppkeeSettingsCoordinator(navigationController: navigationController,
                                                       dependencies: dependencies,
                                                       section: section,
                                                       email: email,
                                                    accountDeletionEnabled: accountDeletionEnabled)

        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }

    private func showPhotoReporter(email: String) {
        let coordinator = AppkeePhotoReportCoordinator(navigationController: navigationController,
                                                       dependencies: dependencies,
                                                       email: email)
        childrens.append(coordinator)
        coordinator.start(root: true)
    }

    private func showPushWarning() {
        let coordinator = AppkeePushWarningCoordinator(navigationController: navigationController,
                                                       dependencies: dependencies)

        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }

    private func showSearch() {
        let coordinator = AppkeeSearchCoordinator(navigationController: navigationController,
                                                  dependencies: dependencies,
                                                  email: email,
                                                  sections: appDescription?.sections ?? [])
        childrens.append(coordinator)
//        coordinator.delegate = self
        coordinator.start()
    }

    private func showFavorites(section: AppkeeSection) {
        let coordinator = AppkeeFavoritesCoordinator(navigationController: navigationController,
                                          dependencies: dependencies,
                                          section: section,
                                          sections: appDescription?.sections ?? [],
                                          favoritesType: appDescription?.favoritesType ?? .article,
                                          email: email)
        childrens.append(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }

    private func showPinAlert(pinCode: String, handler: @escaping () -> Void) {
        navigationController.dismiss(animated: true, completion: {
            let alert = UIAlertController(title: translations.pinSection.TITLE, message: nil, preferredStyle: .alert)

            alert.addTextField { (textField) in
                textField.keyboardType = .numberPad
                textField.isSecureTextEntry = true
                textField.textAlignment = .center
            }

            alert.addAction(UIAlertAction(title: translations.common.OK, style: .default, handler:{ (UIAlertAction) in
                alert.dismiss(animated: true) {
                    if let textField = alert.textFields?[0] {
                        if pinCode == textField.text?.hash() {
                            let alert2 = UIAlertController(title: translations.pinSection.SUCCESS_TITLE, message: nil, preferredStyle: .alert)
                            alert2.addAction(UIKit.UIAlertAction(title: translations.common.OK, style: .cancel, handler: {_ in
                                handler()
                            }))

                            self.navigationController.present(alert2, animated: true, completion: nil)
                            return
                        }
                    }

                    let alert2 = UIAlertController(title: translations.pinSection.ERROR_TITLE, message: translations.pinSection.ERROR_MESSAGE, preferredStyle: .alert)
                    alert2.addAction(UIKit.UIAlertAction(title: translations.common.OK, style: .cancel, handler: nil))

                    self.navigationController.present(alert2, animated: true, completion: nil)
                }
            }))

            //translate
            alert.addAction(UIAlertAction(title: translations.common.CANCEL, style: .cancel, handler: nil))

            self.navigationController.present(alert, animated: true, completion: {
                print("completion block")
            })
        })
    }
}

extension AppkeeCollectionMenuCoordinator: AppkeeSideMenuCoordinatorDelegate {
    func reload() {
    
    }

    func openAccountDelete() {

    }
    
    func passLogout() {

    }

    func openNotificationSettings() {

    }

    func logout() {

    }

    func openGPS(lat: String, lon: String) {

    }

    func openDeeplink(params: String) {
        let str = params.replacingOccurrences(of: "[", with: "").replacingOccurrences(of: "]", with: "")
        guard let url = URL(string: "test.cz?" + str) else {
            return
        }

        let section = url.valueOf(AppkeeConstants.deeplinkSection)
        let article = url.valueOf(AppkeeConstants.deeplinkArticle)

        if let section = section, let sectionID = Int(section) {
            let articleID: Int? = article != nil ? Int(article ?? "") : nil
            self.navigationController.popToRootViewController(animated: false) {
                self.selectItem(sectionID: sectionID, articleID: articleID)
            }
        }
    }

    func openSearch() {
        self.showSearch()
    }

    func openMenu() {

    }

    func openLanguageSelection() {
        delegate?.languageSelection()
    }

    func openPhotoReporter(email: String) {
        self.showPhotoReporter(email: email)
    }

    func openDebugMenu() {
        let alert = UIAlertController(title: translations.debug.TITLE, message: nil, preferredStyle: .actionSheet)

        if dependencies.configManager.appMenu {
            alert.addAction(UIAlertAction(title: translations.debug.MENU, style: .default, handler:{ (UIAlertAction) in
                self.delegate?.close()
            }))

            alert.addAction(UIAlertAction(title: translations.debug.RELOAD, style: .default, handler:{ (UIAlertAction) in
                self.delegate?.reload()
            }))
        }

        alert.addAction(UIAlertAction(title: translations.common.CANCEL, style: .cancel, handler: nil))

        self.navigationController.present(alert, animated: true, completion: {
            print("completion block")
        })
    }

    func openLink(link: String) {
        self.showNotificationLink(link: link)
    }

    private func testPushNotifications() {
        let current = UNUserNotificationCenter.current()

        current.getNotificationSettings(completionHandler: { (settings) in
            if settings.authorizationStatus == .denied {
                DispatchQueue.main.async {
                    self.showPushWarning()
                }
            }
        })
    }
}

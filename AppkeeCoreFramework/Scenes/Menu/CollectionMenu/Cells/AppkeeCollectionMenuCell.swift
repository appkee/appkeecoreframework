//
//  AppkeeCollectionMenuCell.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/10/22.
//  Copyright © 2022 Radek Zmeskal. All rights reserved.
//

import UIKit

protocol AppkeeCollectionMenuCellDelegate: class {
    func configure(with icon: UIImage?, title: String, colorSettings: AppkeeColorSettings?)
}

class AppkeeCollectionMenuCell: UICollectionViewCell {
    @IBOutlet weak var containerView: UIView!

    @IBOutlet weak var icoImageView: UIImageView!

    @IBOutlet weak var titleLabel: UILabel!
        

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}


extension AppkeeCollectionMenuCell: AppkeeCollectionMenuCellDelegate {

    func configure(with icon: UIImage?, title: String, colorSettings: AppkeeColorSettings?) {
        contentView.layer.cornerRadius = 8
        contentView.layer.borderWidth = 1


        icoImageView.image = icon
        titleLabel.text = title

        if let colorSettings = colorSettings {
            titleLabel.textColor = colorSettings.menuText
            icoImageView.tintColor = colorSettings.menuText
            contentView.layer.borderColor = colorSettings.menuText.cgColor
        }
    }
}

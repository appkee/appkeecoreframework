//
//  LanguageView.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 14/03/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeLanguageView: UIView {
    
    //MARK: - Outlets
    @IBOutlet weak var imageImageView: UIImageView!
    
    @IBOutlet weak var button: UIButton!
    
    //MARK: - Properties
    var handler: () -> Void = { () in }
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
    }
        
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

    // MARK: - Functions
    func configure(with image: UIImage, handler: @escaping () -> Void) {
        imageImageView.image = image
        self.handler = handler
    }
    
    @IBAction func buttonTap(_ sender: Any) {
        handler()
    }
}

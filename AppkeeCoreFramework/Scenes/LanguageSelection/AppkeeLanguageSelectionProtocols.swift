//
//  AppkeeLanguageSelectionProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 14/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

protocol AppkeeLanguageSelectionCoordinatorDelegate: class {
//    func coordinator(_ coordinator: AppkeeCoordinator, finishedWithSuccess success: Bool)
    func openApp(language: AppkeeAppPreferenceLanguage)
}

// PRESENTER -> COORDINATOR
protocol AppkeeLanguageSelectionCoordinatorInput: class {
    func navigate(to route: AppkeeLanguageSelection.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeLanguageSelectionInteractorInput {
    // func perform(_ request: LanguageSelection.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeLanguageSelectionInteractorOutput: class {
    // func present(_ response: LanguageSelection.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeLanguageSelectionPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeeLanguageSelection.Action)
}

// PRESENTER -> VIEW
protocol AppkeeLanguageSelectionPresenterOutput: class {
    // func display(_ displayModel: LanguageSelection.DisplayData.Work)
    
    func addLanguageButton(image: String, handler: @escaping () -> Void)
}

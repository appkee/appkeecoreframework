//
//  AppkeeLanguageSelectionCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 14/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeeLanguageSelectionCoordinator: AppkeeCoordinator {
    // MARK: - Properties
//    let navigationController: UINavigationController
    var dependencies: AppkeeFullDependencies
    
    weak var delegate: AppkeeLanguageSelectionCoordinatorDelegate?
    
    private let window: UIWindow
       
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []

    // MARK: - Init
    init(window: UIWindow, dependencies: AppkeeFullDependencies) {
        self.dependencies = dependencies
        self.window = window
        
//        self.navigationController = UINavigationController()
//        self.navigationController.view.backgroundColor = .white
//
//        self.navigationController.navigationBar.isHidden = true
    }
    
    func start(root: Bool = false) {
        let interactor = AppkeeLanguageSelectionInteractor()
        let presenter = AppkeeLanguageSelectionPresenter(
            interactor: interactor,
            coordinator: self,
            configManagers: dependencies.configManager,
            languages: []
        )
        let vc = AppkeeLanguageSelectionViewController.instantiate(with: presenter)
        
        interactor.output = presenter
        presenter.output = vc
        
//        navigationController.setViewControllers([vc], animated: false)
        
        window.rootViewController = vc
    }
    
    func start(languages: [AppkeeAppPreferenceLanguage]) {
        let interactor = AppkeeLanguageSelectionInteractor()
        let presenter = AppkeeLanguageSelectionPresenter(
            interactor: interactor,
            coordinator: self,
            configManagers: dependencies.configManager,
            languages: languages
        )
        let vc = AppkeeLanguageSelectionViewController.instantiate(with: presenter)
        
        interactor.output = presenter
        presenter.output = vc
        
//        navigationController.setViewControllers([vc], animated: false)
        
        window.rootViewController = vc
    }
}
// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeLanguageSelectionCoordinator: AppkeeLanguageSelectionCoordinatorInput {
    func navigate(to route: AppkeeLanguageSelection.Route) {
        switch route {
        case .openApp(let language):
            self.delegate?.openApp(language: language)
        }
    }
}

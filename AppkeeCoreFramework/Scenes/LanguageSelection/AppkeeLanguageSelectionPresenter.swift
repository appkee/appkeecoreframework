//
//  AppkeeLanguageSelectionPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 14/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeLanguageSelectionPresenter {
    // MARK: - Properties
    let interactor: AppkeeLanguageSelectionInteractorInput
    weak var coordinator: AppkeeLanguageSelectionCoordinatorInput?
    weak var output: AppkeeLanguageSelectionPresenterOutput?

    let languages: [AppkeeAppPreferenceLanguage]
    let configManagers: AppkeeConfigManager
    
    // MARK: - Init
    init(interactor: AppkeeLanguageSelectionInteractorInput, coordinator: AppkeeLanguageSelectionCoordinatorInput, configManagers: AppkeeConfigManager, languages: [AppkeeAppPreferenceLanguage]) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.languages = languages
        self.configManagers = configManagers
    }
}

// MARK: - User Events -

extension AppkeeLanguageSelectionPresenter: AppkeeLanguageSelectionPresenterInput {
    func viewCreated() {
        for language in languages {
            output?.addLanguageButton(image: language.image, handler: {
                self.coordinator?.navigate(to: .openApp(language))
            })
        }
    }

    func handle(_ action: AppkeeLanguageSelection.Action) {
        
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeLanguageSelectionPresenter: AppkeeLanguageSelectionInteractorOutput {

}

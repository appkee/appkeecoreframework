//
//  LanguageSelectionInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 14/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeLanguageSelectionInteractor {
    // MARK: - Properties
    weak var output: AppkeeLanguageSelectionInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeLanguageSelectionInteractor: AppkeeLanguageSelectionInteractorInput {
}

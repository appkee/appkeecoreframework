//
//  AppkeeLanguageSelectionModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 14/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeLanguageSelection {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {

    }

    enum Route {
        case openApp(AppkeeAppPreferenceLanguage)
    }
}

extension AppkeeLanguageSelection.Request {

}

extension AppkeeLanguageSelection.Response {

}

extension AppkeeLanguageSelection.DisplayData {
    
}

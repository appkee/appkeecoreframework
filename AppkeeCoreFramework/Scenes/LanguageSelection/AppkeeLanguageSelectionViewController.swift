//
//  AppkeeLanguageSelectionViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 14/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeLanguageSelectionViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var stackView: UIStackView!

    // MARK: - Properties
    private var presenter: AppkeeLanguageSelectionPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeLanguageSelectionPresenterInput) -> AppkeeLanguageSelectionViewController {
        let name = "\(AppkeeLanguageSelectionViewController.self)"
        let bundle = Bundle(for: AppkeeLanguageSelectionViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeLanguageSelectionViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
        
        imageView.image = UIImage(named: "logo")
    }

    // MARK: - Callbacks -

}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeLanguageSelectionViewController: AppkeeLanguageSelectionPresenterOutput {
    func addLanguageButton(image: String, handler: @escaping () -> Void) {
        let view = AppkeeLanguageView()
        if let image = UIImage(named: image, in: Bundle(for: AppkeeLanguageSelectionViewController.self), compatibleWith: nil) {
            view.configure(with: image, handler: handler)
            
            stackView.addArrangedSubview(view)
        }
    }
}

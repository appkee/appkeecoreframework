//
//  AppkeNotificationAreasViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 12/29/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeNotificationRegionsViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var iconImageView: UIImageView!

    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var areasStackView: UIStackView!

    @IBOutlet weak var confirmButton: UIButton!


    // MARK: - Properties
    private var presenter: AppkeNotificationRegionsPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeNotificationRegionsPresenterInput) -> AppkeNotificationRegionsViewController {
        let name = "\(AppkeNotificationRegionsViewController.self)"
        let bundle = Bundle(for: AppkeNotificationRegionsViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeNotificationRegionsViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
    }

    // MARK: - Callbacks -

    @IBAction func confirmButtonTap(_ sender: Any) {
        presenter.handle(.submit)
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeNotificationRegionsViewController: AppkeNotificationRegionsPresenterOutput {
    func clearAreas() {
        for view in areasStackView.arrangedSubviews {
            areasStackView.removeArrangedSubview(view)
            view.removeFromSuperview()
        }
    }

    func setupUI(colorSettings: AppkeeColorSettings, image: UIImage?) {
        self.view.backgroundColor = colorSettings.content
        self.iconImageView.image = image

        self.titleLabel.text = translations.notificationAreas.TITLE
        self.confirmButton.setTitle(translations.notificationAreas.BUTTON, for: .normal)
    }

    func addOption(index: Int, name: String, isOn: Bool, colorSettings: AppkeeColorSettings?, handler: @escaping ((index: Int, isOn: Bool)) -> Void) {
        let checkBoxView = AppkeeCheckboxView(frame: .zero)
        checkBoxView.configure(with: name, isOn: isOn, colorSettings: colorSettings) { (value) in
            handler((index, value))
        }

        self.areasStackView.addArrangedSubview(checkBoxView)
    }
}

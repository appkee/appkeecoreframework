//
//  AppkeNotificationAreasCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 12/29/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeNotificationRegionsCoordinator: AppkeeCoordinator {
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies

    weak var delegate: AppkeeCoordinatorDelegate?

    private let window: UIWindow
    private let passType: PassType?

    private let appDescription: AppkeeAppDescription?

    // MARK: - Init
    init(window: UIWindow, dependencies: AppkeeFullDependencies, passType: PassType?) {
        self.dependencies = dependencies
        self.window = window
        self.passType = passType

        self.navigationController = UINavigationController()
        self.navigationController.view.backgroundColor = .white

        self.navigationController.navigationBar.isHidden = true

        self.appDescription = dependencies.appDescriptionManager.readAppDescription()
    }

    func start(root: Bool = false) {
        let interactor = AppkeNotificationRegionsInteractor()
        let presenter = AppkeNotificationRegionsPresenter(interactor: interactor, coordinator: self, graphicManager: dependencies.graphicManager, imageName: self.appDescription?.logo ?? "", areas: self.appDescription?.notificationRegions ?? [])
        let vc = AppkeNotificationRegionsViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        navigationController.setViewControllers([vc], animated: false)

        window.rootViewController = navigationController
    }
}
// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeNotificationRegionsCoordinator: AppkeNotificationRegionsCoordinatorInput {
    func navigate(to route: AppkeNotificationRegions.Route) {
        switch route {
        case .menu:
            switch self.passType {
            case .senior:
                delegate?.navigate(to: .pass(passType: .senior))
            case .family:
                delegate?.navigate(to: .pass(passType: .family))
            case .none:
                delegate?.navigate(to: .menu)
            }
        }
    }
}

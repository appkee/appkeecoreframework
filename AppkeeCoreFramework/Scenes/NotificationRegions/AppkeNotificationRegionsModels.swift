//
//  AppkeNotificationAreasModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 12/29/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeNotificationRegions {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case submit
    }

    enum Route {
        case menu
    }
}

extension AppkeNotificationRegions.Request {

}

extension AppkeNotificationRegions.Response {

}

extension AppkeNotificationRegions.DisplayData {

    struct Areas {
        let areas: [AppkeeNotificationArea]
    }
}

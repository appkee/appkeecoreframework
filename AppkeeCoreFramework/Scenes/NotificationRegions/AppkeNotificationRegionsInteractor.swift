//
//  AppkeNotificationAreasInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 12/29/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeNotificationRegionsInteractor {
    // MARK: - Properties
    weak var output: AppkeNotificationRegionsInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeNotificationRegionsInteractor: AppkeNotificationRegionsInteractorInput {
}

//
//  AppkeNotificationAreasProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 12/29/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeNotificationAreasCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeNotificationRegionsCoordinatorInput: class {
    func navigate(to route: AppkeNotificationRegions.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeNotificationRegionsInteractorInput {
    // func perform(_ request: AppkeNotificationAreas.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeNotificationRegionsInteractorOutput: class {
    // func present(_ response: AppkeNotificationAreas.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeNotificationRegionsPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeNotificationRegions.Action)
}

// PRESENTER -> VIEW
protocol AppkeNotificationRegionsPresenterOutput: class {
    func setupUI(colorSettings: AppkeeColorSettings, image: UIImage?)

    func clearAreas()
    func addOption(index: Int, name: String, isOn: Bool, colorSettings: AppkeeColorSettings?, handler: @escaping ((index: Int, isOn: Bool)) -> Void)
}

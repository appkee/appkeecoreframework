//
//  AppkeNotificationAreasPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 12/29/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeNotificationRegionsPresenter {
    // MARK: - Properties
    private let graphicManager: AppkeeGraphicManager
    private let areas: [AppkeeNotificationArea]
    private let imageName: String

    let interactor: AppkeNotificationRegionsInteractorInput
    weak var coordinator: AppkeNotificationRegionsCoordinatorInput?
    weak var output: AppkeNotificationRegionsPresenterOutput?

    private var ids: [Int] = []

    // MARK: - Init
    init(interactor: AppkeNotificationRegionsInteractorInput, coordinator: AppkeNotificationRegionsCoordinatorInput, graphicManager: AppkeeGraphicManager, imageName: String, areas: [AppkeeNotificationArea]) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
        self.imageName = imageName
        self.areas = areas.sorted(by: { area1, area2 in
            return area1.id < area2.id
        })

        self.ids = UserDefaults.notificationRegionsID ?? []
    }

    func updateArea(id: Int, isOn: Bool) {
        if isOn {
            if !ids.contains(where: { return $0 == id}) {
                self.ids.append(id)
            }
        } else {
            if let index = ids.firstIndex(of: id) {
                self.ids.remove(at: index)
            }
        }
    }

    func updateAreas() {
        output?.clearAreas()

        let isOn = Set(ids) == Set(self.areas.map { return $0.id })

        output?.addOption(index: -1, name: translations.notificationAreas.ALL_AREAS, isOn: isOn, colorSettings: graphicManager.colorsSettings, handler: { [weak self] (index, isOn) in
            guard let self = self else { return }

            if isOn {
                self.ids = self.areas.map { return $0.id }
            } else {
                self.ids = []
            }

            self.updateAreas()
        })

        for area in self.areas {
            let isOn = ids.contains(area.id) ?? false

            output?.addOption(index: area.id, name: area.regionName, isOn: isOn, colorSettings: graphicManager.colorsSettings, handler: { [weak self] (id, isOn) in
                guard let self = self else { return }

                self.updateArea(id: id, isOn: isOn)

                self.updateAreas()
            })
        }
    }
}

// MARK: - User Events -

extension AppkeNotificationRegionsPresenter: AppkeNotificationRegionsPresenterInput {
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings, image: graphicManager.getImage(name: self.imageName))
        }

        self.updateAreas()
    }

    func handle(_ action: AppkeNotificationRegions.Action) {
        UserDefaults.notificationRegionsID = self.ids

        coordinator?.navigate(to: .menu)
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeNotificationRegionsPresenter: AppkeNotificationRegionsInteractorOutput {

}

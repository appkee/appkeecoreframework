//
//  AppkeeLinkInteractor.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 30/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeLinkInteractor {
    // MARK: - Properties
    weak var output: AppkeeLinkInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeLinkInteractor: AppkeeLinkInteractorInput {
}

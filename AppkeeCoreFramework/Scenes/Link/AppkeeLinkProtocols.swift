//
//  AppkeeLinkProtocols.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 30/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol LinkCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeLinkCoordinatorInput: class {
    func navigate(to route: AppkeeLink.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeLinkInteractorInput {
    // func perform(_ request: Link.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeLinkInteractorOutput: class {
    // func present(_ response: Link.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeLinkPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeeLink.Action)
}

// PRESENTER -> VIEW
protocol AppkeeLinkPresenterOutput: class {
    func display(_ linkModel: AppkeeLink.DisplayData.Link)
    func display(_ error: AppkeeLink.DisplayData.Error)
    
    func setupUI(colorSettings: AppkeeColorSettings)
}

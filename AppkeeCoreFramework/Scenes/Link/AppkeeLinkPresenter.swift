//
//  AppkeeLinkPresenter.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 30/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeLinkPresenter {
    // MARK: - Properties
    private let props: AppkeeLinkProps
    private let graphicManager: AppkeeGraphicManager
    
    let interactor: AppkeeLinkInteractorInput
    weak var coordinator: AppkeeLinkCoordinatorInput?
    weak var output: AppkeeLinkPresenterOutput?

    // MARK: - Init
    init(interactor: AppkeeLinkInteractorInput, coordinator: AppkeeLinkCoordinatorInput, graphicManager: AppkeeGraphicManager, props: AppkeeLinkProps) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.props = props
        self.graphicManager = graphicManager
    }
}

// MARK: - User Events -

extension AppkeeLinkPresenter: AppkeeLinkPresenterInput {    
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }

        loadLink()
    }
        
    func handle(_ action: AppkeeLink.Action) {
        switch action {
        case .error:
            output?.display(AppkeeLink.DisplayData.Error())
        case .reload:
            loadLink()
        }
    }

    func loadLink() {
        if !Connectivity.isConnectedToInternet(), let offlineLink = props.offlineLink, !offlineLink.isEmpty {
            output?.display(AppkeeLink.DisplayData.Link(link: offlineLink, cache: true, css: props.css))
        } else {
            output?.display(AppkeeLink.DisplayData.Link(link: props.link, cache: false, css: props.css))
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeLinkPresenter: AppkeeLinkInteractorOutput {

}

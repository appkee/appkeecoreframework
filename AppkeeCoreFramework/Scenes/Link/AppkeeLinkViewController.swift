//
//  LinkViewController.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 30/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import UIKit
import WebKit
import SafariServices
import AVFoundation

class AppkeeLinkViewController: UIViewController {
    
    // MARK: - Outlets
    private var webView: WKWebView?
    
    private var popupWebView: WKWebView?
    
    @IBOutlet weak var errorView: UIView!
    
    @IBOutlet weak var errorLabel: UILabel! {
        didSet {
            errorLabel.text = translations.link.ERROR_TITLE
        }
    }
    
    @IBOutlet weak var errorButton: UIButton! {
        didSet {
            errorButton.setTitle(translations.link.BUTTON, for: .normal)
        }
    }
    
    // MARK: - Properties
    private var presenter: AppkeeLinkPresenterInput!

    private var css: String?
    private var backgoundDate: Date?

    let group = DispatchGroup()

    // MARK: - Init
    class func instantiate(with presenter: AppkeeLinkPresenterInput) -> AppkeeLinkViewController {
        let name = "\(AppkeeLinkViewController.self)"
        let bundle = Bundle(for: AppkeeLinkViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeLinkViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        
        HTTPCookieStorage.shared.cookieAcceptPolicy = .always

        setupApplicationNotifications()

        errorView.isHidden = true
        
        self.configurationForWebView { (conf) in
            let webView = WKWebView(frame: .zero, configuration: conf)
            webView.translatesAutoresizingMaskIntoConstraints = false
            webView.scrollView.bounces = false
            webView.backgroundColor = .clear
            webView.sizeToFit()
            webView.navigationDelegate = self
            webView.allowsBackForwardNavigationGestures = true
            webView.uiDelegate = self

            webView.isHidden = true

            self.view.addSubview(webView)

            webView.translatesAutoresizingMaskIntoConstraints = false
            webView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
            webView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
            webView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
            webView.bottomAnchor.constraint(lessThanOrEqualTo: self.view.bottomAnchor, constant: 0).isActive = true
            
            self.webView = webView
            
            self.presenter.viewCreated()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        if #available(iOS 11.0, *) {
//            self.webView?.configuration.websiteDataStore.httpCookieStore.getAllCookies { cookies in
//                print("Save cookies = \(cookies)")
////                self.setData(cookies, key: AppkeeConstants.UserDefs.webViewCookies)
//                
//                if let cookie = cookies.first(where: { $0.name == "loggedIn" }) {
//                    UserDefaults.isWebViewLogged = cookie.value.boolValueFromString()
//                } else {
//                    UserDefaults.isWebViewLogged = false
//                }
//            }
//        } else {
//            // Fallback on earlier versions
//        }
    }

    // MARK: - Callbacks -
    
    private func configurationForWebView(_ completion: @escaping (WKWebViewConfiguration) -> Void) {
        let configuration = WKWebViewConfiguration()
        
        configuration.allowsAirPlayForMediaPlayback = true
        configuration.allowsInlineMediaPlayback = true
        configuration.allowsPictureInPictureMediaPlayback = true
        
        configuration.preferences.javaScriptCanOpenWindowsAutomatically = true
        configuration.preferences.javaScriptEnabled = true
        
        if let cookies: [HTTPCookie] = self.getData(key: AppkeeConstants.UserDefs.webViewCookies) {

            for cookie in cookies {
                group.enter()
                configuration.websiteDataStore.httpCookieStore.setCookie(cookie) {
                    print("Set cookie = \(cookie) with name = \(cookie.name)")
                    self.group.leave()
                    }
            }

        }

        completion(configuration)
    }
    
    func setData(_ value: Any, key: String) {
        let ud = UserDefaults.standard
        let archivedPool = NSKeyedArchiver.archivedData(withRootObject: value)
        ud.set(archivedPool, forKey: key)
    }

    func getData<T>(key: String) -> T? {
        let ud = UserDefaults.standard
        if let val = ud.value(forKey: key) as? Data,
            let obj = NSKeyedUnarchiver.unarchiveObject(with: val) as? T {
            return obj
        }

        return nil
    }
    
    @IBAction func errorButtonTap(_ sender: Any) {
        presenter.handle(.reload)
    }

    private func setupApplicationNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(willResignActiveNotification),
                                               name:  UIApplication.willResignActiveNotification,
                                               object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForegroundNotification),
                                               name:  UIApplication.willEnterForegroundNotification,
                                               object: nil)
    }

    @objc private func willResignActiveNotification() {
        self.backgoundDate = Date()
    }

    @objc private func willEnterForegroundNotification() {
        // test offline more tjan 1 min
        if let date = self.backgoundDate {
            if Date().timeIntervalSince(date) > 60 {
                presenter.handle(.reload)
            }
        } else {
            presenter.handle(.reload)
        }
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeLinkViewController: AppkeeLinkPresenterOutput, Progressable {
    
    func display(_ error: AppkeeLink.DisplayData.Error) {
        errorView.isHidden = false
        hideProgress()
    }

    func display(_ linkModel: AppkeeLink.DisplayData.Link) {
        if let url = URL(string: linkModel.link) {
            if linkModel.cache {
                webView?.load(URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.returnCacheDataDontLoad))
            } else {
                var urlRequest = URLRequest(url: url)
//                urlRequest.httpShouldHandleCookies = false
                webView?.load(urlRequest)
            }
            showProgress()
        }

        self.css = linkModel.css
    }
    
    func setupUI(colorSettings: AppkeeColorSettings) {
        self.view.backgroundColor = colorSettings.content
        self.errorView.backgroundColor = colorSettings.content
        self.errorLabel.textColor = colorSettings.contentText
        self.errorButton.setTitleColor(colorSettings.headerText, for: .normal)
    }
}

extension AppkeeLinkViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.configuration.websiteDataStore.httpCookieStore.getAllCookies { cookies in
            print("Save cookies = \(cookies)")

            if let cookie = cookies.first(where: { $0.name == "loggedIn" }) {
                UserDefaults.isWebViewLogged = cookie.value.boolValueFromString()
            } else {
                UserDefaults.isWebViewLogged = false
            }

            self.setData(cookies, key: AppkeeConstants.UserDefs.webViewCookies)
        }

        if let cssString = self.css?.replacingOccurrences(of: "\n", with: "") {
            let jsString = "var style = document.createElement('style'); style.innerHTML = '\(cssString)'; document.head.appendChild(style);"
            webView.evaluateJavaScript(jsString, completionHandler: nil)

            webView.evaluateJavaScript(jsString) { [weak self] _, _ in
                webView.isHidden = false
                self?.hideProgress()

                self?.errorView.isHidden = true
            }

            return
        }

        webView.isHidden = false
        hideProgress()

        errorView.isHidden = true
    }

    // 4
    // MARK: - Encode string to base 64
    private func encodeStringTo64(fromString: String) -> String? {
        let plainData = fromString.data(using: .utf8)
        return plainData?.base64EncodedString(options: [])
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
//        if let error = error as? NSError, error.code == -999 {
//            hideProgress()
//            webView.isHidden = false
//            return
//        }

        hideProgress()
        presenter.handle(.error)
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        presenter.handle(.error)
    }

    func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        presenter.handle(.error)
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if !(navigationAction.targetFrame?.isMainFrame ?? false) {
//            print(">> \(navigationAction.request.url)")
            decisionHandler(.allow)
            return
        }

        webView.isHidden = true
//        showProgress()
        decisionHandler(.allow)
    }

//    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
//        if(challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust) {
//            let cred = URLCredential(trust: challenge.protectionSpace.serverTrust!)
//            completionHandler(.useCredential, cred)
//        } else {
//            completionHandler(.performDefaultHandling, nil)
//        }
//    }

}
 
extension AppkeeLinkViewController: WKUIDelegate {
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        print(">>> \(navigationAction)")
        
        popupWebView = WKWebView(frame: view.bounds, configuration: configuration)
        popupWebView!.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        popupWebView!.navigationDelegate = self
        popupWebView!.uiDelegate = self
        view.addSubview(popupWebView!)
        return popupWebView!
    }
    
    func webViewDidClose(_ webView: WKWebView) {
        // Popup window is closed, we remove it
        
        webView.removeFromSuperview()
        popupWebView = nil
    }
}

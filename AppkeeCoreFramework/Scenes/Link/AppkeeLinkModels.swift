//
//  AppkeeLinkModels.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 30/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

struct AppkeeLinkProps {
    let title: String
    let link: String
    let offlineLink: String?
    let banners: [AppkeeAdvertisement]?
    let css: String?
}

enum AppkeeLink {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case error
        case reload
    }

    enum Route {
        case openAdvertisementLink(link: String)
        case openDeeplink(params: String)
    }
}

extension AppkeeLink.Request {

}

extension AppkeeLink.Response {

}

extension AppkeeLink.DisplayData {
    struct Link {
        let link: String
        let cache: Bool
        let css: String?
    }
    
    struct Error {
        
    }
}

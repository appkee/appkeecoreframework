//
//  AppkeeLoginPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 1/22/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import FBSDKLoginKit
import FBSDKCoreKit
import AuthenticationServices

class AppkeeLoginPresenter {
    // MARK: - Properties
    private let graphicManager: AppkeeGraphicManager
    private let configManager:  AppkeeConfigManager
    private let descriptionText: String?
    private let imageName: String?
    private let loginMethods: [String]

    let interactor: AppkeeLoginInteractorInput
    weak var coordinator: AppkeeLoginCoordinatorInput?
    weak var output: AppkeeLoginPresenterOutput?

    // MARK: - Init
    init(interactor: AppkeeLoginInteractorInput, coordinator: AppkeeLoginCoordinatorInput, graphicManager: AppkeeGraphicManager, configManager:  AppkeeConfigManager, loginMethods: [String], descriptionText: String?, imageName: String?) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
        self.configManager = configManager
        self.loginMethods = loginMethods
        self.descriptionText = descriptionText
        self.imageName = imageName
    }

    private func retrieveEmail(token: AccessToken) {
        GraphRequest(graphPath: "/me", parameters: ["fields": "email,first_name,last_name"], tokenString: token.tokenString, version: nil, httpMethod: .get)
            .start(completion:
            { [weak self] (connection, result, error) -> Void in
                guard let self = self else {
                    return
                }

                if let error = error {
                    self.output?.display(AppkeeLogin.DisplayData.Error(title: translations.loginPage.ERROR_FB_LOGIN, message: error.localizedDescription))
                    LoginManager().logOut()
                } else {
                    if let userDict = result as? [String:Any] {
                        if let email = userDict["email"] as? String {
                            self.interactor.perform(.init(email: email, password: "facebook", appCode: self.configManager.appCode))
                            return
                        }
                    }
                    self.output?.display(AppkeeLogin.DisplayData.Error(title: translations.loginPage.ERROR_FB_LOGIN, message: translations.loginPage.ERROR_FB_NO_EMAIL))
                    LoginManager().logOut()
                }
        })
    }
}

// MARK: - User Events -

extension AppkeeLoginPresenter: AppkeeLoginPresenterInput {
    func viewCreated() {
        output?.display(title: descriptionText, image: graphicManager.getImage(name: self.imageName))

        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }

        if loginMethods.contains("email") {
            let email = UserDefaults.pageEmail
            let password = UserDefaults.pagePassword
            output?.display(email: email, password: password)
            output?.displayEmailLogin(isHidden: false)

            if let email = email, let password = password {
                output?.displayProgress()
                interactor.perform(AppkeeLogin.Request.Login(email: email, password: password, appCode: self.configManager.appCode))
            }
        } else {
            output?.displayEmailLogin(isHidden: true)
        }

        if loginMethods.contains("facebook") {
            output?.displayFacebookLogin(isHidden: false)

            if let token = AccessToken.current, AccessToken.isCurrentAccessTokenActive {
                output?.displayProgress()
                retrieveEmail(token: token)
            } else {
                LoginManager().logOut()
            }
        } else {
            output?.displayFacebookLogin(isHidden: true)
        }

        if loginMethods.contains("apple") {
            if #available(iOS 13, *), let userID = UserDefaults.userIdApple {
                let appleIDProvider = ASAuthorizationAppleIDProvider()
                appleIDProvider.getCredentialState(forUserID: userID) { [weak self] (credentialState, error) in
                    guard let self = self else { return }
                    switch credentialState {
                        case .authorized:
                            DispatchQueue.main.async {
                                self.output?.display(AppkeeLogin.DisplayData.Finish())

                                self.coordinator?.navigate(to: .menu)
                            }
                            break
                        default:
                            break
                     }
                }
            }

            output?.displayAppleLogin(isHidden: false)
        } else {
            output?.displayAppleLogin(isHidden: true)
        }
    }

    func handle(_ action: AppkeeLogin.Action) {
        switch action {
        case .login(let email, let password):
            interactor.perform(AppkeeLogin.Request.Login(email: email, password: password, appCode: self.configManager.appCode))
            UserDefaults.userIdApple = nil
        case .facebook(let token):
            retrieveEmail(token: token)
            UserDefaults.userIdApple = nil
        case .apple(let userID):
            UserDefaults.userIdApple = userID

            coordinator?.navigate(to: .menu)
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeLoginPresenter: AppkeeLoginInteractorOutput {
    func present(_ response: AppkeeLogin.Response.Login) {
        output?.display(AppkeeLogin.DisplayData.Finish())

        coordinator?.navigate(to: .menu)
    }

    func present(_ response: AppkeeLogin.Response.Error) {
        LoginManager().logOut()

        output?.display(AppkeeLogin.DisplayData.Error(title: translations.error.TITLE, message: response.message))
    }
}

//
//  AppkeeLoginViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 1/22/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import AuthenticationServices

class AppkeeLoginViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var containerStackView: UIStackView!
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!

    @IBOutlet weak var emailStackView: UIStackView!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var loginTextField: UITextField!

    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!

    @IBOutlet weak var facebookView: UIView!
    @IBOutlet weak var facebookButton: FBLoginButton!
    @IBOutlet weak var emailButton: UIButton!

    // MARK: - Properties
    private var presenter: AppkeeLoginPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeLoginPresenterInput) -> AppkeeLoginViewController {
        let name = "\(AppkeeLoginViewController.self)"
        let bundle = Bundle(for: AppkeeLoginViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeLoginViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()

        self.titleLabel.text = translations.loginPage.TITLE
//        self.subTitleLabel.text =
        self.loginLabel.text = translations.loginPage.EMAIL
//        self.loginTextField.placeholder = translations.loginPage.EMAIL
        self.passwordLabel.text = translations.loginPage.PASSWORD
//        self.passwordTextField.placeholder = translations.loginPage.PASSWORD
        self.emailButton.setTitle(translations.loginPage.LOGIN, for: .normal)

        self.loginTextField.delegate = self
        self.passwordTextField.delegate = self

        self.emailButton.isEnabled = false

        self.facebookButton.permissions = ["public_profile", "email"]
        self.facebookButton.delegate = self
        self.facebookButton.layer.cornerRadius = 10
    }

    // MARK: - Callbacks -
    
    @IBAction func emailButtonTap(_ sender: Any) {
        showProgress()
        
        presenter.handle(.login(email: loginTextField.text ?? "", password: passwordTextField.text ?? ""))
    }

    @available(iOS 13.0, *)
    @objc func handleAppleIdRequest() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.performRequests()
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeLoginViewController: AppkeeLoginPresenterOutput, Alertable, Progressable {
    func displayEmailLogin(isHidden: Bool) {
        emailStackView.isHidden = isHidden
    }

    func displayFacebookLogin(isHidden: Bool) {
        facebookView.isHidden = isHidden
    }

    func displayAppleLogin(isHidden: Bool) {
        if !isHidden, #available(iOS 13.0, *) {
            let authorizationButton = ASAuthorizationAppleIDButton()
            authorizationButton.translatesAutoresizingMaskIntoConstraints = false
            authorizationButton.addTarget(self, action: #selector(handleAppleIdRequest), for: .touchUpInside)
            authorizationButton.cornerRadius = 10
            authorizationButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
            self.containerStackView.addArrangedSubview(authorizationButton)
        }
    }

    func display(email: String?, password: String?) {
        self.loginTextField.text = email
        self.passwordTextField.text = password
    }

    func displayProgress() {
        showProgress()
    }

    func display(_ displayModel: AppkeeLogin.DisplayData.Finish) {
        hideProgress()
    }

    func display(_ displayModel: AppkeeLogin.DisplayData.Error) {
        hideProgress()

        showAlert(withTitle: displayModel.title, message: displayModel.message) {}
    }

    func display(title: String?, image: UIImage?) {
        self.subTitleLabel.text = title
        self.logoImageView.image = image
    }

    func setupUI(colorSettings: AppkeeColorSettings) {
        self.view.backgroundColor = colorSettings.content

        self.emailButton.setTitleColor(.white, for: .normal)
        self.emailButton.backgroundColor = colorSettings.loginColor
    }
}


extension AppkeeLoginViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.emailButton.isEnabled = !((self.loginTextField.text?.isEmpty ?? true) && (self.passwordTextField.text?.isEmpty ?? true))
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

extension AppkeeLoginViewController: LoginButtonDelegate {
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {

        if let error = error {
            return
        }

        if let token = result?.token {
            presenter.handle(.facebook(token: token))
        }
    }

    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {

    }
}

@available(iOS 13.0, *)
extension AppkeeLoginViewController: ASAuthorizationControllerDelegate {

    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
            presenter.handle(.apple(userID: appleIDCredential.user))
        }
    }

    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        self.showAlert(withTitle: "Error", message: error.localizedDescription, handler: {} )
    }
}

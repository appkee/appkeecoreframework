//
//  AppkeeLoginInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 1/22/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import FirebaseAnalytics

class AppkeeLoginInteractor {
    // MARK: - Properties
    weak var output: AppkeeLoginInteractorOutput?
    private let repository: AppkeeRepository
    private let graphicManager: AppkeeGraphicManager

    // MARK: - Init
    init(repository: AppkeeRepository, graphicManager: AppkeeGraphicManager) {
        self.repository = repository
        self.graphicManager = graphicManager
    }

}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeLoginInteractor: AppkeeLoginInteractorInput {
    func perform(_ request: AppkeeLogin.Request.Login) {
        repository.pageLogin(email: request.email, password: request.password, appCode: request.appCode) { [weak self] response in
            guard let self = self else { return }

            switch response.result {
            case .success(let response):
                if response.success {
                    UserDefaults.pageEmail = request.email
                    UserDefaults.pagePassword = request.password
                    self.output?.present(AppkeeLogin.Response.Login())
                } else {
                    UserDefaults.pageEmail = nil
                    UserDefaults.pagePassword = nil
                    self.output?.present(AppkeeLogin.Response.Error(message: response.error ?? translations.error.UNKNOWN))
                }
                return
            case .failure(let error):
                UserDefaults.pageEmail = nil
                UserDefaults.pagePassword = nil
                self.output?.present(AppkeeLogin.Response.Error(message: error.localizedDescription))

                Analytics.logEvent("Sending login failed", parameters: ["appCode": request.appCode, "error": error.localizedDescription])
            }
        }
    }
    
}

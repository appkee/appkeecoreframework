//
//  AppkeeLoginCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 1/22/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeeLoginCoordinator: AppkeeCoordinator {
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
//    weak var delegate: AppkeeLoginCoordinatorDelegate?

    weak var delegate: AppkeeCoordinatorDelegate?

    private let window: UIWindow

    private let appDescription: AppkeeAppDescription?

    // MARK: - Init
    init(window: UIWindow, dependencies: AppkeeFullDependencies) {
        self.dependencies = dependencies
        self.window = window

        self.navigationController = UINavigationController()
        self.navigationController.view.backgroundColor = .white

        self.navigationController.navigationBar.isHidden = true

        self.appDescription = dependencies.appDescriptionManager.readAppDescription()
    }

//    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, appDescription: AppkeeAppDescription?) {
//        self.dependencies = dependencies
//        self.appDescription = appDescription
//
//        self.navigationController = navigationController
//        self.navigationController.view.backgroundColor = .white
//
//        self.navigationController.navigationBar.isHidden = true
//
//        self.window = nil
//    }

    func start(root: Bool = false) {
        let interactor = AppkeeLoginInteractor(repository: dependencies.repository,
                                          graphicManager: dependencies.graphicManager)
        let presenter = AppkeeLoginPresenter(
            interactor: interactor,
            coordinator: self,
            graphicManager: dependencies.graphicManager,
            configManager: dependencies.configManager,
            loginMethods: appDescription?.loginMethods ?? [],
            descriptionText: appDescription?.loginText,
            imageName: appDescription?.loadingImage
        )
        let vc = AppkeeLoginViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        navigationController.setViewControllers([vc], animated: false)

        window.rootViewController = navigationController
    }

//    func startLogin() {
//        let interactor = AppkeeLoginInteractor(repository: dependencies.repository,
//                                          graphicManager: dependencies.graphicManager)
//        let presenter = AppkeeLoginPresenter(
//            interactor: interactor,
//            coordinator: self,
//            graphicManager: dependencies.graphicManager,
//            configManager: dependencies.configManager,
//            loginMethods: appDescription?.loginMethods ?? [],
//            descriptionText: appDescription?.loginText,
//            imageName: appDescription?.loadingImage
//        )
//        let vc = AppkeeLoginViewController.instantiate(with: presenter)
//
//        interactor.output = presenter
//        presenter.output = vc
//
//        navigationController.pushViewController(vc, animated: true)
//    }
}
// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeLoginCoordinator: AppkeeLoginCoordinatorInput {
    func navigate(to route: AppkeeLogin.Route) {
        switch route {
        case .menu:
            self.delegate?.navigate(to: .menu)
        }
    }
}

//
//  AppkeeLoginModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 1/22/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import FBSDKLoginKit

enum AppkeeLogin {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case login(email: String, password: String)
        case facebook(token: AccessToken)
        case apple(userID: String)
    }

    enum Route {
        case menu
    }
}

extension AppkeeLogin.Request {

    struct Login {
        let email: String
        let password: String
        let appCode: String
    }
}

extension AppkeeLogin.Response {

    struct Login {
        
    }

    struct Error {
        let message: String
    }
}

extension AppkeeLogin.DisplayData {
    struct Finish {

    }

    struct Error {
        let title: String
        let message: String
    }
}

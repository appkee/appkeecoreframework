//
//  AppkeeLoginProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 1/22/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeeLoginCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeLoginCoordinatorInput: class {
    func navigate(to route: AppkeeLogin.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeLoginInteractorInput {
     func perform(_ request: AppkeeLogin.Request.Login)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeLoginInteractorOutput: class {
    func present(_ response: AppkeeLogin.Response.Login)
    func present(_ response: AppkeeLogin.Response.Error)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeLoginPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeeLogin.Action)
}

// PRESENTER -> VIEW
protocol AppkeeLoginPresenterOutput: class {
    // func display(_ displayModel: AppkeeLogin.DisplayData.Work)

    func display( title: String?, image: UIImage?)

    func display( email: String?, password: String?)

    func displayProgress()

    func displayEmailLogin(isHidden: Bool)
    func displayFacebookLogin(isHidden: Bool)
    func displayAppleLogin(isHidden: Bool)

    func display(_ displayModel: AppkeeLogin.DisplayData.Finish)
    func display(_ displayModel: AppkeeLogin.DisplayData.Error)

    func setupUI(colorSettings: AppkeeColorSettings)
}

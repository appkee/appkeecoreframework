//
//  AppkeeVipSikoraMemberInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/4/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeVipSikoraMemberInteractor {
    // MARK: - Properties
    weak var output: AppkeeVipSikoraMemberInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeVipSikoraMemberInteractor: AppkeeVipSikoraMemberInteractorInput {
}

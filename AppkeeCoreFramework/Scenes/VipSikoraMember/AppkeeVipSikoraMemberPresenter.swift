//
//  AppkeeVipSikoraMemberPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/4/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeeVipSikoraMemberPresenter {
    // MARK: - Properties
    private let section: AppkeeSection
    private let graphicManager: AppkeeGraphicManager
    
    let interactor: AppkeeVipSikoraMemberInteractorInput
    weak var coordinator: AppkeeVipSikoraMemberCoordinatorInput?
    weak var output: AppkeeVipSikoraMemberPresenterOutput?

    // MARK: - Init
    init(interactor: AppkeeVipSikoraMemberInteractorInput, coordinator: AppkeeVipSikoraMemberCoordinatorInput, graphicManager: AppkeeGraphicManager, section: AppkeeSection) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.section = section
        self.graphicManager = graphicManager
    }
}

// MARK: - User Events -

extension AppkeeVipSikoraMemberPresenter: AppkeeVipSikoraMemberPresenterInput {
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }

        if let user = UserDefaults.pageUser {
            let image = UIImage.generateQRCode(from: String(user.id) + "|" + user.email)

            output?.display(AppkeeVipSikoraMember.DisplayData.User(id: String(user.id),
                                                                   email: user.email,
                                                                   name: user.name ?? "",
                                                                   tel: user.phone ?? "",
                                                                   address: user.address,
                                                                   image: image))
        }
    }

    func handle(_ action: AppkeeVipSikoraMember.Action) {
        
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeVipSikoraMemberPresenter: AppkeeVipSikoraMemberInteractorOutput {

}

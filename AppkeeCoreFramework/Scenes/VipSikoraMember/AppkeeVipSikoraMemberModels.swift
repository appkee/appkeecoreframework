//
//  AppkeeVipSikoraMemberModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/4/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

enum AppkeeVipSikoraMember {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {

    }

    enum Route {
        case openAdvertisementLink(link: String)
        case openDeeplink(params: String)
    }
}

extension AppkeeVipSikoraMember.Request {

}

extension AppkeeVipSikoraMember.Response {

}

extension AppkeeVipSikoraMember.DisplayData {
    struct User {
        let id: String
        let email: String
        let name: String
        let tel: String
        let address: String?

        let image: UIImage?
    }
}

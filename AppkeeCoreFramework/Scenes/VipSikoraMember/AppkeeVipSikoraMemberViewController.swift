//
//  AppkeeVipSikoraMemberViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/4/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeVipSikoraMemberViewController: UIViewController {
    // MARK: - Outlets

    @IBOutlet weak var idLabel: UILabel!

    @IBOutlet weak var emailLabel: UILabel!

    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var telLabel: UILabel!

    @IBOutlet weak var addressLabel: UILabel!

    @IBOutlet weak var qrImageView: UIImageView!

    // MARK: - Properties
    private var presenter: AppkeeVipSikoraMemberPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeVipSikoraMemberPresenterInput) -> AppkeeVipSikoraMemberViewController {
        let name = "\(AppkeeVipSikoraMemberViewController.self)"
        let bundle = Bundle(for: AppkeeVipSikoraMemberViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeVipSikoraMemberViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
    }

    // MARK: - Callbacks -

}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeVipSikoraMemberViewController: AppkeeVipSikoraMemberPresenterOutput {
    func display(_ displayModel: AppkeeVipSikoraMember.DisplayData.User) {
        idLabel.text = displayModel.id
        emailLabel.text = displayModel.email
        nameLabel.text = displayModel.name
        telLabel.text = displayModel.tel
        addressLabel.text = displayModel.address

        qrImageView.image = displayModel.image
    }

    func setupUI(colorSettings: AppkeeColorSettings) {
        idLabel.textColor = colorSettings.contentText
        emailLabel.textColor = colorSettings.contentText
        nameLabel.textColor = colorSettings.contentText
        telLabel.textColor = colorSettings.contentText
        addressLabel.textColor = colorSettings.contentText
    }
}

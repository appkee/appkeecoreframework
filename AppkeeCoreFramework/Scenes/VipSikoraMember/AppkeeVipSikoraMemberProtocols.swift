//
//  AppkeeVipSikoraMemberProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/4/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeeVipSikoraMemberCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeVipSikoraMemberCoordinatorInput: class {
    func navigate(to route: AppkeeVipSikoraMember.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeVipSikoraMemberInteractorInput {
    // func perform(_ request: AppkeeVipSikoraMember.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeVipSikoraMemberInteractorOutput: class {
    // func present(_ response: AppkeeVipSikoraMember.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeVipSikoraMemberPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeeVipSikoraMember.Action)
}

// PRESENTER -> VIEW
protocol AppkeeVipSikoraMemberPresenterOutput: class {
    func display(_ displayModel: AppkeeVipSikoraMember.DisplayData.User)
    func setupUI(colorSettings: AppkeeColorSettings)
}

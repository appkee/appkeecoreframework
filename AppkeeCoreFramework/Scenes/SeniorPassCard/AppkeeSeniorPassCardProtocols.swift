//
//  AppkeeSeniorPassCardProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/20/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeeSeniorPassCardCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeSeniorPassCardCoordinatorInput: class {
    func navigate(to route: AppkeeSeniorPassCard.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeSeniorPassCardInteractorInput {
    // func perform(_ request: AppkeeSeniorPassCard.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeSeniorPassCardInteractorOutput: class {
    // func present(_ response: AppkeeSeniorPassCard.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeSeniorPassCardPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeeSeniorPassCard.Action)
}

// PRESENTER -> VIEW
protocol AppkeeSeniorPassCardPresenterOutput: class {
    // func display(_ displayModel: AppkeeSeniorPassCard.DisplayData.Work)

    func display( seniorPass: AppkeeSeniorPass)
    
    func setupUI(colorSettings: AppkeeColorSettings)
}

//
//  AppkeeSeniorPassCardPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/20/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeSeniorPassCardPresenter {
    // MARK: - Properties
    private let graphicManager: AppkeeGraphicManager
    
    let interactor: AppkeeSeniorPassCardInteractorInput
    weak var coordinator: AppkeeSeniorPassCardCoordinatorInput?
    weak var output: AppkeeSeniorPassCardPresenterOutput?

    // MARK: - Init
    init(interactor: AppkeeSeniorPassCardInteractorInput, coordinator: AppkeeSeniorPassCardCoordinatorInput, graphicManager: AppkeeGraphicManager) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
    }
}

// MARK: - User Events -

extension AppkeeSeniorPassCardPresenter: AppkeeSeniorPassCardPresenterInput {
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }

        if let seniorPass = UserDefaults.seniorPassLogin {
            output?.display(seniorPass: seniorPass)
        }
    }

    func handle(_ action: AppkeeSeniorPassCard.Action) {
        
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeSeniorPassCardPresenter: AppkeeSeniorPassCardInteractorOutput {

}

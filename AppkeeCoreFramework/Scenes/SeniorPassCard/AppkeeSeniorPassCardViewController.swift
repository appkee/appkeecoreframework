//
//  AppkeeSeniorPassCardViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/20/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeSeniorPassCardViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var ageLabel: UILabel!

    @IBOutlet weak var cardLabel: UILabel!
    
    // MARK: - Properties
    private var presenter: AppkeeSeniorPassCardPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeSeniorPassCardPresenterInput) -> AppkeeSeniorPassCardViewController {
        let name = "\(AppkeeSeniorPassCardViewController.self)"
        let bundle = Bundle(for: AppkeeSeniorPassCardViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeSeniorPassCardViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
    }

    // MARK: - Callbacks -

}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeSeniorPassCardViewController: AppkeeSeniorPassCardPresenterOutput {
    func display( seniorPass: AppkeeSeniorPass) {
        let passImage = UIImage(named: "seniorpas_card_front", in: Bundle(for: AppkeeSeniorPassCardViewController.self), compatibleWith: nil)
        imageView.image = passImage

        nameLabel.text = (seniorPass.firstname + " " + seniorPass.surname).uppercased()

        ageLabel.text = "\(seniorPass.birthYear)" 

        cardLabel.text = seniorPass.number
    }

    func setupUI(colorSettings: AppkeeColorSettings) {
        self.view.backgroundColor = colorSettings.content
    }
}

//
//  AppkeeSeniorPassCardInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/20/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeSeniorPassCardInteractor {
    // MARK: - Properties
    weak var output: AppkeeSeniorPassCardInteractorOutput?
    private let repository: AppkeeRepository
    private let graphicManager: AppkeeGraphicManager

    // MARK: - Init
    init(repository: AppkeeRepository, graphicManager: AppkeeGraphicManager) {
        self.repository = repository
        self.graphicManager = graphicManager
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeSeniorPassCardInteractor: AppkeeSeniorPassCardInteractorInput {
}

//
//  AppkeeSeniorPassCardModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/20/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeSeniorPassCard {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {

    }

    enum Route {
        case openAdvertisementLink(link: String)
        case openDeeplink(params: String)
    }
}

extension AppkeeSeniorPassCard.Request {

}

extension AppkeeSeniorPassCard.Response {

}

extension AppkeeSeniorPassCard.DisplayData {

}

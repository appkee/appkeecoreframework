//
//  AppkeePushWarningProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 12/04/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol PushWarningCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeePushWarningCoordinatorInput: class {
    func navigate(to route: AppkeePushWarning.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeePushWarningInteractorInput {
    // func perform(_ request: PushWarning.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeePushWarningInteractorOutput: class {
    // func present(_ response: PushWarning.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeePushWarningPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeePushWarning.Action)
}

// PRESENTER -> VIEW
protocol AppkeePushWarningPresenterOutput: class {
    // func display(_ displayModel: PushWarning.DisplayData.Work)
}

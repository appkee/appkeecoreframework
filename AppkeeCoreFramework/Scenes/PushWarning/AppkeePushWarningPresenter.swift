//
//  AppkeePushWarningPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 12/04/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeePushWarningPresenter {
    // MARK: - Properties
    let interactor: AppkeePushWarningInteractorInput
    weak var coordinator: AppkeePushWarningCoordinatorInput?
    weak var output: AppkeePushWarningPresenterOutput?

    // MARK: - Init
    init(interactor: AppkeePushWarningInteractorInput, coordinator: AppkeePushWarningCoordinatorInput) {
        self.interactor = interactor
        self.coordinator = coordinator
    }
}

// MARK: - User Events -

extension AppkeePushWarningPresenter: AppkeePushWarningPresenterInput {
    func viewCreated() {

    }

    func handle(_ action: AppkeePushWarning.Action) {
        switch action {
        case .settings:
            coordinator?.navigate(to: .settings)
        case .close:
            coordinator?.navigate(to: .close)
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeePushWarningPresenter: AppkeePushWarningInteractorOutput {

}

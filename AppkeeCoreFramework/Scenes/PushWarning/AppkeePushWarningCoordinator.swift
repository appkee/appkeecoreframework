//
//  PushWarningCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 12/04/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeePushWarningCoordinator: AppkeeCoordinator {
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
//    weak var delegate: PushWarningCoordinatorDelegate?
    
    var delegate: AppkeeSideMenuCoordinatorDelegate?

    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies) {
        self.navigationController = navigationController
        self.dependencies = dependencies
    }
        
    func start(root: Bool = false) {
        let interactor = AppkeePushWarningInteractor()
        let presenter = AppkeePushWarningPresenter(interactor: interactor, coordinator: self)
        let vc = AppkeePushWarningViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        // FIXME: Display as you need
        
        navigationController.present(vc, animated: true, completion: nil)
    }
}
// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeePushWarningCoordinator: AppkeePushWarningCoordinatorInput {
    func navigate(to route: AppkeePushWarning.Route) {
        switch route {
        case .settings:
            navigationController.dismiss(animated: true) {
                if let settingsUrl = URL(string: UIApplication.openSettingsURLString), UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: nil)
                }
            }
        case .close:
            navigationController.dismiss(animated: true, completion: nil)
        }
    }
}

//
//  PushWarningInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 12/04/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeePushWarningInteractor {
    // MARK: - Properties
    weak var output: AppkeePushWarningInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeePushWarningInteractor: AppkeePushWarningInteractorInput {
}

//
//  PushWarningModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 12/04/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeePushWarning {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case close
        case settings
    }

    enum Route {
        case close
        case settings
    }
}

extension AppkeePushWarning.Request {

}

extension AppkeePushWarning.Response {

}

extension AppkeePushWarning.DisplayData {
    
}

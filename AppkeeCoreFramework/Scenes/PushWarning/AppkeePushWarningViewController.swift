//
//  PushWarningViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 12/04/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeePushWarningViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var settingsImageView: UIImageView!
    @IBOutlet weak var settingLabel: UILabel!
    
    @IBOutlet weak var notificationImageView: UIImageView!
    @IBOutlet weak var notificationLabel: UILabel!
    
    @IBOutlet weak var switchImageView: UIImageView!
    @IBOutlet weak var switchLabel: UILabel!
    
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var remindButton: UIButton!
    
    // MARK: - Properties
    private var presenter: AppkeePushWarningPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeePushWarningPresenterInput) -> AppkeePushWarningViewController {
        let name = "\(AppkeePushWarningViewController.self)"
        let bundle = Bundle(for: AppkeePushWarningViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeePushWarningViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
        
        let settingsImage = UIImage(named: "warning_settings", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
        settingsImageView.image = settingsImage
        let notificationImage = UIImage(named: "warning_notification", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
        notificationImageView.image = notificationImage
        let switchImage = UIImage(named: "warning_switch", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
        switchImageView.image = switchImage
        
        titleLabel.text = translations.pushWarning.TITLE
        descriptionLabel.text = translations.pushWarning.DESCRIPTION
        settingLabel.text = translations.pushWarning.ITEM_SETTINGS
        notificationLabel.text = translations.pushWarning.ITEM_NOTIFICATION
        switchLabel.text = translations.pushWarning.ITEM_SWITCH
        settingsButton.setTitle(translations.pushWarning.BUTTON_SETIINGS, for: .normal)
        remindButton.setTitle(translations.pushWarning.BUTTON_CLOSE, for: .normal)
    }

    // MARK: - Callbacks -
    @IBAction func settingsButtonTap(_ sender: Any) {
        presenter.handle(.settings)
    }
    
    @IBAction func remindButtonTap(_ sender: Any) {
        presenter.handle(.close)
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeePushWarningViewController: AppkeePushWarningPresenterOutput {

}

//
//  AppkeeIntroPageModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 5/23/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeePassLogin {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case login(key: String)
        case registration
    }

    enum Route {
        case menu
        case registration(url: String)
    }
}

extension AppkeePassLogin.Request {

    struct Login {
        let key: String
        let appCode: String
    }
}

extension AppkeePassLogin.Response {

    struct Login {
        let data: AppkeeSenioPass
    }

    struct Error {
        let message: String
    }
}

extension AppkeePassLogin.DisplayData {
    struct Finish {

    }

    struct Error {
        let message: String
    }
}

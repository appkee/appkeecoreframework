//
//  AppkeeIntroPageViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 5/23/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeePassLoginViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var containerStackView: UIStackView!

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!

    @IBOutlet weak var loginTextField: UITextField!

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!

    // MARK: - Properties
    private var presenter: AppkeePassLoginPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeePassLoginPresenterInput) -> AppkeePassLoginViewController {
        let name = "\(AppkeePassLoginViewController.self)"
        let bundle = Bundle(for: AppkeePassLoginViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeePassLoginViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()

        self.titleLabel.text = translations.loginPage.TITLE
//        self.subTitleLabel.text =

        self.loginTextField.placeholder = translations.seniorPass.LOGIN_CARD_ID
        self.loginButton.setTitle(translations.loginPage.LOGIN, for: .normal)
        self.registerButton.setTitle(translations.seniorPass.LOGIN_REGISTER, for: .normal)

        self.loginTextField.delegate = self
    }

    // MARK: - Callbacks -

    @IBAction func registrationButtonTap(_ sender: Any) {
        presenter.handle(.registration)
    }

    @IBAction func emailButtonTap(_ sender: Any) {
        showProgress()

        presenter.handle(.login(key: loginTextField.text ?? ""))
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeePassLoginViewController: AppkeePassLoginPresenterOutput, Alertable, Progressable {
    func display(title: String?, image: UIImage?) {
        self.subTitleLabel.setHtml(from: title ?? "")
        self.logoImageView.image = image
    }

    func display(login: String?) {
        self.loginTextField.text = login

        self.showProgress()
        presenter.handle(.login(key: loginTextField.text ?? ""))
    }

    func display(_ displayModel: AppkeePassLogin.DisplayData.Finish) {
        hideProgress()
    }

    func display(_ displayModel: AppkeePassLogin.DisplayData.Error) {
        hideProgress()

        showErrorAlert(withMessage: displayModel.message)
    }

    func setupUI(colorSettings: AppkeeColorSettings) {
        self.view.backgroundColor = colorSettings.content

        self.loginButton.setTitleColor(.white, for: .normal)
        self.loginButton.backgroundColor = colorSettings.loginColor

        self.registerButton.setTitleColor(.white, for: .normal)
        self.registerButton.backgroundColor = colorSettings.menu
    }
}

extension AppkeePassLoginViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.loginButton.isEnabled = !((self.loginTextField.text?.isEmpty ?? true))
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.loginButton.endEditing(true)
        return false
    }
}

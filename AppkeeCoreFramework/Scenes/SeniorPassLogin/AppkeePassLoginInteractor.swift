//
//  AppkeeIntroPageInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 5/23/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import FirebaseAnalytics

class AppkeePassLoginInteractor {
    // MARK: - Properties
    weak var output: AppkeePassLoginInteractorOutput?
    private let repository: AppkeeRepository
    private let graphicManager: AppkeeGraphicManager

    // MARK: - Init
    init(repository: AppkeeRepository, graphicManager: AppkeeGraphicManager) {
        self.repository = repository
        self.graphicManager = graphicManager
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeePassLoginInteractor: AppkeePassLoginInteractorInput {
    func perform(_ request: AppkeePassLogin.Request.Login) {
        repository.seniorPassLogin(key: request.key, appCode: request.appCode) { [weak self] response in
            guard let self = self else { return }

            switch response.result {
            case .success(let response):
                if response.success, let data = response.data {
                    self.output?.present(AppkeePassLogin.Response.Login(data: data))
                } else {
                    self.output?.present(AppkeePassLogin.Response.Error(message: response.error ?? translations.error.UNKNOWN))
                }
                return
            case .failure(let error):
                self.output?.present(AppkeePassLogin.Response.Error(message: error.localizedDescription))

                Analytics.logEvent("Sending login failed", parameters: ["appCode": request.appCode, "error": error.localizedDescription])
            }
        }
    }
}

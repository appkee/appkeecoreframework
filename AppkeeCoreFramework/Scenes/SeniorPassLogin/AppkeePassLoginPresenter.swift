//
//  AppkeeIntroPagePresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 5/23/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import FirebaseMessaging

class AppkeePassLoginPresenter {
    // MARK: - Properties
    private let graphicManager: AppkeeGraphicManager
    private let configManager:  AppkeeConfigManager
    private let passType: PassType
    private let descriptionText: String?
    private let imageName: String?

    let interactor: AppkeePassLoginInteractorInput
    weak var coordinator: AppkeePassLoginCoordinatorInput?
    weak var output: AppkeePassLoginPresenterOutput?

    // MARK: - Init
    init(interactor: AppkeePassLoginInteractorInput, coordinator: AppkeePassLoginCoordinatorInput, graphicManager: AppkeeGraphicManager, configManager:  AppkeeConfigManager, passType: PassType, descriptionText: String?, imageName: String?) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
        self.configManager = configManager
        self.descriptionText = descriptionText
        self.imageName = imageName
        self.passType = passType
    }
}

// MARK: - User Events -

extension AppkeePassLoginPresenter: AppkeePassLoginPresenterInput {
    func viewCreated() {
        output?.display(title: descriptionText, image: graphicManager.getImage(name: self.imageName))

        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }

        if let number = UserDefaults.seniorPassLogin?.number {
            output?.display(login: number)
        }
    }

    func handle(_ action: AppkeePassLogin.Action) {
        switch action {
        case .login(let key):
            interactor.perform(AppkeePassLogin.Request.Login(key: key, appCode: self.configManager.appCode))
        case .registration:
            switch self.passType {
            case .family:
                coordinator?.navigate(to: .registration(url: "http://kraj-vys.rodinnepasy.cz/registrace"))
            case .senior:
                coordinator?.navigate(to: .registration(url: "http://seniorpasy.cz/#registrace"))
            }
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeePassLoginPresenter: AppkeePassLoginInteractorOutput {
    func present(_ response: AppkeePassLogin.Response.Login) {
        UserDefaults.seniorPassLogin = response.data

        let topic = "/topics/apk-\(configManager.appCode)-\(response.data.region)"

        Messaging.messaging().subscribe(toTopic: topic) { error in
            print("Subscribed to topic: \(topic)")
        }

        output?.display(AppkeePassLogin.DisplayData.Finish())

        coordinator?.navigate(to: .menu)
    }

    func present(_ response: AppkeePassLogin.Response.Error) {
        output?.display(AppkeePassLogin.DisplayData.Error(message: response.message))
    }
}

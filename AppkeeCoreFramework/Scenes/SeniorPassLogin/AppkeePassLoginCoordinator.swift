//
//  AppkeeIntroPageCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 5/23/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit
import SafariServices

enum PassType {
    case senior
    case family
}

class AppkeePassLoginCoordinator: AppkeeCoordinator {
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
//    weak var delegate: AppkeeLoginCoordinatorDelegate?


    weak var delegate: AppkeeCoordinatorDelegate?

    private let window: UIWindow
    private let appDescription: AppkeeAppDescription?
    private let passType: PassType
    
    // MARK: - Init
    init(window: UIWindow, dependencies: AppkeeFullDependencies, passType: PassType) {
        self.dependencies = dependencies
        self.window = window
        self.passType = passType

        self.navigationController = UINavigationController()
        self.navigationController.view.backgroundColor = .white

        self.navigationController.navigationBar.isHidden = true

        self.appDescription = dependencies.appDescriptionManager.readAppDescription()
    }

    func start(root: Bool = false) {
        let interactor = AppkeePassLoginInteractor(repository: dependencies.repository,
                                          graphicManager: dependencies.graphicManager)
        let presenter = AppkeePassLoginPresenter(
            interactor: interactor,
            coordinator: self,
            graphicManager: dependencies.graphicManager,
            configManager: dependencies.configManager,
            passType: passType,
            descriptionText: appDescription?.loginText,
            imageName: appDescription?.loadingImage
        )
        let vc = AppkeePassLoginViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        navigationController.setViewControllers([vc], animated: false)

        window.rootViewController = navigationController
    }
}
// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeePassLoginCoordinator: AppkeePassLoginCoordinatorInput {
    func navigate(to route: AppkeePassLogin.Route) {
        switch route {
        case .menu:
            self.delegate?.navigate(to: .menu)
        case .registration(let link):
            if let url = URL(string: link) {
                let vc = SFSafariViewController(url: url)
                navigationController.present(vc, animated: true)
            }
        }
    }
}

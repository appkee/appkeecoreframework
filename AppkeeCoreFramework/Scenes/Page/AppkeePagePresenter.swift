//
//  AppkeePagePresenter.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 26/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeePagePresenter {
    // MARK: - Properties
    private let props: AppkeePageProps
    private let graphicManager: AppkeeGraphicManager
    
    private let interactor: AppkeePageInteractorInput
    weak var coordinator: AppkeePageCoordinatorInput?
    weak var output: AppkeePagePresenterOutput?
    
//    private let contentArray: [Content]
    private let contentIds: [Int]

    // MARK: - Init
    init(interactor: AppkeePageInteractorInput, coordinator: AppkeePageCoordinatorInput, graphicManager: AppkeeGraphicManager, props: AppkeePageProps) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.props = props
        self.graphicManager = graphicManager
        
        contentIds = (props.texts + props.images + props.videos).sorted (by: { $0.order < $1.order }).map({$0.id })
    }
}

// MARK: - User Events -

extension AppkeePagePresenter: AppkeePagePresenterInput {
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }

//        let ids = contentIds.count > 10 ? Array(contentIds.prefix(10)) : contentIds

        for id in contentIds {
            if let text = props.texts.first(where: {$0.id == id }) {
                addText(text: text.content)
                continue
            }
            
            if let image = props.images.first(where: {$0.id == id }) {
                addImage(name: image.content, id: id, clickable: image.clickable, link: image.link)
                continue
            }
            
            if let video = props.videos.first(where: {$0.id == id }) {
                addVideo(link: video.content)
                continue
            }
        }
    }

    func handle(_ action: AppkeePage.Action) {
        switch action {
        case let .openLink(link, name):
            coordinator?.navigate(to: .openLink(link: link, name: name))
        case let .openDeeplink(params):
            coordinator?.navigate(to: .openDeeplink(params: params))
        }
    }
    
    private func addText(text: String?) {
        if let text = text {
            output?.addText(text, colorSettings: graphicManager.colorsSettings)
        }
    }
    
    private func addImage(name: String?, id: Int, clickable: Bool, link: String?) {
        if let image = graphicManager.getImage(name: name) {
            output?.addImage(image, handler: { () in
                if let link = link {
                    self.parseLink(link: link)
                } else if clickable {
                    self.showGallery(images: self.props.images.filter({ $0.clickable && $0.link == nil }), id: id)
                }
            })
        }
    }
    
    private func addVideo(link: String?) {
        if let link = link {
            output?.addVideo(link)
        }
    }
    
    private func showGallery(images: [AppkeeContent], id: Int) {
        let clickableImages = images.sorted (by: { $0.id < $1.id })
        if let index = clickableImages.firstIndex(where: { $0.id == id }) {
            coordinator?.navigate(to: .gallery(images: clickableImages, index: index))
        }
    }

    private func parseLink(link: String) {
        guard let url = URL(string: link) else {
            return
        }

        let deeplink = url.lastPathComponent
        if deeplink.hasPrefix("[\(AppkeeConstants.deeplinkSection)") {
            self.handle( .openDeeplink(params: deeplink))
        } else if let urlString = url.absoluteString.removingPercentEncoding {
            if urlString.starts(with: "http") {
                var name = url.lastPathComponent.capitalizingFirstLetter().replacingOccurrences(of: "/", with: "")
                if name.isEmpty {
                    name = url.host ?? ""
                }

                self.handle( .openLink(link: urlString, name: name))
            } else if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeePagePresenter: AppkeePageInteractorOutput {

}

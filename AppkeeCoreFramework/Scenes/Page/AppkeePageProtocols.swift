//
//  PageProtocols.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 26/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol PageCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeePageCoordinatorInput: class {
    func navigate(to route: AppkeePage.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeePageInteractorInput {
    // func perform(_ request: Page.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeePageInteractorOutput: class {
    // func present(_ response: Page.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeePagePresenterInput {
    func viewCreated()
    func handle(_ action: AppkeePage.Action)
}

// PRESENTER -> VIEW
protocol AppkeePagePresenterOutput: class {
    func addText(_ text: String, colorSettings: AppkeeColorSettings?)
    func addImage(_ image: UIImage, handler: @escaping () -> Void)
    func addVideo(_ link: String)
    
    func setupUI(colorSettings: AppkeeColorSettings)
    
}

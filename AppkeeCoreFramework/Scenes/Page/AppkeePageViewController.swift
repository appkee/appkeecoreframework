//
//  AppkeePageViewController.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 26/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeePageViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var stackView: UIStackView!

    // MARK: - Properties
    private var presenter: AppkeePagePresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeePagePresenterInput) -> AppkeePageViewController {
        let name = "\(AppkeePageViewController.self)"
        let bundle = Bundle(for: AppkeePageViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeePageViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
    }

    // MARK: - Callbacks -
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeePageViewController: AppkeePagePresenterOutput {

    func addText(_ text: String, colorSettings: AppkeeColorSettings?) {
        let textView = AppkeeTextView(frame: CGRect(x: 0, y: 0, width: self.stackView.frame.width, height: 50))
        textView.configure(with: text, colorSettings: colorSettings)
        textView.delegate = self
        
        stackView.addArrangedSubview(textView)
    }
    
    func addImage(_ image: UIImage, handler: @escaping () -> Void) {
        let imageView = AppkeeImageView(frame: .zero)
        imageView.configure(with: image, handler: handler)
       
        stackView.addArrangedSubview(imageView)
   }
    
    func addVideo(_ link: String) {
        let videoView = AppkeeVideoView(frame: .zero)
        videoView.configure(with: link)
       
        stackView.addArrangedSubview(videoView)
    }
    
    func setupUI(colorSettings: AppkeeColorSettings) {
        self.view.backgroundColor = colorSettings.content
    }
}

extension AppkeePageViewController: AppkeeTextViewDelegate {
    func openLink(link: String, name: String) {
        presenter.handle(.openLink(link: link, name: name))
    }

    func openDeeplink(params: String) {
        presenter.handle(.openDeeplink(params: params))
    }
}

//
//  AppkeePageModels.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 26/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

struct AppkeePageProps {
    let articleId: Int
    let title: String
    let texts: [AppkeeContent]
    let images: [AppkeeContent]
    let videos: [AppkeeContent]
    let favorites: Bool
    let favoritesType: AppkeeAppDescription.FavoriteType?
    let banners: [AppkeeAdvertisement]?
}

enum AppkeePage {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case openLink(link: String, name: String)
        case openDeeplink(params: String)
    }

    enum Route {
        case gallery(images: [AppkeeContent], index: Int)
        case openAdvertisementLink(link: String)
        case openLink(link: String, name: String)
        case openDeeplink(params: String)
    }
}

extension AppkeePage.Request {

}

extension AppkeePage.Response {

}

extension AppkeePage.DisplayData {
    
}

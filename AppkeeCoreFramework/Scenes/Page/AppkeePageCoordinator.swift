//
//  AppkeePageCoordinator.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 26/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit
import SafariServices

class AppkeePageCoordinator: AppkeeCoordinator {
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
    private let props: AppkeePageProps
    private let email: String?
    private let search: Bool
//    weak var delegate: PageCoordinatorDelegate?
    
    var delegate: AppkeeSideMenuCoordinatorDelegate?

    private var favoriteButton: UIBarButtonItem?

    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, props: AppkeePageProps, email: String?, search: Bool) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.props = props
        self.email = email
        self.search = search
    }
        
    func start(root: Bool = false) {
        let interactor = AppkeePageInteractor()
        let presenter = AppkeePagePresenter(interactor: interactor, coordinator: self, graphicManager: dependencies.graphicManager, props: props)
        let vc = AppkeePageViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        var menuItems: [UIBarButtonItem] = []
        if dependencies.configManager.appMenu {
            let image = UIImage(named: "dots", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(debugMenu(_:))))
        }

        if let _ = UserDefaults.pageUser {
            let image = UIImage(named: "logoutIcon", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(logoutTap(_:))))
        }

        if search {
            let image = UIImage(named: "search", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(search(_:))))
        }

        if let _ = email {
            let image = UIImage(named: "cameraMenu", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(photoReporter(_:))))
        }

        if props.favorites {
            let image = UIImage(named: "favorite", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
            let favoriteButton = UIBarButtonItem(image: image, style: .done, target: self, action: #selector(favoriteTap))
            menuItems.append(favoriteButton)

            self.favoriteButton = favoriteButton
            setupFavourite()
        }

        if UserDefaults.seniorPassLogin != nil || UserDefaults.familyPassLogin != nil {
            let image = UIImage(named: "logoutIcon", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(passLogoutTap(_:))))
        }

        vc.navigationItem.rightBarButtonItems = menuItems
        vc.title = props.title

        let rootController = AppkeeBannerViewController(graphicManager: dependencies.graphicManager)
        rootController.setViewController(vc)
        rootController.delegate = self

        if let banners = props.banners {
            rootController.setBanners(banners: banners)
        }

        if root {
            let image = UIImage(named: "hamburger", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
            rootController.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  image, style: .done, target: self, action: #selector(menu(_:)))
            
            navigationController.setViewControllers([rootController], animated: false)
            
            return
        }
        
        navigationController.navigationBar.isHidden = false
        navigationController.pushViewController(rootController, animated: true)
    }
    
    @objc func menu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openMenu()
    }
    
    @objc func debugMenu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openDebugMenu()
    }
    
    @objc func photoReporter(_ barButtonItem: UIBarButtonItem) {
        if let email = self.email {
            delegate?.openPhotoReporter(email: email)
        }
    }
    
    @objc func search(_ barButtonItem: UIBarButtonItem) {
        delegate?.openSearch()
    }

    @objc func favoriteTap(_ barButtonItem: UIBarButtonItem) {
        updateFavourite()
    }

    @objc func logoutTap(_ barButtonItem: UIBarButtonItem) {
        delegate?.logout()
    }

    @objc func passLogoutTap(_ barButtonItem: UIBarButtonItem) {
        delegate?.passLogout()
    }

    private func showLink(link: String, name: String) {
//        let props = AppkeeLinkProps(title: name, link: link, offlineLink: nil, banners: self.props.banners, css: nil)
//        let coordinator = AppkeeLinkCoordinator(navigationController: navigationController,
//                                          dependencies: dependencies,
//                                          props: props,
//                                          email: email,
//                                          search: false)
//        childrens.append(coordinator)
////        coordinator.delegate = self
//        coordinator.start(root: false)

        if let url = URL(string: link) {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true

            let vc = SFSafariViewController(url: url, configuration: config)
            navigationController.present(vc, animated: true)
        }
    }
}

extension AppkeePageCoordinator {
    private func setupFavourite() {
        if UserDefaults.containsFavourite(articleId: self.props.articleId) {
            self.favoriteButton?.tintColor = self.dependencies.graphicManager.colorsSettings?.loginColor
        } else {
            self.favoriteButton?.tintColor = .gray
        }
    }

    private func updateFavourite() {
        guard let favoriteButton = self.favoriteButton else {
            return
        }

        var hint: String?

        if UserDefaults.containsFavourite(articleId: self.props.articleId) {
            UserDefaults.removeFavorite(articleId: self.props.articleId)
            favoriteButton.tintColor = .gray

            switch self.props.favoritesType {
            case .article:
                hint = translations.favorites.ARTICLE_REMOVED
            case .recipe:
                hint = translations.favorites.RECIPE_REMOVED
            case .none:
                break
            }

        } else {
            UserDefaults.addFavorite(articleId: self.props.articleId)
            favoriteButton.tintColor = self.dependencies.graphicManager.colorsSettings?.loginColor

            switch self.props.favoritesType {
            case .article:
                hint = translations.favorites.ARTICLE_ADDED
            case .recipe:
                hint = translations.favorites.RECIPE_ADDED
            case .none:
                break
            }
        }

        if let hintText = hint {
            hint = String(format: hintText, self.props.title)

            self.navigationController.topViewController?.showToast(message: hint ?? "")
        }
    }
}


// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeePageCoordinator: AppkeePageCoordinatorInput {
    func navigate(to route: AppkeePage.Route) {
        switch route {
        case let .gallery(images, index):
            let coordinator = AppkeeGalleryDetaildCoordinator(navigationController: self.navigationController, dependencies: dependencies, images: images)
            
            childrens.append(coordinator)
            coordinator.start(index: index)
        case let .openAdvertisementLink(link):
            if let url = URL(string: link) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        case let .openLink(link, name):
            showLink(link: link, name: name)
        case let .openDeeplink(params):
            delegate?.openDeeplink(params: params)
        }
    }
}

extension AppkeePageCoordinator: AppkeeBannerViewControllerDelegate {
    func openLink(link: String) {
        self.navigate(to: .openAdvertisementLink(link: link))
    }

    func openDeeplink(params: String) {
        self.navigate(to: .openDeeplink(params: params))
    }
}

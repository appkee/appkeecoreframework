//
//  AppkeePageInteractor.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 26/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeePageInteractor {
    // MARK: - Properties
    weak var output: AppkeePageInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeePageInteractor: AppkeePageInteractorInput {
}

//
//  AppkeeSeniorPassBranchDetailCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/28/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeePassBranchDetailCoordinator: AppkeeCoordinator {
    /// MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
    private let branch: AppkeePassBranchDetail.AppkeePassBranch
    private let email: String?
    private let search: Bool
//    weak var delegate: GalleryCoordinatorDelegate?

    var delegate: AppkeeSideMenuCoordinatorDelegate?

    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, branch: AppkeePassBranchDetail.AppkeePassBranch, email: String?, search: Bool) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.branch = branch
        self.email = email
        self.search = search
    }

    func start(root: Bool = false) {
        let interactor = AppkeePassBranchDetailInteractor(repository: dependencies.repository, graphicManager: dependencies.graphicManager)
        let presenter = AppkeePassBranchDetailPresenter(interactor: interactor, coordinator: self, graphicManager: dependencies.graphicManager, branch: branch)
        let vc = AppkeePassBranchDetailViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc


        switch self.branch {
        case .senior(let branch):
            vc.title = branch.name
        case .family(let branch):
            vc.title = branch.company_name
        }

        navigationController.navigationBar.isHidden = false
        navigationController.pushViewController(vc, animated: true)
    }

    @objc func menu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openMenu()
    }

    @objc func debugMenu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openDebugMenu()
    }

    @objc func photoReporter(_ barButtonItem: UIBarButtonItem) {
        if let email = self.email {
            delegate?.openPhotoReporter(email: email)
        }
    }

    @objc func search(_ barButtonItem: UIBarButtonItem) {
        delegate?.openSearch()
    }
}

// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeePassBranchDetailCoordinator: AppkeePassBranchDetailCoordinatorInput {
    func navigate(to route: AppkeePassBranchDetail.Route) {
        switch route {
        case .openGPS(let lat, let lon):
            self.delegate?.openGPS(lat: lat, lon: lon)
        }
    }
}

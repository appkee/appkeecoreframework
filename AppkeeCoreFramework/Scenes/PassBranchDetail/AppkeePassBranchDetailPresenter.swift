//
//  AppkeeSeniorPassBranchDetailPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/28/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeePassBranchDetailPresenter {
    // MARK: - Properties
    private let graphicManager: AppkeeGraphicManager

    private let branch: AppkeePassBranchDetail.AppkeePassBranch

    let interactor: AppkeePassBranchDetailInteractorInput
    weak var coordinator: AppkeePassBranchDetailCoordinatorInput?
    weak var output: AppkeePassBranchDetailPresenterOutput?

    // MARK: - Init
    init(interactor: AppkeePassBranchDetailInteractorInput, coordinator: AppkeePassBranchDetailCoordinatorInput, graphicManager: AppkeeGraphicManager, branch: AppkeePassBranchDetail.AppkeePassBranch) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
        self.branch = branch
    }
}

// MARK: - User Events -

extension AppkeePassBranchDetailPresenter: AppkeePassBranchDetailPresenterInput {
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }

        switch self.branch {
        case .senior(let branch):
            setupSenior(branch: branch)
        case .family(let branch):
            setupFamily(branch: branch)
        }
    }

    private func setupFamily(branch: AppkeeFamilyPassBranch) {
        output?.display(AppkeePassBranchDetail.DisplayData.Alignment(left: true))

        output?.display(AppkeePassBranchDetail.DisplayData.Name(name: branch.company_name))

        let address =  "\(branch.street_one) \(branch.street_two ?? "")\n\(branch.post_code) \(branch.city)"
        output?.display(AppkeePassBranchDetail.DisplayData.Address(address: address))

        output?.display(AppkeePassBranchDetail.DisplayData.Tel(contact: branch.phone_number))

        output?.display(AppkeePassBranchDetail.DisplayData.Email(contact: branch.email ?? "="))
        output?.display(AppkeePassBranchDetail.DisplayData.Web(web: branch.web ?? "-"))


        output?.display(AppkeePassBranchDetail.DisplayData.Discount(discount: branch.percent_sale))

        if let description = branch.description, !description.isEmpty {
            output?.display(AppkeePassBranchDetail.DisplayData.Description(description: description))
        }

        output?.display(AppkeePassBranchDetail.DisplayData.Navigation())
    }


    private func setupSenior(branch: AppkeeSeniorPassBranch) {
        output?.display(AppkeePassBranchDetail.DisplayData.Name(name: branch.name))

        var address = ""
        if !branch.address.isEmpty {
            address = branch.address
        }

        if !address.isEmpty {
            address += ", \(branch.psc)"
        } else {
            address = String(branch.psc)
        }

        if !branch.city.isEmpty {
            if !address.isEmpty {
                address += " " + branch.city
            } else {
                address = branch.city
            }
        }

        if !address.isEmpty {
            output?.display(AppkeePassBranchDetail.DisplayData.Address(address: address))
        }

        if !branch.phone.isEmpty {
            output?.display(AppkeePassBranchDetail.DisplayData.Tel(contact: branch.phone))
        }

        if let email = branch.email, !email.isEmpty  {
            output?.display(AppkeePassBranchDetail.DisplayData.Email(contact: email))
        }

        if let web = branch.web {
            output?.display(AppkeePassBranchDetail.DisplayData.Web(web: web))
        }

        var discounts = ""
        for dis in branch.discounts {
            if let value = dis.value {
                discounts += String(Int(value)) + "% " + dis.description + "\n"
            }
        }
        if !discounts.isEmpty {
            discounts.removeLast()
            output?.display(AppkeePassBranchDetail.DisplayData.Discount(discount: discounts))
        }

        if !branch.description.isEmpty {
            output?.display(AppkeePassBranchDetail.DisplayData.Description(description: branch.description))
        }

        if let _ = branch.location {
            output?.display(AppkeePassBranchDetail.DisplayData.Navigation())
        }
    }

    func handle(_ action: AppkeePassBranchDetail.Action) {
        switch action {
        case .navigate:
            switch self.branch {
            case .senior(let branch):
                coordinator?.navigate(to: .openGPS(lat: String(branch.location?.lat ?? 0.0), lon: String(branch.location?.lon ?? 0.0)))
            case .family(let branch):
                coordinator?.navigate(to: .openGPS(lat: String(branch.lat), lon: String(branch.lng)))
            }
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeePassBranchDetailPresenter: AppkeePassBranchDetailInteractorOutput {

}

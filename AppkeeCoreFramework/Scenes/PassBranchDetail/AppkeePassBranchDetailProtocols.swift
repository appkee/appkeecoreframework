//
//  AppkeeSeniorPassBranchDetailProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/28/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeeSeniorPassBranchDetailCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeePassBranchDetailCoordinatorInput: class {
    func navigate(to route: AppkeePassBranchDetail.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeePassBranchDetailInteractorInput {
    // func perform(_ request: AppkeeSeniorPassBranchDetail.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeePassBranchDetailInteractorOutput: class {
    // func present(_ response: AppkeeSeniorPassBranchDetail.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeePassBranchDetailPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeePassBranchDetail.Action)
}

// PRESENTER -> VIEW
protocol AppkeePassBranchDetailPresenterOutput: class {
    func display(_ displayModel: AppkeePassBranchDetail.DisplayData.Alignment)

    func display(_ displayModel: AppkeePassBranchDetail.DisplayData.Name)
    func display(_ displayModel: AppkeePassBranchDetail.DisplayData.Address)
    func display(_ displayModel: AppkeePassBranchDetail.DisplayData.Tel)
    func display(_ displayModel: AppkeePassBranchDetail.DisplayData.Email)
    func display(_ displayModel: AppkeePassBranchDetail.DisplayData.Web)
    func display(_ displayModel: AppkeePassBranchDetail.DisplayData.Discount)
    func display(_ displayModel: AppkeePassBranchDetail.DisplayData.Description)
    func display(_ displayModel: AppkeePassBranchDetail.DisplayData.Navigation)

    func setupUI(colorSettings: AppkeeColorSettings)
}

//
//  AppkeeSeniorPassBranchDetailModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/28/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeePassBranchDetail {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case navigate
    }

    enum Route {
        case openGPS(lat: String, lon: String)
    }

    enum AppkeePassBranch {
        case senior(AppkeeSeniorPassBranch)
        case family(AppkeeFamilyPassBranch)
    }
}

extension AppkeePassBranchDetail.Request {

}

extension AppkeePassBranchDetail.Response {

}

extension AppkeePassBranchDetail.DisplayData {
    struct Alignment {
        let left: Bool
    }

    struct Name {
        let name: String
    }

    struct Address {
        let address: String
    }

    struct Tel {
        let contact: String
    }

    struct Email {
        let contact: String
    }

    struct Web {
        let web: String
    }

    struct Discount {
        let discount: String
    }

    struct Description {
        let description: String
    }

    struct Navigation {
        
    }
}

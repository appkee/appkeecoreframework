//
//  AppkeeSeniorPassBranchDetailViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/28/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeePassBranchDetailViewController: UIViewController {
    // MARK: - Outlets

    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var addressTitleLabel: UILabel! {
        didSet {
            addressTitleLabel.text = translations.seniorPass.BRANCH_ADDRESS
        }
    }
    @IBOutlet weak var addressLabel: UILabel!

    @IBOutlet weak var telView: UIView!
    @IBOutlet weak var telTitleLabel: UILabel! {
        didSet {
            telTitleLabel.text = translations.seniorPass.BRANCH_PHONE
        }
    }
    @IBOutlet weak var contactTextView: UITextView! {
            didSet {
                contactTextView.textContainerInset = UIEdgeInsets.zero
                contactTextView.textContainer.lineFragmentPadding = 0
            }
        }

    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTitleLabel: UILabel! {
        didSet {
            emailTitleLabel.text = translations.seniorPass.BRANCH_EMAIL
        }
    }
    @IBOutlet weak var emailTextView: UITextView! {
            didSet {
                emailTextView.textContainerInset = UIEdgeInsets.zero
                emailTextView.textContainer.lineFragmentPadding = 0
            }
        }


    @IBOutlet weak var webView: UIView!
    @IBOutlet weak var webTitleLabel: UILabel! {
        didSet {
            webTitleLabel.text = translations.seniorPass.BRANCH_WEB
        }
    }
    @IBOutlet weak var webTextView: UITextView! {
        didSet {
            webTextView.textContainerInset = UIEdgeInsets.zero
            webTextView.textContainer.lineFragmentPadding = 0
        }
    }


    @IBOutlet weak var discountView: UIView!
    @IBOutlet weak var discountTitleLabel: UILabel! {
        didSet {
            discountTitleLabel.text = translations.seniorPass.BRANCH_DISCOUNTS
        }
    }
    @IBOutlet weak var discountLabel: UILabel!

    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var descriptionTitleLabel: UILabel! {
        didSet {
            descriptionTitleLabel.text = translations.seniorPass.BRANCH_DESCRIPTION
        }
    }
    @IBOutlet weak var descriptionLabel: UILabel!

    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var navigationButton: UIButton! {
        didSet {
            navigationButton.setTitle(translations.seniorPass.BRANCH_NAVIGATE.uppercased(), for: .normal)
        }
    }

    // MARK: - Properties
    private var presenter: AppkeePassBranchDetailPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeePassBranchDetailPresenterInput) -> AppkeePassBranchDetailViewController {
        let name = "\(AppkeePassBranchDetailViewController.self)"
        let bundle = Bundle(for: AppkeePassBranchDetailViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeePassBranchDetailViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        addressView.isHidden = true
        telView.isHidden = true
        emailView.isHidden = true
        webView.isHidden = true
        discountView.isHidden = true
        descriptionView.isHidden = true
        navigationView.isHidden = true
        
        presenter.viewCreated()
    }

    // MARK: - Callbacks -
    @IBAction func navigationButton(_ sender: Any) {
        presenter.handle(.navigate)
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeePassBranchDetailViewController: AppkeePassBranchDetailPresenterOutput {
    func display(_ displayModel: AppkeePassBranchDetail.DisplayData.Alignment) {
        self.addressLabel.textAlignment = displayModel.left ? .left : .right
        self.contactTextView.textAlignment = displayModel.left ? .left : .right
        self.emailTextView.textAlignment = displayModel.left ? .left : .right
        self.webTextView.textAlignment = displayModel.left ? .left : .right
        self.discountLabel.textAlignment = displayModel.left ? .left : .right
        self.descriptionLabel.textAlignment = displayModel.left ? .left : .right
    }

    func display(_ displayModel: AppkeePassBranchDetail.DisplayData.Name) {
        self.titleLabel.text = displayModel.name
    }

    func display(_ displayModel: AppkeePassBranchDetail.DisplayData.Address) {
        self.addressLabel.text = displayModel.address
        self.addressView.isHidden = false
    }

    func display(_ displayModel: AppkeePassBranchDetail.DisplayData.Tel) {
        self.contactTextView.text = displayModel.contact
        self.telView.isHidden = false
    }

    func display(_ displayModel: AppkeePassBranchDetail.DisplayData.Email) {
        self.emailTextView.text = displayModel.contact
        self.emailView.isHidden = false
    }

    func display(_ displayModel: AppkeePassBranchDetail.DisplayData.Web) {
        self.webTextView.text = displayModel.web
        self.webView.isHidden = false
    }

    func display(_ displayModel: AppkeePassBranchDetail.DisplayData.Discount) {
        self.discountLabel.setHtml(from: displayModel.discount)
        self.discountView.isHidden = false
    }

    func display(_ displayModel: AppkeePassBranchDetail.DisplayData.Description) {
        self.descriptionLabel.setHtml(from: displayModel.description)
        self.descriptionView.isHidden = false
    }

    func display(_ displayModel: AppkeePassBranchDetail.DisplayData.Navigation) {
        self.navigationView.isHidden = false
    }

    func setupUI(colorSettings: AppkeeColorSettings) {
        self.view.backgroundColor = colorSettings.content

        self.titleLabel.textColor = colorSettings.loginColor

        self.contactTextView.tintColor = colorSettings.loginColor
        self.emailTextView.tintColor = colorSettings.loginColor
        self.webTextView.tintColor = colorSettings.loginColor

        self.navigationButton.setTitleColor(.white, for: .normal)
        self.navigationButton.backgroundColor = colorSettings.loginColor
    }
}

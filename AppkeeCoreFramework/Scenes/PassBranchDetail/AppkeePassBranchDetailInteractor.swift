//
//  AppkeeSeniorPassBranchDetailInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/28/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeePassBranchDetailInteractor {
    // MARK: - Properties
    weak var output: AppkeePassBranchDetailInteractorOutput?
    private let repository: AppkeeRepository
    private let graphicManager: AppkeeGraphicManager

    // MARK: - Init
    init(repository: AppkeeRepository, graphicManager: AppkeeGraphicManager) {
        self.repository = repository
        self.graphicManager = graphicManager
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeePassBranchDetailInteractor: AppkeePassBranchDetailInteractorInput {
}

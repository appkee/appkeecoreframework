//
//  AppkeeFormModels.swift
//  AppkeeCore
//
//  Created by Radek Zmeškal on 03/10/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeForm {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case send
        case openLink(link: String, name: String)
    }

    enum Route {
        case openAdvertisementLink(link: String)
        case openDeeplink(params: String)
        case openLink(link: String, name: String)
    }
}

extension AppkeeForm.Request {
    struct Send {
        let sectionId: Int
        let message: String
    }
}

extension AppkeeForm.Response {
    struct Send {        
    }
    
    struct Error {
        let message: String
    }
}

extension AppkeeForm.DisplayData {
    struct Sending {
    }
    
    struct Send {
    }
    
    struct Error {
        let message: String
    }
}

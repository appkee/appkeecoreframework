//
//  AppkeeFormViewController.swift
//  AppkeeCore
//
//  Created by Radek Zmeškal on 03/10/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeFormViewController: UIViewController, UIPickerViewDelegate {
 
    // MARK: - Outlets
    @IBOutlet weak var stackView: UIStackView!
    
    // MARK: - Properties
    private var presenter: AppkeeFormPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeFormPresenterInput) -> AppkeeFormViewController {
        let name = "\(AppkeeFormViewController.self)"
        let bundle = Bundle(for: AppkeeFormViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeFormViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
    }

    // MARK: - Callbacks -

    
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeFormViewController: AppkeeFormPresenterOutput, Alertable, Progressable {
    
    func setupUI(colorSettings: AppkeeColorSettings) {
        self.view.backgroundColor = colorSettings.content
    }
    
    func addTitle( title: String, colorSettings: AppkeeColorSettings?) {
        let titleView = AppkeeTitleView(frame: .zero)
        titleView.configure(with: title, colorSettings: colorSettings)
         
        stackView.addArrangedSubview(titleView)
    }  

    func addTextInput(title: String, colorSettings: AppkeeColorSettings?, handler: @escaping (String) -> Void) {
        let textInputView = AppkeeTextInputView(frame: .zero)
        textInputView.configure(with: title, colorSettings: colorSettings, handler: handler)
        
        stackView.addArrangedSubview(textInputView)
    }
    
    func addNumberInput(title: String, colorSettings: AppkeeColorSettings?, handler: @escaping (String) -> Void) {
        let textInputView = AppkeeTextInputView(frame: .zero)
        textInputView.configure(with: title, numeric: true, colorSettings: colorSettings, handler: handler)
        
        stackView.addArrangedSubview(textInputView)
    }
    
    func addTextAreaInput(title: String, colorSettings: AppkeeColorSettings?, handler: @escaping (String) -> Void) {
        let textAreatView = AppkeeTextAreaInputView(frame: .zero)
        textAreatView.configure(with: title, colorSettings: colorSettings, handler: handler)
        
        stackView.addArrangedSubview(textAreatView)
    }
    
    func addSelection(title: String, items: [AppkeeFieldItem], colorSettings: AppkeeColorSettings?, handler: @escaping (Int) -> Void) {
        let selectionView = AppkeeSelectionView(frame: .zero)
        selectionView.configure(with: title, items: items, colorSettings: colorSettings, handler: { (item) in
            handler(item.id)
        })
        
        stackView.addArrangedSubview(selectionView)
    }
    
    func addCheckBoxes(title: String, items: [AppkeeFieldItem], colorSettings: AppkeeColorSettings?, handler: @escaping (Int, Bool) -> Void) {
        let checkBoxes = AppkeeCheckboxesView(frame: .zero)
        checkBoxes.configure(with: title, items: items, colorSettings: colorSettings, handler: handler)
        
        stackView.addArrangedSubview(checkBoxes)
    }
    
    func addConsent(title: String, colorSettings: AppkeeColorSettings?) {
        let textView = AppkeeTextView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
        textView.configure(with: title, colorSettings: colorSettings)
        textView.delegate = self
        
        stackView.addArrangedSubview(textView)
    }
    
    func addSendButton(title: String, colorSettings: AppkeeColorSettings?, handler: @escaping () -> Void) {
        let sendView = AppkeeButtonView(frame: .zero)
        sendView.configure(with: title, colorSettings: colorSettings, handler: handler)
        
        stackView.addArrangedSubview(sendView)
    }
    
    func cleanViews() {
        hideProgress()
        
        for view in stackView.arrangedSubviews {
            view.removeFromSuperview()
            stackView.removeArrangedSubview(view)
        }
    }
    
    func display(_ displayModel: AppkeeForm.DisplayData.Sending) {
        showProgress(title: translations.form.SENDING)
    }
    
    func display(_ displayModel: AppkeeForm.DisplayData.Error) {
        hideProgress()
        
        showErrorAlert(withMessage: displayModel.message)
    }
    
    func display(_ displayModel: AppkeeForm.DisplayData.Send) {
        showAlert(withTitle: translations.form.SEND, okTitle: translations.common.OK)
    }
}

extension AppkeeFormViewController: AppkeeTextViewDelegate {
    func openLink(link: String, name: String) {
        presenter.handle(.openLink(link: link, name: name))
    }

    func openDeeplink(params: String) {

    }
}

//
//  AppkeeFormPresenter.swift
//  AppkeeCore
//
//  Created by Radek Zmeškal on 03/10/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeFormPresenter {
    // MARK: - Properties
    private let section: AppkeeSection
    private let graphicManager: AppkeeGraphicManager
    
    let interactor: AppkeeFormInteractorInput
    weak var coordinator: AppkeeFormCoordinatorInput?
    weak var output: AppkeeFormPresenterOutput?
    
    let fields: [AppkeeField]
    var values: [Int: Any] = [:]

    // MARK: - Init
    init(interactor: AppkeeFormInteractorInput, coordinator: AppkeeFormCoordinatorInput, graphicManager: AppkeeGraphicManager, section: AppkeeSection) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
        self.section = section
        
        self.fields = section.fields.sorted(by: { $0.order < $1.order })
    }
}

// MARK: - User Events -

extension AppkeeFormPresenter: AppkeeFormPresenterInput {
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }
        
        createViews()
    }

    func handle(_ action: AppkeeForm.Action) {
        switch action {
        case .send:
            self.send()
        case .openLink(link: let link, name: let name):
            coordinator?.navigate(to: .openLink(link: link, name: name))
        }
    }
    
    private func createViews() {
        for field in fields {
            switch field.type {
            case .title:
                output?.addTitle(title: field.name ?? "", colorSettings: graphicManager.colorsSettings)
            case .text:
                output?.addTextInput(title: field.name ?? "", colorSettings: graphicManager.colorsSettings, handler: { (value) in
                    self.values[field.id] = value
                })
            case .number:
                output?.addNumberInput(title: field.name ?? "", colorSettings: graphicManager.colorsSettings, handler: { (value) in
                    self.values[field.id] = value
                })
            case .textArea:
                output?.addTextAreaInput(title: field.name ?? "", colorSettings: graphicManager.colorsSettings, handler: { (value) in
                    self.values[field.id] = value
                })
            case .select:
                let items = section.fieldItems.filter({ $0.fieldId == field.id}).sorted(by: { $0.order < $1.order })
                
                output?.addSelection(title: field.name ?? "", items: items, colorSettings: graphicManager.colorsSettings, handler: { (value) in
                    self.values[field.id] = value
                })
            case .checkbox:
                let items = section.fieldItems.filter({ $0.fieldId == field.id}).sorted(by: { $0.order < $1.order })
                
                output?.addCheckBoxes(title: field.name ?? "", items: items, colorSettings: graphicManager.colorsSettings, handler: { (id, value) in
                    let array = self.values[field.id] ?? []
                    if var values = array as? [Int] {
                        if value {
                            values.append(id)
                        } else {
                            values.removeAll(where: { $0 == id})
                        }
                        
                        self.values[field.id] = values
                    }
                })
            }
        }

        if let gdpr = section.gdprText {
            self.output?.addConsent(title: gdpr, colorSettings: graphicManager.colorsSettings)
        }

        
        output?.addSendButton(title: section.confirm ?? "", colorSettings: graphicManager.colorsSettings, handler: { () in
            self.output?.display(AppkeeForm.DisplayData.Sending())
            self.send()
        })
    }
    
    private func send() {
        var message = ""
        
        for field in fields {
            switch field.type {
            case .title:
                break
            case .text, .textArea, .number:
                    if let text = values[field.id] as? String, !text.isEmpty {
                        message.append(String(format: "<p><strong>%@: %@</strong></p>", arguments: [field.name ?? "", text]))
                    } else if field.required {
                        self.showError(field: field)
                        return
                    } else {
                        message.append(String(format: "<p><strong>%@:</strong></p>", arguments: [field.name ?? ""]))
                    }
            case .select:
                    if let valueID = values[field.id] as? Int {
                        let fieldItem = section.fieldItems.first(where: { $0.id == valueID })
                        message.append(String(format: "<p><strong>%@: %@</strong></p>", arguments: [field.name ?? "", fieldItem?.name ?? ""]))
                    } else if field.required {
                        self.showError(field: field)
                        return
                    } else {
                        message.append(String(format: "<p><strong>%@:</strong></p>", arguments: [field.name ?? ""]))
                    }
            case .checkbox:
                if let values = values[field.id] as? [Int] {
                    var valueString = ""
                    
                    for valueID in values {
                        if let fieldItem = section.fieldItems.first(where: { $0.id == valueID }) {
                            valueString += ", "
                            valueString += fieldItem.name ?? ""
                        }
                    }
                    
                    let index = valueString.index(after: message.startIndex)
                    valueString = String(valueString.suffix(from: index))
                
                    message.append(String(format: "<p><strong>%@: %@</strong></p>", arguments: [field.name ?? "", valueString]))
                } else if field.required {
                    self.showError(field: field)
                    return
                } else {
                    message.append(String(format: "<p><strong>%@:</strong></p>", arguments: [field.name ?? ""]))
                }
            }
        }
        
        interactor.perform(AppkeeForm.Request.Send(sectionId: section.id, message: message))
    }
    
    private func showError(field: AppkeeField) {
        output?.display(AppkeeForm.DisplayData.Error(message: translations.form.ERROR + (field.name ?? "")))
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeFormPresenter: AppkeeFormInteractorOutput {
    func present(_ response: AppkeeForm.Response.Error) {
        func present(_ response: AppkeeRSS.Response.Error) {
            output?.display(AppkeeForm.DisplayData.Error(message: response.message))
        }
    }
    
    func present(_ response: AppkeeForm.Response.Send) {
        values = [:]
        output?.cleanViews()
        self.createViews()
        output?.display(AppkeeForm.DisplayData.Send())
    }
}

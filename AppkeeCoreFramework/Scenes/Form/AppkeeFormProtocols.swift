//
//  AppkeeFormProtocols.swift
//  AppkeeCore
//
//  Created by Radek Zmeškal on 03/10/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

// PRESENTER -> COORDINATOR
protocol AppkeeFormCoordinatorInput: class {
    func navigate(to route: AppkeeForm.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeFormInteractorInput {
    func perform(_ request: AppkeeForm.Request.Send)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeFormInteractorOutput: class {
    func present(_ response: AppkeeForm.Response.Send)
    func present(_ response: AppkeeForm.Response.Error)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeFormPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeeForm.Action)
}

// PRESENTER -> VIEW
protocol AppkeeFormPresenterOutput: class {
    func addTitle(title: String, colorSettings: AppkeeColorSettings?)
    func addTextInput(title: String, colorSettings: AppkeeColorSettings?, handler: @escaping (String) -> Void)
    func addNumberInput(title: String, colorSettings: AppkeeColorSettings?, handler: @escaping (String) -> Void)
    func addTextAreaInput(title: String, colorSettings: AppkeeColorSettings?, handler: @escaping (String) -> Void)
    
    func addSelection(title: String, items: [AppkeeFieldItem], colorSettings: AppkeeColorSettings?, handler: @escaping (Int) -> Void)
    func addCheckBoxes(title: String, items: [AppkeeFieldItem], colorSettings: AppkeeColorSettings?, handler: @escaping (Int, Bool) -> Void)
    
    func addConsent(title: String, colorSettings: AppkeeColorSettings?)
    
    func addSendButton(title: String, colorSettings: AppkeeColorSettings?, handler: @escaping () -> Void)
    
    func cleanViews()
    
    func display(_ displayModel: AppkeeForm.DisplayData.Error)
    func display(_ displayModel: AppkeeForm.DisplayData.Sending)
    func display(_ displayModel: AppkeeForm.DisplayData.Send)
    
    func setupUI(colorSettings: AppkeeColorSettings)
}

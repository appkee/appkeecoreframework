//
//  AppkeeFormInteractor.swift
//  AppkeeCore
//
//  Created by Radek Zmeškal on 03/10/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import FirebaseAnalytics

class AppkeeFormInteractor {
    // MARK: - Properties
    weak var output: AppkeeFormInteractorOutput?
    private let repository: AppkeeRepository
    private let graphicManager: AppkeeGraphicManager
    
    // MARK: - Init
    init(repository: AppkeeRepository, graphicManager: AppkeeGraphicManager) {
        self.repository = repository
        self.graphicManager = graphicManager
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeFormInteractor: AppkeeFormInteractorInput {
    func perform(_ request: AppkeeForm.Request.Send) {
        repository.sendForm(sectionId: request.sectionId, message: request.message) { [weak self] response in
            guard let self = self else { return }
            
            switch response.result {
            case .success(let response):
                if response.success {
                    self.output?.present(AppkeeForm.Response.Send())
                } else {
                    self.output?.present(AppkeeForm.Response.Error(message: response.error ?? translations.error.UNKNOWN))
                }
            case .failure(let error):
                self.output?.present(AppkeeForm.Response.Error(message: error.localizedDescription))
                
                Analytics.logEvent("Form sending failed", parameters: ["sectionId": request.sectionId, "message": request.sectionId, "error": error.localizedDescription])
            }
        }
    }
}

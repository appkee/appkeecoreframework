//
//  AppkeeSeniorPassBenefitsProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/24/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeeSeniorPassBenefitsCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeePassBenefitsCoordinatorInput: class {
    func navigate(to route: AppkeePassBenefits.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeePassBenefitsInteractorInput {
    func perform(_ request: AppkeePassBenefits.Request.Categories)
    func perform(_ request: AppkeePassBenefits.Request.Branches)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeePassBenefitsInteractorOutput: class {
    // func present(_ response: AppkeeSeniorPassBenefits.Response.Work)
    func present(_ response: AppkeePassBenefits.Response.Categories)
    func present(_ response: AppkeePassBenefits.Response.Error)
    func present(_ response: AppkeePassBenefits.Response.SeniorBranches)
    func present(_ response: AppkeePassBenefits.Response.FamilyBranches)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeePassBenefitsPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeePassBenefits.Action)
}

// PRESENTER -> VIEW
protocol AppkeePassBenefitsPresenterOutput: class {
    // func display(_ displayModel: AppkeeSeniorPassBenefits.DisplayData.Work)

    func display(_ displayModel: AppkeePassBenefits.DisplayData.Loading)
    func display(_ displayModel: AppkeePassBenefits.DisplayData.Regions)
    func display(_ displayModel: AppkeePassBenefits.DisplayData.SelectRegion)
    func display(_ displayModel: AppkeePassBenefits.DisplayData.Districts)
    func display(_ displayModel: AppkeePassBenefits.DisplayData.SelectDistrict)
    func display(_ displayModel: AppkeePassBenefits.DisplayData.Error)
    func display(_ displayModel: AppkeePassBenefits.DisplayData.Location)
    func display(_ displayModel: AppkeePassBenefits.DisplayData.NoData)

    func addCategory(id: Int, title: String, colorSettings: AppkeeColorSettings?, handler: @escaping (Bool) -> Void)

    func setupUI(colorSettings: AppkeeColorSettings)
}

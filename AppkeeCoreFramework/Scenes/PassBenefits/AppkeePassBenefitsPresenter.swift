//
//  AppkeeSeniorPassBenefitsPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/24/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import CoreLocation

class AppkeePassBenefitsPresenter: NSObject {
    
    // MARK: - Properties
    private let graphicManager: AppkeeGraphicManager
    private let locationManager = CLLocationManager()

    let interactor: AppkeePassBenefitsInteractorInput
    weak var coordinator: AppkeePassBenefitsCoordinatorInput?
    weak var output: AppkeePassBenefitsPresenterOutput?

    private var regions: [AppkeeSeniorPassInfoRegion] = []
    private var regionIndex: Int = 0
    private var districts: [AppkeeSeniorPassInfoCategory] = []
    private var districtIndex: Int = 0

    private var selectedCategories: [AppkeeSeniorPassInfoCategory] = []

    private var seniorBranches: [Int: [AppkeeSeniorPassBranch]] = [:]
    private var familyBranches: [Int: [AppkeeFamilyPassBranch]] = [:]

    private var performSearch = false
    private var searchText: String?

    private let passType: PassType

    // MARK: - Init
    init(interactor: AppkeePassBenefitsInteractorInput, coordinator: AppkeePassBenefitsCoordinatorInput, graphicManager: AppkeeGraphicManager, passType: PassType) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
        self.passType = passType
    }

    func getLocation() -> (CLLocationCoordinate2D?) {

        guard let location = locationManager.location else {
            return nil
        }

        let locValue:CLLocationCoordinate2D = location.coordinate

        return (locValue)
    }

    private func filterBranches(text: String?, seniorBranches: [AppkeeSeniorPassBranch]) {
        var branches = seniorBranches

        let districtID = districts[districtIndex].id
        if districtID != 0 {
            branches = branches.filter { return $0.district == districtID }
        }

        var updatedBranches = branches.filter { (branch) -> Bool in
            var textFound = false
            if let text = text {
                if text.isEmpty {
                    textFound = true
                } else if branch.name.unaccent().uppercased().contains(text.unaccent().uppercased()) {
                    textFound = true
                } else if branch.description.unaccent().uppercased().contains(text.unaccent().uppercased()) {
                    textFound = true
                } else if branch.city.unaccent().uppercased().contains(text.unaccent().uppercased()) {
                    textFound = true
                } else if branch.address.unaccent().uppercased().contains(text.unaccent().uppercased()) {
                    textFound = true
                }
            }
            if self.selectedCategories.count == 0 {
                return textFound
            } else if self.selectedCategories.contains(where: { $0.id == branch.category }) {
                return textFound && true
            }

            return false
        }

        if let location = getLocation() {
            let coordinate = CLLocation(latitude: location.latitude, longitude: location.longitude)

            updatedBranches = updatedBranches.map{
                var branch = $0
                if let lat = branch.location?.lat, let lon = branch.location?.lon {
                    branch.distance = CLLocation(latitude: lat, longitude: lon).distance(from: coordinate)
                }
                return branch
            }

            updatedBranches = updatedBranches.sorted { $0.distance ?? 0.0 < $1.distance ?? 0.0 }
        }

        output?.display(AppkeePassBenefits.DisplayData.Loading(isFinished: true))

        if updatedBranches.count == 0 {
            output?.display(AppkeePassBenefits.DisplayData.NoData())

            return
        }

        coordinator?.navigate(to: .showResults(data: .senior(updatedBranches)))

        print(">>> \(updatedBranches.count)")
    }

    private func filterBranches(text: String?, familyBranches: [AppkeeFamilyPassBranch]) {
        var branches = familyBranches

        let districtID = districts[districtIndex].id
        if districtID != 0 {
            branches = branches.filter { return $0.district_id == districtID }
        }

        var updatedBranches = branches.filter { (branch) -> Bool in
            var textFound = false
            if let text = text {
                if text.isEmpty {
                    textFound = true
                } else if branch.company_name.unaccent().uppercased().contains(text.unaccent().uppercased()) {
                    textFound = true
                } else if let description = branch.description, description.unaccent().uppercased().contains(text.unaccent().uppercased()) {
                    textFound = true
                } else if branch.city.unaccent().uppercased().contains(text.unaccent().uppercased()) {
                    textFound = true
                } else if branch.street_one.unaccent().uppercased().contains(text.unaccent().uppercased()) {
                    textFound = true
                }
            }
            if self.selectedCategories.count == 0 {
                return textFound
            } else if self.selectedCategories.contains(where: { $0.id == branch.category_id }) {
                return textFound && true
            }

            return false
        }

        if let location = getLocation() {
            let coordinate = CLLocation(latitude: location.latitude, longitude: location.longitude)

            updatedBranches = updatedBranches.map{
                var branch = $0
                branch.distance = CLLocation(latitude: branch.lat, longitude: branch.lng).distance(from: coordinate)
                return branch
            }

            updatedBranches = updatedBranches.sorted { $0.distance ?? 0.0 < $1.distance ?? 0.0 }
        }

        output?.display(AppkeePassBenefits.DisplayData.Loading(isFinished: true))

        if updatedBranches.count == 0 {
            output?.display(AppkeePassBenefits.DisplayData.NoData())

            return
        }

        coordinator?.navigate(to: .showResults(data: .family(updatedBranches)))

        print(">>> \(updatedBranches.count)")
    }


    private func setupCategories(categories: AppkeeSeniorPassInfo) {
//        self.regions = [AppkeeSeniorPassInfoCategory(id: 0, name: "_All")]
//        regions.append(contentsOf: categories.regions)

        self.regions = categories.regions

        switch passType {
        case .senior:
            let regionID = UserDefaults.seniorPassLogin?.region ?? 0
            self.regionIndex = regions.firstIndex { $0.id == regionID } ?? 0
        case .family:
            let regionID = Int(UserDefaults.familyPassLogin?.region ?? "0")
            self.regionIndex = regions.firstIndex { $0.id == regionID } ?? 0
        }

        output?.display(AppkeePassBenefits.DisplayData.Regions(data: regions))

        let region = self.regions[self.regionIndex]
        output?.display(AppkeePassBenefits.DisplayData.SelectRegion(index: self.regionIndex, region: region))

        self.districts = [AppkeeSeniorPassInfoCategory(id: 0, name: translations.seniorPass.BRANCHES_DISTRICT_ALL)]
        self.districts.append(contentsOf: region.districts)

        output?.display(AppkeePassBenefits.DisplayData.Districts(data: self.districts))
        output?.display(AppkeePassBenefits.DisplayData.SelectDistrict(index: self.districtIndex, district: districts[self.districtIndex]))

        for category in categories.categories {
            output?.addCategory(id: category.id, title: category.name, colorSettings: self.graphicManager.colorsSettings, handler: { [weak self] (isOn) in
                guard let self = self else { return }

                if isOn {
                    self.selectedCategories.append(category)
                } else {
                    if let index = self.selectedCategories.firstIndex(where: { $0.id == category.id }) {
                        self.selectedCategories.remove(at: index)
                    }
                }
            })
        }
    }
}

// MARK: - User Events -

extension AppkeePassBenefitsPresenter: AppkeePassBenefitsPresenterInput {
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }
        
        output?.display(AppkeePassBenefits.DisplayData.Loading(isFinished: false))

        switch self.passType {
        case .senior:
            if let date = UserDefaults.seniorPassCategoriesDate, Calendar.current.isDate(date, inSameDayAs: Date()), let categories = UserDefaults.seniorPassCategories {
                self.setupCategories(categories: categories)
            } else {
                self.interactor.perform(AppkeePassBenefits.Request.Categories(passType: .senior))
            }
        case .family:
            if let date = UserDefaults.familyPassCategoriesDate, Calendar.current.isDate(date, inSameDayAs: Date()), let categories = UserDefaults.familyPassCategories {
                self.setupCategories(categories: categories)
            } else {
                self.interactor.perform(AppkeePassBenefits.Request.Categories(passType: .family))
            }
        }

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.delegate = self

        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation

            self.updateLocalizationStatus()
        }

        if let date = UserDefaults.seniorPassBranchesDate, Calendar.current.isDate(date, inSameDayAs: Date()), let branches = UserDefaults.seniorPassBranches {
            self.seniorBranches = branches
        }

        if let date = UserDefaults.familyPassBranchesDate, Calendar.current.isDate(date, inSameDayAs: Date()), let branches = UserDefaults.familyPassBranches {
            self.familyBranches = branches
        }

        output?.display(AppkeePassBenefits.DisplayData.Loading(isFinished: true))
    }

    func handle(_ action: AppkeePassBenefits.Action) {
        switch action {
        case .region(let region):
            self.regionIndex = regions.firstIndex { $0.id == region.id } ?? 0
            self.districtIndex = 0

            let region = self.regions[self.regionIndex]
            output?.display(AppkeePassBenefits.DisplayData.SelectRegion(index: self.regionIndex, region: region))

            self.districts = [AppkeeSeniorPassInfoCategory(id: 0, name: translations.seniorPass.BRANCHES_DISTRICT_ALL)]
            self.districts.append(contentsOf: region.districts)

            output?.display(AppkeePassBenefits.DisplayData.Districts(data: self.districts))
            output?.display(AppkeePassBenefits.DisplayData.SelectDistrict(index: self.districtIndex, district: districts[self.districtIndex]))
            
        case .search(let text):
            output?.display(AppkeePassBenefits.DisplayData.Loading(isFinished: false))

            let regionID = self.regions[regionIndex].id
            switch passType {
            case .senior:
                if let branches = seniorBranches[regionID] {
                    self.filterBranches(text: text, seniorBranches: branches)
                } else {
                    self.performSearch = true
                    self.searchText = text
                    interactor.perform(AppkeePassBenefits.Request.Branches(regionID: regionID, passType: self.passType))
                }
            case .family:
                if let branches = familyBranches[regionID] {
                    self.filterBranches(text: text, familyBranches: branches)
                } else {
                    self.performSearch = true
                    self.searchText = text
                    interactor.perform(AppkeePassBenefits.Request.Branches(regionID: regionID, passType: self.passType))
                }
            }
        case .district(let district):
            self.districtIndex = self.districts.firstIndex { $0.id == district.id } ?? 0

            output?.display(AppkeePassBenefits.DisplayData.SelectDistrict(index: self.districtIndex, district: self.districts[self.districtIndex]))
        }
    }

    private func updateLocalizationStatus() {
        switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                output?.display(AppkeePassBenefits.DisplayData.Location(text: translations.seniorPass.DISTANCE_DEFAULT_TEXT))
            case .authorizedAlways, .authorizedWhenInUse:
                output?.display(AppkeePassBenefits.DisplayData.Location(text: translations.seniorPass.DISTANCE_TEXT_FOUND))
            @unknown default:
            break
        }
    }
}

extension AppkeePassBenefitsPresenter: CLLocationManagerDelegate {
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        self.updateLocalizationStatus()
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeePassBenefitsPresenter: AppkeePassBenefitsInteractorOutput {
    func present(_ response: AppkeePassBenefits.Response.FamilyBranches) {
        self.familyBranches[response.regionID] = response.data

        UserDefaults.familyPassBranches = self.familyBranches
        UserDefaults.familyPassBranchesDate = Date()

        if self.performSearch {
            self.filterBranches(text: self.searchText, familyBranches: self.familyBranches[response.regionID] ?? [])
        } else {
            output?.display(AppkeePassBenefits.DisplayData.Loading(isFinished: true))
        }
    }

    func present(_ response: AppkeePassBenefits.Response.SeniorBranches) {
        self.seniorBranches[response.regionID] = response.data

        UserDefaults.seniorPassBranches = self.seniorBranches
        UserDefaults.seniorPassBranchesDate = Date()

        if self.performSearch {
            self.filterBranches(text: self.searchText, seniorBranches: self.seniorBranches[response.regionID] ?? [])
        } else {
            output?.display(AppkeePassBenefits.DisplayData.Loading(isFinished: true))
        }
    }

    func present(_ response: AppkeePassBenefits.Response.Categories) {
        print("\(response.data)")

        self.setupCategories(categories: response.data)

        UserDefaults.seniorPassCategories = response.data
        UserDefaults.seniorPassCategoriesDate = Date()

        output?.display(AppkeePassBenefits.DisplayData.Loading(isFinished: true))
    }

    func present(_ response: AppkeePassBenefits.Response.Error) {
        self.performSearch = false
        output?.display(AppkeePassBenefits.DisplayData.Error(message: response.message))
    }
}

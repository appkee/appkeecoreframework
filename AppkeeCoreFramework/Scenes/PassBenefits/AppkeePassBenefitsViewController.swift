//
//  AppkeeSeniorPassBenefitsViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/24/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import UIKit

class SelectionTextField: UITextField {
    override func caretRect(for position: UITextPosition) -> CGRect {
        return .zero
    }

    override func addGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer) {

         if gestureRecognizer.isKind(of: UILongPressGestureRecognizer.self) {
                  gestureRecognizer.isEnabled = false
         }
        return super.addGestureRecognizer(gestureRecognizer)
       }
}

class AppkeePassBenefitsViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.text = translations.seniorPass.BRANCHES_SEARCH_TITLE
        }
    }
    @IBOutlet weak var nameTextField: UITextField!

    @IBOutlet weak var regionLabel: UILabel! {
        didSet {
            regionLabel.text = translations.seniorPass.BRANCHES_REGION_TITLE
        }
    }
    @IBOutlet weak var regionTextField: SelectionTextField! {
        didSet {
            regionTextField.delegate = self
            regionTextField.inputView = self.regionPickerView
        }
    }

    @IBOutlet weak var districtLabel: UILabel! {
        didSet {
            districtLabel.text = translations.seniorPass.BRANCHES_DISTRICT_TITLE
        }
    }

    @IBOutlet weak var districtTextField: SelectionTextField! {
        didSet {
            districtTextField.delegate = self
            districtTextField.inputView = self.districtPickerView
        }
    }



    @IBOutlet weak var categoryLabel: UILabel! {
        didSet {
            categoryLabel.text = translations.seniorPass.BRANCHES_CATEGORIES_TITLE
        }
    }
    @IBOutlet weak var categoryStackView: UIStackView!

    @IBOutlet weak var locationLabel: UILabel! {
        didSet {
            locationLabel.text = translations.seniorPass.DISTANCE_DEFAULT_TEXT
        }
    }
    @IBOutlet weak var searchButton: UIButton! {
        didSet {
            searchButton.setTitle(translations.seniorPass.BRANCHES_SEARCH.uppercased(), for: .normal)
        }
    }

    private lazy var regionPickerView: SenioPassRegionPickerView = {
        let picker = SenioPassRegionPickerView()
        picker.handler = { [weak self] (category) in
            guard let self = self else { return }

            self.presenter.handle(.region(region: category))
        }
        return picker
    }()

    private lazy var districtPickerView: SenioPassDistrictPickerView = {
        let picker = SenioPassDistrictPickerView()
        picker.handler = { [weak self] (district) in
            guard let self = self else { return }

            self.presenter.handle(.district(district: district))
        }
        return picker
    }()
    

    // MARK: - Properties
    private var presenter: AppkeePassBenefitsPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeePassBenefitsPresenterInput) -> AppkeePassBenefitsViewController {
        let name = "\(AppkeePassBenefitsViewController.self)"
        let bundle = Bundle(for: AppkeePassBenefitsViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeePassBenefitsViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
        
    }

    // MARK: - Callbacks -
    @IBAction func searchButtonTap(_ sender: Any) {
        self.presenter.handle(.search(name: self.nameTextField.text))
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeePassBenefitsViewController: AppkeePassBenefitsPresenterOutput, Alertable, Progressable {
    func display(_ displayModel: AppkeePassBenefits.DisplayData.NoData) {
        self.showAlert(withTitle: translations.seniorPass.BRANCHES_NO_ITEMS, okTitle: translations.common.OK)
    }

    func display(_ displayModel: AppkeePassBenefits.DisplayData.Districts) {
        self.districtPickerView.inputs = displayModel.data
    }

    func display(_ displayModel: AppkeePassBenefits.DisplayData.SelectDistrict) {
        self.districtPickerView.selectRow(displayModel.index, inComponent: 0, animated: false)
        self.districtTextField.text = displayModel.district.name
    }

    func display(_ displayModel: AppkeePassBenefits.DisplayData.Loading) {
        if displayModel.isFinished {
            self.hideProgress()
        } else {
            self.showProgress()
        }
    }

    func display(_ displayModel: AppkeePassBenefits.DisplayData.Location) {
        self.locationLabel.text = displayModel.text
    }

    func display(_ displayModel: AppkeePassBenefits.DisplayData.SelectRegion) {
        self.regionPickerView.selectRow(displayModel.index, inComponent: 0, animated: false)
        self.regionTextField.text = displayModel.region.name
    }

    func display(_ displayModel: AppkeePassBenefits.DisplayData.Regions) {
        self.regionPickerView.inputs = displayModel.data
    }

    func addCategory(id: Int, title: String, colorSettings: AppkeeColorSettings?, handler: @escaping (Bool) -> Void) {
        let categoryView = AppkeeCategoryView(frame: .zero)
        categoryView.configure(with: title, id: id, colorSettings: colorSettings, handler: handler)

        categoryStackView.addArrangedSubview(categoryView)
    }

    func display(_ displayModel: AppkeePassBenefits.DisplayData.Error) {
        hideProgress()

        showErrorAlert(withMessage: displayModel.message)
    }

    func setupUI(colorSettings: AppkeeColorSettings) {
        self.view.backgroundColor = colorSettings.content

        self.searchButton.setTitleColor(.white, for: .normal)
        self.searchButton.backgroundColor = colorSettings.loginColor
    }
}

extension AppkeePassBenefitsViewController: UITextFieldDelegate {
//    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
//        return false
//    }
    override open func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        guard inputView != nil else { return super.canPerformAction(action, withSender: sender) }

        return action == #selector(UIResponderStandardEditActions.paste(_:)) ?
            false : super.canPerformAction(action, withSender: sender)
    }
}

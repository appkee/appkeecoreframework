//
//  AppkeeSeniorPassBenefitsModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/24/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeePassBenefits {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case region(region: AppkeeSeniorPassInfoRegion)
        case district(district: AppkeeSeniorPassInfoCategory)
        case search(name: String?)
    }

    enum Route {
        case openAdvertisementLink(link: String)
        case openDeeplink(params: String)
        case showResults(data: AppkeePassResult)
    }

    enum AppkeePassResult {
        case senior([AppkeeSeniorPassBranch])
        case family([AppkeeFamilyPassBranch])
    }
}

extension AppkeePassBenefits.Request {
    struct Categories {
        let passType: PassType
    }

    struct Branches {
        let regionID: Int
        let passType: PassType
    }
}

extension AppkeePassBenefits.Response {
    struct Categories {
        let data: AppkeeSeniorPassInfo
    }

    struct SeniorBranches {
        let regionID: Int
        let data: [AppkeeSeniorPassBranch]
    }

    struct FamilyBranches {
        let regionID: Int
        let data: [AppkeeFamilyPassBranch]
    }

    struct Error {
        let message: String
    }
}

extension AppkeePassBenefits.DisplayData {
    struct Regions {
        let data: [AppkeeSeniorPassInfoRegion]
    }

    struct Districts {
        let data: [AppkeeSeniorPassInfoCategory]
    }

    struct SelectRegion {
        let index: Int
        let region: AppkeeSeniorPassInfoRegion
    }

    struct SelectDistrict {
        let index: Int
        let district: AppkeeSeniorPassInfoCategory
    }

    struct Location {
        let text: String
    }

    struct Error {
        let message: String
    }

    struct Loading {
        let isFinished: Bool
    }

    struct NoData {
        
    }
}

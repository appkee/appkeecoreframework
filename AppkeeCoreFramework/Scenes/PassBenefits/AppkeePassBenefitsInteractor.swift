//
//  AppkeeSeniorPassBenefitsInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/24/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import FirebaseAnalytics
import Alamofire

class AppkeePassBenefitsInteractor {
    // MARK: - Properties
    weak var output: AppkeePassBenefitsInteractorOutput?
    private let repository: AppkeeRepository
    private let graphicManager: AppkeeGraphicManager

    // MARK: - Init
    init(repository: AppkeeRepository, graphicManager: AppkeeGraphicManager) {
        self.repository = repository
        self.graphicManager = graphicManager
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeePassBenefitsInteractor: AppkeePassBenefitsInteractorInput {

    private func processCategoriesResponse(response: DataResponse<AppkeeSeniorPassInfoResponse>) {
        switch response.result {
        case .success(let response):
            if response.success, let data = response.data {
                self.output?.present(AppkeePassBenefits.Response.Categories(data: data))
            } else {
                self.output?.present(AppkeePassBenefits.Response.Error(message: response.error ?? translations.error.UNKNOWN))
            }
            return
        case .failure(let error):
            self.output?.present(AppkeePassBenefits.Response.Error(message: error.localizedDescription))
            Analytics.logEvent("Loading senior pass categories", parameters: [ "error": error.localizedDescription])
        }
    }

    func perform(_ request: AppkeePassBenefits.Request.Categories) {

        switch request.passType {
        case .senior:
            repository.seniorPassCategories() { [weak self] response in
                guard let self = self else { return }

                self.processCategoriesResponse(response: response)
            }
        case .family:
            repository.familyPassCategories() { [weak self] response in
                guard let self = self else { return }

                self.processCategoriesResponse(response: response)
            }
        }
    }

    private func processSeniorBranchesResponse(regionID: Int, response: DataResponse<AppkeeSeniorPassBranchesResponse>) {
        switch response.result {
        case .success(let response):
            if response.success, let data = response.data {
                self.output?.present(AppkeePassBenefits.Response.SeniorBranches(regionID: regionID, data: data.branches))
            } else {
                self.output?.present(AppkeePassBenefits.Response.Error(message: response.error ?? translations.error.UNKNOWN))
            }
            return
        case .failure(let error):
            self.output?.present(AppkeePassBenefits.Response.Error(message: error.localizedDescription))

            Analytics.logEvent("Loading branches failed", parameters: ["regionID": regionID, "error": error.localizedDescription])
        }
    }

    private func processFamilyBranchesResponse(regionID: Int, response: DataResponse<AppkeeFamilyPassBranchesResponse>) {
        switch response.result {
        case .success(let response):
            if response.success, let data = response.data {
                self.output?.present(AppkeePassBenefits.Response.FamilyBranches(regionID: regionID, data: data.branches))
            } else {
                self.output?.present(AppkeePassBenefits.Response.Error(message: response.error ?? translations.error.UNKNOWN))
            }
            return
        case .failure(let error):
            self.output?.present(AppkeePassBenefits.Response.Error(message: error.localizedDescription))

            Analytics.logEvent("Loading branches failed", parameters: ["regionID": regionID, "error": error.localizedDescription])
        }
    }

    func perform(_ request: AppkeePassBenefits.Request.Branches) {

        switch request.passType {
        case .senior:
            repository.seniorPassBranches(regionID: request.regionID) { [weak self] (response) in
                guard let self = self else { return }

                self.processSeniorBranchesResponse(regionID: request.regionID, response: response)
            }
        case .family:
            repository.familyPassBranches(regionID: request.regionID) { [weak self] response in
                guard let self = self else { return }

                self.processFamilyBranchesResponse(regionID: request.regionID, response: response)
            }
        }


    }
}

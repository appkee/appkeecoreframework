//
//  AppkeeLoginRegisterCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/2/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeeLoginRegisterCoordinator: AppkeeCoordinator {
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
//    weak var delegate: AppkeeLoginCoordinatorDelegate?

    weak var delegate: AppkeeCoordinatorDelegate?

    private let window: UIWindow

    private let appDescription: AppkeeAppDescription?

    // MARK: - Init
    init(window: UIWindow, dependencies: AppkeeFullDependencies) {
        self.dependencies = dependencies
        self.window = window

        self.navigationController = UINavigationController()
        self.navigationController.view.backgroundColor = .white

        if let colorsSettings = dependencies.graphicManager.colorsSettings {
            if #available(iOS 15, *) {
                let appearance = UINavigationBarAppearance()
                appearance.configureWithTransparentBackground()
                appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: colorsSettings.loginColor]
                UINavigationBar.appearance().standardAppearance = appearance
                UINavigationBar.appearance().scrollEdgeAppearance = appearance
                UINavigationBar.appearance().tintColor = colorsSettings.loginColor
            } else {
                self.navigationController.navigationBar.tintColor = colorsSettings.loginColor
                self.navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: colorsSettings.loginColor]

                self.navigationController.navigationBar.setBackgroundImage(UIImage(), for:.default)
                self.navigationController.navigationBar.shadowImage = UIImage()
                self.navigationController.navigationBar.layoutIfNeeded()
            }
        }

        self.navigationController.navigationBar.isHidden = true

        self.appDescription = dependencies.appDescriptionManager.readAppDescription()
    }

    func start(root: Bool = false) {
        let interactor = AppkeeLoginRegisterInteractor(repository: dependencies.repository,
                                          graphicManager: dependencies.graphicManager)
        let presenter = AppkeeLoginRegisterPresenter(
            interactor: interactor,
            coordinator: self,
            graphicManager: dependencies.graphicManager,
            configManager: dependencies.configManager,
            descriptionText: appDescription?.loginText,
            imageName: appDescription?.loadingImage
        )
        let vc = AppkeeLoginRegisterViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        navigationController.setViewControllers([vc], animated: false)

        window.rootViewController = navigationController
    }

    private func showLogin() {
        let coordinator = AppkeeLoginRegisterLoginCoordinator(navigationController: navigationController,
                                          dependencies: dependencies)
        childrens.append(coordinator)
        coordinator.start(root: false)
        coordinator.delegate = self.delegate
    }

    private func showRegister() {
        let coordinator = AppkeeLoginRegisterRegisterCoordinator(navigationController: navigationController,
                                                                 dependencies: dependencies,
                                                                 style: .standart)
        childrens.append(coordinator)
        coordinator.start(root: false)
    }
}
// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeLoginRegisterCoordinator: AppkeeLoginRegisterCoordinatorInput {
    func navigate(to route: AppkeeLoginRegister.Route) {
        switch route {
        case .menu:
            self.delegate?.navigate(to: .menu)
        case .registration:
            showRegister()
        case .login:
            showLogin()
        }
    }
}

//
//  AppkeeLoginRegisterPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/2/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import FirebaseMessaging

class AppkeeLoginRegisterPresenter {
    // MARK: - Properties
    private let graphicManager: AppkeeGraphicManager
    private let configManager:  AppkeeConfigManager
    private let descriptionText: String?
    private let imageName: String?

    let interactor: AppkeeLoginRegisterInteractorInput
    weak var coordinator: AppkeeLoginRegisterCoordinatorInput?
    weak var output: AppkeeLoginRegisterPresenterOutput?

    // MARK: - Init
    init(interactor: AppkeeLoginRegisterInteractorInput, coordinator: AppkeeLoginRegisterCoordinatorInput, graphicManager: AppkeeGraphicManager, configManager:  AppkeeConfigManager, descriptionText: String?, imageName: String?) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
        self.configManager = configManager
        self.descriptionText = descriptionText
        self.imageName = imageName
    }
}

// MARK: - User Events -

extension AppkeeLoginRegisterPresenter: AppkeeLoginRegisterPresenterInput {
    func viewCreated() {
        output?.display(title: descriptionText, image: graphicManager.getImage(name: self.imageName))

        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }
    }

    func handle(_ action: AppkeeLoginRegister.Action) {
        switch action {
        case .noLogin:
            coordinator?.navigate(to: .menu)
        case .login:
            coordinator?.navigate(to: .login)
        case .registration:
            coordinator?.navigate(to: .registration)
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeLoginRegisterPresenter: AppkeeLoginRegisterInteractorOutput {

}

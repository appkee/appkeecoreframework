//
//  AppkeeLoginRegisterPasswordProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/20/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeeLoginRegisterPasswordCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeLoginRegisterPasswordCoordinatorInput: class {
    func navigate(to route: AppkeeLoginRegisterPassword.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeLoginRegisterPasswordInteractorInput {
     func perform(_ request: AppkeeLoginRegisterPassword.Request.Email)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeLoginRegisterPasswordInteractorOutput: class {
    func present(_ response: AppkeeLoginRegisterPassword.Response.Email)
    func present(_ response: AppkeeLoginRegisterPassword.Response.Error)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeLoginRegisterPasswordPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeeLoginRegisterPassword.Action)
}

// PRESENTER -> VIEW
protocol AppkeeLoginRegisterPasswordPresenterOutput: class {
    func display( image: UIImage?)

    func display(_ displayModel: AppkeeLoginRegisterPassword.DisplayData.Progress)
    func display(_ displayModel: AppkeeLoginRegisterPassword.DisplayData.HideProgress)
    func display(_ displayModel: AppkeeLoginRegisterPassword.DisplayData.Error)
    func display(_ displayModel: AppkeeLoginRegisterPassword.DisplayData.Success)

    func display(_ displayModel: AppkeeLoginRegisterPassword.DisplayData.EmailValidation)

    func setupUI(colorSettings: AppkeeColorSettings)
}

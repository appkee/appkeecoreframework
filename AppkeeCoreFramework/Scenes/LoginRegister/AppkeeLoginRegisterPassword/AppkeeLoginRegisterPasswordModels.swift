//
//  AppkeeLoginRegisterPasswordModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/20/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeLoginRegisterPassword {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case send(email: String?)
        case close
    }

    enum Route {
        case close
    }
}

extension AppkeeLoginRegisterPassword.Request {
    struct Email {
        let email: String
        let appCode: String
    }
}

extension AppkeeLoginRegisterPassword.Response {
    struct Email {
        let message: String
    }

    struct Error {
        let message: String
    }
}

extension AppkeeLoginRegisterPassword.DisplayData {
    struct Progress {

    }

    struct HideProgress {

    }

    struct Success {
        let message: String
    }

    struct Error {
        let message: String
    }

    struct EmailValidation {
        let errorMessage: String?
    }
}

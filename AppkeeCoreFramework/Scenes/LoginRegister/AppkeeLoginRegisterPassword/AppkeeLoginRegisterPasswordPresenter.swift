//
//  AppkeeLoginRegisterPasswordPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/20/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeLoginRegisterPasswordPresenter {
    // MARK: - Properties
    private let graphicManager: AppkeeGraphicManager
    private let configManager:  AppkeeConfigManager
    private let imageName: String?

    let interactor: AppkeeLoginRegisterPasswordInteractorInput
    weak var coordinator: AppkeeLoginRegisterPasswordCoordinatorInput?
    weak var output: AppkeeLoginRegisterPasswordPresenterOutput?

    // MARK: - Init
    init(interactor: AppkeeLoginRegisterPasswordInteractorInput, coordinator: AppkeeLoginRegisterPasswordCoordinatorInput, graphicManager: AppkeeGraphicManager, configManager:  AppkeeConfigManager, imageName: String?) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
        self.configManager = configManager
        self.imageName = imageName
    }
}

// MARK: - User Events -

extension AppkeeLoginRegisterPasswordPresenter: AppkeeLoginRegisterPasswordPresenterInput {
    func viewCreated() {
        output?.display(image: graphicManager.getImage(name: self.imageName))

        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }
    }

    func handle(_ action: AppkeeLoginRegisterPassword.Action) {
        switch action {
        case .send(let email):
            if let email = email, !email.isEmpty{
                if !email.isValidEmail() {
                    output?.display(AppkeeLoginRegisterPassword.DisplayData.EmailValidation(errorMessage: translations.registePage.ERROR_INVALID_EMAIL))
                    return
                } else {
                    output?.display(AppkeeLoginRegisterPassword.DisplayData.EmailValidation(errorMessage: nil))
                }
            } else {
                output?.display(AppkeeLoginRegisterPassword.DisplayData.EmailValidation(errorMessage: translations.registePage.ERROR_NO_EMAIL))
                return
            }

            output?.display(AppkeeLoginRegisterPassword.DisplayData.Progress())
            interactor.perform(AppkeeLoginRegisterPassword.Request.Email(email: email ?? "", appCode: self.configManager.appCode))
        case .close:
            coordinator?.navigate(to: .close)
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeLoginRegisterPasswordPresenter: AppkeeLoginRegisterPasswordInteractorOutput {
    func present(_ response: AppkeeLoginRegisterPassword.Response.Email) {
        output?.display(AppkeeLoginRegisterPassword.DisplayData.Success(message: response.message))
    }

    func present(_ response: AppkeeLoginRegisterPassword.Response.Error) {
        output?.display(AppkeeLoginRegisterPassword.DisplayData.Error(message: response.message))
    }
}

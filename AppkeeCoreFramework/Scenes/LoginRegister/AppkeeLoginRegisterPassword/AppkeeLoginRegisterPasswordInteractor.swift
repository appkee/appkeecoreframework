//
//  AppkeeLoginRegisterPasswordInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/20/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import FirebaseAnalytics

class AppkeeLoginRegisterPasswordInteractor {
    // MARK: - Properties
    weak var output: AppkeeLoginRegisterPasswordInteractorOutput?
    private let repository: AppkeeRepository
    private let graphicManager: AppkeeGraphicManager

    // MARK: - Init
    init(repository: AppkeeRepository, graphicManager: AppkeeGraphicManager) {
        self.repository = repository
        self.graphicManager = graphicManager
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeLoginRegisterPasswordInteractor: AppkeeLoginRegisterPasswordInteractorInput {
    func perform(_ request: AppkeeLoginRegisterPassword.Request.Email) {
        repository.forgotPass(email: request.email, appCode: request.appCode) { [weak self] response in
            guard let self = self else { return }

            switch response.result {
            case .success(let response):
                if response.success, let message = response.data {
                    self.output?.present(AppkeeLoginRegisterPassword.Response.Email(message: message.message))
                } else {
                    self.output?.present(AppkeeLoginRegisterPassword.Response.Error(message: response.error ?? translations.error.UNKNOWN))
                }
                return
            case .failure(let error):
                self.output?.present(AppkeeLoginRegisterPassword.Response.Error(message: error.localizedDescription))

                Analytics.logEvent("Sending forgotrn password failed", parameters: ["appCode": request.appCode, "error": error.localizedDescription])
            }
        }
    }
}

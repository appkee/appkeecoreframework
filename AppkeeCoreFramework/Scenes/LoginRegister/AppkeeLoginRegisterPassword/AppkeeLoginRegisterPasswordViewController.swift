//
//  AppkeeLoginRegisterPasswordViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/20/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeLoginRegisterPasswordViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var iconImageView: UIImageView!

    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var errorEmailLabel: UILabel!

    @IBOutlet weak var sendButton: UIButton!

    // MARK: - Properties
    private var presenter: AppkeeLoginRegisterPasswordPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeLoginRegisterPasswordPresenterInput) -> AppkeeLoginRegisterPasswordViewController {
        let name = "\(AppkeeLoginRegisterPasswordViewController.self)"
        let bundle = Bundle(for: AppkeeLoginRegisterPasswordViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeLoginRegisterPasswordViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()

        self.title = translations.loginPage.FORGOT_PASS
        self.emailTextField.placeholder = translations.loginPage.EMAIL

        self.sendButton.setTitle(translations.loginPage.SEND.uppercased(), for: .normal)

        emailTextField.layer.borderColor = UIColor.red.cgColor
        emailTextField.layer.cornerRadius = 4
    }

    // MARK: - Callbacks -
    @IBAction func sendButtonTap(_ sender: Any) {
        presenter.handle(.send(email: self.emailTextField.text))
    }

    @IBAction func textFieldTapOutside(_ sender: UITextField) {
        sender.resignFirstResponder()
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeLoginRegisterPasswordViewController: AppkeeLoginRegisterPasswordPresenterOutput, Alertable, Progressable {
    func display(image: UIImage?) {
        self.iconImageView.image = image
    }
    
    func display(_ displayModel: AppkeeLoginRegisterPassword.DisplayData.Progress) {
        showProgress()
    }
    
    func display(_ displayModel: AppkeeLoginRegisterPassword.DisplayData.HideProgress) {
        hideProgress()
    }
    
    func display(_ displayModel: AppkeeLoginRegisterPassword.DisplayData.Error) {
        hideProgress()

        showErrorAlert(withMessage: displayModel.message)
    }

    func display(_ displayModel: AppkeeLoginRegisterPassword.DisplayData.Success) {
        hideProgress()

        showAlert(withTitle: displayModel.message, message: nil) { [weak self] in
            self?.presenter.handle(.close)
        }
    }
    
    func setupUI(colorSettings: AppkeeColorSettings) {
        self.view.backgroundColor = colorSettings.content

        self.sendButton.setTitleColor(.white, for: .normal)
        self.sendButton.backgroundColor = colorSettings.loginColor
    }
    
    func display(_ displayModel: AppkeeLoginRegisterPassword.DisplayData.EmailValidation) {
        if let message = displayModel.errorMessage {
            emailTextField.layer.borderWidth = 1

            errorEmailLabel.text = message
            errorEmailLabel.isHidden = false
        } else {
            emailTextField.layer.borderWidth = 0

            errorEmailLabel.text = nil
            errorEmailLabel.isHidden = true
        }
    }
}

//
//  AppkeeLoginRegisterModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/2/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeLoginRegister {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case login
        case noLogin
        case registration
    }

    enum Route {
        case menu
        case login
        case registration
    }
}

extension AppkeeLoginRegister.Request {

}

extension AppkeeLoginRegister.Response {
}

extension AppkeeLoginRegister.DisplayData {
    
}

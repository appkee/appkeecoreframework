//
//  AppkeeLoginRegisterRegisterCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/4/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeeLoginRegisterRegisterCoordinator: AppkeeCoordinator {
    enum Style {
        case dialog
        case standart
    }

    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
//    weak var delegate: PageCoordinatorDelegate?

    var delegate: AppkeeSideMenuCoordinatorDelegate?

    private let appDescription: AppkeeAppDescription?
    private let style: Style

    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, style: Style) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.style = style

        self.appDescription = dependencies.appDescriptionManager.readAppDescription()
    }

    func start(root: Bool = false) {
        let interactor = AppkeeLoginRegisterRegisterInteractor(repository: dependencies.repository,
                                          graphicManager: dependencies.graphicManager)
        let presenter = AppkeeLoginRegisterRegisterPresenter(interactor: interactor,
                                                             coordinator: self,
                                                             graphicManager: dependencies.graphicManager,
                                                             configManager: dependencies.configManager,
                                                             imageName: appDescription?.loadingImage,
                                                             style: style
        )
        let vc = AppkeeLoginRegisterRegisterViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        navigationController.navigationBar.isHidden = false
        navigationController.pushViewController(vc, animated: true)
    }
}
// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeLoginRegisterRegisterCoordinator: AppkeeLoginRegisterRegisterCoordinatorInput {
    func navigate(to route: AppkeeLoginRegisterRegister.Route) {
        switch route {
        case .entry:
            self.navigationController.popViewController(animated: true)
        }
    }
}

//
//  AppkeeLoginRegisterRegisterProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/4/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeeLoginRegisterRegisterCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeLoginRegisterRegisterCoordinatorInput: class {
    func navigate(to route: AppkeeLoginRegisterRegister.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeLoginRegisterRegisterInteractorInput {
     func perform(_ request: AppkeeLoginRegisterRegister.Request.Register)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeLoginRegisterRegisterInteractorOutput: class {
    func present(_ response: AppkeeLoginRegisterRegister.Response.Error)
    func present(_ response: AppkeeLoginRegisterRegister.Response.Register)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeLoginRegisterRegisterPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeeLoginRegisterRegister.Action)
}

// PRESENTER -> VIEW
protocol AppkeeLoginRegisterRegisterPresenterOutput: class {
    // func display(_ displayModel: AppkeeLoginRegisterRegister.DisplayData.Work)

    func display( image: UIImage?, desriptionText: String?)

    func display(_ displayModel: AppkeeLoginRegisterRegister.DisplayData.Progress)
    func display(_ displayModel: AppkeeLoginRegisterRegister.DisplayData.Register)
    func display(_ displayModel: AppkeeLoginRegisterRegister.DisplayData.Error)

    func display(_ displayModel: AppkeeLoginRegisterRegister.DisplayData.EmailValidation)
    func display(_ displayModel: AppkeeLoginRegisterRegister.DisplayData.NameValidation)
    func display(_ displayModel: AppkeeLoginRegisterRegister.DisplayData.TelValidation)
    func display(_ displayModel: AppkeeLoginRegisterRegister.DisplayData.PasswordValidation)
    func display(_ displayModel: AppkeeLoginRegisterRegister.DisplayData.ConfirmPassValidation)

    func setupUI(colorSettings: AppkeeColorSettings)
}

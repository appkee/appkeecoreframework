//
//  AppkeeLoginRegisterRegisterInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/4/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import FirebaseAnalytics

class AppkeeLoginRegisterRegisterInteractor {
    // MARK: - Properties
    weak var output: AppkeeLoginRegisterRegisterInteractorOutput?
    private let repository: AppkeeRepository
    private let graphicManager: AppkeeGraphicManager

    // MARK: - Init
    init(repository: AppkeeRepository, graphicManager: AppkeeGraphicManager) {
        self.repository = repository
        self.graphicManager = graphicManager
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeLoginRegisterRegisterInteractor: AppkeeLoginRegisterRegisterInteractorInput {
    func perform(_ request: AppkeeLoginRegisterRegister.Request.Register) {
        repository.pageRegister(email: request.email,
                                password: request.password,
                                confirmPass: request.confirmPass,
                                name: request.name,
                                phone: request.tel,
                                address: request.address,
                                appCode: request.appCode
        ) { [weak self] response in
            guard let self = self else { return }

            switch response.result {
            case .success(let response):
                if response.success, let user = response.data {
                    self.output?.present(AppkeeLoginRegisterRegister.Response.Register(user: user))
                } else {
                    self.output?.present(AppkeeLoginRegisterRegister.Response.Error(message: response.error ?? translations.error.UNKNOWN))
                }
                return
            case .failure(let error):
                self.output?.present(AppkeeLoginRegisterRegister.Response.Error(message: error.localizedDescription))

                Analytics.logEvent("Sending register failed", parameters: ["appCode": request.appCode, "error": error.localizedDescription])
            }
        }
    }
}

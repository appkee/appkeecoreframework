//
//  AppkeeLoginRegisterRegisterModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/4/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeLoginRegisterRegister {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case register(email: String?, name: String?, tel: String?, address: String?, password: String?, confirmPass: String?)
        case entry
    }

    enum Route {
        case entry
    }
}

extension AppkeeLoginRegisterRegister.Request {
    struct Register {
        let email: String
        let name: String
        let tel: String
        let address: String
        let password: String
        let confirmPass: String
        let appCode: String
    }
}

extension AppkeeLoginRegisterRegister.Response {
    struct Register {
        let user: AppkeeUser
    }

    struct Error {
        let message: String
    }
}

extension AppkeeLoginRegisterRegister.DisplayData {
    struct Progress {

    }

    struct Register {
        let dialogText: String
    }

    struct Error {
        let message: String
    }

    struct EmailValidation {
        let errorMessage: String?
    }

    struct NameValidation {
        let errorMessage: String?
    }

    struct TelValidation {
        let errorMessage: String?
    }

    struct PasswordValidation {
        let errorMessage: String?
    }

    struct ConfirmPassValidation {
        let errorMessage: String?
    }
}

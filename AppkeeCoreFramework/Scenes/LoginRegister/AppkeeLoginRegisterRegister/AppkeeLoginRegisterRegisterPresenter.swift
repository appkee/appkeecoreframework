//
//  AppkeeLoginRegisterRegisterPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/4/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeLoginRegisterRegisterPresenter {
    // MARK: - Properties
    private let graphicManager: AppkeeGraphicManager
    private let configManager:  AppkeeConfigManager
    private let imageName: String?
    private let style: AppkeeLoginRegisterRegisterCoordinator.Style
    
    let interactor: AppkeeLoginRegisterRegisterInteractorInput
    weak var coordinator: AppkeeLoginRegisterRegisterCoordinatorInput?
    weak var output: AppkeeLoginRegisterRegisterPresenterOutput?

    // MARK: - Init
    init(interactor: AppkeeLoginRegisterRegisterInteractorInput,
         coordinator: AppkeeLoginRegisterRegisterCoordinatorInput,
         graphicManager: AppkeeGraphicManager,
         configManager:  AppkeeConfigManager,
         imageName: String?,
         style: AppkeeLoginRegisterRegisterCoordinator.Style
    ) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
        self.configManager = configManager
        self.imageName = imageName
        self.style = style
    }
}

// MARK: - User Events -

extension AppkeeLoginRegisterRegisterPresenter: AppkeeLoginRegisterRegisterPresenterInput {
    func viewCreated() {
        var desriptionText: String?

        switch style {
        case .dialog:
            desriptionText = nil
        case .standart:
            desriptionText = translations.registePage.DESCRIPTION
        }

        output?.display(image: graphicManager.getImage(name: self.imageName), desriptionText: desriptionText)

        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }
    }

    func handle(_ action: AppkeeLoginRegisterRegister.Action) {
        switch action {
        case let .register(email, name, tel, address, password, confirmPass):
            var error = false

            if let email = email, !email.isEmpty{
                if !email.isValidEmail() {
                    output?.display(AppkeeLoginRegisterRegister.DisplayData.EmailValidation(errorMessage: translations.registePage.ERROR_INVALID_EMAIL))
                    error = true
                } else {
                    output?.display(AppkeeLoginRegisterRegister.DisplayData.EmailValidation(errorMessage: nil))
                }
            } else {
                output?.display(AppkeeLoginRegisterRegister.DisplayData.EmailValidation(errorMessage: translations.registePage.ERROR_NO_EMAIL))
                error = true
            }

            if let name = name, !name.isEmpty {
                output?.display(AppkeeLoginRegisterRegister.DisplayData.NameValidation(errorMessage: nil))
            } else {
                output?.display(AppkeeLoginRegisterRegister.DisplayData.NameValidation(errorMessage: translations.registePage.ERROR_MISSING_NAME))
                error = true
            }

//            if let address = address, !address.isEmpty {
//
//            } else {
//                output?.display(AppkeeLoginRegisterRegister.DisplayData.val(message: translations.registePage.ERROR_MISSING_ADDRESS))
//                error = true
//            }

            if let tel = tel, !tel.isEmpty {
                output?.display(AppkeeLoginRegisterRegister.DisplayData.TelValidation(errorMessage: nil))
            } else {
                output?.display(AppkeeLoginRegisterRegister.DisplayData.TelValidation(errorMessage: translations.registePage.ERROR_MISSING_PHONE))
                error = true
            }

            if let password = password, !password.isEmpty {
                if password.count < 6 {
                    output?.display(AppkeeLoginRegisterRegister.DisplayData.PasswordValidation(errorMessage: translations.registePage.ERROR_INVALID_PASS))
                    error = true
                } else {
                    output?.display(AppkeeLoginRegisterRegister.DisplayData.PasswordValidation(errorMessage: nil))
                }
            } else {
                output?.display(AppkeeLoginRegisterRegister.DisplayData.PasswordValidation(errorMessage: translations.registePage.ERROR_MISSING_PASSWORD))
                error = true
            }

            if let confirmPass = confirmPass, !confirmPass.isEmpty {
                if confirmPass != password {
                    output?.display(AppkeeLoginRegisterRegister.DisplayData.ConfirmPassValidation(errorMessage: translations.registePage.ERROR_MISSING_PASSWORD))
                    error = true
                } else {
                    output?.display(AppkeeLoginRegisterRegister.DisplayData.ConfirmPassValidation(errorMessage: nil))
                }
            } else {
                output?.display(AppkeeLoginRegisterRegister.DisplayData.ConfirmPassValidation(errorMessage: translations.registePage.ERROR_MISSING_PASSWORD))
                error = true
            }

            if error {
                return
            }

            output?.display(AppkeeLoginRegisterRegister.DisplayData.Progress())
            interactor.perform(AppkeeLoginRegisterRegister.Request.Register(email: email ?? "",
                                                                            name: name ?? "",
                                                                            tel: tel ?? "",
                                                                            address: address ?? "",
                                                                            password: password ?? "",
                                                                            confirmPass: confirmPass ?? "",
                                                                            appCode: self.configManager.appCode))
        case .entry:
            coordinator?.navigate(to: .entry)
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeLoginRegisterRegisterPresenter: AppkeeLoginRegisterRegisterInteractorOutput {
    func present(_ response: AppkeeLoginRegisterRegister.Response.Error) {
        output?.display(AppkeeLoginRegisterRegister.DisplayData.Error(message: response.message))
    }

    func present(_ response: AppkeeLoginRegisterRegister.Response.Register) {
        UserDefaults.pageUser = response.user

        var dialogText: String

        switch style {
        case .dialog:
            dialogText = translations.registePage.ALERT_TEXT2
        case .standart:
            dialogText = translations.registePage.ALERT_TEXT
        }

        output?.display(AppkeeLoginRegisterRegister.DisplayData.Register(dialogText: dialogText))
    }
}

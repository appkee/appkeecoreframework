//
//  AppkeeLoginRegisterRegisterViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/4/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeLoginRegisterRegisterViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var errorEmailLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var errorNameLabel: UILabel!
    @IBOutlet weak var telTextField: UITextField!
    @IBOutlet weak var errorTelLabel: UILabel!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var errorPasswordLabel: UILabel!
    @IBOutlet weak var confirmPassTextField: UITextField!
    @IBOutlet weak var errorConfirmPassLabel: UILabel!
    @IBOutlet weak var privacyTextView: UITextView!
    @IBOutlet weak var registerButton: UIButton!


    // MARK: - Properties
    private var presenter: AppkeeLoginRegisterRegisterPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeLoginRegisterRegisterPresenterInput) -> AppkeeLoginRegisterRegisterViewController {
        let name = "\(AppkeeLoginRegisterRegisterViewController.self)"
        let bundle = Bundle(for: AppkeeLoginRegisterRegisterViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeLoginRegisterRegisterViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()

        self.title = translations.loginPage.REGISTER

        self.emailTextField.placeholder = translations.registePage.EMAIL
        self.nameTextField.placeholder = translations.registePage.NAME
        self.telTextField.placeholder = translations.registePage.TEL
        self.addressTextField.placeholder = translations.registePage.ADDRESS
        self.passwordTextField.placeholder = translations.registePage.PASSWORD
        self.confirmPassTextField.placeholder = translations.registePage.CONFIRM_PASS

        self.privacyTextView.setHtml(from: translations.registePage.PRIVACY)

        self.registerButton.setTitle(translations.registePage.BUTTON, for: .normal)


        emailTextField.layer.borderColor = UIColor.red.cgColor
        emailTextField.layer.cornerRadius = 4
        nameTextField.layer.borderColor = UIColor.red.cgColor
        nameTextField.layer.cornerRadius = 4
        telTextField.layer.borderColor = UIColor.red.cgColor
        telTextField.layer.cornerRadius = 4
//        addressTextField.layer.borderColor = UIColor.red.cgColor
//        addressTextField.layer.cornerRadius = 4
        passwordTextField.layer.borderColor = UIColor.red.cgColor
        passwordTextField.layer.cornerRadius = 4
        confirmPassTextField.layer.borderColor = UIColor.red.cgColor
        confirmPassTextField.layer.cornerRadius = 4
    }

    // MARK: - Callbacks -

    @IBAction func registerButtonTap(_ sender: Any) {
        presenter.handle(.register(email: emailTextField.text,
                                   name: nameTextField.text,
                                   tel: telTextField.text,
                                   address: addressTextField.text,
                                   password: passwordTextField.text,
                                   confirmPass: confirmPassTextField.text))
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeLoginRegisterRegisterViewController: AppkeeLoginRegisterRegisterPresenterOutput, Progressable, Alertable {
    func display(image: UIImage?, desriptionText: String?) {
        self.descriptionLabel.text = desriptionText

        self.iconImageView.image = image
    }
    
    func display(_ displayModel: AppkeeLoginRegisterRegister.DisplayData.Progress) {
        showProgress()
    }
    
    func display(_ displayModel: AppkeeLoginRegisterRegister.DisplayData.Register) {
        hideProgress()

        showAlert(withTitle: translations.registePage.ALERT_TITLE, message: displayModel.dialogText) {
            self.presenter.handle(.entry)
        }
    }
    
    func display(_ displayModel: AppkeeLoginRegisterRegister.DisplayData.Error) {
        hideProgress()

        showErrorAlert(withMessage: displayModel.message)
    }
    
    func setupUI(colorSettings: AppkeeColorSettings) {
        self.view.backgroundColor = colorSettings.content

        self.descriptionLabel.textColor = colorSettings.contentText

        self.registerButton.setTitleColor(.white, for: .normal)
        self.registerButton.backgroundColor = colorSettings.registerColor
    }

    func display(_ displayModel: AppkeeLoginRegisterRegister.DisplayData.EmailValidation) {
        if let message = displayModel.errorMessage {
            emailTextField.layer.borderWidth = 1

            errorEmailLabel.text = message
            errorEmailLabel.isHidden = false
        } else {
            emailTextField.layer.borderWidth = 0

            errorEmailLabel.text = nil
            errorEmailLabel.isHidden = true
        }
    }

    func display(_ displayModel: AppkeeLoginRegisterRegister.DisplayData.NameValidation) {
        if let message = displayModel.errorMessage {
            nameTextField.layer.borderWidth = 1

            errorNameLabel.text = message
            errorNameLabel.isHidden = false
        } else {
            nameTextField.layer.borderWidth = 0

            errorNameLabel.text = nil
            errorNameLabel.isHidden = true
        }
    }

    func display(_ displayModel: AppkeeLoginRegisterRegister.DisplayData.TelValidation) {
        if let message = displayModel.errorMessage {
            telTextField.layer.borderWidth = 1

            errorTelLabel.text = message
            errorTelLabel.isHidden = false
        } else {
            telTextField.layer.borderWidth = 0

            errorTelLabel.text = nil
            errorTelLabel.isHidden = true
        }
    }

    func display(_ displayModel: AppkeeLoginRegisterRegister.DisplayData.PasswordValidation) {
        if let message = displayModel.errorMessage {
            passwordTextField.layer.borderWidth = 1

            errorPasswordLabel.text = message
            errorPasswordLabel.isHidden = false
        } else {
            passwordTextField.layer.borderWidth = 0

            errorPasswordLabel.text = nil
            errorPasswordLabel.isHidden = true
        }
    }

    func display(_ displayModel: AppkeeLoginRegisterRegister.DisplayData.ConfirmPassValidation) {
        if let message = displayModel.errorMessage {
            confirmPassTextField.layer.borderWidth = 1

            errorConfirmPassLabel.text = message
            errorConfirmPassLabel.isHidden = false
        } else {
            confirmPassTextField.layer.borderWidth = 0

            errorConfirmPassLabel.text = nil
            errorConfirmPassLabel.isHidden = true
        }
    }
}

extension AppkeeLoginRegisterRegisterViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.emailTextField {
            self.nameTextField.becomeFirstResponder()
        }

        if textField == self.nameTextField {
            self.telTextField.becomeFirstResponder()
        }

        if textField == self.telTextField {
            self.addressTextField.becomeFirstResponder()
        }

        if textField == self.addressTextField {
            self.passwordTextField.becomeFirstResponder()
        }

        if textField == self.passwordTextField {
            self.confirmPassTextField.becomeFirstResponder()
        }

        if textField == self.confirmPassTextField {
            textField.resignFirstResponder()
            self.registerButtonTap(self)
            return true
        }

        return true
    }
}

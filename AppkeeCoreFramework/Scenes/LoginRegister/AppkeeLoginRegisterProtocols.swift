//
//  AppkeeLoginRegisterProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/2/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeeLoginRegisterCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeLoginRegisterCoordinatorInput: class {
    func navigate(to route: AppkeeLoginRegister.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeLoginRegisterInteractorInput {
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeLoginRegisterInteractorOutput: class {

}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeLoginRegisterPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeeLoginRegister.Action)
}

// PRESENTER -> VIEW
protocol AppkeeLoginRegisterPresenterOutput: class {
    // func display(_ displayModel: AppkeeLoginRegister.DisplayData.Work)

    func display( title: String?, image: UIImage?)

    func setupUI(colorSettings: AppkeeColorSettings)
}

//
//  AppkeeLoginRegisterInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/2/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import FirebaseAnalytics

class AppkeeLoginRegisterInteractor {
    // MARK: - Properties
    weak var output: AppkeeLoginRegisterInteractorOutput?
    private let repository: AppkeeRepository
    private let graphicManager: AppkeeGraphicManager

    // MARK: - Init
    init(repository: AppkeeRepository, graphicManager: AppkeeGraphicManager) {
        self.repository = repository
        self.graphicManager = graphicManager
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeLoginRegisterInteractor: AppkeeLoginRegisterInteractorInput {
    
}

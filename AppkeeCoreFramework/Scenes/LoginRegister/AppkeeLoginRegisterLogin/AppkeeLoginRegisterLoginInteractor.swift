//
//  AppkeeLoginRegisterLoginInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/3/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import FirebaseAnalytics

class AppkeeLoginRegisterLoginInteractor {
    // MARK: - Properties
    weak var output: AppkeeLoginRegisterLoginInteractorOutput?
    private let repository: AppkeeRepository
    private let graphicManager: AppkeeGraphicManager

    // MARK: - Init
    init(repository: AppkeeRepository, graphicManager: AppkeeGraphicManager) {
        self.repository = repository
        self.graphicManager = graphicManager
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeLoginRegisterLoginInteractor: AppkeeLoginRegisterLoginInteractorInput {
    func perform(_ request: AppkeeLoginRegisterLogin.Request.Login) {
        repository.pageLogin(email: request.email, password: request.password, appCode: request.appCode) { [weak self] response in
            guard let self = self else { return }

            switch response.result {
            case .success(let response):
                if response.success, let user = response.data {
                    self.output?.present(AppkeeLoginRegisterLogin.Response.Login(user: user))
                } else {
                    self.output?.present(AppkeeLoginRegisterLogin.Response.Error(message: response.error ?? translations.error.UNKNOWN))
                }
                return
            case .failure(let error):
                self.output?.present(AppkeeLoginRegisterLogin.Response.Error(message: error.localizedDescription))

                Analytics.logEvent("Sending login failed", parameters: ["appCode": request.appCode, "error": error.localizedDescription])
            }
        }
    }
}

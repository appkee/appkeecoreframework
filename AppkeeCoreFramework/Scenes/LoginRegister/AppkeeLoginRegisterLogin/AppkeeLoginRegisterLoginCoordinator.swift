//
//  AppkeeLoginRegisterLoginCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/3/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeeLoginRegisterLoginCoordinator: AppkeeCoordinator {
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
//    weak var delegate: PageCoordinatorDelegate?

    var delegate: AppkeeCoordinatorDelegate?
    var loginDialogDelegate: AppkeeLoginDialogCoordinatorDelegate?

    private let appDescription: AppkeeAppDescription?

    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies) {
        self.navigationController = navigationController
        self.dependencies = dependencies

        self.appDescription = dependencies.appDescriptionManager.readAppDescription()
    }

    func start(root: Bool = false) {
        let interactor = AppkeeLoginRegisterLoginInteractor(repository: dependencies.repository,
                                          graphicManager: dependencies.graphicManager)
        let presenter = AppkeeLoginRegisterLoginPresenter(interactor: interactor,
                                                          coordinator: self,
                                                          graphicManager: dependencies.graphicManager,
                                                          configManager: dependencies.configManager,
                                                          imageName: appDescription?.loadingImage)
        let vc = AppkeeLoginRegisterLoginViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        navigationController.navigationBar.isHidden = false
        navigationController.pushViewController(vc, animated: true)
    }

    private func showForgotrmPassword() {
        let coordinator = AppkeeLoginRegisterPasswordCoordinator(navigationController: navigationController,
                                                                 dependencies: dependencies)
        childrens.append(coordinator)
        coordinator.start(root: false)
    }
}
    
// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeLoginRegisterLoginCoordinator: AppkeeLoginRegisterLoginCoordinatorInput {
    func navigate(to route: AppkeeLoginRegisterLogin.Route) {
        switch route {
        case .menu:
            delegate?.navigate(to: .menu)
            loginDialogDelegate?.logged()
        case .forgot:
            showForgotrmPassword()
        }
    }
}

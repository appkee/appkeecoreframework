//
//  AppkeeLoginRegisterLoginModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/3/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeLoginRegisterLogin {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case login(email: String?, password: String?)
        case forgot
    }

    enum Route {
        case menu
        case forgot
    }
}

extension AppkeeLoginRegisterLogin.Request {
    struct Login {
        let email: String
        let password: String
        let appCode: String
    }
}

extension AppkeeLoginRegisterLogin.Response {
    struct Login {
        let user: AppkeeUser
    }

    struct Error {
        let message: String
    }
}

extension AppkeeLoginRegisterLogin.DisplayData {
    struct Progress {

    }

    struct HideProgress {

    }

    struct Error {
        let message: String
    }

    struct EmailValidation {
        let errorMessage: String?
    }

    struct PasswordValidation {
        let errorMessage: String?
    }
}

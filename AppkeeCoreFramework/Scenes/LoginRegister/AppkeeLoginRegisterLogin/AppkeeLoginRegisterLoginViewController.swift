//
//  AppkeeLoginRegisterLoginViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/3/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeLoginRegisterLoginViewController: UIViewController {
    // MARK: - Outlets

    @IBOutlet weak var iconImageView: UIImageView!

    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var errorEmailLabel: UILabel!
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var errorPasswordLabel: UILabel!
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var forgotButton: UIButton!

    // MARK: - Properties
    private var presenter: AppkeeLoginRegisterLoginPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeLoginRegisterLoginPresenterInput) -> AppkeeLoginRegisterLoginViewController {
        let name = "\(AppkeeLoginRegisterLoginViewController.self)"
        let bundle = Bundle(for: AppkeeLoginRegisterLoginViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeLoginRegisterLoginViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()

        self.title = translations.loginPage.TITLE
        self.emailTextField.placeholder = translations.loginPage.EMAIL
        self.passwordTextField.placeholder = translations.loginPage.PASSWORD

        self.loginButton.setTitle(translations.loginPage.LOGIN, for: .normal)
        self.forgotButton.setTitle(translations.loginPage.FORGOT_PASS, for: .normal)

        emailTextField.layer.borderColor = UIColor.red.cgColor
        emailTextField.layer.cornerRadius = 4

        passwordTextField.layer.borderColor = UIColor.red.cgColor
        passwordTextField.layer.cornerRadius = 4
    }

    // MARK: - Callbacks -

    @IBAction func loginButtonTap(_ sender: Any) {
        presenter.handle(.login(email: self.emailTextField.text, password: self.passwordTextField.text))
    }
    @IBAction func forgotButtonTap(_ sender: Any) {
        presenter.handle(.forgot)
    }

    @IBAction func textFieldTapOutside(_ sender: UITextField) {
        sender.resignFirstResponder()
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeLoginRegisterLoginViewController: AppkeeLoginRegisterLoginPresenterOutput, Alertable, Progressable {
    func display(image: UIImage?) {
        self.iconImageView.image = image
    }

    func display(_ displayModel: AppkeeLoginRegisterLogin.DisplayData.Progress) {
        showProgress()
    }

    func display(_ displayModel: AppkeeLoginRegisterLogin.DisplayData.HideProgress) {
        hideProgress()
    }

    func display(_ displayModel: AppkeeLoginRegisterLogin.DisplayData.Error) {
        hideProgress()

        showErrorAlert(withMessage: displayModel.message)
    }

    func setupUI(colorSettings: AppkeeColorSettings) {
        self.view.backgroundColor = colorSettings.content

        self.loginButton.setTitleColor(.white, for: .normal)
        self.loginButton.backgroundColor = colorSettings.loginColor

        self.forgotButton.setTitleColor(colorSettings.loginColor, for: .normal)
    }

    func display(_ displayModel: AppkeeLoginRegisterLogin.DisplayData.EmailValidation) {
        if let message = displayModel.errorMessage {
            emailTextField.layer.borderWidth = 1

            errorEmailLabel.text = message
            errorEmailLabel.isHidden = false
        } else {
            emailTextField.layer.borderWidth = 0

            errorEmailLabel.text = nil
            errorEmailLabel.isHidden = true
        }
    }

    func display(_ displayModel: AppkeeLoginRegisterLogin.DisplayData.PasswordValidation) {
        if let message = displayModel.errorMessage {
            passwordTextField.layer.borderWidth = 1

            errorPasswordLabel.text = message
            errorPasswordLabel.isHidden = false
        } else {
            passwordTextField.layer.borderWidth = 0

            errorPasswordLabel.text = nil
            errorPasswordLabel.isHidden = true
        }
    }
}

extension AppkeeLoginRegisterLoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.emailTextField {
            self.passwordTextField.becomeFirstResponder()
        }

        if textField == self.passwordTextField {
            textField.resignFirstResponder()
            self.loginButtonTap(self)
            return true
        }

        return true
    }
}

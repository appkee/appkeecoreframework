//
//  AppkeeLoginRegisterLoginProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/3/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeeLoginRegisterLoginCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeLoginRegisterLoginCoordinatorInput: class {
    func navigate(to route: AppkeeLoginRegisterLogin.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeLoginRegisterLoginInteractorInput {
    func perform(_ request: AppkeeLoginRegisterLogin.Request.Login)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeLoginRegisterLoginInteractorOutput: class {
    func present(_ response: AppkeeLoginRegisterLogin.Response.Login)
    func present(_ response: AppkeeLoginRegisterLogin.Response.Error)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeLoginRegisterLoginPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeeLoginRegisterLogin.Action)
}

// PRESENTER -> VIEW
protocol AppkeeLoginRegisterLoginPresenterOutput: class {
    // func display(_ displayModel: AppkeeLoginRegisterLogin.DisplayData.Work)

    func display( image: UIImage?)

    func display(_ displayModel: AppkeeLoginRegisterLogin.DisplayData.Progress)
    func display(_ displayModel: AppkeeLoginRegisterLogin.DisplayData.HideProgress)
    func display(_ displayModel: AppkeeLoginRegisterLogin.DisplayData.Error)

    func display(_ displayModel: AppkeeLoginRegisterLogin.DisplayData.EmailValidation)
    func display(_ displayModel: AppkeeLoginRegisterLogin.DisplayData.PasswordValidation)

    func setupUI(colorSettings: AppkeeColorSettings)
}

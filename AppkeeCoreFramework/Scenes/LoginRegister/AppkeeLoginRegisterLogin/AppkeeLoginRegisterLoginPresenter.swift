//
//  AppkeeLoginRegisterLoginPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/3/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeLoginRegisterLoginPresenter {
    // MARK: - Properties
    private let graphicManager: AppkeeGraphicManager
    private let configManager:  AppkeeConfigManager
    private let imageName: String?

    let interactor: AppkeeLoginRegisterLoginInteractorInput
    weak var coordinator: AppkeeLoginRegisterLoginCoordinatorInput?
    weak var output: AppkeeLoginRegisterLoginPresenterOutput?

    // MARK: - Init
    init(interactor: AppkeeLoginRegisterLoginInteractorInput, coordinator: AppkeeLoginRegisterLoginCoordinatorInput, graphicManager: AppkeeGraphicManager, configManager:  AppkeeConfigManager, imageName: String?) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
        self.configManager = configManager
        self.imageName = imageName
    }
}

// MARK: - User Events -

extension AppkeeLoginRegisterLoginPresenter: AppkeeLoginRegisterLoginPresenterInput {
    func viewCreated() {
        output?.display(image: graphicManager.getImage(name: self.imageName))

        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }
    }

    func handle(_ action: AppkeeLoginRegisterLogin.Action) {
        switch action {
        case let .login(email, password):
            var error = false

            if let email = email, !email.isEmpty{
                if !email.isValidEmail() {
                    output?.display(AppkeeLoginRegisterLogin.DisplayData.EmailValidation(errorMessage: translations.registePage.ERROR_INVALID_EMAIL))
                    error = true
                } else {
                    output?.display(AppkeeLoginRegisterLogin.DisplayData.EmailValidation(errorMessage: nil))
                }
            } else {
                output?.display(AppkeeLoginRegisterLogin.DisplayData.EmailValidation(errorMessage: translations.registePage.ERROR_NO_EMAIL))
                error = true
            }

            if let password = password, !password.isEmpty {
                output?.display(AppkeeLoginRegisterLogin.DisplayData.PasswordValidation(errorMessage: nil))
            } else {
                output?.display(AppkeeLoginRegisterLogin.DisplayData.PasswordValidation(errorMessage: translations.registePage.ERROR_MISSING_PASSWORD))
                error = true
            }

            if error {
                return
            }

            output?.display(AppkeeLoginRegisterLogin.DisplayData.Progress())
            interactor.perform(AppkeeLoginRegisterLogin.Request.Login(email: email ?? "", password: password ?? "", appCode: self.configManager.appCode))
        case .forgot:
            coordinator?.navigate(to: .forgot)
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeLoginRegisterLoginPresenter: AppkeeLoginRegisterLoginInteractorOutput {
    func present(_ response: AppkeeLoginRegisterLogin.Response.Login) {
        UserDefaults.pageUser = response.user

        output?.display(AppkeeLoginRegisterLogin.DisplayData.HideProgress())

        coordinator?.navigate(to: .menu)
    }

    func present(_ response: AppkeeLoginRegisterLogin.Response.Error) {
        output?.display(AppkeeLoginRegisterLogin.DisplayData.Error(message: response.message))
    }
}

//
//  AppkeeLoginRegisterViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/2/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeLoginRegisterViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var iconImageView: UIImageView!

    @IBOutlet weak var descriptionLabel: UILabel!

    @IBOutlet weak var continueButton: UIButton!

    @IBOutlet weak var loginButton: UIButton!

    @IBOutlet weak var registerButton: UIButton!


    // MARK: - Properties
    private var presenter: AppkeeLoginRegisterPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeLoginRegisterPresenterInput) -> AppkeeLoginRegisterViewController {
        let name = "\(AppkeeLoginRegisterViewController.self)"
        let bundle = Bundle(for: AppkeeLoginRegisterViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeLoginRegisterViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()

        self.loginButton.setTitle(translations.loginPage.LOGIN.uppercased(), for: .normal)
        self.registerButton.setTitle(translations.loginPage.REGISTER.uppercased(), for: .normal)
        self.continueButton.setTitle(translations.loginPage.NO_LOGIN, for: .normal)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.navigationBar.isHidden = true
    }

    // MARK: - Callbacks -

    @IBAction func continueButtonTap(_ sender: Any) {
        presenter.handle(.noLogin)
    }

    @IBAction func LoginButtonTap(_ sender: Any) {
        presenter.handle(.login)
    }

    @IBAction func registerButtonTap(_ sender: Any) {
        presenter.handle(.registration)
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeLoginRegisterViewController: AppkeeLoginRegisterPresenterOutput, Alertable, Progressable {
    func display(title: String?, image: UIImage?) {
        self.iconImageView.image = image
        if let title = title {
            self.descriptionLabel.setHtml(from: title)
        } else {
            self.descriptionLabel.text = ""
        }
    }

    func setupUI(colorSettings: AppkeeColorSettings) {
        self.view.backgroundColor = colorSettings.content

        self.loginButton.backgroundColor = colorSettings.loginColor
        self.continueButton.backgroundColor = colorSettings.loginColor
        self.registerButton.backgroundColor = colorSettings.registerColor

        self.loginButton.setTitleColor(.white, for: .normal)
        self.continueButton.setTitleColor(.white, for: .normal)
        self.registerButton.setTitleColor(colorSettings.loginColor, for: .normal)

        self.descriptionLabel.textColor = colorSettings.contentText
    }
}

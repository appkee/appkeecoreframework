//
//  AppkeePhotoReportProtocols.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 07/02/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeePhotoReportCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeePhotoReportCoordinatorInput: class {
    func navigate(to route: AppkeePhotoReport.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeePhotoReportInteractorInput {
    // func perform(_ request: AppkeePhotoReport.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeePhotoReportInteractorOutput: class {
    // func present(_ response: AppkeePhotoReport.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeePhotoReportPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeePhotoReport.Action)
}

// PRESENTER -> VIEW
protocol AppkeePhotoReportPresenterOutput: class {
    func display(_ displayModel: AppkeePhotoReport.DisplayData.Camera)
    func display(_ displayModel: AppkeePhotoReport.DisplayData.Description)
    func display(_ displayModel: AppkeePhotoReport.DisplayData.Address)
    func display(_ displayModel: AppkeePhotoReport.DisplayData.Processing)
    func display(_ displayModel: AppkeePhotoReport.DisplayData.StopProcessing)
    func display(_ displayModel: AppkeePhotoReport.DisplayData.Present)
    func display(_ displayModel: AppkeePhotoReport.DisplayData.Send)
    func display(_ displayModel: AppkeePhotoReport.DisplayData.Saved)
    func display(_ displayModel: AppkeePhotoReport.DisplayData.Error)
    func display(_ displayModel: AppkeePhotoReport.DisplayData.TitleError)
}

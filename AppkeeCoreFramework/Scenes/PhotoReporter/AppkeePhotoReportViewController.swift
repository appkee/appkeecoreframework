//
//  AppkeePhotoReportViewController.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 07/02/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import UIKit
import MapKit
import MessageUI

class AppkeePhotoReportViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var labelTitle: UILabel!

    @IBOutlet weak var imagePhoto: UIImageView!
    
    @IBOutlet weak var buttonPhoto: UIButton!
    
    @IBOutlet weak var buttonSend: UIButton! {
        didSet {
            buttonSend.tintColor = .white
        }
    }
    
    @IBOutlet weak var buttonSave: UIButton!
    
    @IBOutlet weak var indicatorProgress: UIActivityIndicatorView!

    // MARK: - Properties
    private var presenter: AppkeePhotoReportPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeePhotoReportPresenterInput) -> AppkeePhotoReportViewController {
        let name = "\(AppkeePhotoReportViewController.self)"
        let bundle = Bundle(for: AppkeePhotoReportViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeePhotoReportViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let locationManager = CLLocationManager()
//
//        locationManager.requestAlwaysAuthorization()
//
//
//        if CLLocationManager.locationServicesEnabled() {
//            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
//            locationManager.startUpdatingLocation()
//        }
        
        presenter.viewCreated()
    }

    // MARK: - Callbacks -

    @IBAction func buttonSendTap() {
        presenter.handle(.send(self.imagePhoto.image))
    }
    
    @IBAction func buttonSaveTap() {
        presenter.handle(.save)
    }
    
    @IBAction func buttonCameraTap() {
        presenter.handle(.camera)
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeePhotoReportViewController: AppkeePhotoReportPresenterOutput, Alertable, Progressable {
    
    func display(_ displayModel: AppkeePhotoReport.DisplayData.Camera) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func display(_ displayModel: AppkeePhotoReport.DisplayData.Description) {
        showInputAlert(withTitle: translations.photoReporter.INPUT_DESCRIPTION, okTitle: translations.common.USE, cancelTitle: translations.common.CANCEL) { (text) in
            self.presenter.handle(.description(text))
        }
    }
    
    func display(_ displayModel: AppkeePhotoReport.DisplayData.Address) {
        showInputAlert(withTitle: translations.photoReporter.INPUT_ADDRESS, okTitle: translations.common.USE, cancelTitle: translations.common.CANCEL) { (text) in
            self.presenter.handle(.address(text))
        }
    }
    
    func display(_ displayModel: AppkeePhotoReport.DisplayData.Processing) {
        showProgress()
    }

    func display(_ displayModel: AppkeePhotoReport.DisplayData.StopProcessing) {
        hideProgress()
    }
    
    func display(_ displayModel: AppkeePhotoReport.DisplayData.Present) {
        hideProgress()
        
        imagePhoto.image = displayModel.image
    }
    
    func display(_ displayModel: AppkeePhotoReport.DisplayData.Send) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self;
            mail.setToRecipients([displayModel.email])

            if let imageData: Data = displayModel.image.jpegData(compressionQuality: 0.75) {
                mail.addAttachmentData(imageData, mimeType: "image/png", fileName: "image.jpg")
                self.present(mail, animated: true, completion: nil)
            }
        }
    }
    
    func display(_ displayModel: AppkeePhotoReport.DisplayData.Saved) {
        hideProgress()
        showAlert(withTitle: translations.photoReporter.SAVED, okTitle: translations.common.OK)
    }
    
    func display(_ displayModel: AppkeePhotoReport.DisplayData.Error) {
        hideProgress()
        
        showErrorAlert(withMessage: displayModel.message)
    }
    
    func display(_ displayModel: AppkeePhotoReport.DisplayData.TitleError) {
        showAlert(withTitle: displayModel.title, message: displayModel.message, okTitle: translations.common.OK)
    }
}

extension AppkeePhotoReportViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {            
            presenter.handle(.photo(pickedImage))
        }
        dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}


extension AppkeePhotoReportViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

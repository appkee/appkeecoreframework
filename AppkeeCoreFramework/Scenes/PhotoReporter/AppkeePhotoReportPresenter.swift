//
//  AppkeePhotoReportPresenter.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 07/02/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit
import MessageUI

class AppkeePhotoReportPresenter {
    // MARK: - Properties
    private let email: String
    private let graphicManager: AppkeeGraphicManager
    
    let interactor: AppkeePhotoReportInteractorInput
    weak var coordinator: AppkeePhotoReportCoordinatorInput?
    weak var output: AppkeePhotoReportPresenterOutput?
    
    private let locationManager = CLLocationManager()
    
    private var address: String?
    private var image: UIImage?
    private var location: String?
    private var description: String?

    // MARK: - Init
    init(interactor: AppkeePhotoReportInteractorInput, coordinator: AppkeePhotoReportCoordinatorInput, graphicManager: AppkeeGraphicManager, email: String) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
        self.email = email
    }
}

// MARK: - User Events -

extension AppkeePhotoReportPresenter: AppkeePhotoReportPresenterInput {
    func viewCreated() {
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.startUpdatingLocation()
        }
    }

    func handle(_ action: AppkeePhotoReport.Action) {
        switch action {
        case .camera:
            output?.display(AppkeePhotoReport.DisplayData.Camera())
        case let .photo(image):
            if let image = image {
                self.image = image
                if let location = self.getLocation() {
                    self.location = String(format: "%.5f, %.5f", location.latitude, location.longitude)
                    self.getAddress(location: location, success: { (address) -> (Void) in
                        self.address = address
                        self.output?.display(AppkeePhotoReport.DisplayData.Description())
                    })
                } else {
                    cleanData()
                    output?.display(AppkeePhotoReport.DisplayData.Address())
                }
            } else {
                cleanData()
                output?.display(AppkeePhotoReport.DisplayData.Error(message: translations.photoReporter.ERROR_PHOTO))
            }
        case .save:
            //            if save {
            //                UIImageWriteToSavedPhotosAlbum(newImage!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
            //
            //                return;
            //            }

            break
        case .send(let image):
            if let image = image {
                output?.display(AppkeePhotoReport.DisplayData.Send(email: email, image: image))
            }
            
        case let .description(text):
            if let text = text, !text.isEmpty {
                self.description = text
                
                if let image = generateImage() {
                    output?.display(AppkeePhotoReport.DisplayData.Present(image: image))
                }
            } else {
                cleanData()
                output?.display(AppkeePhotoReport.DisplayData.Error(message: translations.photoReporter.ERROR_DESCRIPTION))
            }
        case let .address(address):
            self.address = address
            self.output?.display(AppkeePhotoReport.DisplayData.Description())
        }
    }
    
    func cleanData() {
        address = nil
        image = nil
        location = nil
        description = nil
    }
    
    func generateImage() -> UIImage? {
        self.output?.display(AppkeePhotoReport.DisplayData.Processing())
        
        guard let image = self.image else {
            cleanData()
            output?.display(AppkeePhotoReport.DisplayData.Error(message: translations.photoReporter.ERROR_PHOTO))
            return nil
        }
        
        guard let address = self.address, !address.isEmpty else {
            cleanData()
            output?.display(AppkeePhotoReport.DisplayData.Error(message: translations.photoReporter.ERROR_ADDRESS))
            return nil
        }
        
        guard let description = self.description, !description.isEmpty else {
            cleanData()
            output?.display(AppkeePhotoReport.DisplayData.Error(message: translations.photoReporter.ERROR_DESCRIPTION))
            return nil
        }
              
        let location = self.location ?? "-";
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"
        
        let date = dateFormatter.string(from: Date())
        
        let height = image.size.height
        let width = image.size.width
        
        let rect = CGRect(x: 0, y: 0, width: width, height: height)
        
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.5)
        
        image.draw(in: rect)
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.lineBreakMode = NSLineBreakMode.byWordWrapping
        paragraph.alignment = .left
        
        let font = UIFont.systemFont(ofSize: 100.0)
        let color = UIColor.white
        let colorBackgound = UIColor.black
        
        let attributedStringDescription = NSAttributedString(string: translations.photoReporter.IMAGE_DESCRIPTION + description, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: color, NSAttributedString.Key.backgroundColor: colorBackgound, NSAttributedString.Key.paragraphStyle:paragraph])
        
        let attributedStringAddress = NSAttributedString(string: translations.photoReporter.IMAGE_ADDRESS + address, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: color, NSAttributedString.Key.backgroundColor: colorBackgound, NSAttributedString.Key.paragraphStyle:paragraph])
        
        let attributedStringLocation = NSAttributedString(string: translations.photoReporter.IMAGE_LOCATION + location, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: color, NSAttributedString.Key.backgroundColor: colorBackgound, NSAttributedString.Key.paragraphStyle:paragraph])
        
        let attributedStringDate = NSAttributedString(string: translations.photoReporter.IMAGE_DATE + date, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: color, NSAttributedString.Key.backgroundColor: colorBackgound, NSAttributedString.Key.paragraphStyle:paragraph])
        
        let heightText:CGFloat = 200.0
               
        let rectTextDescription = CGRect(x: 40, y: height - (1.0 * (heightText + 10)), width: width - 20, height: heightText)
        
        let rectTextAddress = CGRect(x: 40, y: height - 2 * (heightText + 10) , width: width - 20, height: heightText)
       
        let rectTextLocation = CGRect(x: 40, y: height - 3 * (heightText + 10) , width: width - 20, height: heightText)
       
        let rectTextDate = CGRect(x: 40, y: height - 4 * (heightText + 10) , width: width - 20, height: heightText)
       
       
        attributedStringDescription.draw(in: rectTextDescription)
        attributedStringAddress.draw(in: rectTextAddress)
        attributedStringLocation.draw(in: rectTextLocation)
        attributedStringDate.draw(in: rectTextDate)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();

        self.output?.display(AppkeePhotoReport.DisplayData.StopProcessing())
        
        return newImage
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            output?.display(AppkeePhotoReport.DisplayData.TitleError(title: translations.photoReporter.ERROR_SAVE, message: error.localizedDescription))
        } else {
            output?.display(AppkeePhotoReport.DisplayData.Error(message: translations.photoReporter.SAVED))
        }
        
        cleanData()
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeePhotoReportPresenter: AppkeePhotoReportInteractorOutput {

}

extension AppkeePhotoReportPresenter {
    func getLocation() -> (CLLocationCoordinate2D?) {
        
        guard let location = locationManager.location else {
            return nil
        }
        
        let locValue:CLLocationCoordinate2D = location.coordinate
        
        return (locValue)
    }
    
    func getAddress( location:CLLocationCoordinate2D, success:@escaping ( String?) -> ( Void)) {
        
        let geoCoder = CLGeocoder()
        
        let location = CLLocation(latitude: location.latitude, longitude: location.longitude)
        
        geoCoder.reverseGeocodeLocation(location) { (placemarks, error) -> Void in

            if let placeMark = placemarks?[0] {
              
                var address = ""
                
                // Street address
                if let street = placeMark.thoroughfare
                {
                    address = street
                }
                
                // Street number address
                if let street = placeMark.subThoroughfare
                {
                    address = " " + street
                }
                
                // City
                if let city = placeMark.locality
                {
                    address += ", " + city
                }
                
                // Country
                if let country = placeMark.locality
                {
                    address += ", " + country
                }                                
                
                success(address)
                return
            }
            
            success(nil)
        }
    }
}

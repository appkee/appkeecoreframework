//
//  AppkeePhotoReportModels.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 07/02/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

enum AppkeePhotoReport {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case send(UIImage?)
        case save
        case camera
        case photo(UIImage?)
        case description(String?)
        case address(String?)
    }

    enum Route {

    }
}

extension AppkeePhotoReport.Request {
    
}

extension AppkeePhotoReport.Response {

}

extension AppkeePhotoReport.DisplayData {
    struct Camera {
        
    }
    
    struct Description {
        
    }
    
    struct Address {
        
    }
    
    struct Processing {
        
    }

    struct StopProcessing {
        
    }
    
    struct Present {
        let image: UIImage
    }
    
    struct Send {
        let email: String
        let image: UIImage
    }
    
    struct Saved {
        
    }
    
    struct Error {
        let message: String
    }
    
    struct TitleError {
        let title: String
        let message: String
    }
}

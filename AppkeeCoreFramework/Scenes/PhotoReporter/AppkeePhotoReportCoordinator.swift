//
//  AppkeePhotoReportCoordinator.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 07/02/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeePhotoReportCoordinator: AppkeeCoordinator {
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
    private let email: String

//    weak var delegate: AppkeePhotoReportCoordinatorDelegate?
    
    var delegate: AppkeeSideMenuCoordinatorDelegate?
    
    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, email: String) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.email = email        
    }

    func start(root: Bool) {
        let interactor = AppkeePhotoReportInteractor()
        let presenter = AppkeePhotoReportPresenter(interactor: interactor, coordinator: self, graphicManager: dependencies.graphicManager, email: email)
        let vc = AppkeePhotoReportViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        if dependencies.configManager.appMenu {
            let image = UIImage(named: "dots", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
             vc.navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: .done, target: self, action: #selector(debugMenu(_:)))        
        }
                
        vc.title = translations.photoReporter.TITLE
                    
        navigationController.navigationBar.isHidden = false
        navigationController.pushViewController(vc, animated: true)
    }
        
    
    @objc func debugMenu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openDebugMenu()
    }
}
// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeePhotoReportCoordinator: AppkeePhotoReportCoordinatorInput {
    func navigate(to route: AppkeePhotoReport.Route) {
        
    }
}

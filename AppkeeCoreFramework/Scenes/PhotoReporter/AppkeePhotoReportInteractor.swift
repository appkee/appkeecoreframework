//
//  AppkeePhotoReportInteractor.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 07/02/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeePhotoReportInteractor {
    // MARK: - Properties
    weak var output: AppkeePhotoReportInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeePhotoReportInteractor: AppkeePhotoReportInteractorInput {
}

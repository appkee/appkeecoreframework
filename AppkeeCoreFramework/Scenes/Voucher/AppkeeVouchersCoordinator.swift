//
//  AppkeeVouchersCoordinator.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 14/01/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit
import CardPresentationController

class AppkeeVouchersCoordinator: AppkeeCoordinator {
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []    
    var dependencies: AppkeeFullDependencies
    private let section: AppkeeSection
    private let email: String?
    private let search: Bool
//    weak var delegate: VouchersCoordinatorDelegate?
    
    var delegate: AppkeeSideMenuCoordinatorDelegate?

    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, section: AppkeeSection, email: String?, search: Bool) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.section = section
        self.email = email
        self.search = search
    }
        
    func start(root: Bool = false) {
        let interactor = AppkeeVouchersInteractor(repository: dependencies.repository, graphicManager: dependencies.graphicManager)
        let presenter = AppkeeVouchersPresenter(
            interactor: interactor,
            coordinator: self,
            graphicManager: dependencies.graphicManager,
            configManager: dependencies.configManager,
            section: section)
        let vc = AppkeeVouchersViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        var menuItems: [UIBarButtonItem] = []
        if dependencies.configManager.appMenu {
            let image = UIImage(named: "dots", in: Bundle(for: AppkeeVouchersCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(debugMenu(_:))))
        }
        if search {
            let image = UIImage(named: "search", in: Bundle(for: AppkeeVouchersCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(search(_:))))
        }
        if let _ = email {
            let image = UIImage(named: "cameraMenu", in: Bundle(for: AppkeeVouchersCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(photoReporter(_:))))
        }
        vc.navigationItem.rightBarButtonItems = menuItems
        
        vc.title = section.name
        
        let rootController = AppkeeBannerViewController(graphicManager: dependencies.graphicManager)
        rootController.setViewController(vc)
        rootController.delegate = self

        if let banners = self.section.banners {
            rootController.setBanners(banners: banners)
        }

        if root {
            let image = UIImage(named: "hamburger", in: Bundle(for: AppkeeVouchersCoordinator.self), compatibleWith: nil)
            rootController.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  image, style: .done, target: self, action: #selector(menu(_:)))

            navigationController.setViewControllers([rootController], animated: false)

            return
        }
        
        navigationController.navigationBar.isHidden = false
        navigationController.pushViewController(rootController, animated: true)
    }
    
    @objc func menu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openMenu()
    }
    
    @objc func debugMenu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openDebugMenu()
    }
    
    @objc func photoReporter(_ barButtonItem: UIBarButtonItem) {
        if let email = self.email {
            delegate?.openPhotoReporter(email: email)
        }
    }
    
    @objc func search(_ barButtonItem: UIBarButtonItem) {
        delegate?.openSearch()
    }
}

// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeVouchersCoordinator: AppkeeVouchersCoordinatorInput {
    
    func navigate(to route: AppkeeVouchers.Route) {
        switch route {
        case .open(let voucher):
            showDetail(voucher: voucher)
        case .close:
            navigationController.popViewController(animated: true)
        case let .openAdvertisementLink(link):
            if let url = URL(string: link) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        case let .openDeeplink(params):
            delegate?.openDeeplink(params: params)
        }
    }    
}

extension AppkeeVouchersCoordinator {

    private func showDetail(voucher: AppkeeVoucher) {
        let interactor = AppkeeVoucherDetailInteractor(repository: dependencies.repository, graphicManager: dependencies.graphicManager)
        let presenter = AppkeeVoucherDetailPresenter(interactor: interactor, coordinator: self, graphicManager: dependencies.graphicManager, voucher: voucher)
        let vc = AppkeeVoucherDetailViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc
        
//        navigationController.present(vc, animated: true, completion: nil)

//        prese

        navigationController.presentCard(vc, animated: true)
    }
}

extension AppkeeVouchersCoordinator: AppkeeBannerViewControllerDelegate {
    func openLink(link: String) {
        self.navigate(to: .openAdvertisementLink(link: link))
    }

    func openDeeplink(params: String) {
        self.navigate(to: .openDeeplink(params: params))
    }
}

//
//  VouchersPresenter.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 14/01/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeVouchersPresenter {
    // MARK: - Properties
    private let section: AppkeeSection
    private let appCode: String
    private var vouchers: [AppkeeVoucher] = []
    private let graphicManager: AppkeeGraphicManager
    private let configManager: AppkeeConfigManager
    
    let interactor: AppkeeVouchersInteractorInput
    weak var coordinator: AppkeeVouchersCoordinatorInput?
    weak var output: AppkeeVouchersPresenterOutput?

    // MARK: - Init
    init(interactor: AppkeeVouchersInteractorInput, coordinator: AppkeeVouchersCoordinatorInput, graphicManager: AppkeeGraphicManager, configManager: AppkeeConfigManager, section: AppkeeSection) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
        self.configManager = configManager
        
        self.section = section
        self.appCode = ""
    }
}

// MARK: - User Events -

extension AppkeeVouchersPresenter: AppkeeVouchersPresenterInput {
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfItems(in section: Int) -> Int {
        return vouchers.count
    }            
    
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }
        
        interactor.perform(AppkeeVouchers.Request.Vouchers(appCode: configManager.appCode))
    }

    func handle(_ action: AppkeeVouchers.Action) {
        switch action {
        case .navigate(let index):            
            let voucher = vouchers[index]
            coordinator?.navigate(to: .open(voucher))
        case .reload:
            interactor.perform(AppkeeVouchers.Request.Vouchers(appCode: configManager.appCode))
        }
    }
    
    func configure(_ item: AppkeeVouchersTableViewCellDelegate, at indexPath: IndexPath) {
        let voucherItem = vouchers[indexPath.row]
        
        let iconImage = graphicManager.imageLink(name: voucherItem.image)
        
        item.configure(with: voucherItem.name, imageLink: iconImage, colorSettings: graphicManager.colorsSettings)
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeVouchersPresenter: AppkeeVouchersInteractorOutput {
    func present(_ response: AppkeeVouchers.Response.Error) {        
        output?.display(AppkeeVouchers.DisplayData.Error(message: response.message))
    }
    
    func present(_ response: AppkeeVouchers.Response.Vouchers) {
        self.vouchers = response.vouchers.sorted(by: { $0.validTo < $1.validTo })
        output?.display(AppkeeVouchers.DisplayData.Reload(emptyArray: vouchers.isEmpty))
    }
}

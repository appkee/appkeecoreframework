//
//  AppkeeVoucherDetailViewController.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 15/01/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

final class AppkeeVoucherDetailViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var voucherImageView: UIImageView!
    
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var validLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var useButton: UIButton!
    
    
    // MARK: - Properties
    private var presenter: AppkeeVoucherDetailPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeVoucherDetailPresenterInput) -> AppkeeVoucherDetailViewController {
        let name = "\(AppkeeVoucherDetailViewController.self)"
        let bundle = Bundle(for: AppkeeVoucherDetailViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeVoucherDetailViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
    }

    // MARK: - Callbacks -

}

// MARK: - Actions -

extension AppkeeVoucherDetailViewController {
    
    @IBAction func useTap(_ sender: UIButton) {
        showInputAlert(withTitle: translations.voucher.USE_TITLE, message: translations.voucher.USE_MESSAGE, okTitle: translations.common.USE, cancelTitle: translations.common.CANCEL) { (text) in
            self.presenter.handle(.useVoucher(text))
        }
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeVoucherDetailViewController: AppkeeVoucherDetailPresenterOutput, Alertable, Progressable {
    func display(_ displayModel: AppkeeVoucherDetail.DisplayData.ShowProgress) {
        showProgress()
    }
        
    func display(_ displayModel: AppkeeVoucherDetail.DisplayData.VoucherUsed) {
        hideProgress()
        showAlert(withTitle: translations.voucher.USED_TITLE, message: translations.voucher.USED_MESSAGE) {
            self.presenter.handle(.close)
        }
    }
    
    func display(_ displayModel: AppkeeVoucherDetail.DisplayData.Error) {
        hideProgress()
        showErrorAlert(withMessage: displayModel.message)
    }
    
    
    func display(_ displayModel: AppkeeVoucherDetail.DisplayData.Display) {
        nameLabel.text = displayModel.title
        descriptionLabel.text = displayModel.description
        validLabel.text = translations.voucher.VALID + displayModel.date
        
        useButton.isEnabled = !displayModel.used
        
        if let imageLink = displayModel.imageLink {
            Alamofire.request(imageLink).responseImage { response in
                self.voucherImageView.image = response.value
            }
        }
    }
    
    func setupUI(colorSettings: AppkeeColorSettings) {
        self.view.backgroundColor = colorSettings.voucherBackground
        
        containerView.layer.masksToBounds = true
        containerView.layer.cornerRadius = 16.0
        containerView.layer.borderWidth = 4
        containerView.layer.borderColor = colorSettings.vouchersText.cgColor
        
        titleLabel.text = translations.voucher.DETAIL_TITLE
        titleLabel.textColor = colorSettings.vouchersText
        
        nameLabel.textColor = colorSettings.voucherBackground
        nameView.backgroundColor = colorSettings.vouchersText
        nameView.layer.masksToBounds = true
        nameView.layer.cornerRadius = 8.0
        
        descriptionLabel.textColor = colorSettings.voucherDescription
        validLabel.textColor = colorSettings.voucherDescription
        
        useButton.backgroundColor = colorSettings.voucherButton
        useButton.tintColor = colorSettings.voucherBackground
        useButton.layer.masksToBounds = true
        useButton.layer.cornerRadius = 8.0
        useButton.setTitle(translations.voucher.USE_TITLE, for: .normal) 
    }
}

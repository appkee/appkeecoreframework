//
//  AppkeeVoucherDetailInteractor.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 15/01/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import FirebaseAnalytics

class AppkeeVoucherDetailInteractor {
    // MARK: - Properties
    weak var output: AppkeeVoucherDetailInteractorOutput?
    private let repository: AppkeeRepository
    private let graphicManager: AppkeeGraphicManager
    
    // MARK: - Init
    init(repository: AppkeeRepository, graphicManager: AppkeeGraphicManager) {
        self.repository = repository
        self.graphicManager = graphicManager
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeVoucherDetailInteractor: AppkeeVoucherDetailInteractorInput {
    func perform(_ request: AppkeeVoucherDetail.Request.UseVoucher) {
        repository.useVouchers(idCoupon: request.couponId, confirmCode: request.confirmCode) { [weak self] response in
                guard let self = self else { return }
                
                switch response.result {
                case .success(let response):
                    if response.success {
                        self.output?.present(AppkeeVoucherDetail.Response.VoucherUsed())
                    } else {
                        switch response.error {
                        case "[wrong-confirm-code]":
                            self.output?.present(AppkeeVoucherDetail.Response.Error(message: translations.error.WRONG_CODE))
                        case "Unknown Voucher id.":
                            self.output?.present(AppkeeVoucherDetail.Response.Error(message: translations.error.WRONG_VOUCHER_ID))
                        default:
                            self.output?.present(AppkeeVoucherDetail.Response.Error(message: translations.error.UNKNOWN))
                        }
                    }
                case .failure(let error):
                    self.output?.present(AppkeeVoucherDetail.Response.Error(message: error.localizedDescription))
                    
                    Analytics.logEvent("Use of voucher failed", parameters: ["idCoupon": request.couponId, "confirmCode": request.confirmCode, "error": error.localizedDescription])
                }
            }
        }
}

//
//  AppkeeVoucherDetailPresenter.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 15/01/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeVoucherDetailPresenter {
    // MARK: - Properties
    private let appCode: String
    private let voucher: AppkeeVoucher
    private let graphicManager: AppkeeGraphicManager
    
    let interactor: AppkeeVoucherDetailInteractorInput
    weak var output: AppkeeVoucherDetailPresenterOutput?
    
    // Your custom coordinator
     weak var coordinator: AppkeeVouchersCoordinatorInput?

    // MARK: - Init
    init(interactor: AppkeeVoucherDetailInteractorInput, coordinator: AppkeeVouchersCoordinatorInput, graphicManager: AppkeeGraphicManager, voucher: AppkeeVoucher) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
        self.voucher = voucher
        
        self.appCode = ""
    }
}

// MARK: - User Events -

extension AppkeeVoucherDetailPresenter: AppkeeVoucherDetailPresenterInput {
    func viewCreated() {
        if let colorsSettins = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettins)
        }
        
        let iconImage = graphicManager.imageLink(name: voucher.image)
        
        let date = voucher.validTo.formatedDate()
        
        let used = false // voucher.validTo.f
        
        output?.display(AppkeeVoucherDetail.DisplayData.Display(title: voucher.name, date: date, imageLink: iconImage, description: voucher.description, used: used))
    }

    func handle(_ action: AppkeeVoucherDetail.Action) {
        switch action {
        case let .useVoucher(code):
            self.interactor.perform(AppkeeVoucherDetail.Request.UseVoucher(couponId: self.voucher.id, confirmCode: code))
            self.output?.display(AppkeeVoucherDetail.DisplayData.ShowProgress())            
        case .close:
            coordinator?.navigate(to: .close)
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeVoucherDetailPresenter: AppkeeVoucherDetailInteractorOutput {
    func present(_ response: AppkeeVoucherDetail.Response.VoucherUsed) {
                
        UserDefaults.addVoucher(voucherId: voucher.id)
        output?.display(AppkeeVoucherDetail.DisplayData.VoucherUsed())
    }
    
    func present(_ response: AppkeeVoucherDetail.Response.Error) {
        output?.display(AppkeeVoucherDetail.DisplayData.Error(message: response.message))
    }
}

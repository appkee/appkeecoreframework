//
//  VoucherDetailProtocols.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 15/01/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeVoucherDetailInteractorInput {
    func perform(_ request: AppkeeVoucherDetail.Request.UseVoucher)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeVoucherDetailInteractorOutput: class {
    func present(_ response: AppkeeVoucherDetail.Response.VoucherUsed)
    func present(_ response: AppkeeVoucherDetail.Response.Error)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeVoucherDetailPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeeVoucherDetail.Action)
}

// PRESENTER -> VIEW
protocol AppkeeVoucherDetailPresenterOutput: class {
    func display(_ displayModel: AppkeeVoucherDetail.DisplayData.Display)
    func display(_ displayModel: AppkeeVoucherDetail.DisplayData.VoucherUsed)
    func display(_ displayModel: AppkeeVoucherDetail.DisplayData.ShowProgress)
    func display(_ displayModel: AppkeeVoucherDetail.DisplayData.Error)
    
    func setupUI(colorSettings: AppkeeColorSettings)
}

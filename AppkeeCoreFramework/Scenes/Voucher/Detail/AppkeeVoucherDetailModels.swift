//
//  AppkeeVoucherDetailModels.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 15/01/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeVoucherDetail {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case useVoucher(String?)
        case close
    }

    enum Route {

    }
}

extension AppkeeVoucherDetail.Request {
    struct UseVoucher {
        let couponId: Int
        let confirmCode: String?
    }
}

extension AppkeeVoucherDetail.Response {
    struct VoucherUsed {
        
    }
    
    struct Error {
        let message: String
    }
}

extension AppkeeVoucherDetail.DisplayData {
    struct Display {
        let title: String
        let date: String
        let imageLink: String?
        let description: String?
        let used: Bool
    }
    
    struct ShowInput {
        
    }
    
    struct ShowProgress {
        
    }
    
    struct VoucherUsed {
        
    }
    
    struct Error {
        let message: String
    }
}

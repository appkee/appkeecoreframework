//
//  VouchersTableViewCell.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 15/01/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

protocol AppkeeVouchersTableViewCellDelegate: class {
    func configure(with title: String?, imageLink:String?, colorSettings: AppkeeColorSettings?)
}

class AppkeeVouchersTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

extension AppkeeVouchersTableViewCell: AppkeeVouchersTableViewCellDelegate {
 
    func configure(with title: String?, imageLink:String?, colorSettings: AppkeeColorSettings?) {
        if let colorSettings = colorSettings {
            contentView.backgroundColor = colorSettings.content
            titleLabel.textColor = colorSettings.contentText
        }
        
        titleLabel.text = title
        
        if let imageLink = imageLink {
            Alamofire.request(imageLink).responseImage { response in
                self.iconImageView.image = response.value
            }
        }        
    }
}

//
//  VouchersViewController.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 14/01/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeVouchersViewController: UIViewController {
        
    // MARK: - Outlets
    private let pullToRefresh: UIRefreshControl = {
        let pullToRefresh = UIRefreshControl()
        pullToRefresh.attributedTitle = NSAttributedString(string: translations.common.TABLE_PULL_TO_REFRESH)
        pullToRefresh.tintColor = .clear
//        pullToRefresh.subviews.first?.alpha = 0
        return pullToRefresh
    }()
    
    private let tableLabel: UILabel = {
        let label = UILabel()
        label.text = translations.voucher.NO_DATA
        label.textAlignment = .center
        label.isHidden = true
        return label
   }()
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(AppkeeVouchersTableViewCell.nib, forCellReuseIdentifier: AppkeeVouchersTableViewCell.reuseId)
            tableView.estimatedRowHeight = 40
            tableView.tableFooterView = UIView()
            tableView.backgroundView = tableLabel
        }
    }
    
    // MARK: - Properties
    private var presenter: AppkeeVouchersPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeVouchersPresenterInput) -> AppkeeVouchersViewController {
        let name = "\(AppkeeVouchersViewController.self)"
        let bundle = Bundle(for: AppkeeVouchersViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeVouchersViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()

        pullToRefresh.addTarget(self, action: #selector(reloadData), for: .valueChanged)
        tableView.refreshControl = pullToRefresh
        
        presenter.viewCreated()
        showProgress()
    }
    
    @objc func reloadData() {
        pullToRefresh.endRefreshing()
        tableView.setContentOffset(.zero, animated:true)
        
        showProgress()
        presenter.handle(.reload)
    }

    // MARK: - Callbacks -

}

extension AppkeeVouchersViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.numberOfItems(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AppkeeVouchersTableViewCell.reuseId, for: indexPath) as! AppkeeVouchersTableViewCell

        presenter.configure(cell, at: indexPath)
        return cell
    }
}

extension AppkeeVouchersViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.handle(.navigate(indexPath.row))
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeVouchersViewController: AppkeeVouchersPresenterOutput, Alertable, Progressable {
    
    func setupUI(colorSettings: AppkeeColorSettings) {
        pullToRefresh.backgroundColor = colorSettings.contentText
        if let attributedTitle = pullToRefresh.attributedTitle {
            let mutableAttributedTitle = NSMutableAttributedString(attributedString: attributedTitle)
            mutableAttributedTitle.addAttribute(.foregroundColor, value: colorSettings.content, range: NSRange(location: 0, length: attributedTitle.length))
            
            pullToRefresh.attributedTitle = mutableAttributedTitle
        }
        
        tableView.separatorColor = colorSettings.header
        tableLabel.textColor = colorSettings.contentText
        
        self.view.backgroundColor = colorSettings.content
    }

    func display(_ displayModel: AppkeeVouchers.DisplayData.Reload) {
        hideProgress()
        tableLabel.isHidden = !displayModel.emptyArray
        tableView.reloadData()
        
    }
    
    func display(_ displayModel: AppkeeVouchers.DisplayData.Error) {
        hideProgress()
        tableLabel.isHidden = false
        tableView.reloadData()
        
        showErrorAlert(withMessage: displayModel.message)
    }
}

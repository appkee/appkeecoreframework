//
//  AppkeeVouchersInteractor.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 14/01/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import FirebaseAnalytics

class AppkeeVouchersInteractor {
    // MARK: - Properties
    weak var output: AppkeeVouchersInteractorOutput?
    private let repository: AppkeeRepository
    private let graphicManager: AppkeeGraphicManager
    
    // MARK: - Init
    init(repository: AppkeeRepository, graphicManager: AppkeeGraphicManager) {
        self.repository = repository
        self.graphicManager = graphicManager
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeVouchersInteractor: AppkeeVouchersInteractorInput {
    
    func perform(_ request: AppkeeVouchers.Request.Vouchers) {
        repository.getVouchers(appCode: request.appCode) { [weak self] response in
            guard let self = self else { return }
            
            switch response.result {
            case .success(let response):
                if response.success {
                    let vouchers = response.data?.vouchers ?? []
                    let usedVouchers = UserDefaults.Vouchers
                    
                    self.output?.present(AppkeeVouchers.Response.Vouchers(vouchers: vouchers.filter({ return !usedVouchers.contains($0.id) })))
                } else {
                    self.output?.present(AppkeeVouchers.Response.Error(message: response.error ?? translations.error.UNKNOWN))
                }
                
            case .failure(let error):
                self.output?.present(AppkeeVouchers.Response.Error(message: error.localizedDescription))
                
                Analytics.logEvent("Loading of vouchers failed", parameters: ["appCode": request.appCode, "error": error.localizedDescription])
            }
        }
    }
}

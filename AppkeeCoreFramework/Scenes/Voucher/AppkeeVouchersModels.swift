//
//  AppkeeVouchersModels.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 14/01/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeVouchers {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case navigate(Int)
        case reload
    }

    enum Route {
        case open(AppkeeVoucher)
        case close
        case openAdvertisementLink(link: String)
        case openDeeplink(params: String)
    }
}

extension AppkeeVouchers.Request {
    struct Vouchers {
        let appCode: String
    }
}

extension AppkeeVouchers.Response {
    struct Vouchers {
        let vouchers: [AppkeeVoucher]
    }
    
    struct Error {
        let message: String
    }
}

extension AppkeeVouchers.DisplayData {
    struct Reload {
        let emptyArray: Bool
    }
    
    struct Error {
        let message: String
    }
}

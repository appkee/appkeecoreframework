//
//  AppkeeVouchersProtocols.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 14/01/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

protocol AppkeeVouchersCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
    func codeInput(code: String)
}

// PRESENTER -> COORDINATOR
protocol AppkeeVouchersCoordinatorInput: class {
    func navigate(to route: AppkeeVouchers.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeVouchersInteractorInput {
    func perform(_ request: AppkeeVouchers.Request.Vouchers)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeVouchersInteractorOutput: class {
    func present(_ response: AppkeeVouchers.Response.Vouchers)
    func present(_ response: AppkeeVouchers.Response.Error)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeVouchersPresenterInput {
    var numberOfSections: Int { get }
    func numberOfItems(in section: Int) -> Int
    
    func viewCreated()
    func handle(_ action: AppkeeVouchers.Action)
    
    func configure(_ item: AppkeeVouchersTableViewCellDelegate, at indexPath: IndexPath)
}

// PRESENTER -> VIEW
protocol AppkeeVouchersPresenterOutput: class {
    func display(_ displayModel: AppkeeVouchers.DisplayData.Reload)
    func display(_ displayModel: AppkeeVouchers.DisplayData.Error)
    
    func setupUI(colorSettings: AppkeeColorSettings)
}

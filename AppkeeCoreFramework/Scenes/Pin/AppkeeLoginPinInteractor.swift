//
//  AppkeeLoginPinInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/9/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeLoginPinInteractor {
    // MARK: - Properties
    weak var output: AppkeeLoginPinInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeLoginPinInteractor: AppkeeLoginPinInteractorInput {
}

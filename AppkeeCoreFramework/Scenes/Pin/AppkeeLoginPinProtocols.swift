//
//  AppkeeLoginPinProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/9/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol LoginPinCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeLoginPinCoordinatorInput: class {
    func navigate(to route: AppkeeLoginPin.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeLoginPinInteractorInput {
    // func perform(_ request: LoginPin.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeLoginPinInteractorOutput: class {
    // func present(_ response: LoginPin.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeLoginPinPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeeLoginPin.Action)
}

// PRESENTER -> VIEW
protocol AppkeeLoginPinPresenterOutput: class {
    // func display(_ displayModel: LoginPin.DisplayData.Work)

    func error(title: String?)
}

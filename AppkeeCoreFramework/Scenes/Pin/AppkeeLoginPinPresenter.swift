//
//  AppkeeLoginPinPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/9/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import CommonCrypto

class AppkeeLoginPinPresenter {
    // MARK: - Properties
    private let graphicManager: AppkeeGraphicManager

    private let interactor: AppkeeLoginPinInteractorInput
    weak var coordinator: AppkeeLoginPinCoordinatorInput?
    weak var output: AppkeeLoginPinPresenterOutput?

    private let pinCode: String

    // MARK: - Init
    init(interactor: AppkeeLoginPinInteractorInput, coordinator: AppkeeLoginPinCoordinatorInput, graphicManager: AppkeeGraphicManager, pinCode: String) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.pinCode = pinCode
        self.graphicManager = graphicManager
    }
}

// MARK: - User Events -

extension AppkeeLoginPinPresenter: AppkeeLoginPinPresenterInput {
    func viewCreated() {

    }

    func handle(_ action: AppkeeLoginPin.Action) {
        switch action {
        case .pinEntry(let pin):
            let pinCode = pin.hash()
            if pinCode == self.pinCode {
                UserDefaults.addPinCode(pinCode: self.pinCode)
                coordinator?.navigate(to: .confirmed)
            } else {
                output?.error(title: nil)
            }
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeLoginPinPresenter: AppkeeLoginPinInteractorOutput {

}

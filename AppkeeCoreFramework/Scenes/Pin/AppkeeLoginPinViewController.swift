//
//  AppkeeLoginPinViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/9/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import UIKit
import KAPinField

class AppkeeLoginPinViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var iconImageView: UIImageView! {
        didSet {
            iconImageView.image = Bundle.main.icon
        }
    }

    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.text = translations.pinLogin.TITLE
        }
    }

    @IBOutlet weak var pinField: KAPinField!
    
    // MARK: - Properties
    private var presenter: AppkeeLoginPinPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeLoginPinPresenterInput) -> AppkeeLoginPinViewController {
        let name = "\(AppkeeLoginPinViewController.self)"
        let bundle = Bundle(for: AppkeeLoginPinViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeLoginPinViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()

        pinField.properties.delegate = self

        pinField.text = ""
        pinField.properties.numberOfCharacters = 4
        pinField.properties.isSecure = true
        pinField.properties.token = "-"
        pinField.properties.animateFocus = false
        pinField.properties.validCharacters = "1234567890"

//            pinField.appearance.tokenColor = UIColor.label.withAlphaComponent(0.2)
//            pinField.appearance.tokenFocusColor = UIColor.label.withAlphaComponent(0.2)
//            pinField.appearance.textColor = UIColor.label
        pinField.appearance.font = .courierBold(40)
        pinField.appearance.kerning = 24
        pinField.appearance.backOffset = 5
        pinField.appearance.backColor = UIColor.clear
        pinField.appearance.backBorderWidth = 1
//            pinField.appearance.backBorderColor = UIColor.label.withAlphaComponent(0.2)
        pinField.appearance.backCornerRadius = 4
//            pinField.appearance.backFocusColor = UIColor.clear
//            pinField.appearance.backBorderFocusColor = UIColor.label.withAlphaComponent(0.8)
//            pinField.appearance.backActiveColor = UIColor.clear
//            pinField.appearance.backBorderActiveColor = UIColor.label
        pinField.appearance.backRounded = false

        pinField.becomeFirstResponder()
    }

    // MARK: - Callbacks -

}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeLoginPinViewController: AppkeeLoginPinPresenterOutput, Alertable {
    func error(title: String?) {
//        pinField.text = ""
        pinField.text = ""
        pinField.properties.numberOfCharacters = 4

        self.showAlert(withTitle: translations.pinLogin.ERROR_TITLE, message: translations.pinLogin.ERROR_MESSAGE) { [weak self] in
            self?.pinField.becomeFirstResponder()
        }
    }
}

extension AppkeeLoginPinViewController: KAPinFieldDelegate {
    func pinField(_ field: KAPinField, didFinishWith code: String) {
        presenter.handle(.pinEntry(pin: code))
    }
}

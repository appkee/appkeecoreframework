//
//  AppkeeLoginPinModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/9/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeLoginPin {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case pinEntry(pin: String)

    }

    enum Route {
        case confirmed
    }
}

extension AppkeeLoginPin.Request {

}

extension AppkeeLoginPin.Response {

}

extension AppkeeLoginPin.DisplayData {
    
}

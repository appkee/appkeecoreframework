//
//  AppkeeLoginPinCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/9/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeeLoginPinCoordinator: AppkeeCoordinator {
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies

    weak var delegate: AppkeeCoordinatorDelegate?

    private let window: UIWindow

    let pinCode: String

    // MARK: - Init
    init(window: UIWindow, dependencies: AppkeeFullDependencies, pinCode: String) {
        self.dependencies = dependencies
        self.window = window
        self.pinCode = pinCode

        self.navigationController = UINavigationController()
        self.navigationController.view.backgroundColor = .white

        self.navigationController.navigationBar.isHidden = true
    }

    func start(root: Bool = false) {
        let interactor = AppkeeLoginPinInteractor()
        let presenter = AppkeeLoginPinPresenter(interactor: interactor,
                                          coordinator: self,
                                          graphicManager: dependencies.graphicManager,
                                          pinCode: self.pinCode)
        let vc = AppkeeLoginPinViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        navigationController.setViewControllers([vc], animated: false)

        window.rootViewController = navigationController
    }
}
// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeLoginPinCoordinator: AppkeeLoginPinCoordinatorInput {
    func navigate(to route: AppkeeLoginPin.Route) {
        switch route {
        case .confirmed:
            self.delegate?.navigate(to: .menu)
        }
    }
}

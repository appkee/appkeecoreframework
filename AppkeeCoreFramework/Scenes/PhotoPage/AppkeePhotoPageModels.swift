//
//  AppkeePageModels.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 26/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

enum AppkeePhotoPage {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case openLink(link: String, name: String)
        case openDeeplink(params: String)
    }

    enum Route {
        case gallery(images: [AppkeeContent], index: Int)
        case openAdvertisementLink(link: String)
        case openLink(link: String, name: String)
        case openDeeplink(params: String)
        case presentPhotoPicker( picker: UIImagePickerController)
        case dissmissPhotoPicker( picker: UIImagePickerController)
        case presentMailComposer( composer: MFMailComposeViewController)
        case dissmissMailComposer( composer: MFMailComposeViewController)
    }
}

extension AppkeePhotoPage.Request {

}

extension AppkeePhotoPage.Response {

}

extension AppkeePhotoPage.DisplayData {
    
}

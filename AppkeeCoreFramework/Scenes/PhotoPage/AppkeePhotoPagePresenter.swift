//
//  AppkeePagePresenter.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 26/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

class AppkeePhotoPagePresenter: NSObject {
    // MARK: - Properties
    private let props: AppkeePageProps
    private let graphicManager: AppkeeGraphicManager
    
    private let interactor: AppkeePhotoPageInteractorInput
    weak var coordinator: AppkeePhotoPageCoordinatorInput?
    weak var output: AppkeePhotoPagePresenterOutput?
    
//    private let contentArray: [Content]
    private let contentIds: [Int]
    private var photos: [Int: UIImage] = [:]
    private let recipients: [String]

    // MARK: - Init
    init(interactor: AppkeePhotoPageInteractorInput, coordinator: AppkeePhotoPageCoordinatorInput, graphicManager: AppkeeGraphicManager, props: AppkeePageProps, recipients: [String]) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.props = props
        self.recipients = recipients
        self.graphicManager = graphicManager
        
        contentIds = (props.texts + props.images + props.videos).sorted (by: { $0.order < $1.order }).map({$0.id })
    }
}

// MARK: - User Events -

extension AppkeePhotoPagePresenter: AppkeePhotoPagePresenterInput {
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }

        var photoCpture = false
        for id in contentIds {
            if let text = props.texts.first(where: {$0.id == id }) {
                addText(text: text.content)
                continue
            }
            
            if let image = props.images.first(where: {$0.id == id }) {
                if image.link != nil {
                    addPhotoCature(name: image.content, id: id)
                    photoCpture = true
                } else {
                    addImage(name: image.content, id: id, clickable: image.clickable)
                }

                continue
            }
            
            if let video = props.videos.first(where: {$0.id == id }) {
                addVideo(link: video.content)
                continue
            }
        }

        if photoCpture {
            addSendButton()
        }
    }

    func handle(_ action: AppkeePhotoPage.Action) {
        switch action {
        case let .openLink(link, name):
            coordinator?.navigate(to: .openLink(link: link, name: name))
        case let .openDeeplink(params):
            coordinator?.navigate(to: .openDeeplink(params: params))
        }
    }
    
    private func addText(text: String?) {
        if let text = text {
            output?.addText(text, colorSettings: graphicManager.colorsSettings)
        }
    }
    
    private func addImage(name: String?, id: Int, clickable: Bool) {
        if let image = graphicManager.getImage(name: name) {
            output?.addImage(image, handler: { () in
                if clickable {
                    self.showGallery(images: self.props.images.filter({ $0.clickable }), id: id)
                }
            })
        }
    }
    
    private func addVideo(link: String?) {
        if let link = link {
            output?.addVideo(link)
        }
    }


    private func addSendButton() {
        output?.addSendButton(graphicManager.colorsSettings, handler: {
            self.sendMail()
        })
    }


    private func addPhotoCature(name: String?, id: Int) {
        if let image = graphicManager.getImage(name: name) {
            output?.addPhotoCapture(image, id: id, colorSettings: graphicManager.colorsSettings, handler: {
//                self.output?.presentPhotoPicker(id: id)

                let vc = UIImagePickerController()
                vc.sourceType = .camera
                vc.cameraCaptureMode = .photo
//                vc.allowsEditing = true
                vc.delegate = self
                vc.view.tag = id

                self.coordinator?.navigate(to: .presentPhotoPicker(picker: vc))
            })
        }
    }


    
    private func showGallery(images: [AppkeeContent], id: Int) {
        let clickableImages = images.sorted (by: { $0.id < $1.id })
        if let index = clickableImages.firstIndex(where: { $0.id == id }) {
                coordinator?.navigate(to: .gallery(images: clickableImages, index: index))
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeePhotoPagePresenter: AppkeePhotoPageInteractorOutput {

}

extension AppkeePhotoPagePresenter: MFMailComposeViewControllerDelegate {
    private func sendMail() {
        for image in self.props.images.filter( { $0.link != nil } ) {
            if !photos.keys.contains(image.id) {
                output?.showAlert(title: translations.photoPage.SEND_ERROR)
                return
            }
        }

        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(recipients)

            mail.setSubject(translations.photoPage.SUBJECT + self.props.title)
            mail.setMessageBody("", isHTML: false)

            var counter = 1;
            for key in photos.keys.sorted() {
                guard let image = photos[key], let imageData = image.jpegData(compressionQuality: 0.75) else {
                    continue
                }

                let title = self.props.title + " - " + String(counter) + ".jpg"
                mail.addAttachmentData(imageData, mimeType: "image/jpg", fileName: title)

                counter += 1
            }

            coordinator?.navigate(to: .presentMailComposer(composer: mail))
        }
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        coordinator?.navigate(to: .dissmissMailComposer(composer: controller))
    }
}

extension AppkeePhotoPagePresenter: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let chosenImage = info[.originalImage] as? UIImage else {
            print("No image found")
            return
        }

        let date = Date()

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"

        let formatedDate = dateFormatter.string(from: date)

        let max_size: CGFloat = 640
        var clipRect = CGRect(x: 0, y: 0, width: chosenImage.size.width, height: chosenImage.size.height)

        if chosenImage.size.width > max_size {
            let ratio = chosenImage.size.height / chosenImage.size.width

            clipRect = CGRect(x: 0, y: 0, width: max_size, height: (ratio * max_size))
        }
        if clipRect.size.height > max_size {
            let ratio = chosenImage.size.width / chosenImage.size.height

            clipRect = CGRect(x: 0, y: 0, width: (ratio * max_size), height: max_size)
        }

        UIGraphicsBeginImageContextWithOptions(clipRect.size, false, 1)

        chosenImage.draw(in: clipRect)

        let paragraph = NSMutableParagraphStyle()
        paragraph.lineBreakMode = .byWordWrapping
        paragraph.alignment = .left

        let font = UIFont.systemFont(ofSize: 50.0)
        let color = UIColor.white
        let colorBackgound = UIColor.black

        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: color,
            .backgroundColor: colorBackgound,
            .paragraphStyle: paragraph
        ]

        let attributedStringDate = NSAttributedString(string: formatedDate, attributes: attributes)

        let heightText: CGFloat = 60.0

        let rectTextDate = CGRect(x: 20, y: clipRect.size.height - (1.0 * (heightText + 20.0)), width: clipRect.size.width - 40, height: heightText)

        attributedStringDate.draw(in: rectTextDate)

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();

        let id = picker.view.tag
        if let image = image {
            photos[id] = image
            output?.updatePhoto(id: id, image: image)
        }

        coordinator?.navigate(to: .dissmissPhotoPicker(picker: picker))
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        coordinator?.navigate(to: .dissmissPhotoPicker(picker: picker))
    }
}



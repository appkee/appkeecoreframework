//
//  AppkeePageViewController.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 26/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import UIKit
import CoreLocation

class AppkeePhotoPageViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var stackView: UIStackView!

    // MARK: - Properties
    private var presenter: AppkeePhotoPagePresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeePhotoPagePresenterInput) -> AppkeePhotoPageViewController {
        let name = "\(AppkeePhotoPageViewController.self)"
        let bundle = Bundle(for: AppkeePhotoPageViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeePhotoPageViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()        
    }

    // MARK: - Callbacks -
//    func takePhoto() {
//        let vc = UIImagePickerController()
//        vc.sourceType = .camera
//        vc.allowsEditing = true
//        vc.delegate = self
//        present(vc, animated: true)
//    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeePhotoPageViewController: AppkeePhotoPagePresenterOutput, Progressable, Alertable {
    func showAlert(title: String) {
        showAlert(withTitle: title, message: nil) {}
    }

    func updatePhoto(id: Int, image: UIImage) {
        if let photoCaptureView = self.stackView.viewWithTag(id + AppkeeConstants.photoReportSalt) as? AppkeePhotoCaptureView {
            photoCaptureView.setPhoto(image: image)
        }
    }

    func displayProgress() {
        showProgress()
    }

    func addText(_ text: String, colorSettings: AppkeeColorSettings?) {
        let textView = AppkeeTextView(frame: CGRect(x: 0, y: 0, width: self.stackView.frame.width, height: 50))
        textView.configure(with: text, colorSettings: colorSettings)
        textView.delegate = self
        
        stackView.addArrangedSubview(textView)
    }
    
    func addImage(_ image: UIImage, handler: @escaping () -> Void) {
        let imageView = AppkeeImageView(frame: .zero)
        imageView.configure(with: image, handler: handler)
       
        stackView.addArrangedSubview(imageView)
   }
    
    func addVideo(_ link: String) {
        let videoView = AppkeeVideoView(frame: .zero)
        videoView.configure(with: link)
       
        stackView.addArrangedSubview(videoView)
    }

    func addPhotoCapture(_ image: UIImage, id: Int, colorSettings: AppkeeColorSettings?, handler: @escaping () -> Void) {
        let photoCaptureView = AppkeePhotoCaptureView(frame: .zero)
        photoCaptureView.configure(with: image, id: id, colorSettings: colorSettings, handler: handler)

        photoCaptureView.tag = id + AppkeeConstants.photoReportSalt
        stackView.addArrangedSubview(photoCaptureView)
    }

    func addSendButton(_ colorSettings: AppkeeColorSettings?, handler: @escaping () -> Void) {
        let sendView = AppkeeButtonView(frame: .zero)
        sendView.configure(with: translations.photoPage.SEND, colorSettings: colorSettings, handler: handler)

        stackView.addArrangedSubview(sendView)
    }

    func setupUI(colorSettings: AppkeeColorSettings) {
        self.view.backgroundColor = colorSettings.content
    }
}

extension AppkeePhotoPageViewController: AppkeeTextViewDelegate {
    func openLink(link: String, name: String) {
        presenter.handle(.openLink(link: link, name: name))
    }

    func openDeeplink(params: String) {
        presenter.handle(.openDeeplink(params: params))
    }
}

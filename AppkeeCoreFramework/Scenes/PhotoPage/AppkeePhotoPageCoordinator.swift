//
//  AppkeePageCoordinator.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 26/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit
import MessageUI
import SafariServices

class AppkeePhotoPageCoordinator: AppkeeCoordinator {
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
    private let props: AppkeePageProps
    private let email: String?
    private let search: Bool
    private let recipients: [String]
//    weak var delegate: PageCoordinatorDelegate?
    
    var delegate: AppkeeSideMenuCoordinatorDelegate?

    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, props: AppkeePageProps, recipients: [String],  email: String?, search: Bool) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.props = props
        self.recipients = recipients
        self.email = email
        self.search = search
    }
        
    func start(root: Bool = false) {
        let interactor = AppkeePhotoPageInteractor()
        let presenter = AppkeePhotoPagePresenter(interactor: interactor, coordinator: self, graphicManager: dependencies.graphicManager, props: props, recipients: recipients)
        let vc = AppkeePhotoPageViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        var menuItems: [UIBarButtonItem] = []
        if dependencies.configManager.appMenu {
            let image = UIImage(named: "dots", in: Bundle(for: AppkeePhotoPageCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(debugMenu(_:))))
        }
        if search {
            let image = UIImage(named: "search", in: Bundle(for: AppkeePhotoPageCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(search(_:))))
        }
        if let _ = email {
            let image = UIImage(named: "cameraMenu", in: Bundle(for: AppkeePhotoPageCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(photoReporter(_:))))
        }
        vc.navigationItem.rightBarButtonItems = menuItems
        vc.title = props.title

        let rootController = AppkeeBannerViewController(graphicManager: dependencies.graphicManager)
        rootController.setViewController(vc)
        rootController.delegate = self

        if let banners = props.banners {
            rootController.setBanners(banners: banners)
        }

        if root {
            let image = UIImage(named: "hamburger", in: Bundle(for: AppkeePhotoPageCoordinator.self), compatibleWith: nil)
            rootController.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  image, style: .done, target: self, action: #selector(menu(_:)))

            navigationController.setViewControllers([rootController], animated: false)

            return
        }

        navigationController.navigationBar.isHidden = false
        navigationController.pushViewController(rootController, animated: true)
    }
    
    @objc func menu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openMenu()
    }
    
    @objc func debugMenu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openDebugMenu()
    }
    
    @objc func photoReporter(_ barButtonItem: UIBarButtonItem) {
        if let email = self.email {
            delegate?.openPhotoReporter(email: email)
        }
    }
    
    @objc func search(_ barButtonItem: UIBarButtonItem) {
        delegate?.openSearch()
    }
    
    private func showLink(link: String, name: String) {
//        let props = AppkeeLinkProps(title: name, link: link, offlineLink: nil, banners: self.props.banners, css: nil)
//        let coordinator = AppkeeLinkCoordinator(navigationController: navigationController,
//                                          dependencies: dependencies,
//                                          props: props,
//                                          email: email,
//                                          search: false)
//        childrens.append(coordinator)
////        coordinator.delegate = self
//        coordinator.start(root: false)

        if let url = URL(string: link) {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true

            let vc = SFSafariViewController(url: url, configuration: config)
            navigationController.present(vc, animated: true)
        }
    }
}
// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeePhotoPageCoordinator: AppkeePhotoPageCoordinatorInput {
    func navigate(to route: AppkeePhotoPage.Route) {
        switch route {
        case let .gallery(images, index):
            let coordinator = AppkeeGalleryDetaildCoordinator(navigationController: self.navigationController, dependencies: dependencies, images: images)
            
            childrens.append(coordinator)
            coordinator.start(index: index)
        case let .openAdvertisementLink(link):
            if let url = URL(string: link) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        case let .openLink(link, name):
            showLink(link: link, name: name)
        case let .openDeeplink(params):
            delegate?.openDeeplink(params: params)
        case .presentPhotoPicker(let picker):
            self.navigationController.present(picker, animated: true)
        case .dissmissPhotoPicker:
            if let topViewController = navigationController.presentedViewController, topViewController.isKind(of: UIImagePickerController.self) {
                self.navigationController.dismiss(animated: true)
            }
        case .presentMailComposer(let composer):
            self.navigationController.present(composer, animated: true)
        case .dissmissMailComposer:
            if let topViewController = navigationController.presentedViewController, topViewController.isKind(of: MFMailComposeViewController.self) {
                self.navigationController.dismiss(animated: true)
            }
        }
    }
}

extension AppkeePhotoPageCoordinator: AppkeeBannerViewControllerDelegate {
    func openLink(link: String) {
        self.navigate(to: .openAdvertisementLink(link: link))
    }

    func openDeeplink(params: String) {
        self.navigate(to: .openDeeplink(params: params))
    }
}


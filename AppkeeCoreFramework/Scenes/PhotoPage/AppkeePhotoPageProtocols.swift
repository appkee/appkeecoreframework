//
//  PageProtocols.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 26/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol PageCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeePhotoPageCoordinatorInput: class {
    func navigate(to route: AppkeePhotoPage.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeePhotoPageInteractorInput {
    // func perform(_ request: Page.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeePhotoPageInteractorOutput: class {
    // func present(_ response: Page.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeePhotoPagePresenterInput {
    func viewCreated()
    func handle(_ action: AppkeePhotoPage.Action)
}

// PRESENTER -> VIEW
protocol AppkeePhotoPagePresenterOutput: class {
    func addText(_ text: String, colorSettings: AppkeeColorSettings?)
    func addImage(_ image: UIImage, handler: @escaping () -> Void)
    func addVideo(_ link: String)
    func addPhotoCapture(_ image: UIImage, id: Int, colorSettings: AppkeeColorSettings?, handler: @escaping () -> Void)
    func addSendButton(_ colorSettings: AppkeeColorSettings?, handler: @escaping () -> Void)

    func updatePhoto(id: Int, image: UIImage)

    func showAlert(title: String)

    func displayProgress()

    func setupUI(colorSettings: AppkeeColorSettings)
}

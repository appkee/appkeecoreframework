//
//  AppkeeSettingsProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 16/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeeSettingsCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeSettingsCoordinatorInput: class {
    func navigate(to route: AppkeeSettings.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeSettingsInteractorInput {
     func perform(_ request: AppkeeSettings.Request.DeleteAccount)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeSettingsInteractorOutput: class {
    func present(_ response: AppkeeSettings.Response.DeleteAccount)
    func present(_ response: AppkeeSettings.Response.Error)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeSettingsPresenterInput {
    var numberOfSections: Int { get }
    func numberOfItems(in section: Int) -> Int
    
    func viewCreated()
    func handle(_ action: AppkeeSettings.Action)
    
    func configure(_ item: AppkeeLanguageTableViewCell, at indexPath: IndexPath)
    func configure(_ item: AppkeeNotificationsTableViewCell, at indexPath: IndexPath)
    func configure(_ item: AppkeeDeleteAccountTableViewCellDelegate, at indexPath: IndexPath)
//    func configure(_ item: AppkeeArticleHeaderTableViewDelegate, at section: Int)
    func header(_ section: Int) -> String?
    func configure(_ header: UITableViewHeaderFooterView)
    func sectonType(_ section: Int) -> AppkeeSettings.Section
}

// PRESENTER -> VIEW
protocol AppkeeSettingsPresenterOutput: class {
    // func display(_ displayModel: AppkeeSettings.DisplayData.Work)
        
    func setupUI(colorSettings: AppkeeColorSettings)

    func display(_ displayModel: AppkeeSettings.DisplayData.DeleteAccountAlert)
    func display(_ displayModel: AppkeeSettings.DisplayData.DeleteAccountPassAlert)
    func display(_ displayModel: AppkeeSettings.DisplayData.Progress)
    func display(_ displayModel: AppkeeSettings.DisplayData.Error)
    func display(_ displayModel: AppkeeSettings.DisplayData.DeleteAccount)
}

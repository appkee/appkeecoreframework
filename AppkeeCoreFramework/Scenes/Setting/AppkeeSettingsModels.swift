//
//  AppkeeSettingsModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 16/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeSettings {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case accountDeleteConfirm
        case accountDeletePass(String)
        case login
    }

    enum Route {
        case resetLanguage
        case changeNotification
        case openAdvertisementLink(link: String)
        case openDeeplink(params: String)
        case accountDelete
    }

    enum Section {
        case language
        case notification
        case accountDelete
    }
}

extension AppkeeSettings.Request {
    struct DeleteAccount {
        let appCode: String
        let email: String
        let password: String
    }
}

extension AppkeeSettings.Response {
    struct DeleteAccount {
    }

    struct Error {
        let message: String
    }
}

extension AppkeeSettings.DisplayData {
    struct DeleteAccountAlert {
    }

    struct DeleteAccountPassAlert {
    }

    struct DeleteAccount {
    }

    struct Progress {
    }

    struct Error {
        let message: String
    }
}

//
//  AppkeeNotificationsTableViewCell.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 1/2/22.
//  Copyright © 2022 Radek Zmeskal. All rights reserved.
//

import UIKit

protocol AppkeeNotificationsTableViewCellDelegate: class {
    func configure(with image: UIImage?, notification: String, colorSettings: AppkeeColorSettings?, handler: @escaping () -> Void)
}

class AppkeeNotificationsTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!

    @IBOutlet weak var settingsButton: UIButton!

    //MARK: - Properties
    var handler: () -> Void = { () in }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func settingsButtonTap(_ sender: Any) {
        self.handler()
    }
}

extension AppkeeNotificationsTableViewCell: AppkeeNotificationsTableViewCellDelegate {
    func configure(with image: UIImage?, notification: String, colorSettings: AppkeeColorSettings?, handler: @escaping () -> Void) {
        if let colorSettings = colorSettings {
            self.settingsButton.tintColor = colorSettings.contentText
            self.iconImageView.tintColor = colorSettings.contentText
        }

        self.handler = handler

        self.iconImageView.image = image
        self.settingsButton.setTitle(notification, for: .normal)
    }
}

//
//  AppkeeLanguageTableViewCell.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 16/03/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import UIKit

protocol AppkeeLanguageTableViewCellDelegate: class {
    func configure(with language: String, image: UIImage?, colorSettings: AppkeeColorSettings?, handler: @escaping () -> Void)
}

class AppkeeLanguageTableViewCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.text = translations.settings.CURRENT_LANGUAGE
        }
    }
    
    @IBOutlet weak var languageImageView: UIImageView!
    
    @IBOutlet weak var languageLabel: UILabel!
    
    @IBOutlet weak var changeButton: UIButton! {
        didSet {
            changeButton.setTitle(translations.settings.CHANGE_LANGUAGE, for: .normal)
        }
    }
    
    //MARK: - Properties
    var handler: () -> Void = { () in }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func changeButtonTap(_ sender: Any) {
        handler()
    }
}

extension AppkeeLanguageTableViewCell: AppkeeLanguageTableViewCellDelegate {
    func configure(with language: String, image: UIImage?, colorSettings: AppkeeColorSettings?, handler: @escaping () -> Void) {
        if let colorSettings = colorSettings {
            titleLabel.textColor = colorSettings.contentText
            languageLabel.textColor = colorSettings.contentText
            changeButton.tintColor = colorSettings.contentText
        }
        
        self.handler = handler
        
        languageLabel.text = language
        languageImageView.image = image
    }
}

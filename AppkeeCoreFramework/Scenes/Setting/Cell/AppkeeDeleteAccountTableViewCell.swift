//
//  AppkeeDeleteAccountTableViewCell.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 3/9/22.
//  Copyright © 2022 Radek Zmeskal. All rights reserved.
//

import UIKit

protocol AppkeeDeleteAccountTableViewCellDelegate: class {
    func configure(with image: UIImage?, title: String, colorSettings: AppkeeColorSettings?, handler: @escaping () -> Void)
}

class AppkeeDeleteAccountTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    var handler: () -> Void = { () in }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func buttonTap(_ sender: Any) {
        self.handler()
    }
}

extension AppkeeDeleteAccountTableViewCell: AppkeeDeleteAccountTableViewCellDelegate {
    func configure(with image: UIImage?, title: String, colorSettings: AppkeeColorSettings?, handler: @escaping () -> Void) {
        self.iconImageView.image = image
        self.titleLabel.text = title

        self.handler = handler

        if let colorSettings = colorSettings {
            iconImageView.tintColor = colorSettings.contentText
            titleLabel.textColor = colorSettings.contentText
        }
    }
}

//
//  AppkeeSettingsCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 16/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeeSettingsCoordinator: AppkeeCoordinator {

    // MARK: - Properties
   private let navigationController: UINavigationController
   // NOTE: This array is used to retain child coordinators. Don't forget to
   // remove them when the coordinator is done.
   var childrens: [AppkeeCoordinator] = []
   var dependencies: AppkeeFullDependencies
   private let section: AppkeeSection
   private let email: String?
    private let accountDeletionEnabled: Bool?
   //    weak var delegate: PageCoordinatorDelegate?
   
   var delegate: AppkeeSideMenuCoordinatorDelegate?

   // MARK: - Init
   init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, section: AppkeeSection, email: String?, accountDeletionEnabled: Bool?) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.section = section
        self.email = email
       self.accountDeletionEnabled = accountDeletionEnabled
   }
   
   func start(root: Bool = false) {
       let interactor = AppkeeSettingsInteractor(repository: dependencies.repository, graphicManager: dependencies.graphicManager)
       let presenter = AppkeeSettingsPresenter(interactor: interactor, coordinator: self, configManager: dependencies.configManager, graphicManager: dependencies.graphicManager, section: section, accountDeletionEnabled: accountDeletionEnabled)
        let vc = AppkeeSettingsViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc
       
        var menuItems: [UIBarButtonItem] = []
        if dependencies.configManager.appMenu {
            let image = UIImage(named: "dots", in: Bundle(for: AppkeeSettingsCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(debugMenu(_:))))
        }

        vc.navigationItem.rightBarButtonItems = menuItems
        vc.title = section.name
    
        let rootController = AppkeeBannerViewController(graphicManager: dependencies.graphicManager)
        rootController.setViewController(vc)
        rootController.delegate = self

        if let banners = section.banners {
            rootController.setBanners(banners: banners)
        }

        if root {
            let image = UIImage(named: "hamburger", in: Bundle(for: AppkeeSettingsCoordinator.self), compatibleWith: nil)
            rootController.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  image, style: .done, target: self, action: #selector(menu(_:)))

            navigationController.setViewControllers([rootController], animated: false)

            return
        }
       
        navigationController.navigationBar.isHidden = false
        navigationController.pushViewController(rootController, animated: true)
   }
   
   @objc func menu(_ barButtonItem: UIBarButtonItem) {
       delegate?.openMenu()
   }
   
   @objc func debugMenu(_ barButtonItem: UIBarButtonItem) {
       delegate?.openDebugMenu()
   }
   
   @objc func photoReporter(_ barButtonItem: UIBarButtonItem) {
       if let email = self.email {
           delegate?.openPhotoReporter(email: email)
       }
   }
}
// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeSettingsCoordinator: AppkeeSettingsCoordinatorInput {
    func navigate(to route: AppkeeSettings.Route) {
        switch route {
        case .resetLanguage:
            self.delegate?.openLanguageSelection()
        case let .openAdvertisementLink(link):
            if let url = URL(string: link) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        case let .openDeeplink(params):
            delegate?.openDeeplink(params: params)
        case .changeNotification:
            self.delegate?.openNotificationSettings()
        case .accountDelete:
            self.delegate?.openAccountDelete()
        
        }
    }
}

extension AppkeeSettingsCoordinator: AppkeeBannerViewControllerDelegate {
    func openLink(link: String) {
        self.navigate(to: .openAdvertisementLink(link: link))
    }

    func openDeeplink(params: String) {
        self.navigate(to: .openDeeplink(params: params))
    }
}

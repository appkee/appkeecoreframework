//
//  AppkeeSettingsPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 16/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeeSettingsPresenter {
    // MARK: - Properties
    private let section: AppkeeSection
    private let graphicManager: AppkeeGraphicManager
    private let configManager: AppkeeConfigManager
    
    let interactor: AppkeeSettingsInteractorInput
    weak var coordinator: AppkeeSettingsCoordinatorInput?
    weak var output: AppkeeSettingsPresenterOutput?

    private let sections: [AppkeeSettings.Section]
    
    // MARK: - Init
    init(interactor: AppkeeSettingsInteractorInput, coordinator: AppkeeSettingsCoordinatorInput, configManager: AppkeeConfigManager, graphicManager: AppkeeGraphicManager, section: AppkeeSection, accountDeletionEnabled: Bool?) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.section = section
        self.configManager = configManager
        self.graphicManager = graphicManager

        var sections: [AppkeeSettings.Section] = []

        if UserDefaults.getLanguage() != nil {
            sections.append(.language)
        }

        if UserDefaults.notificationRegionsID != nil {
            sections.append(.notification)
        }

        if UserDefaults.pageUser != nil, accountDeletionEnabled ?? false {
            sections.append(.accountDelete)
        }

        self.sections = sections
    }
}

// MARK: - User Events -

extension AppkeeSettingsPresenter: AppkeeSettingsPresenterInput {
    func header(_ section: Int) -> String? {
        switch sections[section] {
        case .language:
            return translations.settings.HEADER_LANGUAGE
        case .notification:
            return translations.settings.HEADER_NOTIFICATIONS
        case .accountDelete:
            return translations.settings.HEADER_ACCOUNT
        }

        return nil
    }
    
    var numberOfSections: Int {
        return self.sections.count
    }
    
    func numberOfItems(in section: Int) -> Int {
        return 1
    }
    
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }
    }

    func handle(_ action: AppkeeSettings.Action) {
        switch action {
        case .accountDeleteConfirm:
            output?.display(AppkeeSettings.DisplayData.DeleteAccountPassAlert())
        case .accountDeletePass(let password):
            guard let user = UserDefaults.pageUser else {
                return
            }

            output?.display(AppkeeSettings.DisplayData.Progress())

            interactor.perform(AppkeeSettings.Request.DeleteAccount(appCode: configManager.appCode, email: user.email, password: password))
        case .login:
            coordinator?.navigate(to: .accountDelete)
        }
    }
    
    func configure(_ item: AppkeeLanguageTableViewCell, at indexPath: IndexPath) {
        switch sections[indexPath.section] {
        case .language:
            if let language = UserDefaults.getLanguage() {
                item.configure(with: language.title, image: UIImage(named: language.image, in: Bundle(for: AppkeeSettingsPresenter.self), compatibleWith: nil), colorSettings: graphicManager.colorsSettings) {
                    self.coordinator?.navigate(to: .resetLanguage)
                }
            }
        default:
            break
        }
    }

    func configure(_ item: AppkeeNotificationsTableViewCell, at indexPath: IndexPath) {
        switch sections[indexPath.section] {
        case .notification:
            let image = UIImage(named: "notificationsSettings", in: Bundle(for: AppkeeSettingsPresenter.self), compatibleWith: nil)
            item.configure(with: image, notification: translations.settings.CHANGE_NOTIFICATIONS, colorSettings: graphicManager.colorsSettings) {
                self.coordinator?.navigate(to: .changeNotification)
            }
        default:
            break
        }
    }

    func configure(_ item: AppkeeDeleteAccountTableViewCellDelegate, at indexPath: IndexPath) {
        switch sections[indexPath.section] {
        case .accountDelete:
            let image = UIImage(named: "deleteAccount", in: Bundle(for: AppkeeSettingsPresenter.self), compatibleWith: nil)
            item.configure(with: image, title: translations.settings.ACCOUNT_DELETION_TEXT, colorSettings: graphicManager.colorsSettings) {
                self.output?.display(AppkeeSettings.DisplayData.DeleteAccountAlert())
            }
        default:
            break
        }
    }

    func configure(_ header: UITableViewHeaderFooterView) {
        header.textLabel?.textColor = graphicManager.colorsSettings?.menu
        header.contentView.backgroundColor = graphicManager.colorsSettings?.menuText
    }

    func sectonType(_ section: Int) -> AppkeeSettings.Section {
        return self.sections[section]
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeSettingsPresenter: AppkeeSettingsInteractorOutput {
    func present(_ response: AppkeeSettings.Response.DeleteAccount) {
        UserDefaults.pageUser = nil
        output?.display(AppkeeSettings.DisplayData.DeleteAccount())
    }

    func present(_ response: AppkeeSettings.Response.Error) {
        output?.display(AppkeeSettings.DisplayData.Error(message: response.message))
    }
}

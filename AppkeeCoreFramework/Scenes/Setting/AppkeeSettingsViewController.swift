//
//  AppkeeSettingsViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 16/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeSettingsViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(AppkeeLanguageTableViewCell.nib, forCellReuseIdentifier: AppkeeLanguageTableViewCell.reuseId)
            tableView.register(AppkeeNotificationsTableViewCell.nib, forCellReuseIdentifier: AppkeeNotificationsTableViewCell.reuseId)
            tableView.register(AppkeeDeleteAccountTableViewCell.nib, forCellReuseIdentifier: AppkeeDeleteAccountTableViewCell.reuseId)
            tableView.estimatedRowHeight = 30
            tableView.separatorStyle = .none
            tableView.tableFooterView = UIView()
            tableView.rowHeight = UITableView.automaticDimension
            tableView.sectionHeaderHeight = 20
        }
    }
    
    // MARK: - Properties
    private var presenter: AppkeeSettingsPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeSettingsPresenterInput) -> AppkeeSettingsViewController {
        let name = "\(AppkeeSettingsViewController.self)"
        let bundle = Bundle(for: AppkeeSettingsViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeSettingsViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
    }

    // MARK: - Callbacks -

}

extension AppkeeSettingsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.numberOfItems(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch presenter.sectonType(indexPath.section) {
        case .language:
            let cell = tableView.dequeueReusableCell(withIdentifier: AppkeeLanguageTableViewCell.reuseId, for: indexPath) as! AppkeeLanguageTableViewCell
            presenter.configure(cell, at: indexPath)

            return cell
        case .notification:
            let cell = tableView.dequeueReusableCell(withIdentifier: AppkeeNotificationsTableViewCell.reuseId, for: indexPath) as! AppkeeNotificationsTableViewCell
            presenter.configure(cell, at: indexPath)

            return cell
        case .accountDelete:
            let cell = tableView.dequeueReusableCell(withIdentifier: AppkeeDeleteAccountTableViewCell.reuseId, for: indexPath) as! AppkeeDeleteAccountTableViewCell
            presenter.configure(cell, at: indexPath)

            return cell
        }

        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return presenter.header(section)
    }


}

extension AppkeeSettingsViewController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, willDisplayHeaderView view:UIView, forSection: Int) {
//        if let headerView = view as? UITableViewHeaderFooterView {
//            presenter.configure(headerView)
//        }
//    }

    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let header = view as? UITableViewHeaderFooterView {
            presenter.configure(header)
        }
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeSettingsViewController: AppkeeSettingsPresenterOutput, Progressable, Alertable {
    func setupUI(colorSettings: AppkeeColorSettings) {
        self.tableView.separatorColor = colorSettings.header
        self.view.backgroundColor = colorSettings.content
    }

    func display(_ displayModel: AppkeeSettings.DisplayData.Progress) {
        showProgress()
    }


    func display(_ displayModel: AppkeeSettings.DisplayData.Error) {
        hideProgress()

        showErrorAlert(withMessage: displayModel.message)
    }

    func display(_ displayModel: AppkeeSettings.DisplayData.DeleteAccount) {
        hideProgress()

        showAlert(withTitle: translations.settings.ACCOUNT_DELETION_SUCCESS, okTitle: translations.common.OK) { [weak self] in
            self?.presenter.handle(.login)
        }
    }

    func display(_ displayModel: AppkeeSettings.DisplayData.DeleteAccountAlert) {
        showAlert(withTitle: translations.settings.ACCOUNT_DELETION_TITLE, message: translations.settings.ACCOUNT_DELETION_DELETE, okTitle: translations.common.OK, cancelTitle: translations.common.CANCEL) { [weak self] in
            self?.presenter.handle(.accountDeleteConfirm)
        }
    }

    func display(_ displayModel: AppkeeSettings.DisplayData.DeleteAccountPassAlert) {
        showSecureInputAlert(withTitle: translations.settings.ACCOUNT_DELETION_TITLE, placeholderMessage: translations.settings.ACCOUNT_DELETION_PASSWORD, okTitle: translations.common.OK) { [weak self] password in
            self?.presenter.handle(.accountDeletePass(password ?? ""))
        }
    }
}

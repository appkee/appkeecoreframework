//
//  AppkeeSettingsInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 16/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import Alamofire

class AppkeeSettingsInteractor {
    // MARK: - Properties
    weak var output: AppkeeSettingsInteractorOutput?
    private let repository: AppkeeRepository
    private let graphicManager: AppkeeGraphicManager

    // MARK: - Init
    init(repository: AppkeeRepository, graphicManager: AppkeeGraphicManager) {
        self.repository = repository
        self.graphicManager = graphicManager
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeSettingsInteractor: AppkeeSettingsInteractorInput {
    func perform(_ request: AppkeeSettings.Request.DeleteAccount) {
        repository.deleteAccount(appCode: request.appCode, email: request.email, password: request.password) { [weak self] response in
            guard let self = self else { return }

            switch response.result {
            case .success(let response):
                if response.success {
                    self.output?.present(AppkeeSettings.Response.DeleteAccount())
                } else {
                    self.output?.present(AppkeeSettings.Response.Error(message: response.error ?? translations.error.UNKNOWN))
                }
            case .failure(let error):
                self.output?.present(AppkeeSettings.Response.Error(message: error.localizedDescription))

//                Analytics.logEvent("Form sending failed", parameters: ["sectionId": request.sectionId, "message": request.sectionId, "error": error.localizedDescription])
            }
        }
    }
}

//
//  AppkeeSeniorPassBranchesProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/27/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeeSeniorPassBranchesCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeePassBranchesCoordinatorInput: class {
    func navigate(to route: AppkeePassBranches.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeePassBranchesInteractorInput {
    // func perform(_ request: AppkeeSeniorPassBranches.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeePassBranchesInteractorOutput: class {
    // func present(_ response: AppkeeSeniorPassBranches.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeePassBranchesPresenterInput {
    var numberOfSections: Int { get }
    func numberOfItems(in section: Int) -> Int

    func viewCreated()
    func handle(_ action: AppkeePassBranches.Action)

    func configure(_ item: AppkeePassBranchesTableViewCellDelegate, at indexPath: IndexPath)
}

// PRESENTER -> VIEW
protocol AppkeePassBranchesPresenterOutput: class {
    // func display(_ displayModel: AppkeeSeniorPassBranches.DisplayData.Work)
    func setupUI(colorSettings: AppkeeColorSettings)
}

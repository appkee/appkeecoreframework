//
//  AppkeeSeniorPassBranchesCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/27/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeePassBranchesCoordinator: AppkeeCoordinator {
    /// MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
    private let result: AppkeePassBenefits.AppkeePassResult
    private let title: String
    private let email: String?
    private let search: Bool
//    weak var delegate: GalleryCoordinatorDelegate?

    var delegate: AppkeeSideMenuCoordinatorDelegate?

    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, result: AppkeePassBenefits.AppkeePassResult, title: String, email: String?, search: Bool) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.result = result
        self.title = title
        self.email = email
        self.search = search
    }

    func start(root: Bool = false) {
        let interactor = AppkeePassBranchesInteractor(repository: dependencies.repository, graphicManager: dependencies.graphicManager)
        let presenter = AppkeePassBranchesPresenter(interactor: interactor, coordinator: self, graphicManager: dependencies.graphicManager, result: result)
        let vc = AppkeeBranchesViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

//        var menuItems: [UIBarButtonItem] = []
//        if dependencies.configManager.appMenu {
//            let image = UIImage(named: "dots", in: Bundle(for: AppkeeGalleryCoordinator.self), compatibleWith: nil)
//            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(debugMenu(_:))))
//        }
//        if search {
//            let image = UIImage(named: "search", in: Bundle(for: AppkeeGalleryCoordinator.self), compatibleWith: nil)
//            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(search(_:))))
//        }
//        if let _ = email {
//            let image = UIImage(named: "cameraMenu", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
//            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(photoReporter(_:))))
//        }
//        vc.navigationItem.rightBarButtonItems = menuItems

        switch self.result {
        case .senior:
            vc.title = translations.seniorPass.BRANCHES_TITLE
        case .family:
            vc.title = self.title
        }



//        let rootController = AppkeeBannerViewController(graphicManager: dependencies.graphicManager)
//        rootController.setViewController(vc)
//        rootController.delegate = self

//        if let banners = section.banners {
//            rootController.setBanners(banners: banners)
//        }

//        if root {
//            let image = UIImage(named: "hamburger", in: Bundle(for: AppkeeGalleryCoordinator.self), compatibleWith: nil)
//            rootController.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  image, style: .done, target: self, action: #selector(menu(_:)))
//
//            navigationController.setViewControllers([rootController], animated: false)
//
//            return
//        }

        navigationController.navigationBar.isHidden = false
        navigationController.pushViewController(vc, animated: true)
    }

    @objc func menu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openMenu()
    }

    @objc func debugMenu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openDebugMenu()
    }

    @objc func photoReporter(_ barButtonItem: UIBarButtonItem) {
        if let email = self.email {
            delegate?.openPhotoReporter(email: email)
        }
    }

    @objc func search(_ barButtonItem: UIBarButtonItem) {
        delegate?.openSearch()
    }
}

// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeePassBranchesCoordinator: AppkeePassBranchesCoordinatorInput {
    func navigate(to route: AppkeePassBranches.Route) {
        switch route {
        case .openSenior(let branch):
            let coordinator = AppkeePassBranchDetailCoordinator(navigationController: self.navigationController, dependencies: dependencies, branch: .senior(branch), email: email, search: search)
            childrens.append(coordinator)
            coordinator.delegate = delegate
            coordinator.start()
        case .openFamily(let branch):
            let coordinator = AppkeePassBranchDetailCoordinator(navigationController: self.navigationController, dependencies: dependencies, branch: .family(branch), email: email, search: search)
            childrens.append(coordinator)
            coordinator.delegate = delegate
            coordinator.start()
        }
    }

}

//extension AppkeeSeniorPassBranchesCoordinator: AppkeeBannerViewControllerDelegate {
//    func openLink(link: String) {
//        self.navigate(to: .openAdvertisementLink(link: link))
//    }
//}

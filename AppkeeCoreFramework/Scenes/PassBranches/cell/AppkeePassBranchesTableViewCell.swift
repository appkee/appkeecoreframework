//
//  AppkeeSeniorPassBranchTableViewCell.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/27/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import UIKit

protocol AppkeePassBranchesTableViewCellDelegate: class {
    func configure(with branch: AppkeeSeniorPassBranch, colorSettings: AppkeeColorSettings?)
    func configure(with branch: AppkeeFamilyPassBranch, colorSettings: AppkeeColorSettings?)
}

class AppkeePassBranchesTableViewCell: UITableViewCell {

    @IBOutlet weak var containerLabel: UIView! {
        didSet {
            containerLabel.layer.cornerRadius = 8.0
        }
    }

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var separatorView: UIView!

    @IBOutlet weak var addressTitleLabel: UILabel! {
        didSet {
            addressTitleLabel.text = translations.seniorPass.BRANCH_ADDRESS
        }
    }
    @IBOutlet weak var addressLabel: UILabel!

    @IBOutlet weak var discountTitleLabel: UILabel! {
        didSet {
            discountTitleLabel.text = translations.seniorPass.BRANCH_DISCOUNTS
        }
    }
    @IBOutlet weak var discountLabel: UILabel!

    @IBOutlet weak var distanceLabel: UILabel! {
        didSet {
            distanceLabel.text = ""
        }
    }
    @IBOutlet weak var searchImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension AppkeePassBranchesTableViewCell: AppkeePassBranchesTableViewCellDelegate {
    func configure(with branch: AppkeeSeniorPassBranch, colorSettings: AppkeeColorSettings?) {
        nameLabel.text = branch.name
        addressLabel.text = branch.address

        let dis: [String] =  branch.discounts.compactMap { return $0.value != nil ? String(Int($0.value ?? 0)) : nil }
        var discounts = dis.joined(separator: "%, ")
        discounts = discounts.count > 0 ? discounts + "%" : discounts

        discountLabel.text = discounts

        if let distance = branch.distance {
            distanceLabel.text = String(format: translations.seniorPass.BRANCH_DISTANCE, distance/1000.0)
        }

        searchImageView.isHidden = true
        if let colorSettings = colorSettings {
            nameLabel.textColor = colorSettings.menu
            searchImageView.tintColor = colorSettings.menu
        }
    }

    func configure(with branch: AppkeeFamilyPassBranch, colorSettings: AppkeeColorSettings?) {
        nameLabel.text = branch.company_name
        addressLabel.text =  "\(branch.street_one) \(branch.street_two ?? "")\n\(branch.post_code) \(branch.city)"

        discountLabel.text = branch.text_sale

        if let distance = branch.distance {
            distanceLabel.text = String(format: translations.seniorPass.BRANCH_DISTANCE, distance/1000.0)
        }

        if let colorSettings = colorSettings {
            nameLabel.textColor = colorSettings.loginColor
            searchImageView.tintColor = colorSettings.loginColor
        }
    }
}

//
//  AppkeeSeniorPassBranchesViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/27/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeBranchesViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(AppkeePassBranchesTableViewCell.nib, forCellReuseIdentifier: AppkeePassBranchesTableViewCell.reuseId)
            tableView.estimatedRowHeight = 30
            tableView.separatorStyle = .none
            tableView.tableFooterView = UIView()
            tableView.rowHeight = UITableView.automaticDimension
        }
    }
    
    // MARK: - Properties
    private var presenter: AppkeePassBranchesPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeePassBranchesPresenterInput) -> AppkeeBranchesViewController {
        let name = "\(AppkeeBranchesViewController.self)"
        let bundle = Bundle(for: AppkeeBranchesViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeBranchesViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
    }

    // MARK: - Callbacks -

}

extension AppkeeBranchesViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.numberOfItems(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AppkeePassBranchesTableViewCell.reuseId, for: indexPath) as! AppkeePassBranchesTableViewCell
        presenter.configure(cell, at: indexPath)

        cell.selectionStyle = .none

        return cell
    }
}

extension AppkeeBranchesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.handle(.navigate(indexPath.row))
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeBranchesViewController: AppkeePassBranchesPresenterOutput {
    func setupUI(colorSettings: AppkeeColorSettings) {

        self.view.backgroundColor = colorSettings.content
    }
}

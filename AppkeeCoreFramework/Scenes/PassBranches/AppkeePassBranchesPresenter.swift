//
//  AppkeeSeniorPassBranchesPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/27/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeePassBranchesPresenter {
    // MARK: - Properties
    private let graphicManager: AppkeeGraphicManager

    private let result: AppkeePassBenefits.AppkeePassResult

    let interactor: AppkeePassBranchesInteractorInput
    weak var coordinator: AppkeePassBranchesCoordinatorInput?
    weak var output: AppkeePassBranchesPresenterOutput?

    // MARK: - Init
    init(interactor: AppkeePassBranchesInteractorInput, coordinator: AppkeePassBranchesCoordinatorInput, graphicManager: AppkeeGraphicManager, result: AppkeePassBenefits.AppkeePassResult) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
        self.result = result
    }
}

// MARK: - User Events -

extension AppkeePassBranchesPresenter: AppkeePassBranchesPresenterInput {
    func configure(_ item: AppkeePassBranchesTableViewCellDelegate, at indexPath: IndexPath) {
        switch self.result {
        case .senior(let data):
            let branch = data[indexPath.row]
            item.configure(with: branch, colorSettings: graphicManager.colorsSettings)
        case .family(let data):
            let branch = data[indexPath.row]
            item.configure(with: branch, colorSettings: graphicManager.colorsSettings)
        }
    }

    var numberOfSections: Int {
        return 1
    }

    func numberOfItems(in section: Int) -> Int {
        switch self.result {
        case .senior(let data):
            return data.count
        case .family(let data):
            return data.count
        }
        return 0
    }

    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }
    }

    func handle(_ action: AppkeePassBranches.Action) {
        switch action {
        case .navigate(let index):
            switch self.result {
            case .senior(let data):
                let branch = data[index]
                coordinator?.navigate(to: .openSenior(branch))
            case .family(let data):
                let branch = data[index]
                coordinator?.navigate(to: .openFamily(branch))
            }
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeePassBranchesPresenter: AppkeePassBranchesInteractorOutput {

}

//
//  AppkeeSeniorPassBranchesModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 6/27/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeePassBranches {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case navigate(Int)
    }

    enum Route {
        case openSenior(AppkeeSeniorPassBranch)
        case openFamily(AppkeeFamilyPassBranch)
    }
}

extension AppkeePassBranches.Request {

}

extension AppkeePassBranches.Response {

}

extension AppkeePassBranches.DisplayData {
    
}

//
//  AppkeeQRCodeReaderInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 15/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeQRCodeReaderInteractor {
    // MARK: - Properties
    weak var output: AppkeeQRCodeReaderInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeQRCodeReaderInteractor: AppkeeQRCodeReaderInteractorInput {
}

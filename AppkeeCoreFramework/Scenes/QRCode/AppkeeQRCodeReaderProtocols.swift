//
//  AppkeeQRCodeReaderProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 15/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeeQRCodeReaderCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeQRCodeReaderCoordinatorInput: class {
    func navigate(to route: AppkeeQRCodeReader.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeQRCodeReaderInteractorInput {
    // func perform(_ request: AppkeeQRCodeReader.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeQRCodeReaderInteractorOutput: class {
    // func present(_ response: AppkeeQRCodeReader.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeQRCodeReaderPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeeQRCodeReader.Action)
}

// PRESENTER -> VIEW
protocol AppkeeQRCodeReaderPresenterOutput: class {    
    // func display(_ displayModel: AppkeeQRCodeReader.DisplayData.Work)
    func setupUI(content: AppkeeContent?, colorSettings: AppkeeColorSettings)
    
    func startScanning()
}

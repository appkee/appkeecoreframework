//
//  AppkeeQRCodeReaderViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 15/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeQRCodeReaderViewController: UIViewController {
    // MARK: - Outlets

    @IBOutlet weak var textView: AppkeeTextView!

    @IBOutlet weak var qrCodeScanner: QRScannerView!
    
    @IBOutlet weak var rescanButton: UIButton! {
        didSet {
            rescanButton.setTitle(translations.qrCode.RESCAN, for: .normal)
        }
    }
    
    // MARK: - Properties
    private var presenter: AppkeeQRCodeReaderPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeQRCodeReaderPresenterInput) -> AppkeeQRCodeReaderViewController {
        let name = "\(AppkeeQRCodeReaderViewController.self)"
        let bundle = Bundle(for: AppkeeQRCodeReaderViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeQRCodeReaderViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
        
        qrCodeScanner.delegate = self
    }

    // MARK: - Callbacks -

    @IBAction func rescanButtonTap(_ sender: Any) {
        presenter.handle(.rescan)
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeQRCodeReaderViewController: AppkeeQRCodeReaderPresenterOutput {
    
    func startScanning() {
        qrCodeScanner.startScanning()
    }
    
    func setupUI(content: AppkeeContent?, colorSettings: AppkeeColorSettings) {
        self.view.backgroundColor = colorSettings.content
        
        self.rescanButton.backgroundColor = colorSettings.menu
        self.rescanButton.setTitleColor(colorSettings.menuText, for: .normal)
        
        if let text = content?.content {
            textView.configure(with: text, colorSettings: colorSettings)
        }
    }
}

extension AppkeeQRCodeReaderViewController: QRScannerViewDelegate {
    func qrScanningDidFail() {
        
    }
    
    func qrScanningSucceededWithCode(_ str: String?) {
        if let str = str {
            presenter.handle(.open(request: str))
        }
    }
    
    func qrScanningDidStop() {
        
    }
}

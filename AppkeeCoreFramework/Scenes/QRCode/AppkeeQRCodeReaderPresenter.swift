//
//  AppkeeQRCodeReaderPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 15/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeeQRCodeReaderPresenter {
    // MARK: - Properties
    private let section: AppkeeSection
    private let graphicManager: AppkeeGraphicManager
    
    let interactor: AppkeeQRCodeReaderInteractorInput
    weak var coordinator: AppkeeQRCodeReaderCoordinatorInput?
    weak var output: AppkeeQRCodeReaderPresenterOutput?

    // MARK: - Init
    init(interactor: AppkeeQRCodeReaderInteractorInput, coordinator: AppkeeQRCodeReaderCoordinatorInput, graphicManager: AppkeeGraphicManager, section: AppkeeSection) {
        self.interactor = interactor
        self.coordinator = coordinator
        
        self.graphicManager = graphicManager
        self.section = section
    }
}

// MARK: - User Events -

extension AppkeeQRCodeReaderPresenter: AppkeeQRCodeReaderPresenterInput {
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(content: section.texts?.first, colorSettings: colorsSettings)
        }
    }

    func handle(_ action: AppkeeQRCodeReader.Action) {
        switch action {
        case let .open(request):
            if request.hasPrefix("openImage:") {
                let image = String(request.dropFirst(10))
                if let image = UIImage(named: image) {
                    coordinator?.navigate(to: .openImage(image: image))
                    return
                }
            }
            
            output?.startScanning()
        case .rescan:
            output?.startScanning()
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeQRCodeReaderPresenter: AppkeeQRCodeReaderInteractorOutput {

}

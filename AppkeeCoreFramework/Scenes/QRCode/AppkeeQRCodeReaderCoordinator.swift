//
//  AppkeeQRCodeReaderCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 15/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeeQRCodeReaderCoordinator: AppkeeCoordinator {
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
    private let section: AppkeeSection
//    weak var delegate: FormCoordinatorDelegate?
    private let email: String?
    private let search: Bool
        
    var delegate: AppkeeSideMenuCoordinatorDelegate?
    
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, section: AppkeeSection, email: String?, search: Bool) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.section = section
        self.email = email
        self.search = search
    }

    func start(root: Bool = false) {
        let interactor = AppkeeQRCodeReaderInteractor()
        let presenter = AppkeeQRCodeReaderPresenter(interactor: interactor, coordinator: self, graphicManager: dependencies.graphicManager, section: section)
        let vc = AppkeeQRCodeReaderViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        var menuItems: [UIBarButtonItem] = []
        if dependencies.configManager.appMenu {
            let image = UIImage(named: "dots", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(debugMenu(_:))))
        }

        if let _ = UserDefaults.pageUser {
            let image = UIImage(named: "logoutIcon", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(logoutTap(_:))))
        }

        if search {
            let image = UIImage(named: "search", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(search(_:))))
        }

        if let _ = email {
            let image = UIImage(named: "cameraMenu", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(photoReporter(_:))))
        }

        if UserDefaults.seniorPassLogin != nil || UserDefaults.familyPassLogin != nil {
            let image = UIImage(named: "logoutIcon", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(passLogoutTap(_:))))
        }
        
        vc.navigationItem.rightBarButtonItems = menuItems
        vc.title = section.name

        let rootController = AppkeeBannerViewController(graphicManager: dependencies.graphicManager)
        rootController.setViewController(vc)
        rootController.delegate = self

        if let banners = section.banners {
            rootController.setBanners(banners: banners)
        }

        if root {
            let image = UIImage(named: "hamburger", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
            rootController.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  image, style: .done, target: self, action: #selector(menu(_:)))

            navigationController.setViewControllers([rootController], animated: false)

            return
        }

        navigationController.navigationBar.isHidden = false
        navigationController.pushViewController(rootController, animated: true)
    }

    @objc func menu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openMenu()
    }
    
    @objc func debugMenu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openDebugMenu()
    }
    
    @objc func photoReporter(_ barButtonItem: UIBarButtonItem) {
        if let email = self.email {
            delegate?.openPhotoReporter(email: email)
        }
    }
    
    @objc func search(_ barButtonItem: UIBarButtonItem) {
        delegate?.openSearch()
    }

    @objc func logoutTap(_ barButtonItem: UIBarButtonItem) {
        delegate?.logout()
    }

    @objc func passLogoutTap(_ barButtonItem: UIBarButtonItem) {
        delegate?.passLogout()
    }
}
// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeQRCodeReaderCoordinator: AppkeeQRCodeReaderCoordinatorInput {
    func navigate(to route: AppkeeQRCodeReader.Route) {
        switch route {
        case let .openImage(image):
            let coordinator = AppkeeGalleryDetaildCoordinator(navigationController: self.navigationController, dependencies: dependencies, image: image)
            
            childrens.append(coordinator)
            coordinator.start()
        case let .openAdvertisementLink(link):
            if let url = URL(string: link) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        case let .openDeeplink(params):
            delegate?.openDeeplink(params: params)
        }
    }
}

extension AppkeeQRCodeReaderCoordinator: AppkeeBannerViewControllerDelegate {
    func openLink(link: String) {
        self.navigate(to: .openAdvertisementLink(link: link))
    }

    func openDeeplink(params: String) {
        self.navigate(to: .openDeeplink(params: params))
    }
}

//
//  AppkeeQRCodeReaderModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 15/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

enum AppkeeQRCodeReader {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case open(request: String)
        case rescan
    }

    enum Route {
        case openImage(image: UIImage)
        case openAdvertisementLink(link: String)
        case openDeeplink(params: String)
    }
}

extension AppkeeQRCodeReader.Request {

}

extension AppkeeQRCodeReader.Response {

}

extension AppkeeQRCodeReader.DisplayData {
    
}

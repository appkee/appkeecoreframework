//
//  SearchModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 7/3/20.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeSearch {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case navigate(index: Int, force: Bool)
    }

    enum Route {
        case open(AppkeePageProps)
        case openLink(title: String, link: String)        
    }
}

extension AppkeeSearch.Request {

}

extension AppkeeSearch.Response {

}

extension AppkeeSearch.DisplayData {
    
}

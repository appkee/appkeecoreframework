//
//  AppkeeSearchInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 7/3/20.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeSearchInteractor {
    // MARK: - Properties
    weak var output: AppkeeSearchInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeSearchInteractor: AppkeeSearchInteractorInput {
}

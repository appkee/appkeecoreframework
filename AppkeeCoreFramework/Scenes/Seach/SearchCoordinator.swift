//
//  SearchCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 7/3/20.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeeSearchCoordinator: AppkeeCoordinator {
    
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
//    weak var delegate: SearchCoordinatorDelegate?

    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies) {
        self.navigationController = navigationController
        self.dependencies = dependencies
    }

    func start(root: Bool = false) {
        let interactor = SearchInteractor()
        let presenter = SearchPresenter(interactor: interactor, coordinator: self)
        let vc = SearchViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        // FIXME: Display as you need
        // navigationController.setViewControllers([vc], animated: false)
    }
}
// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeSearchCoordinator: SearchCoordinatorInput {
    func navigate(to route: Search.Route) {
        
    }
}

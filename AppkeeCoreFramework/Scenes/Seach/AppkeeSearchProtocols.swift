//
//  AppkeeSearchProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 7/3/20.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol SearchCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeSearchCoordinatorInput: class {
    func navigate(to route: AppkeeSearch.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeSearchInteractorInput {
    // func perform(_ request: Search.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeSearchInteractorOutput: class {
    // func present(_ response: Search.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeSearchPresenterInput {
    var numberOfSections: Int { get }
    func numberOfItems(in section: Int) -> Int
    
    func viewCreated()
    func handle(_ action: AppkeeSearch.Action)
    func updateSearch(string: String)
    
    func configure(_ item: AppkeeSearchTableViewCellDelegate, at indexPath: IndexPath)
}

// PRESENTER -> VIEW
protocol SearchPresenterOutput: class {
    func setupUI(colorSettings: AppkeeColorSettings)
    
    func reloadTable()
    func showPin(index: Int, pinCode: String)
}

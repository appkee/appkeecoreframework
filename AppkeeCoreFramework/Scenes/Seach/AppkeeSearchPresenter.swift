//
//  AppkeeSearchPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 7/3/20.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeSearchPresenter {
    
    struct SearchField {
        let name: String
        let nameLovercase: String
        let sectionId: Int
        let articleId: Int
    }

    struct Data {
        let article: AppkeeArticle
        let sectionName: String
        let articleName: String
    }
    
    // MARK: - Properties
    let interactor: AppkeeSearchInteractorInput
    weak var coordinator: AppkeeSearchCoordinatorInput?
    weak var output: SearchPresenterOutput?
    
    private let graphicManager: AppkeeGraphicManager
    private var results: [Data] = []
    private var dataSource: [Data] = []
    private var sections: [AppkeeSection] = []

    // MARK: - Init
    init(interactor: AppkeeSearchInteractorInput, coordinator: AppkeeSearchCoordinatorInput, graphicManager: AppkeeGraphicManager, sections: [AppkeeSection]) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
        self.sections = sections
        
        dataSouce()
    }
    
    private func dataSouce() {
        var dataSource: [Data] = []
        
        for section in self.sections {
            if section.visibility, section.type == .articles, let articles = section.articles {
                dataSource.append(contentsOf:
                                    (articles.filter({ $0.name != nil && $0.visibility}).map { Data(
                                                article: $0,
                                                sectionName: section.name,
                                                articleName: ($0.name ?? "").unaccent().lowercased()
                                    )
                }))
            }
        }

        dataSource = dataSource.sorted(by: { $0.articleName < $1.articleName   })

        self.dataSource = dataSource
        self.results = dataSource
    }
}

// MARK: - User Events -

extension AppkeeSearchPresenter: AppkeeSearchPresenterInput {
    
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfItems(in section: Int) -> Int {
        return results.count
    }
        
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }                
    }

    func handle(_ action: AppkeeSearch.Action) {
        switch action {
        case let  .navigate(index, force):
            let article = results[index].article
            guard let section = sections.first(where: { $0.id == article.sectionId }) else {
                return
            }

            if let pinCode = section.pinCode {
                if force {
                    UserDefaults.addSectionPinCode(sectionID: section.id, pinCode: pinCode)
                } else {
                    let pinCodeSection = UserDefaults.sectionPinCode(sectionID: section.id)
                    if pinCode != pinCodeSection {
                        output?.showPin(index: index, pinCode: pinCode)
                        return
                    }
                }
            }

            switch article.type {
            case .page:
                let texts = section.texts?.filter({ $0.articleId == article.id }) ?? []
                let images = section.images?.filter({ $0.articleId == article.id }) ?? []
                let videos = section.videos?.filter({ $0.articleId == article.id }) ?? []
                
//                let props = AppkeePageProps(articleId: -1, title: article.name ?? "", texts: texts, images: images, videos: videos, favorites: false, favoritesType: nil, advertisement: section.advertisement)
                let props = AppkeePageProps(articleId: -1, title: article.name ?? "", texts: texts, images: images, videos: videos, favorites: false, favoritesType: nil, banners: nil)
                
                coordinator?.navigate(to: .open(props))
            case .link:
                guard let link = article.link else {
                    return
                }

                coordinator?.navigate(to: .openLink(title: article.name ?? "", link: link))
            case .program:
                break
//                if let program = section.programs.first(where: { $0.articleId == article.id }) {
//                if let program = section.programs.first {
//                    reorderProgram(program: program)
//                    coordinator?.navigate(to: .openProgram(program: program, title: article.name))
//                }
            case .myProgram:
                break
//                if let program = section.programs.first {
//                    reorderProgram(program: program)
//
//                    coordinator?.navigate(to: .openMyProgram(program: program, title: article.name, emptyMessage: section.texts?.first(where: { $0.articleId == article.id })?.content))
//                }
            case .articles:
                fatalError()
            }
            break
        }
    }
    
    func updateSearch(string: String) {
        if string.isEmpty {
            self.results = self.dataSource
        } else {
            let string = string.unaccent().lowercased()
            self.results = self.dataSource.filter({ $0.articleName.contains(string) })
        }
        
        output?.reloadTable()
    }
        
    func configure(_ item: AppkeeSearchTableViewCellDelegate, at indexPath: IndexPath) {
        let field = results[indexPath.row]
        
        item.configure(with: (field.article.name ?? ""), subTitle: field.sectionName, colorSettings: graphicManager.colorsSettings)
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeSearchPresenter: AppkeeSearchInteractorOutput {

}

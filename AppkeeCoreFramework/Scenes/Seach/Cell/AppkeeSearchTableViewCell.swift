//
//  AppkeeSearchTableViewCell.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 7/3/20.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import UIKit

protocol AppkeeSearchTableViewCellDelegate: class {
    func configure(with title: String, subTitle: String?, colorSettings: AppkeeColorSettings?)
}

class AppkeeSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


extension AppkeeSearchTableViewCell: AppkeeSearchTableViewCellDelegate {
 
    func configure(with title: String, subTitle: String?, colorSettings: AppkeeColorSettings?) {
        if let colorSettings = colorSettings {
            titleLabel.textColor = colorSettings.contentText
        }

        let text = NSMutableAttributedString()

        let titleAttributes = [ NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16) ]
        let titleAttributed = NSMutableAttributedString(string: title, attributes: titleAttributes)
        text.append(titleAttributed)

        if let subtitle = subTitle {
            let subtitle = " (" + subtitle + ")"

            let subtitleAttributes = [ NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12) ]
            let subtitleAttributed = NSMutableAttributedString(string: subtitle, attributes: subtitleAttributes)
            text.append(subtitleAttributed)
        }
        titleLabel.attributedText = text
    }
}

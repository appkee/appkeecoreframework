//
//  SearchTableViewCell.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 7/3/20.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import UIKit

protocol AppkeeSearchTableViewCellDelegate: class {
    func configure(with title: String, colorSettings: AppkeeColorSettings?)
}

class AppkeeSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


extension AppkeeSearchTableViewCell: AppkeeSearchTableViewCellDelegate {
 
    func configure(with title: String, colorSettings: AppkeeColorSettings?) {
        if let colorSettings = colorSettings {
            titleLabel.textColor = colorSettings.contentText
        }
            
        titleLabel.text = title
    }
}

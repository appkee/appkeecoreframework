//
//  AppkeeSearchCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 7/3/20.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeeSearchCoordinator: AppkeeCoordinator {
    
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
//    weak var delegate: SearchCoordinatorDelegate?
    private var sections: [AppkeeSection] = []
    private let email: String?

    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, email: String?, sections: [AppkeeSection]) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.sections = sections
        self.email = email
    }

    func start(root: Bool = false) {
        let interactor = AppkeeSearchInteractor()
        let presenter = AppkeeSearchPresenter(interactor: interactor, coordinator: self, graphicManager: dependencies.graphicManager, sections: sections)
        let vc = AppkeeSearchViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc
        
        navigationController.navigationBar.isHidden = false
        navigationController.pushViewController(vc, animated: true)
    }
    
    private func showLink(name: String, link: String) {
        let props = AppkeeLinkProps(title: name, link: link, offlineLink: nil, banners: nil, css: nil)
            let coordinator = AppkeeLinkCoordinator(navigationController: navigationController,
                                              dependencies: dependencies,
                                              props: props,
                                              email: email,
                                              search: false)
            childrens.append(coordinator)
    //        coordinator.delegate = self
            coordinator.start(root: false)
        }
}

// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeSearchCoordinator: AppkeeSearchCoordinatorInput {
    func navigate(to route: AppkeeSearch.Route) {
        switch route {
        case .open(let props):
            let coordinator = AppkeePageCoordinator(navigationController: navigationController,
                                              dependencies: dependencies,
                                              props: props,
                                              email: email,
                                              search: false)
            childrens.append(coordinator)
            coordinator.start()
//        case let .openProgram(program, title):
//            let coordinator = AppkeeProgramCoordinator(navigationController: self.navigationController, dependencies: dependencies, section: program, advertisment: section.advertisement, title: title)
//            childrens.append(coordinator)
//            coordinator.start()
//        case let .openMyProgram(program, title, emptyMessage):
//            let coordinator = AppkeeMyProgramCoordinator(navigationController: self.navigationController, dependencies: dependencies, section: program, advertisment: section.advertisement, title: title, emptyMessage: emptyMessage)
//            childrens.append(coordinator)
//            coordinator.start()
        case let .openLink(name, link):
            showLink(name: name, link: link)
        }
    }
}


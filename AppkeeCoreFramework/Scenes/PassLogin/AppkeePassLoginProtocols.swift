//
//  AppkeeIntroPageProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 5/23/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeeIntroPageCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeePassLoginCoordinatorInput: class {
    func navigate(to route: AppkeePassLogin.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeePassLoginInteractorInput {
    func perform(_ request: AppkeePassLogin.Request.Login)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeePassLoginInteractorOutput: class {
    func present(_ response: AppkeePassLogin.Response.LoginFamily)
    func present(_ response: AppkeePassLogin.Response.LoginSenior)
    func present(_ response: AppkeePassLogin.Response.Error)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeePassLoginPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeePassLogin.Action)
}

// PRESENTER -> VIEW
protocol AppkeePassLoginPresenterOutput: class {
    // func display(_ displayModel: AppkeeIntroPage.DisplayData.Work)

    func display(title: String?, image: UIImage?, preview: Bool, info: String?)
    func display( login: String?)

    func display(_ displayModel: AppkeePassLogin.DisplayData.Finish)
    func display(_ displayModel: AppkeePassLogin.DisplayData.Error)

    func setupUI(colorSettings: AppkeeColorSettings)
}

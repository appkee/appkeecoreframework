//
//  AppkeeIntroPageModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 5/23/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

enum AppkeePassLogin {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case login(key: String)
        case registration
        case preview
    }

    enum Route {
        case menu
        case registration(url: String)
        case preview(UIImage)
    }
}

extension AppkeePassLogin.Request {

    struct Login {
        let key: String
        let appCode: String
        let passType: PassType
    }
}

extension AppkeePassLogin.Response {

    struct LoginSenior {
        let data: AppkeeSeniorPass
    }
    
    struct LoginFamily {
        let data: AppkeeFamilyPass
    }

    struct Error {
        let message: String
    }
}

extension AppkeePassLogin.DisplayData {
    struct Finish {

    }

    struct Error {
        let message: String
    }
}

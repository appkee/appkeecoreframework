//
//  AppkeeIntroPageViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 5/23/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeePassLoginViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var containerStackView: UIStackView!

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!

    @IBOutlet weak var loginTextField: UITextField!

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!

    @IBOutlet weak var infoTextView: UITextView!

    // MARK: - Properties
    private var presenter: AppkeePassLoginPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeePassLoginPresenterInput) -> AppkeePassLoginViewController {
        let name = "\(AppkeePassLoginViewController.self)"
        let bundle = Bundle(for: AppkeePassLoginViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeePassLoginViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()

        self.titleLabel.text = translations.loginPage.TITLE
        self.titleLabel.text = translations.loginPage.TITLE

        self.loginTextField.placeholder = translations.seniorPass.LOGIN_CARD_ID
        self.loginButton.setTitle(translations.loginPage.LOGIN.uppercased(), for: .normal)
        self.registerButton.setTitle(translations.seniorPass.LOGIN_REGISTER.uppercased(), for: .normal)

        self.loginTextField.delegate = self
    }

    // MARK: - Callbacks -

    @IBAction func registrationButtonTap(_ sender: Any) {
        presenter.handle(.registration)
    }

    @IBAction func emailButtonTap(_ sender: Any) {
        showProgress()

        presenter.handle(.login(key: loginTextField.text ?? ""))
    }

    @objc func previewButtonTap(_ sender: Any) {
        presenter.handle(.preview)
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeePassLoginViewController: AppkeePassLoginPresenterOutput, Alertable, Progressable {
    func display(title: String?, image: UIImage?, preview: Bool, info: String?) {
        self.subTitleLabel.setHtml(from: title ?? "")
        self.logoImageView.image = image

        self.infoTextView.text = info

        if preview {
            let button = UIButton(type: .infoLight)
            button.tintColor = self.titleLabel.textColor
            button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -2, bottom: 0, right: 0)
            button.frame = CGRect(x: loginTextField.frame.size.width - 30.0, y: 0.0, width: 25.0, height: 25.0)
            button.addTarget(self, action: #selector(self.previewButtonTap), for: .touchUpInside)
            loginTextField.rightView = button
            loginTextField.rightViewMode = .always
        }
    }

    func display(login: String?) {
        self.loginTextField.text = login

        self.showProgress()
        presenter.handle(.login(key: loginTextField.text ?? ""))
    }

    func display(_ displayModel: AppkeePassLogin.DisplayData.Finish) {
        hideProgress()
    }

    func display(_ displayModel: AppkeePassLogin.DisplayData.Error) {
        hideProgress()

        showErrorAlert(withMessage: displayModel.message)
    }

    func setupUI(colorSettings: AppkeeColorSettings) {
        self.view.backgroundColor = colorSettings.content

        self.loginButton.setTitleColor(.white, for: .normal)
        self.loginButton.backgroundColor = colorSettings.loginColor

        self.registerButton.setTitleColor(.white, for: .normal)
        self.registerButton.backgroundColor = colorSettings.menu

        self.infoTextView.tintColor = colorSettings.loginColor

        self.titleLabel.textColor = colorSettings.contentText
        self.subTitleLabel.textColor = colorSettings.contentText
        self.infoTextView.textColor = colorSettings.contentText
    }
}

extension AppkeePassLoginViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.loginButton.isEnabled = !((self.loginTextField.text?.isEmpty ?? true))
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.loginButton.endEditing(true)
        return false
    }
}

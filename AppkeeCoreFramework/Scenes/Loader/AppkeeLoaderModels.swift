//
//  AppkeeLoaderModels.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 05/05/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeLoader {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action { }

    enum Route {
        case customLoader
        case menu
        case login
        case pass(passType: PassType)
        case loginRegister
        case photoReport
        case pin(pinCode: String)
        case notificationAreas
    }
}

extension AppkeeLoader.Request {
    struct AppData {
        let appCode: String
        let loggedIn: Bool
        let appUserId: Int?
    }
    
    struct NewApp {
        let appCode: String
        let pass: PassType?
    }

    struct SavedAppData {
    }
}

extension AppkeeLoader.Response {
    struct Menu {
        
    }

    struct PhotoReport {
        
    }
    
    struct CustomLoader {
        
    }

    struct Login {

    }

    struct SeniorPass {

    }

    struct FamillyPass {

    }

    struct LoginRegister {

    }

    struct Pin {
        let pinCode: String
    }
    
    struct NewApp {
        let result: Bool
        let pass: PassType?
    }

    struct Error {
        let message: String
    }

    struct NotificationAreas {
        
    }
}

extension AppkeeLoader.DisplayData {
    struct Error {
        let message: String
    }
}

//
//  AppkeeLoaderViewController.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 05/05/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeLoaderViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var imageView: UIImageView!
    
    // MARK: - Properties
    private var presenter: AppkeeLoaderPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeLoaderPresenterInput) -> AppkeeLoaderViewController {
        let name = "\(AppkeeLoaderViewController.self)"
        let bundle = Bundle(for: AppkeeLoaderViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeLoaderViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
        
        imageView.image = UIImage(named: "loader")
    }

    // MARK: - Callbacks -

}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeLoaderViewController: AppkeeLoaderPresenterOutput, Alertable {
    func display(_ displayModel: AppkeeLoader.DisplayData.Error) {
        showErrorAlert(withMessage: displayModel.message)
    }
}

//
//  AppkeeLoaderCoordinator.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 05/05/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeeLoaderCoordinator: AppkeeCoordinator {
    // MARK: - PropertiesAppkeeAppDescription
    let navigationController: UINavigationController
    var dependencies: AppkeeFullDependencies
    
    weak var delegate: AppkeeCoordinatorDelegate?
    
    private let window: UIWindow
    
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    
    // MARK: - Init
    init(window: UIWindow, dependencies: AppkeeFullDependencies) {
        self.dependencies = dependencies
        self.window = window
        
        self.navigationController = UINavigationController()
        self.navigationController.view.backgroundColor = .white
        
        self.navigationController.navigationBar.isHidden = true
    }
    
    func start(root: Bool = false) {
        let interactor = AppkeeLoaderInteractor(repository: dependencies.repository,
                                          graphicManager: dependencies.graphicManager,
                                          appDescriptionManager: dependencies.appDescriptionManager)
        let presenter = AppkeeLoaderPresenter(
            interactor: interactor,
            coordinator: self,
            configManager: dependencies.configManager
        )
        let vc = AppkeeLoaderViewController.instantiate(with: presenter)
        
        interactor.output = presenter
        presenter.output = vc
        
        navigationController.setViewControllers([vc], animated: false)
        
        window.rootViewController = navigationController
    }
}

// MARK: - User Events -

extension AppkeeLoaderCoordinator: AppkeeLoaderCoordinatorInput {
    func newApp() {
        delegate?.newApp()
    }

    func navigate(to route: AppkeeLoader.Route) {
        switch route {
        case .menu:
            delegate?.navigate(to: .menu)
        case .customLoader:
            delegate?.navigate(to: .customLoader)
        case .login:
            delegate?.navigate(to: .login)
        case .pass(let passType):
            delegate?.navigate(to: .pass(passType: passType))
        case .photoReport:
            delegate?.navigate(to: .photoReport)
        case .pin(let pinCode):
            delegate?.navigate(to: .pin(pinCode: pinCode))
        case .notificationAreas:
            delegate?.navigate(to: .notificationAreas)
        case .loginRegister:
            delegate?.navigate(to: .loginRegister)
        }
    }
}


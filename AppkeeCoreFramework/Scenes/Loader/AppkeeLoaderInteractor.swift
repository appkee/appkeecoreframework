//
//  AppkeeLoaderInteractor.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 05/05/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import FirebaseAnalytics
import FBSDKLoginKit

class AppkeeLoaderInteractor {
    // MARK: - Properties
    weak var output: AppkeeLoaderInteractorOutput?
    private let repository: AppkeeRepository
    private let graphicManager: AppkeeGraphicManager
    private let appDescriptionManager: AppkeeAppDescriptionManager        
    
    // MARK: - Init
    init(repository: AppkeeRepository, graphicManager: AppkeeGraphicManager, appDescriptionManager: AppkeeAppDescriptionManager) {
        self.repository = repository
        self.graphicManager = graphicManager
        self.appDescriptionManager = appDescriptionManager
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeLoaderInteractor: AppkeeLoaderInteractorInput {
    func perform(_ request: AppkeeLoader.Request.NewApp) {
        repository.newApp(appCode: request.appCode) { [weak self] response in
            guard let self = self else { return }
            
            switch response.result {
            case .success(let response):
                self.output?.present(AppkeeLoader.Response.NewApp(result: true, pass: request.pass))
                return
            case .failure(let error):
                self.output?.present(AppkeeLoader.Response.NewApp(result: false, pass: request.pass))
                
                Analytics.logEvent("Sending new app indicator failed", parameters: ["appCode": request.appCode, "error": error.localizedDescription])
            }
        }
    }
        
    func perform(_ request: AppkeeLoader.Request.AppData) {
        repository.appData(appCode: request.appCode, loggedIn: request.loggedIn, appUserId: request.appUserId){ [weak self] response in
            guard let self = self else { return }
            
            switch response.result {
            case .success(let response):
                if let description = response.data, response.success {
                    self.appDescriptionManager.saveAppDescription(appDescription: description)
                    self.initGraphic(appDescrition: description, appCode: request.appCode)

                    self.parseResponse(description: description)
                } else {
                    self.output?.present(AppkeeLoader.Response.Error(message: response.error ?? translations.error.UNKNOWN))
                }
            case .failure(let error):
                if let description = self.appDescriptionManager.readAppDescription() {
                    self.initGraphic(appDescrition: description, appCode: request.appCode)
                    self.parseResponse(description: description)
                } else {
                    self.output?.present(AppkeeLoader.Response.Error(message: error.localizedDescription))
                    print("\(error)")
                    Analytics.logEvent("Loading of App data failed", parameters: ["appCode": request.appCode, "error": error.localizedDescription])
                    
                }
            }
        }
    }

    private func parseResponse(description: AppkeeAppDescription) {
        UserDefaults.setLanguageCode(code: description.language)

        if description.photoReportApp, !(description.photoReportAppEmails?.isEmpty ?? true) {
            self.output?.present(AppkeeLoader.Response.PhotoReport())
            return
        }

        if description.customIntroPage == "login" && !UserDefaults.isWebViewLogged {
            self.output?.present(AppkeeLoader.Response.CustomLoader())
        } else if description.customIntroPage == "seniorpas" {
            self.output?.present(AppkeeLoader.Response.SeniorPass())
        } else if description.customIntroPage == "rodinnepasy" {
            self.output?.present(AppkeeLoader.Response.FamillyPass())
        } else if description.customIntroPage == "login-register" {
            self.output?.present(AppkeeLoader.Response.LoginRegister())
        } else if description.loginPage {
            if !Connectivity.isConnectedToInternet(), AccessToken.current != nil {
                self.output?.present(AppkeeLoader.Response.Menu())
            } else {
                self.output?.present(AppkeeLoader.Response.Login())
            }
        } else if let pinCode = description.pinCode, pinCode != UserDefaults.pinCode() {
            self.output?.present(AppkeeLoader.Response.Pin(pinCode: pinCode))
        } else if description.regionNotificationsEnabled, !(description.notificationRegions?.isEmpty ?? true), UserDefaults.notificationRegionsID == nil {
            self.output?.present(AppkeeLoader.Response.NotificationAreas())
        } else {
            self.output?.present(AppkeeLoader.Response.Menu())
        }
    }

    private func initGraphic(appDescrition:  AppkeeAppDescription, appCode: String) {
        graphicManager.generateColors(appDesription: appDescrition)
        graphicManager.downloadImages(appDesription: appDescrition, appCode: appCode)
    }
}

//
//  AppkeeLoaderProtocols.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 05/05/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol LoaderCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeLoaderCoordinatorInput: class {
    func navigate(to route: AppkeeLoader.Route)
    func newApp()
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeLoaderInteractorInput {
    func perform(_ request: AppkeeLoader.Request.AppData)
    func perform(_ request: AppkeeLoader.Request.NewApp)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeLoaderInteractorOutput: class {
    func present(_ response: AppkeeLoader.Response.Menu)
    func present(_ response: AppkeeLoader.Response.PhotoReport)
    func present(_ response: AppkeeLoader.Response.CustomLoader)
    func present(_ response: AppkeeLoader.Response.Login)
    func present(_ response: AppkeeLoader.Response.SeniorPass)
    func present(_ response: AppkeeLoader.Response.FamillyPass)
    func present(_ response: AppkeeLoader.Response.LoginRegister)
    func present(_ response: AppkeeLoader.Response.Pin)
    func present(_ response: AppkeeLoader.Response.NewApp)
    func present(_ response: AppkeeLoader.Response.NotificationAreas)
    func present(_ response: AppkeeLoader.Response.Error)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeLoaderPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeeLoader.Action)
}

// PRESENTER -> VIEW
protocol AppkeeLoaderPresenterOutput: class {
    func display(_ displayModel: AppkeeLoader.DisplayData.Error)
}

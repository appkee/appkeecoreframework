//
//  AppkeeLoaderPresenter.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 05/05/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeLoaderPresenter {
    // MARK: - Properties
    let interactor: AppkeeLoaderInteractorInput
    weak var coordinator: AppkeeLoaderCoordinatorInput?
    weak var output: AppkeeLoaderPresenterOutput?
    
    private let configManager: AppkeeConfigManager

    // MARK: - Init
    init(interactor: AppkeeLoaderInteractorInput, coordinator: AppkeeLoaderCoordinatorInput, configManager: AppkeeConfigManager) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.configManager = configManager
    }
}

// MARK: - User Events -

extension AppkeeLoaderPresenter: AppkeeLoaderPresenterInput {
    func viewCreated() {
        // FIXME: - test
        let request = AppkeeLoader.Request.AppData(appCode: configManager.appCode, loggedIn: UserDefaults.isWebViewLogged, appUserId: UserDefaults.pageUser?.id)
        interactor.perform(request)
        
        UserDefaults.clearAdvertisement()
        UserDefaults.bannersClean()
    }

    func handle(_ action: AppkeeLoader.Action) {
        
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeLoaderPresenter: AppkeeLoaderInteractorOutput {
    func present(_ response: AppkeeLoader.Response.Error) {
//        output?.display(Loader.DisplayData.Error(message: response.message))
    }
    
    func present(_ response: AppkeeLoader.Response.NewApp) {
        UserDefaults.newLogin = !response.result
        coordinator?.newApp()

        switch response.pass {
        case .family:
            coordinator?.navigate(to: .pass(passType: .family))
        case .senior:
            coordinator?.navigate(to: .pass(passType: .senior))
        case .none:
            coordinator?.navigate(to: .menu)
        }
    }
    
    func present(_ response: AppkeeLoader.Response.CustomLoader) {
        coordinator?.navigate(to: .customLoader)
    }

    func present(_ response: AppkeeLoader.Response.PhotoReport) {
        coordinator?.navigate(to: .photoReport)
    }

    func present(_ response: AppkeeLoader.Response.Login) {
        coordinator?.navigate(to: .login)
    }

    func present(_ response: AppkeeLoader.Response.SeniorPass) {
        if UserDefaults.newLogin, !configManager.appMenu {
            let request = AppkeeLoader.Request.NewApp(appCode: configManager.appCode, pass: .senior)
            interactor.perform(request)

            return
        }
        
        coordinator?.navigate(to: .pass(passType: .senior))
    }

    func present(_ response: AppkeeLoader.Response.FamillyPass) {
        if UserDefaults.newLogin, !configManager.appMenu {
            let request = AppkeeLoader.Request.NewApp(appCode: configManager.appCode, pass: .family)
            interactor.perform(request)

            return
        }

        coordinator?.navigate(to: .pass(passType: .family))
    }

    func present(_ response: AppkeeLoader.Response.LoginRegister) {
        coordinator?.navigate(to: .loginRegister)
    }


    func present(_ response: AppkeeLoader.Response.Pin) {
        coordinator?.navigate(to: .pin(pinCode: response.pinCode))
    }
    
    func present(_ response: AppkeeLoader.Response.Menu) {
        if UserDefaults.newLogin, !configManager.appMenu {
            let request = AppkeeLoader.Request.NewApp(appCode: configManager.appCode, pass: nil)
            interactor.perform(request)
            
            return
        }
        
        coordinator?.navigate(to: .menu)
    }

    func present(_ response: AppkeeLoader.Response.NotificationAreas) {
        coordinator?.navigate(to: .notificationAreas)
    }
}

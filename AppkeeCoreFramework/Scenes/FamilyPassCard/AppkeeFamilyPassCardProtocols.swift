//
//  AppkeeFamilyPassCardProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/20/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

// ======== Coordinator ======== //

//protocol AppkeeFamilyPassCardCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeFamilyPassCardCoordinatorInput: class {
    func navigate(to route: AppkeeFamilyPassCard.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeFamilyPassCardInteractorInput {
    // func perform(_ request: AppkeeFamilyPassCard.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeFamilyPassCardInteractorOutput: class {
    // func present(_ response: AppkeeFamilyPassCard.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeFamilyPassCardPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeeFamilyPassCard.Action)
}

// PRESENTER -> VIEW
protocol AppkeeFamilyPassCardPresenterOutput: class {
    // func display(_ displayModel: AppkeeFamilyPassCard.DisplayData.Work)

    func display( text: String, frontImage: UIImage?, backImage: UIImage?, barcode: UIImage?)

    func setupUI(colorSettings: AppkeeColorSettings)
}

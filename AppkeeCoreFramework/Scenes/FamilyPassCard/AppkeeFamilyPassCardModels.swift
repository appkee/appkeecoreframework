//
//  AppkeeFamilyPassCardModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/20/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeFamilyPassCard {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {

    }

    enum Route {
        case openAdvertisementLink(link: String)
        case openDeeplink(params: String)
    }
}

extension AppkeeFamilyPassCard.Request {

}

extension AppkeeFamilyPassCard.Response {

}

extension AppkeeFamilyPassCard.DisplayData {
    
}

//
//  AppkeeFamilyPassCardViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/20/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeFamilyPassCardViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var frontImageView: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var barcodeImageView: UIImageView!
    
    // MARK: - Properties
    private var presenter: AppkeeFamilyPassCardPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeFamilyPassCardPresenterInput) -> AppkeeFamilyPassCardViewController {
        let name = "\(AppkeeFamilyPassCardViewController.self)"
        let bundle = Bundle(for: AppkeeFamilyPassCardViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeFamilyPassCardViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
    }

    // MARK: - Callbacks -

}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeFamilyPassCardViewController: AppkeeFamilyPassCardPresenterOutput {
    func display(text: String, frontImage: UIImage?, backImage: UIImage?, barcode: UIImage?) {
        textLabel.text = text
        frontImageView.image = frontImage
        backImageView.image = backImage
        barcodeImageView.image = barcode
    }

    func setupUI(colorSettings: AppkeeColorSettings) {
        self.textLabel.textColor = colorSettings.contentText
    }
}

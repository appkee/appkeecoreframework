//
//  AppkeeFamilyPassCardInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/20/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeFamilyPassCardInteractor {
    // MARK: - Properties
    weak var output: AppkeeFamilyPassCardInteractorOutput?
    private let repository: AppkeeRepository
    private let graphicManager: AppkeeGraphicManager

    // MARK: - Init
    init(repository: AppkeeRepository, graphicManager: AppkeeGraphicManager) {
        self.repository = repository
        self.graphicManager = graphicManager
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeFamilyPassCardInteractor: AppkeeFamilyPassCardInteractorInput {
}

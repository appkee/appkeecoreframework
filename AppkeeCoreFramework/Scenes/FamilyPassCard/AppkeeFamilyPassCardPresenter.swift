//
//  AppkeeFamilyPassCardPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 2/20/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeFamilyPassCardPresenter {
    // MARK: - Properties
    private let graphicManager: AppkeeGraphicManager

    let interactor: AppkeeFamilyPassCardInteractorInput
    weak var coordinator: AppkeeFamilyPassCardCoordinatorInput?
    weak var output: AppkeeFamilyPassCardPresenterOutput?

    // MARK: - Init
    init(interactor: AppkeeFamilyPassCardInteractorInput, coordinator: AppkeeFamilyPassCardCoordinatorInput, graphicManager: AppkeeGraphicManager) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
    }

    func generateBarcode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CICode128BarcodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }
}

// MARK: - User Events -

extension AppkeeFamilyPassCardPresenter: AppkeeFamilyPassCardPresenterInput {
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }

        if let familyPass = UserDefaults.familyPassLogin {
            let plurar = familyPass.kidsYears.count > 1 ? translations.seniorPass.BRANCH_KIDS_MANY : translations.seniorPass.BRANCH_KIDS_ONE

            var text = "\(translations.seniorPass.CARD_FAMILY)\n"
            text += "\(familyPass.familyName.uppercased())\n"
            text += "\(familyPass.kidsYears.count) \(plurar) - \(familyPass.kidsYears.map { String( $0 % 100 ) }.joined(separator: ", ") )\n"
            text += "\(familyPass.number)"

            let frontImage = UIImage(named: "card_region_\(familyPass.region)_front", in: Bundle(for: AppkeeFamilyPassCardPresenter.self), compatibleWith: nil)
            let backImage = UIImage(named: "card_region_\(familyPass.region)_back", in: Bundle(for: AppkeeFamilyPassCardPresenter.self), compatibleWith: nil)

            let code =  String(repeating: "0", count: 12 - familyPass.number.count) + familyPass.number
            let barcodeImage = self.generateBarcode(from: code)

            output?.display(text: text, frontImage: frontImage, backImage: backImage, barcode: barcodeImage)
        }
    }

    func handle(_ action: AppkeeFamilyPassCard.Action) {
        
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeFamilyPassCardPresenter: AppkeeFamilyPassCardInteractorOutput {

}

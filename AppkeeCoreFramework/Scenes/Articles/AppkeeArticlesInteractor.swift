//
//  AppkeeArticlesInteractor.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 27/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeArticlesInteractor {
    // MARK: - Properties
    weak var output: AppkeeArticlesInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeArticlesInteractor: AppkeeArticlesInteractorInput {
}

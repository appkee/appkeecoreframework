//
//  AppkeeArticleTableViewCell.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 27/09/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

protocol AppkeeArticleTableViewCellDelegate: class {
    func configure(with type: AppkeeArticleContentType, hideImages: Int, icon: UIImage?, title: String?, description: String?, colorSettings: AppkeeColorSettings?)
    func configure(icon: UIImage?)
}

class AppkeeArticleTableViewCell: UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var imageStackView: UIStackView!
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension AppkeeArticleTableViewCell: AppkeeArticleTableViewCellDelegate {
    func configure(icon: UIImage?) {
        iconImageView.image = icon
    }
 
    func configure(with type: AppkeeArticleContentType, hideImages: Int, icon: UIImage?, title: String?, description: String?, colorSettings: AppkeeColorSettings?) {
        if let colorSettings = colorSettings {
            titleLabel.textColor = colorSettings.contentText
            descriptionLabel.textColor = colorSettings.contentText
        }
    
        switch type {
        case .page, .link, .articles:
            switch hideImages {
            case 3:
                iconImageView.isHidden = true
                imageStackView.isHidden = true
                titleLabel.isHidden = false
                titleLabel.numberOfLines = 2
                descriptionLabel.isHidden = true

                iconImageView.image = icon

                self.titleLabel.text = title ?? ""
            case 0:
                iconImageView.isHidden = false
                imageStackView.isHidden = false
                titleLabel.isHidden = false
                titleLabel.numberOfLines = 1
                descriptionLabel.isHidden = false

                iconImageView.image = icon
                titleLabel.text = title ?? ""

                self.descriptionLabel.text = description?.stripOutHtml()
            case 1:
                iconImageView.isHidden = true
                imageStackView.isHidden = true
                titleLabel.isHidden = false
                titleLabel.numberOfLines = 1
                descriptionLabel.isHidden = false

                titleLabel.text = title ?? ""

                self.descriptionLabel.text = description?.stripOutHtml()
            case 2:
                iconImageView.isHidden = false
                imageStackView.isHidden = false
                titleLabel.isHidden = false
                titleLabel.numberOfLines = 2
                descriptionLabel.isHidden = true

                iconImageView.image = icon
                titleLabel.text = title ?? ""
            default:
                iconImageView.isHidden = true
                imageStackView.isHidden = true
                titleLabel.isHidden = true
                descriptionLabel.isHidden = true
            }

            // TODO: move to switch
//            iconImageView.image = icon
//                            
//            titleLabel.text = title ?? ""
//
//            descriptionLabel.text = description?.stripOutHtml()

            
        case .myProgram, .program:
            iconImageView.isHidden = false
            imageStackView.isHidden = false
            titleLabel.isHidden = false
            descriptionLabel.isHidden = true

            iconImageView.image = icon
            titleLabel.text = title ?? ""
            descriptionLabel.text = nil
        }
    }
}

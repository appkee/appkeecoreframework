//
//  AppkeeHeaderTableViewCell.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 10/11/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit
import WebKit

protocol AppkeeArticleHeaderTableViewDelegate: class {
    func configure(with image: UIImage?, content: String?, colorSettings: AppkeeColorSettings?, handler: @escaping () -> Void)
}

class AppkeeArticleHeaderTableView: UIView {

    //MARK: IBOutlets
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var imageImageView: UIImageViewSizeToFit!
    
    //MARK: - Properties
    var handler: () -> Void = { () in }
    
    var webView: WKWebViewSizeToFit? = nil
    
    //MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadNib()
    }
        
    //MARK: - Action
    
    @IBAction func buttonTap(_ sender: Any) {
        handler()
    }
}

extension AppkeeArticleHeaderTableView: AppkeeArticleHeaderTableViewDelegate {
    
    func configure(with image: UIImage?, content: String?, colorSettings: AppkeeColorSettings?, handler: @escaping () -> Void) {
        self.handler = handler
        
        self.layoutIfNeeded()
        
        stackView.isHidden = false
        
        if let image = image {
            imageImageView.isHidden = false
            imageImageView.image = image
            
        } else {
            imageImageView.isHidden = true
        }
        
        for subview in stackView.subviews {
            if subview.isKind(of: WKWebViewSizeToFit.self) {
                stackView.removeArrangedSubview(subview)
                subview.removeFromSuperview()
            }
        }
        
        guard let content = content else {
            if imageImageView.isHidden {
                stackView.removeFromSuperview()
            }
            return
        }
        
        let webView = WKWebViewSizeToFit(frame: self.frame)
        
        webView.scrollView.isScrollEnabled = false
        webView.scrollView.bounces = false
        webView.sizeToFit()
        webView.isOpaque = false
//        webView.navigationDelegate = self
        
        self.webView = webView
        
        let view = UIView()
        view.addSubview(webView)
        view.backgroundColor = .red
        
        stackView.addArrangedSubview(webView)
                
        webView.loadHTMLString(content.toHtml(colorSettings: colorSettings), baseURL: nil)

        self.layoutIfNeeded()
    }
}

extension AppkeeArticleHeaderTableView: WKNavigationDelegate {
   
}

//
//  AppkeeArticlesProtocols.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 27/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol ArticlesCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeArticlesCoordinatorInput: class {
    func navigate(to route: AppkeeArticles.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeArticlesInteractorInput {
    // func perform(_ request: Articles.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeArticlesInteractorOutput: class {
    // func present(_ response: Articles.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeArticlesPresenterInput {
    var numberOfSections: Int { get }
    func numberOfItems(in section: Int) -> Int
    
    func viewCreated()
    func handle(_ action: AppkeeArticles.Action)
    
    func configure(_ item: AppkeeArticleTableViewCellDelegate, at indexPath: IndexPath)
    func configure(_ item: AppkeeArticleHeaderTableViewDelegate)
}

// PRESENTER -> VIEW
protocol AppkeeArticlesPresenterOutput: class {
    func display(_ displayModel: AppkeeArticles.DisplayData.Header)    
    func setupUI(colorSettings: AppkeeColorSettings)
}

//
//  AppkeeMyProgramCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeeMyProgramCoordinator: AppkeeCoordinator {
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
    private let section: AppkeeProgramSection
    private let banners: [AppkeeAdvertisement]?
    private let title: String?
    private let email: String?
    private let emptyMessage: String?
//    weak var delegate: GalleryCoordinatorDelegate?
    
    var delegate: AppkeeSideMenuCoordinatorDelegate?

    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, section: AppkeeProgramSection, banners: [AppkeeAdvertisement]?, email: String? = nil, title: String? = nil, emptyMessage: String?) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.section = section
        self.banners = banners
        self.title = title
        self.email = email
        self.emptyMessage = emptyMessage
    }

    func start(root: Bool = false) {
        let interactor = AppkeeMyProgramInteractor()
        let presenter = AppkeeMyProgramPresenter(interactor: interactor, coordinator: self, graphicManager: dependencies.graphicManager, program: section, emptyMessage: emptyMessage)
        let vc = AppkeeMyProgramViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        var menuItems: [UIBarButtonItem] = []
        if dependencies.configManager.appMenu {
            let image = UIImage(named: "dots", in: Bundle(for: AppkeeMyProgramCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(debugMenu(_:))))
        }
        if let _ = email {
            let image = UIImage(named: "cameraMenu", in: Bundle(for: AppkeeMyProgramCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(photoReporter(_:))))
        }
        vc.navigationItem.rightBarButtonItems = menuItems
        vc.title = title
        
        let rootController = AppkeeBannerViewController(graphicManager: dependencies.graphicManager)
        rootController.setViewController(vc)
        rootController.delegate = self

        if let banners = self.banners {
            rootController.setBanners(banners: banners)
        }

        if root {
            let image = UIImage(named: "hamburger", in: Bundle(for: AppkeeMyProgramCoordinator.self), compatibleWith: nil)
            rootController.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  image, style: .done, target: self, action: #selector(menu(_:)))

            navigationController.setViewControllers([rootController], animated: false)

            return
        }
        
        navigationController.pushViewController(rootController, animated: true)
    }
    
    @objc func menu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openMenu()
    }
    
    @objc func debugMenu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openDebugMenu()
    }
    
    @objc func photoReporter(_ barButtonItem: UIBarButtonItem) {
        if let email = self.email {
            delegate?.openPhotoReporter(email: email)
        }
    }
}


// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeMyProgramCoordinator: AppkeeMyProgramCoordinatorInput {
    func navigate(to route: AppkeeMyProgram.Route) {
        switch route {
        case let .openAdvertisementLink(link):
            if let url = URL(string: link) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        case let .openDeeplink(params):
            delegate?.openDeeplink(params: params)
        case .detail(content: let content):
            let coordinator = AppkeeProgramDetailCoordinator(navigationController: self.navigationController, dependencies: dependencies, content: content)
            childrens.append(coordinator)
            coordinator.start()
        }
    }
}

extension AppkeeMyProgramCoordinator: AppkeeBannerViewControllerDelegate {
    func openLink(link: String) {
        self.navigate(to: .openAdvertisementLink(link: link))
    }

    func openDeeplink(params: String) {
        self.navigate(to: .openDeeplink(params: params))
    }
}


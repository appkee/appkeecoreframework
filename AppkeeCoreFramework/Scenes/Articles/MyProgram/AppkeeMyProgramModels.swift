//
//  AppkeeMyProgramModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeMyProgram {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case updateDay(Int)
        case rowSelected(Int)
    }

    enum Route {
        case openAdvertisementLink(link: String)
        case openDeeplink(params: String)
        case detail(content: String)
    }
}

extension AppkeeMyProgram.Request {

}

extension AppkeeMyProgram.Response {

}

extension AppkeeMyProgram.DisplayData {
    
}

//
//  AppkeeMyProgramPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeMyProgramPresenter {
    struct MyProgramDay {
        let name: String
        let order: Int
        var content: [AppkeeContentProgram] = []
    }
        
    // MARK: - Properties
    let interactor: AppkeeMyProgramInteractorInput
    weak var coordinator: AppkeeMyProgramCoordinatorInput?
    weak var output: AppkeeMyProgramPresenterOutput?
    
    private let program: AppkeeProgramSection
    private let graphicManager: AppkeeGraphicManager
    
    private let emptyMessage: String?
    
    private var data: [MyProgramDay] = []
    
    private var selectedDay: Int = 0;

    // MARK: - Init
    init(interactor: AppkeeMyProgramInteractorInput,
         coordinator: AppkeeMyProgramCoordinatorInput,
         graphicManager: AppkeeGraphicManager,
         program: AppkeeProgramSection,
         emptyMessage: String?) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.program = program
        self.emptyMessage = emptyMessage
        self.graphicManager = graphicManager
        
        dataSource()
    }
    
    func dataSource() {
        
        var days: [MyProgramDay] = []
        
        for day in self.program.program {
            var newDay = MyProgramDay(name: day.name, order: day.order)
            
            var newPrograms: [AppkeeContentProgram] = []
            for room in day.content {
                
                for (index, program) in room.content.enumerated() {
                    if UserDefaults.containsProgramFavourite(programId: program.id) {
                        var program = room.content[index]
                        program.room = room.name
                        newPrograms.append(program)
                    }
                }
            }
            
            newDay.content = newPrograms.sorted(by: {$0.dateTime > $1.dateTime})
            
            days.append(newDay)
        }
        
        self.data = days
    }
}

// MARK: - User Events -

extension AppkeeMyProgramPresenter: AppkeeMyProgramPresenterInput {
    
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfItems(in section: Int) -> Int {
        let day = self.data[selectedDay]
        return day.content.count
    }
    
    var numberOfSectionsForDays: Int {
        return 1
    }
    
    func numberOfItemsForDays(in section: Int) -> Int {
        self.data.count
    }
            
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings, emptyMessage: self.emptyMessage?.toHtml() ?? NSAttributedString(string: translations.common.TABLE_FAVOURITES_NO_DATA))
        }
        
        output?.updateIndexes(dayIndex: selectedDay)
    }

    func handle(_ action: AppkeeMyProgram.Action) {
        switch action {
        case .updateDay(let index):
            if (selectedDay != index) {
                selectedDay = index
            }
            output?.reload()
        case .rowSelected(let index):
            let day = self.data[selectedDay]
            let program = day.content[index]

            if let content = program.content {
                self.coordinator?.navigate(to: .detail(content: content))
            }

        }
    }
            
    func configure(_ item: AppkeeProgramTableViewCell, at indexPath: IndexPath) {
        var day = self.data[selectedDay]
        let program = day.content[indexPath.row]
        
        item.configure(with: program.title,
                       day: day.name,
                       time: program.time,
                       room: program.room ?? "",
                       favorite: true,
                       backgroundColor: program.color,
                       textColor: program.textColor,
                       colorSettings: graphicManager.colorsSettings,
                       favouriteHandler: { (favourite) in
                        
                        if (!favourite) {
                            UserDefaults.removeProgramFavourite(programId: program.id)
                            
                            day.content.remove(at: indexPath.row)
                            self.data[self.selectedDay] = day

                            DispatchQueue.main.async {
                                self.output?.reload()
                            }
                        }
        })
    }
    
    func configure(day item: AppkeeProgramCollectionViewCell, at indexPath: IndexPath) {
        let day = self.program.program[indexPath.row]
        
        item.configure(with: day.name, colorSettings: graphicManager.colorsSettings)
    }        
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeMyProgramPresenter: AppkeeMyProgramInteractorOutput {

}

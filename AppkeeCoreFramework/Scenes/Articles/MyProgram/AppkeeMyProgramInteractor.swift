//
//  AppkeeMyProgramInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeMyProgramInteractor {
    // MARK: - Properties
    weak var output: AppkeeMyProgramInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeMyProgramInteractor: AppkeeMyProgramInteractorInput {
}

//
//  AppkeeMyProgramViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeMyProgramViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var dayCollectionView: UICollectionView! {
        didSet {
            dayCollectionView.register(AppkeeProgramCollectionViewCell.nib, forCellWithReuseIdentifier: AppkeeProgramCollectionViewCell.reuseId)
        }
    }
    
    @IBOutlet weak var programTableView: UITableView! {
        didSet {
            programTableView.register(AppkeeProgramTableViewCell.nib, forCellReuseIdentifier: AppkeeProgramTableViewCell.reuseId)
            programTableView.register(AppkeeProgramBreakTableViewCell.nib, forCellReuseIdentifier: AppkeeProgramBreakTableViewCell.reuseId)
            
            programTableView.tableFooterView = UIView()
        }
    }

    // MARK: - Properties
    private var presenter: AppkeeMyProgramPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeMyProgramPresenterInput) -> AppkeeMyProgramViewController {
        let name = "\(AppkeeMyProgramViewController.self)"
        let bundle = Bundle(for: AppkeeMyProgramViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeMyProgramViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
    }

    // MARK: - Callbacks -

}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeMyProgramViewController: AppkeeMyProgramPresenterOutput {
    
    func updateIndexes(dayIndex: Int) {
        dayCollectionView.selectItem(at: IndexPath(row: dayIndex, section: 0), animated: true, scrollPosition: .left)
        programTableView.reloadData()
    }
    
    func setupUI(colorSettings: AppkeeColorSettings, emptyMessage: NSAttributedString) {
        self.view.backgroundColor = colorSettings.content                
        programTableView.setEmptyMessage(emptyMessage, textColor: colorSettings.contentText)
    }
    
    func reload() {
        programTableView.reloadData()
    }
}

extension AppkeeMyProgramViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.handle(.rowSelected(indexPath.row))
    }
}

// table view

extension AppkeeMyProgramViewController: UICollectionViewDelegateFlowLayout {
     func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.size.width / CGFloat(presenter.numberOfItemsForDays(in: 0))) - 16.0
        return CGSize(width: width, height: 40)
    }
}

extension AppkeeMyProgramViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = presenter.numberOfItems(in: section)
        
        if count == 0 {
            tableView.showEmptyMessage()
        } else {
            tableView.hideEmptyMessage()
        }
        
        return count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AppkeeProgramTableViewCell.reuseId, for: indexPath) as! AppkeeProgramTableViewCell
        presenter.configure(cell, at: indexPath)
        
        return cell
    }
}

// collection view

extension AppkeeMyProgramViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return presenter.numberOfSectionsForDays
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfItemsForDays(in: section)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AppkeeProgramCollectionViewCell.reuseId, for: indexPath) as! AppkeeProgramCollectionViewCell
        
        presenter.configure(day: cell, at: indexPath)
                
        return cell
    }
}

extension AppkeeMyProgramViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.handle(.updateDay(indexPath.row))
        programTableView.reloadData()
    }
}


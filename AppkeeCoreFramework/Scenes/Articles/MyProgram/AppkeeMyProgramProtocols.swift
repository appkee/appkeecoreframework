//
//  AppkeeMyProgramProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeeMyProgramCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeMyProgramCoordinatorInput: class {
    func navigate(to route: AppkeeMyProgram.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeMyProgramInteractorInput {
    // func perform(_ request: AppkeeMyProgram.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeMyProgramInteractorOutput: class {
    // func present(_ response: AppkeeMyProgram.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeMyProgramPresenterInput {
    var numberOfSections: Int { get }
    func numberOfItems(in section: Int) -> Int
    
    var numberOfSectionsForDays: Int { get }
    func numberOfItemsForDays(in section: Int) -> Int
    
    func viewCreated()
    func handle(_ action: AppkeeMyProgram.Action)
    
    func configure(_ item: AppkeeProgramTableViewCell, at indexPath: IndexPath)
    func configure(day item: AppkeeProgramCollectionViewCell, at indexPath: IndexPath)    
}

// PRESENTER -> VIEW
protocol AppkeeMyProgramPresenterOutput: class {
    // func display(_ displayModel: AppkeeMyProgram.DisplayData.Work)
    func setupUI(colorSettings: AppkeeColorSettings, emptyMessage: NSAttributedString)
    
    func reload()
    func updateIndexes(dayIndex: Int)
}

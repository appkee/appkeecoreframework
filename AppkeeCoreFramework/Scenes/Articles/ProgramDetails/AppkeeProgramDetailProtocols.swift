//
//  AppkeeProgramDetailProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 10/19/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeeProgramDetailCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeProgramDetailCoordinatorInput: class {
    func navigate(to route: AppkeeProgramDetail.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeProgramDetailInteractorInput {
    // func perform(_ request: AppkeeProgramDetail.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeProgramDetailInteractorOutput: class {
    // func present(_ response: AppkeeProgramDetail.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeProgramDetailPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeeProgramDetail.Action)
}

// PRESENTER -> VIEW
protocol AppkeeProgramDetailPresenterOutput: class {
    func display(_ linkModel: AppkeeProgramDetail.DisplayData.Content)
    // func display(_ displayModel: AppkeeProgramDetail.DisplayData.Work)
}

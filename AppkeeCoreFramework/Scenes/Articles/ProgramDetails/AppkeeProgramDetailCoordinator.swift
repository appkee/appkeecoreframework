//
//  AppkeeProgramDetailCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 10/19/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeeProgramDetailCoordinator: AppkeeCoordinator {
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
    private let content: String

    var delegate: AppkeeSideMenuCoordinatorDelegate?

    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, content: String) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.content = content
    }

    func start(root: Bool = false) {
        let interactor = AppkeeProgramDetailInteractor()
        let presenter = AppkeeProgramDetailPresenter(interactor: interactor, coordinator: self, graphicManager: dependencies.graphicManager, content: self.content)
        let vc = AppkeeProgramDetailViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        navigationController.present(vc, animated: true)
    }
}


// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeProgramDetailCoordinator: AppkeeProgramDetailCoordinatorInput {
    func navigate(to route: AppkeeProgramDetail.Route) {
        switch route {
        case .close:
            self.navigationController.dismiss(animated: true, completion: nil)
        }
    }
}


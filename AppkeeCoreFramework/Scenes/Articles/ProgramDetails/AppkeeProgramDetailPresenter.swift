//
//  AppkeeProgramDetailPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 10/19/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeProgramDetailPresenter {
    // MARK: - Properties
    let interactor: AppkeeProgramDetailInteractorInput
    weak var coordinator: AppkeeProgramDetailCoordinatorInput?
    weak var output: AppkeeProgramDetailPresenterOutput?

    private let graphicManager: AppkeeGraphicManager

    private let content: String

    // MARK: - Init
    init(interactor: AppkeeProgramDetailInteractorInput,
         coordinator: AppkeeProgramDetailCoordinatorInput,
         graphicManager: AppkeeGraphicManager,
         content: String) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.content = content
        self.graphicManager = graphicManager
    }
}

// MARK: - User Events -

extension AppkeeProgramDetailPresenter: AppkeeProgramDetailPresenterInput {
    func viewCreated() {
        self.output?.display(AppkeeProgramDetail.DisplayData.Content(content: self.content, colorSettings: graphicManager.colorsSettings))
    }

    func handle(_ action: AppkeeProgramDetail.Action) {
        switch action {
        case .tap:
            coordinator?.navigate(to: .close)
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeProgramDetailPresenter: AppkeeProgramDetailInteractorOutput {

}

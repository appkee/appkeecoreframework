//
//  AppkeeProgramDetailViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 10/19/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import UIKit
import WebKit

class AppkeeProgramDetailViewController: UIViewController {
    // MARK: - Outlets
    private var webView: WKWebViewSizeToFit?

    @IBOutlet weak var contentView: UIView!

    // MARK: - Properties
    private var presenter: AppkeeProgramDetailPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeProgramDetailPresenterInput) -> AppkeeProgramDetailViewController {
        let name = "\(AppkeeProgramDetailViewController.self)"
        let bundle = Bundle(for: AppkeeLinkViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeProgramDetailViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()

        let webView = WKWebViewSizeToFit(frame: .zero)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.scrollView.isScrollEnabled = true
        webView.scrollView.bounces = false
        webView.sizeToFit()
        webView.isOpaque = false
        webView.sizeToFitDelegate = self
//        webView.configuration.dataDetectorTypes = [
//            WKDataDetectorTypes.phoneNumber,
////            WKDataDetectorTypes.address
//            WKDataDetectorTypes.calendarEvent,
//            WKDataDetectorTypes.trackingNumber,
//            WKDataDetectorTypes.flightNumber,
//            WKDataDetectorTypes.lookupSuggestion
//        ];
//        webView.navigationDelegate = self

        webView.isUserInteractionEnabled = true

        self.contentView.addSubview(webView)
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[webView]-10-|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["webView" : webView]))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[webView]-10-|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["webView" : webView]))

        self.webView = webView

        let recognizer = UITapGestureRecognizer(target: self,
                                                action: #selector(handleTap(recognizer:)))
        self.view.addGestureRecognizer(recognizer)

        self.presenter.viewCreated()
    }

    @objc func handleTap(recognizer: UITapGestureRecognizer) {
        self.presenter.handle(.tap)
    }

    // MARK: - Callbacks -

}

extension AppkeeProgramDetailViewController: WKWebViewSizeToFitDelegate {

    func sizeUpdated() {

    }

    func openLink(link: String, name: String) {
//        delegate?.openLink(link: link, name: name)
    }

    func openDeeplink(params: String) {
//        delegate?.openDeeplink(params: params)
    }

}


// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeProgramDetailViewController: AppkeeProgramDetailPresenterOutput {
    func display(_ linkModel: AppkeeProgramDetail.DisplayData.Content) {
        self.webView?.loadHTMLString(linkModel.content.toHtml(colorSettings: linkModel.colorSettings), baseURL: nil)
    }
}

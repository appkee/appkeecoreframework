//
//  AppkeeProgramDetailModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 10/19/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeProgramDetail {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case tap
    }

    enum Route {
        case close
    }
}

extension AppkeeProgramDetail.Request {

}

extension AppkeeProgramDetail.Response {

}

extension AppkeeProgramDetail.DisplayData {
    struct Content {
        let content: String
        let colorSettings: AppkeeColorSettings?
    }
}

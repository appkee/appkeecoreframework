//
//  AppkeeProgramDetailInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 10/19/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeProgramDetailInteractor {
    // MARK: - Properties
    weak var output: AppkeeProgramDetailInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeProgramDetailInteractor: AppkeeProgramDetailInteractorInput {
}

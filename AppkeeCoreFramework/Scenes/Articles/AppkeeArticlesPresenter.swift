//
//  AppkeeArticlesPresenter.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 27/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeArticlesPresenter {
    // MARK: - Properties
    private let section: AppkeeSection
    private let favoritesType: AppkeeAppDescription.FavoriteType?
    private let graphicManager: AppkeeGraphicManager
    
    private let interactor: AppkeeArticlesInteractorInput
    weak var coordinator: AppkeeArticlesCoordinatorInput?
    weak var output: AppkeeArticlesPresenterOutput?

    private let articleID: Int?

    private let articles: [AppkeeArticle]
    private let subArticles: [AppkeeSubArticle]


    // MARK: - Init
    init(interactor: AppkeeArticlesInteractorInput, coordinator: AppkeeArticlesCoordinatorInput, graphicManager: AppkeeGraphicManager, section: AppkeeSection, articleID: Int? = nil, favoritesType: AppkeeAppDescription.FavoriteType?) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.section = section
        self.graphicManager = graphicManager
        self.favoritesType = favoritesType
        self.articleID = articleID

        if let articleID = articleID {
            if let article = section.articles?.first(where: { $0.id == articleID }) {
                self.articles = []
                self.subArticles = article.articles.filter { $0.visibility } .sorted { $0.order < $1.order }
            } else {
                self.articles = []
                self.subArticles = []
            }
        } else {
//            self.articles = section.articles?.filter { $0.visibility } .sorted { $0.order < $1.order } ?? []
            self.articles = section.articles?.sorted { $0.order < $1.order } ?? []
            self.subArticles = []
        }
    }
}

// MARK: - User Events -

extension AppkeeArticlesPresenter: AppkeeArticlesPresenterInput {
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfItems(in section: Int) -> Int {
        if let _ = articleID {
            return subArticles.count
        } else {
            return articles.count
        }
    }
        
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }
        
        if let image = section.images?.first(where: { $0.order == 0 }), !(image.content?.isEmpty ?? true) {
            self.output?.display(AppkeeArticles.DisplayData.Header())
        }  else if let text = section.texts?.first(where: { $0.order == 0 }), !(text.content?.isEmpty ?? true) {
            self.output?.display(AppkeeArticles.DisplayData.Header())
        }
    }

    func handle(_ action: AppkeeArticles.Action) {
        switch action {
        case .navigate(let index):
            if let _ = articleID {
                let article = subArticles[index]

                self.openSubArticle(article: article)
            } else {
                let article = articles[index]

                self.openArticle(article: article)
            }
        case .menu:
            coordinator?.navigate(to: .menu)
        case let .openLink(name, link):
            coordinator?.navigate(to: .openLink(title: name, link: link))
        case .openArticle(let articleID):
            if let article = self.articles.first(where: { $0.id == articleID }) {
                self.openArticle(article: article)
            }
        }
    }
    
    func reorderProgram(program: AppkeeProgramSection) {
        for i in 0..<program.program.count {
            var day = program.program[i]
            for j in 0..<day.content.count {
                var room = day.content[j]
                room.content = room.content.sorted(by: {$0.order < $1.order})
            }
            day.content = day.content.sorted(by: {$0.order < $1.order})
        }
    }
    
    func configure(_ item: AppkeeArticleTableViewCellDelegate, at indexPath: IndexPath) {
        if let _ = articleID {
            let article = subArticles[indexPath.row]

            let image = section.images?.first(where: { $0.articleId == article.id })
            let text = section.texts?.first(where: { $0.articleId == article.id })

            DispatchQueue.main.async {
                item.configure(icon: self.graphicManager.getScaledImage(name: image?.content))
            }

            item.configure(with: article.type, hideImages: section.hideImages, icon: nil, title: article.name, description: text?.content, colorSettings: graphicManager.colorsSettings)
        } else {
            let article = articles[indexPath.row]

            let image = section.images?.first(where: { $0.articleId == article.id })
            let text = section.texts?.first(where: { $0.articleId == article.id })

            let icon = self.graphicManager.getScaledImage(name: image?.content)

//            DispatchQueue.main.async {
//                item.configure(icon: self.graphicManager.getScaledImage(name: image?.content))
//            }

            item.configure(with: article.type, hideImages: section.hideImages, icon: icon, title: article.name, description: text?.content, colorSettings: graphicManager.colorsSettings)
        }
    }
    
    func configure(_ item: AppkeeArticleHeaderTableViewDelegate) {
        if let articleID = articleID {
            let image = section.images?.first(where: { $0.articleId == articleID })
            let text = section.texts?.first(where: { $0.articleId == articleID })

            let icon = graphicManager.getImage(name: image?.content, placeholder: false)

            item.configure(with: icon, content: text?.content, colorSettings: graphicManager.colorsSettings) {
                if let image = image, image.clickable {
                    self.showGallery(images: [image])
                }
            }
        } else {
            let image = section.images?.first(where: { $0.articleId == 0 })
            let text = section.texts?.first(where: { $0.articleId == 0 })

            let icon = graphicManager.getImage(name: image?.content, placeholder: false)

            item.configure(with: icon, content: text?.content, colorSettings: graphicManager.colorsSettings) {
                if let image = image, image.clickable {
                    self.showGallery(images: [image])
                }
            }
        }
    }
    
    private func showGallery(images: [AppkeeContent]) {
        let clickableImages = images.filter { $0.clickable == true } .sorted (by: { $0.id < $1.id })
        coordinator?.navigate(to: .gallery(images: clickableImages))
    }

    private func openArticle(article: AppkeeArticle) {
        switch article.type {
        case .page:
            let texts = section.texts?.filter({ $0.articleId == article.id }) ?? []
            let images = section.images?.filter({ $0.articleId == article.id }) ?? []
            let videos = section.videos?.filter({ $0.articleId == article.id }) ?? []

//            let props = AppkeePageProps(articleId: article.id, title: article.name ?? "", texts: texts, images: images, videos: videos, favorites: self.section.favorites, favoritesType: self.favoritesType, advertisement: section.advertisement)

            let props = AppkeePageProps(articleId: article.id, title: article.name ?? "", texts: texts, images: images, videos: videos, favorites: self.section.favorites, favoritesType: self.favoritesType, banners: nil)

            coordinator?.navigate(to: .open(props))
        case .link:
            guard let link = article.link else {
                return
            }

            coordinator?.navigate(to: .openLink(title: article.name ?? "", link: link))
        case .program:
    //                if let program = section.programs.first(where: { $0.articleId == article.id }) {
            if let program = section.programs.first {
                reorderProgram(program: program)
                coordinator?.navigate(to: .openProgram(program: program, title: article.name))
            }
        case .myProgram:
            if let program = section.programs.first {
                reorderProgram(program: program)

                coordinator?.navigate(to: .openMyProgram(program: program, title: article.name, emptyMessage: section.texts?.first(where: { $0.articleId == article.id })?.content))
            }
        case .articles:
            coordinator?.navigate(to: .openArticles(articleID: article.id))
        }
    }

    private func openSubArticle(article: AppkeeSubArticle) {
        switch article.type {
        case .page:
            let texts = section.texts?.filter({ $0.articleId == article.id }) ?? []
            let images = section.images?.filter({ $0.articleId == article.id }) ?? []
            let videos = section.videos?.filter({ $0.articleId == article.id }) ?? []

//            let props = AppkeePageProps(articleId: article.id, title: article.name ?? "", texts: texts, images: images, videos: videos, favorites: self.section.favorites, favoritesType: self.favoritesType, advertisement: section.advertisement)

            let props = AppkeePageProps(articleId: article.id, title: article.name ?? "", texts: texts, images: images, videos: videos, favorites: self.section.favorites, favoritesType: self.favoritesType, banners: nil)

            coordinator?.navigate(to: .open(props))
        case .link:
            guard let link = article.link else {
                return
            }

            coordinator?.navigate(to: .openLink(title: article.name ?? "", link: link))
        case .program, .myProgram, .articles:
            break
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeArticlesPresenter: AppkeeArticlesInteractorOutput {

}

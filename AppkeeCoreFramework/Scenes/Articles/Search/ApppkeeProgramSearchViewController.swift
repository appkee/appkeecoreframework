//
//  ApppkeeProgramSearchViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 3/15/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import UIKit

class ApppkeeProgramSearchViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var searchBar: UISearchBar! {
        didSet {
            searchBar.showsCancelButton = false
            searchBar.delegate = self
        }
    }

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(AppkeeSearchTableViewCell.nib, forCellReuseIdentifier: AppkeeSearchTableViewCell.reuseId)
            tableView.estimatedRowHeight = 30
            tableView.estimatedSectionHeaderHeight = 30
            tableView.separatorStyle = .singleLine
            tableView.tableFooterView = UIView()
            tableView.rowHeight = UITableView.automaticDimension
        }
    }

    // MARK: - Properties
    private var presenter: ApppkeeProgramSearchPresenterInput!

    private let searchController = UISearchController(searchResultsController: nil)


    // MARK: - Init
    class func instantiate(with presenter: ApppkeeProgramSearchPresenterInput) -> ApppkeeProgramSearchViewController {
        let name = "\(ApppkeeProgramSearchViewController.self)"
        let bundle = Bundle(for: ApppkeeProgramSearchViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! ApppkeeProgramSearchViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
    }

    // MARK: - Callbacks -

}

extension ApppkeeProgramSearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter.updateSearch(string: searchText)
    }
}

extension ApppkeeProgramSearchViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.numberOfItems(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AppkeeSearchTableViewCell.reuseId, for: indexPath) as! AppkeeSearchTableViewCell
        presenter.configure(cell, at: indexPath)

        return cell
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if let _ = tableView.headerView(forSection: section) {
            return UITableView.automaticDimension
        } else {
            return 1
        }
    }
}

extension ApppkeeProgramSearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.handle(.navigate(index: indexPath.row))
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension ApppkeeProgramSearchViewController: ApppkeeProgramSearchPresenterOutput {
    func setupUI(colorSettings: AppkeeColorSettings) {
        self.view.backgroundColor = colorSettings.content
//        self.searchBar.tintColor = colorSettings.header
        self.searchBar.barTintColor = colorSettings.header
        self.searchBar.textColor = colorSettings.headerText
//        self.searchBar.textBackgroundColor = colorSettings.headerText
    }

    func reloadTable() {
        self.tableView.reloadData()
    }
}

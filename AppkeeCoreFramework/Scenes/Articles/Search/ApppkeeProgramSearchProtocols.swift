//
//  ApppkeeProgramSearchProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 3/15/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol ApppkeeProgramSearchCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol ApppkeeProgramSearchCoordinatorInput: class {
    func navigate(to route: ApppkeeProgramSearch.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol ApppkeeProgramSearchInteractorInput {
    // func perform(_ request: ApppkeeProgramSearch.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol ApppkeeProgramSearchInteractorOutput: class {
    // func present(_ response: ApppkeeProgramSearch.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol ApppkeeProgramSearchPresenterInput {
    var numberOfSections: Int { get }
    func numberOfItems(in section: Int) -> Int
    
    func viewCreated()
    func handle(_ action: ApppkeeProgramSearch.Action)
    func updateSearch(string: String)

    func configure(_ item: AppkeeSearchTableViewCellDelegate, at indexPath: IndexPath)
}

// PRESENTER -> VIEW
protocol ApppkeeProgramSearchPresenterOutput: class {
    // func display(_ displayModel: ApppkeeProgramSearch.DisplayData.Work)

    func setupUI(colorSettings: AppkeeColorSettings)

    func reloadTable()
}

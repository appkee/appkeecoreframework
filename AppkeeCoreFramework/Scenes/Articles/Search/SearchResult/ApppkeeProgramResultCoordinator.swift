//
//  ApppkeeProgramResultCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 3/24/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class ApppkeeProgramResultCoordinator: AppkeeCoordinator {
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
    //    weak var delegate: ApppkeeProgramResultCoordinatorDelegate?
    private let title: String
    private let result: [ApppkeeProgramSearchPresenter.ResultField]

    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, title: String, result: [ApppkeeProgramSearchPresenter.ResultField]) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.title = title
        self.result = result
    }

    func start(root: Bool = false) {
        let interactor = ApppkeeProgramResultInteractor()
        let presenter = ApppkeeProgramResultPresenter(interactor: interactor, coordinator: self, graphicManager: dependencies.graphicManager, result: result)
        let vc = ApppkeeProgramResultViewController.instantiate(with: presenter)

        vc.title = title

        interactor.output = presenter
        presenter.output = vc

        navigationController.navigationBar.isHidden = false
        navigationController.pushViewController(vc, animated: true)
    }
}
// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension ApppkeeProgramResultCoordinator: ApppkeeProgramResultCoordinatorInput {
    func navigate(to route: ApppkeeProgramResult.Route) {
        switch route {
        case .detail(let content):
            let coordinator = AppkeeProgramDetailCoordinator(navigationController: self.navigationController, dependencies: dependencies, content: content)
            childrens.append(coordinator)
            coordinator.start()
        }
    }
}



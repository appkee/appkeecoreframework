//
//  ApppkeeProgramResultViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 3/24/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import UIKit

class ApppkeeProgramResultViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(AppkeeProgramTableViewCell.nib, forCellReuseIdentifier: AppkeeProgramTableViewCell.reuseId)

            tableView.tableFooterView = UIView()
        }
    }

    // MARK: - Properties
    private var presenter: ApppkeeProgramResultPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: ApppkeeProgramResultPresenterInput) -> ApppkeeProgramResultViewController {
        let name = "\(ApppkeeProgramResultViewController.self)"
        let bundle = Bundle(for: ApppkeeProgramResultViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! ApppkeeProgramResultViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
    }

    // MARK: - Callbacks -

}

extension ApppkeeProgramResultViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.numberOfSections
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  presenter.numberOfItems(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AppkeeProgramTableViewCell.reuseId, for: indexPath) as! AppkeeProgramTableViewCell
        presenter.configure(cell, at: indexPath)

        return cell
    }
}

extension ApppkeeProgramResultViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.handle(.open(indexPath.row))
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension ApppkeeProgramResultViewController: ApppkeeProgramResultPresenterOutput {
    func setupUI(colorSettings: AppkeeColorSettings) {
        self.view.backgroundColor = colorSettings.content
    }
}

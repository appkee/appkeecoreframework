//
//  ApppkeeProgramResultInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 3/24/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

class ApppkeeProgramResultInteractor {
    // MARK: - Properties
    weak var output: ApppkeeProgramResultInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension ApppkeeProgramResultInteractor: ApppkeeProgramResultInteractorInput {
}

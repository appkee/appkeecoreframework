//
//  ApppkeeProgramResultProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 3/24/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol ApppkeeProgramResultCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol ApppkeeProgramResultCoordinatorInput: class {
    func navigate(to route: ApppkeeProgramResult.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol ApppkeeProgramResultInteractorInput {
    // func perform(_ request: ApppkeeProgramResult.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol ApppkeeProgramResultInteractorOutput: class {
    // func present(_ response: ApppkeeProgramResult.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol ApppkeeProgramResultPresenterInput {
    var numberOfSections: Int { get }
    func numberOfItems(in section: Int) -> Int

    func viewCreated()
    func handle(_ action: ApppkeeProgramResult.Action)

    func configure(_ item: AppkeeProgramTableViewCell, at indexPath: IndexPath)
}

// PRESENTER -> VIEW
protocol ApppkeeProgramResultPresenterOutput: class {
    // func display(_ displayModel: ApppkeeProgramResult.DisplayData.Work)

    func setupUI(colorSettings: AppkeeColorSettings)
}

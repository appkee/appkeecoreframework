//
//  ApppkeeProgramResultModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 3/24/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

enum ApppkeeProgramResult {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case open(Int)
    }

    enum Route {
        case detail(content: String)
    }
}

extension ApppkeeProgramResult.Request {

}

extension ApppkeeProgramResult.Response {

}

extension ApppkeeProgramResult.DisplayData {
    
}

//
//  ApppkeeProgramResultPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 3/24/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

class ApppkeeProgramResultPresenter {
    // MARK: - Properties
    let interactor: ApppkeeProgramResultInteractorInput
    weak var coordinator: ApppkeeProgramResultCoordinatorInput?
    weak var output: ApppkeeProgramResultPresenterOutput?

    private let graphicManager: AppkeeGraphicManager
    private var results: [ApppkeeProgramSearchPresenter.ResultField] = []

    // MARK: - Init
    init(interactor: ApppkeeProgramResultInteractorInput, coordinator: ApppkeeProgramResultCoordinatorInput, graphicManager: AppkeeGraphicManager, result: [ApppkeeProgramSearchPresenter.ResultField]) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
        self.results = result
    }
}

// MARK: - User Events -

extension ApppkeeProgramResultPresenter: ApppkeeProgramResultPresenterInput {

    var numberOfSections: Int {
        return 1
    }

    func numberOfItems(in section: Int) -> Int {
        return results.count
    }

    func configure(_ item: AppkeeProgramTableViewCell, at indexPath: IndexPath) {
        let result = results[indexPath.row]

        let favourite = UserDefaults.containsProgramFavourite(programId: result.program.id)

        item.configure(
            with: result.program.title,
            day: result.day,
            time: result.program.time,
            room: result.room,
            favorite: favourite,
            backgroundColor: result.program.color,
            textColor: result.program.textColor,
            colorSettings: graphicManager.colorsSettings,
            favouriteHandler: { (favourite) in
                if (favourite) {
                    UserDefaults.addProgramFavourite(programId: result.program.id)
                } else {
                    UserDefaults.removeProgramFavourite(programId: result.program.id)
                }
        })
    }


    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }
    }

    func handle(_ action: ApppkeeProgramResult.Action) {
        switch action {
        case .open(let row):
            let program = results[row].program

            if let content = program.content {
                self.coordinator?.navigate(to: .detail(content: content))
            }
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension ApppkeeProgramResultPresenter: ApppkeeProgramResultInteractorOutput {

}

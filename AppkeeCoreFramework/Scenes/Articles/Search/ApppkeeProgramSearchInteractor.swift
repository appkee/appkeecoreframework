//
//  ApppkeeProgramSearchInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 3/15/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

class ApppkeeProgramSearchInteractor {
    // MARK: - Properties
    weak var output: ApppkeeProgramSearchInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension ApppkeeProgramSearchInteractor: ApppkeeProgramSearchInteractorInput {
}

//
//  ApppkeeProgramSearchModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 3/15/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

enum ApppkeeProgramSearch {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case navigate(index: Int)
    }

    enum Route {
        case showResult(String, [ApppkeeProgramSearchPresenter.ResultField])
    }
}

extension ApppkeeProgramSearch.Request {

}

extension ApppkeeProgramSearch.Response {

}

extension ApppkeeProgramSearch.DisplayData {
    
}

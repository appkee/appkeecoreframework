//
//  ApppkeeProgramSearchCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 3/15/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class ApppkeeProgramSearchCoordinator: AppkeeCoordinator {
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
//    weak var delegate: SearchCoordinatorDelegate?
    private var section: AppkeeProgramSection

    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, section: AppkeeProgramSection) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.section = section
    }


    func start(root: Bool = false) {
        let interactor = ApppkeeProgramSearchInteractor()
        let presenter = ApppkeeProgramSearchPresenter(interactor: interactor, coordinator: self, graphicManager: dependencies.graphicManager, section: section)
        let vc = ApppkeeProgramSearchViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        navigationController.navigationBar.isHidden = false
        navigationController.pushViewController(vc, animated: true)
    }

    private func showResult(name: String, results: [ApppkeeProgramSearchPresenter.ResultField]) {
        let coordinator = ApppkeeProgramResultCoordinator(
            navigationController: navigationController,
            dependencies: dependencies,
            title: name,
            result: results
        )


        childrens.append(coordinator)
//        coordinator.delegate = self
        coordinator.start(root: false)
    }
}
// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension ApppkeeProgramSearchCoordinator: ApppkeeProgramSearchCoordinatorInput {
    func navigate(to route: ApppkeeProgramSearch.Route) {
        switch route {
        case let .showResult(name, results):
            showResult(name: name, results: results)
        }
    }
}

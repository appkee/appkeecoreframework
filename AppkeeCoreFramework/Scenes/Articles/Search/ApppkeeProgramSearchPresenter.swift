//
//  ApppkeeProgramSearchPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 3/15/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

class ApppkeeProgramSearchPresenter {
    struct ResultField {
        let program: AppkeeContentProgram
        let day: String
        let room: String
    }

    // MARK: - Properties
    let interactor: ApppkeeProgramSearchInteractorInput
    weak var coordinator: ApppkeeProgramSearchCoordinatorInput?
    weak var output: ApppkeeProgramSearchPresenterOutput?

    private let graphicManager: AppkeeGraphicManager
    private var results: [String] = []
    private var dataSource: [String] = []
    private var section: AppkeeProgramSection
    private var search: Bool = false

    // MARK: - Init
    init(interactor: ApppkeeProgramSearchInteractorInput, coordinator: ApppkeeProgramSearchCoordinatorInput, graphicManager: AppkeeGraphicManager, section: AppkeeProgramSection) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
        self.section = section

        dataSouce()
    }

    private func dataSouce() {
        var dataSource: [String] = []

        for programDay in section.program {
            for room in programDay.content {
                for program in room.content {
                    for name in program.authors {
                        if !dataSource.contains(name) {
                            dataSource.append(name)
                        }
                    }
                }
            }
        }

        self.dataSource = dataSource
    }
}

// MARK: - User Events -

extension ApppkeeProgramSearchPresenter: ApppkeeProgramSearchPresenterInput {
    var numberOfSections: Int {
        return 1
    }

    func numberOfItems(in section: Int) -> Int {
        if search {
            return results.count
        } else {
            return dataSource.count
        }
    }

    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }
    }

    func handle(_ action: ApppkeeProgramSearch.Action) {
        switch action {
        case let  .navigate(index):
            let name = search ? results[index] : dataSource[index]

            var results: [ResultField] = []
            for programDay in section.program {
                for room in programDay.content {
                    for program in room.content {
                        if program.authors.contains(name) {
                            results.append(ResultField(
                                program: program,
                                day: programDay.name,
                                room: room.name)
                            )
                        }

                    }
                }
            }

            coordinator?.navigate(to: .showResult(name, results))
        }
    }

    func updateSearch(string: String) {
        if string.isEmpty {
            self.results = []
            self.search = false
        } else {
            let string = string.unaccent()
            self.results = self.dataSource.filter({ $0.unaccent().lowercased().contains(string.lowercased())})
            self.search = true
        }

        output?.reloadTable()
    }

    func configure(_ item: AppkeeSearchTableViewCellDelegate, at indexPath: IndexPath) {
        if search {
            let field = results[indexPath.row]

            item.configure(with: field, subTitle: nil, colorSettings: graphicManager.colorsSettings)
        } else {
            let field = dataSource[indexPath.row]

            item.configure(with: field, subTitle: nil, colorSettings: graphicManager.colorsSettings)
        }

    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension ApppkeeProgramSearchPresenter: ApppkeeProgramSearchInteractorOutput {

}

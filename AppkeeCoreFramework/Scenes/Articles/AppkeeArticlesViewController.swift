//
//  AppkeeArticlesViewController.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 27/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import UIKit
import WebKit

class AppkeeArticlesViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.register(AppkeeArticleTableViewCell.nib, forCellReuseIdentifier: AppkeeArticleTableViewCell.reuseId)
            tableView.estimatedRowHeight = 30
            tableView.estimatedSectionHeaderHeight = 30
            tableView.separatorStyle = .singleLine
            tableView.tableFooterView = UIView()
            tableView.rowHeight = UITableView.automaticDimension
        }
    }

    // MARK: - Properties
    private var presenter: AppkeeArticlesPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeArticlesPresenterInput) -> AppkeeArticlesViewController {
        let name = "\(AppkeeArticlesViewController.self)"
        let bundle = Bundle(for: AppkeeArticlesViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeArticlesViewController
        vc.presenter = presenter        
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
//    override func viewDidLayoutSubviews() {
//        tableView.reloadData()
//
//        guard let headerView = tableView.tableHeaderView else { return }
//
//        let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
//        var headerFrame = headerView.frame
//
//        if height != headerFrame.size.height {
//            headerFrame.size.height = height
//            headerView.frame = headerFrame
//            tableView.tableHeaderView = headerView
//        }
//
////        headerView.translatesAutoresizingMaskIntoConstraints = true
//    }
    
    // MARK: - Callbacks -

}

extension AppkeeArticlesViewController: UITableViewDataSource {
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return UITableView.automaticDimension
//    }
//
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerView = AppkeeArticleHeaderTableView(frame: self.view.frame)
//
//        presenter.configure(headerView)
//
//        if let webview = headerView.webView {
//            webview.sizeToFitDelegate = self
//        }
//
//
//        return headerView
//    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.numberOfItems(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AppkeeArticleTableViewCell.reuseId, for: indexPath) as! AppkeeArticleTableViewCell
        presenter.configure(cell, at: indexPath)
        
        return cell
    }

//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if let cell = cell as? AppkeeArticleTableViewCell {
//            presenter.configure(cell, at: indexPath)
//        }
//    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if let _ = tableView.headerView(forSection: section) {
            return UITableView.automaticDimension
        } else {
            return 1
        }
    }
}

extension AppkeeArticlesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.handle(.navigate(indexPath.row))
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeArticlesViewController: AppkeeArticlesPresenterOutput {
    
    func display(_ displayModel: AppkeeArticles.DisplayData.Header) {        
        let headerView = AppkeeArticleHeaderTableView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 40))
        
//        headerView.autoresizingMask = .flexibleHeight
//        headerView.translatesAutoresizingMaskIntoConstraints = true
        
        tableView.tableHeaderView = headerView
        
        presenter.configure(headerView)
        
        if let webview = headerView.webView {
            webview.sizeToFitDelegate = self
        } else {
            DispatchQueue.main.async {
                self.sizeUpdated()
            }
        }
//        headerView.webView?.navigationDelegate = self
    }
    
    func setupUI(colorSettings: AppkeeColorSettings) {
        self.tableView.separatorColor = colorSettings.header
        self.view.backgroundColor = colorSettings.content
    }
}

extension AppkeeArticlesViewController: WKWebViewSizeToFitDelegate {
    func openLink(link: String, name: String) {
        presenter.handle(.openLink(name: name, link: link))
    }

    func openDeeplink(params: String) {
        //TODO: - deeplink
    }
    
    func sizeUpdated() {
        
        guard let headerView = self.tableView.tableHeaderView else { return }
    
            var fittingSize: CGSize = UIView.layoutFittingCompressedSize;
            fittingSize.width = headerView.frame.width;

//            let height = headerView.systemLayoutSizeFitting(fittingSize, withHorizontalFittingPriority: .defaultHigh, verticalFittingPriority: .defaultLow).height
            let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height

            var headerFrame = headerView.frame

            if height != headerFrame.size.height {
                headerFrame.size.height = height
                headerView.frame = headerFrame
                self.tableView.tableHeaderView = headerView
            }
//        self.tableView.reloadData()
////        }
    }
}

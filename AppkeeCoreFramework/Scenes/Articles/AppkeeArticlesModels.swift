//
//  AppkeeArticlesModels.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 27/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeArticles {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case navigate(Int)
        case menu
        case openLink(name: String, link: String)
        case openArticle(Int)
    }

    enum Route {
        case open(AppkeePageProps)
        case openArticles(articleID: Int)
        case openLink(title: String, link: String)
        case openProgram(program: AppkeeProgramSection, title: String?)
        case openMyProgram(program: AppkeeProgramSection, title: String?, emptyMessage: String?)
        case menu
        case gallery(images: [AppkeeContent])
        case openAdvertisementLink(link: String)
        case openDeeplink(params: String)
        case openArticle(Int)
    }
}

extension AppkeeArticles.Request {

}

extension AppkeeArticles.Response {

}

extension AppkeeArticles.DisplayData {
    struct Header {
        
    }
}

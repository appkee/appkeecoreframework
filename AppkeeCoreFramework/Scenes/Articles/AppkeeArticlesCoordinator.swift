//
//  AppkeeArticlesCoordinator.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 27/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit
import SafariServices

class AppkeeArticlesCoordinator: AppkeeCoordinator {    
    
    // MARK: - Properties
    private let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
    private let section: AppkeeSection
    private let email: String?
    private let search: Bool
    private let favoritesType: AppkeeAppDescription.FavoriteType?
    private let recipients: [String]?

    private let articleID: Int?
    //    weak var delegate: PageCoordinatorDelegate?
    
    var delegate: AppkeeSideMenuCoordinatorDelegate?

    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, section: AppkeeSection, articleID: Int? = nil, favoritesType: AppkeeAppDescription.FavoriteType?, recipients: [String]?, email: String?, search: Bool) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.section = section
        self.email = email
        self.search = search
        self.recipients = recipients
        self.favoritesType = favoritesType
        self.articleID = articleID
    }
    
    func start(root: Bool = false) {
        let interactor = AppkeeArticlesInteractor()
        let presenter = AppkeeArticlesPresenter(interactor: interactor, coordinator: self, graphicManager: dependencies.graphicManager, section: section, articleID: articleID, favoritesType: self.favoritesType)
        let vc = AppkeeArticlesViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc
        
        var menuItems: [UIBarButtonItem] = []
        if dependencies.configManager.appMenu {
            let image = UIImage(named: "dots", in: Bundle(for: AppkeeArticlesCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(debugMenu(_:))))
        }

        if let _ = UserDefaults.pageUser {
            let image = UIImage(named: "logoutIcon", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(logoutTap(_:))))
        }
        
        if search {
            let image = UIImage(named: "search", in: Bundle(for: AppkeeArticlesCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(search(_:))))
        }

        if let _ = email {
            let image = UIImage(named: "cameraMenu", in: Bundle(for: AppkeeArticlesCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(photoReporter(_:))))
        }

        if UserDefaults.seniorPassLogin != nil || UserDefaults.familyPassLogin != nil {
            let image = UIImage(named: "logoutIcon", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(passLogoutTap(_:))))
        }

        vc.navigationItem.rightBarButtonItems = menuItems

        if let articleID = articleID {
            vc.title = section.articles?.first(where: { $0.id == articleID })?.name
        } else {
            vc.title = section.name
        }
        
        let rootController = AppkeeBannerViewController(graphicManager: dependencies.graphicManager)
        rootController.setViewController(vc)
        rootController.delegate = self

        if let banners = section.banners {
            rootController.setBanners(banners: banners)
        }

        if root {
            let image = UIImage(named: "hamburger", in: Bundle(for: AppkeeArticlesCoordinator.self), compatibleWith: nil)
            rootController.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  image, style: .done, target: self, action: #selector(menu(_:)))

            navigationController.setViewControllers([rootController], animated: false)

            return
        }
        
        navigationController.navigationBar.isHidden = false
        navigationController.pushViewController(rootController, animated: true)
    }
    
    @objc func menu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openMenu()
    }
    
    @objc func debugMenu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openDebugMenu()
    }
    
    @objc func photoReporter(_ barButtonItem: UIBarButtonItem) {
        if let email = self.email {
            delegate?.openPhotoReporter(email: email)
        }
    }
    
    @objc func search(_ barButtonItem: UIBarButtonItem) {
        delegate?.openSearch()
    }

    @objc func logoutTap(_ barButtonItem: UIBarButtonItem) {
        delegate?.logout()
    }

    @objc func passLogoutTap(_ barButtonItem: UIBarButtonItem) {
        delegate?.passLogout()
    }
    
    private func showLink(name: String, link: String) {
//        let props = AppkeeLinkProps(title: name, link: link, offlineLink: nil, banners: section.banners, css: section.css)
//        let coordinator = AppkeeLinkCoordinator(navigationController: navigationController,
//                                          dependencies: dependencies,
//                                          props: props,
//                                          email: email,
//                                          search: false)
//        childrens.append(coordinator)
////        coordinator.delegate = self
//        coordinator.start(root: false)

        if let url = URL(string: link) {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true

            let vc = SFSafariViewController(url: url, configuration: config)
            navigationController.present(vc, animated: true)
        }
    }
}

// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeArticlesCoordinator: AppkeeArticlesCoordinatorInput {
    func navigate(to route: AppkeeArticles.Route) {
        switch route {
        case .open(let props):
            if var recipients = self.recipients {
                if self.section.id == 394 {
                    recipients = recipients.filter({ $0 == "l.chlebovsky@lch.cz"})
                    recipients.append("v.pohlova@lch.cz")
                }

                if self.section.id == 397 {
                    recipients.append("m.rauvolf@lch.cz")
                    recipients.append("p.lescak@lch.cz")
                }

                let coordinator = AppkeePhotoPageCoordinator(navigationController: navigationController,
                                                  dependencies: dependencies,
                                                  props: props,
                                                  recipients: recipients,
                                                  email: email,
                                                  search: search)
                childrens.append(coordinator)
                coordinator.delegate = delegate
                coordinator.start()

                return
            }

            let coordinator = AppkeePageCoordinator(navigationController: navigationController,
                                              dependencies: dependencies,
                                              props: props,
                                              email: email,
                                              search: false)
            childrens.append(coordinator)
            coordinator.delegate = delegate
            coordinator.start()
        case .menu:
            delegate?.openMenu()
        case let .gallery(images):
            let coordinator = AppkeeGalleryDetaildCoordinator(navigationController: self.navigationController, dependencies: dependencies, images: images)
            
            childrens.append(coordinator)
            coordinator.start(index: 0)
        case let .openAdvertisementLink(link):
            if let url = URL(string: link) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        case let .openDeeplink(params):
            delegate?.openDeeplink(params: params)
        case let .openProgram(program, title):
            let coordinator = AppkeeProgramCoordinator(navigationController: self.navigationController, dependencies: dependencies, section: program, banners: self.section.banners, title: title)
            childrens.append(coordinator)
            coordinator.start()
        case let .openMyProgram(program, title, emptyMessage):
            let coordinator = AppkeeMyProgramCoordinator(navigationController: self.navigationController, dependencies: dependencies, section: program, banners: self.section.banners, title: title, emptyMessage: emptyMessage)
            childrens.append(coordinator)
            coordinator.start()
        case let .openLink(name, link):
            showLink(name: name, link: link)
        case .openArticle(let articleID):
//            if let vc = self.navigationController.topViewController as? AppkeeArticlesViewController {
//                vc.
//            }

            guard let article = self.section.articles?.first(where: { $0.id == articleID }) else {
                return
            }

            switch article.type {
            case .page:
                let texts = section.texts?.filter({ $0.articleId == article.id }) ?? []
                let images = section.images?.filter({ $0.articleId == article.id }) ?? []
                let videos = section.videos?.filter({ $0.articleId == article.id }) ?? []

//                let props = AppkeePageProps(articleId: -1, title: article.name ?? "", texts: texts, images: images, videos: videos, favorites: false, favoritesType: nil, advertisement: section.advertisement)
                let props = AppkeePageProps(articleId: -1, title: article.name ?? "", texts: texts, images: images, videos: videos, favorites: false, favoritesType: nil, banners: nil)

                self.navigate(to: .open(props))
            case .link:
                guard let link = article.link else {
                    return
                }

                self.navigate(to: .openLink(title: article.name ?? "", link: link))
            case .program:
                break
            case .myProgram:
                break
            case .articles:
                self.navigate(to: .openArticles(articleID: article.id))
            }
        case .openArticles(let articleID):
            let coordinator = AppkeeArticlesCoordinator(navigationController: navigationController,
                                              dependencies: dependencies,
                                              section: section,
                                              articleID: articleID,
                                              favoritesType: favoritesType,
                                              recipients: recipients,
                                              email: email,
                                              search: search)
            childrens.append(coordinator)
            coordinator.delegate = delegate
            coordinator.start(root: false)
        }
    }
}

extension AppkeeArticlesCoordinator: AppkeeBannerViewControllerDelegate {
    func openLink(link: String) {
        self.navigate(to: .openAdvertisementLink(link: link))
    }

    func openDeeplink(params: String) {
        self.navigate(to: .openDeeplink(params: params))
    }
}

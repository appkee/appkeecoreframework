//
//  AppkeeProgramModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 09/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeProgram {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case updateDay(Int)
        case updateRoom(Int)
        case rowSelected(Int)
    }

    enum Route {
        case openAdvertisementLink(link: String)
        case openDeeplink(params: String)
        case detail(content: String)
    }
}

extension AppkeeProgram.Request {

}

extension AppkeeProgram.Response {

}

extension AppkeeProgram.DisplayData {
    
}

//
//  AppkeeProgramPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 09/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeeProgramPresenter {
    // MARK: - Properties
    let interactor: AppkeeProgramInteractorInput
    weak var output: AppkeeProgramPresenterOutput?
    
    // Your custom coordinator
     weak var coordinator: AppkeeProgramCoordinatorInput?
    
    private let program: AppkeeProgramSection
    private let graphicManager: AppkeeGraphicManager
    
    private var selectedDay: Int = 0;
    private var selectedRoom: Int = 0;

    // MARK: - Init
    init(interactor: AppkeeProgramInteractorInput,
         coordinator: AppkeeProgramCoordinatorInput,
         graphicManager: AppkeeGraphicManager,
         program: AppkeeProgramSection) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.program = program
        self.graphicManager = graphicManager
    }
}

// MARK: - User Events -

extension AppkeeProgramPresenter: AppkeeProgramPresenterInput {
    func textSizeForDay(at indexPath: IndexPath) -> CGFloat {
        let day = self.program.program[indexPath.row]

        let size = day.name.size(withAttributes: [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17.0, weight: .semibold)
       ])
        return size.width
    }

    func textSizeForRoom(at indexPath: IndexPath) -> CGFloat {
        let day = self.program.program[selectedDay]
        let room = day.content[indexPath.row]

        let size = room.name.size(withAttributes: [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17.0, weight: .semibold)
       ])
        return size.width
    }

    
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfItems(in section: Int) -> Int {
        let day = self.program.program[selectedDay]
        let room = day.content[selectedRoom]
        return room.content.count
    }
    
    var numberOfSectionsForDays: Int {
        return 1
    }
    
    func numberOfItemsForDays(in section: Int) -> Int {
        self.program.program.count
    }
    
    var numberOfSectionsForRooms: Int {
        return 1
    }
    
    func numberOfItemsForRooms(in section: Int) -> Int {
        let day = self.program.program[selectedDay]
        return day.content.count
    }
            
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }
        
        output?.updateIndexes(dayIndex: selectedDay, roomIndex: selectedRoom)
    }

    func handle(_ action: AppkeeProgram.Action) {
        switch action {
        case .updateDay(let index):
            if (selectedDay != index) {
                selectedDay = index
                selectedRoom = 0
            }
        case .updateRoom(let index):
            selectedRoom = index;
        case .rowSelected(let index):
            let day = self.program.program[selectedDay]
            let room = day.content[selectedRoom]
            let program = room.content[index]

            if let content = program.content {
                self.coordinator?.navigate(to: .detail(content: content))
            }

            return
        }
        
        output?.updateIndexes(dayIndex: selectedDay, roomIndex: selectedRoom)
    }
    
    func typyOfCell(indexPath: IndexPath) -> AppkeeContentProgram.ContentType {
        let day = self.program.program[selectedDay]
        let room = day.content[selectedRoom]
        let program = room.content[indexPath.row]
        
        return program.type
    }
    
    func configure(_ item: AppkeeProgramBreakTableViewCell, at indexPath: IndexPath) {
        let day = self.program.program[selectedDay]
        let room = day.content[selectedRoom]
        let program = room.content[indexPath.row]
        
        item.configure(with: program.title, time: program.time, backgroundColor: program.color, textColor: program.textColor, colorSettings: graphicManager.colorsSettings)
    }
    
    func configure(_ item: AppkeeProgramTableViewCell, at indexPath: IndexPath) {
        let day = self.program.program[selectedDay]
        let room = day.content[selectedRoom]
        let program = room.content[indexPath.row]
        
        let favourite = UserDefaults.containsProgramFavourite(programId: program.id)
        
        item.configure(with:
            program.title,
            day: day.name,
            time: program.time,
            room: room.name,
            favorite: favourite,
            backgroundColor: program.color,
            textColor: program.textColor,
            colorSettings: graphicManager.colorsSettings,
            favouriteHandler: { (favourite) in
                if (favourite) {
                    UserDefaults.addProgramFavourite(programId: program.id)
                } else {
                    UserDefaults.removeProgramFavourite(programId: program.id)
                }
        })
    }
    
    func configure(day item: AppkeeProgramCollectionViewCell, at indexPath: IndexPath) {
        let day = self.program.program[indexPath.row]
        
        item.configure(with: day.name, colorSettings: graphicManager.colorsSettings)
    }
    
    func configure(room item: AppkeeProgramCollectionViewCell, at indexPath: IndexPath) {
        let day = self.program.program[selectedDay]
        let room = day.content[indexPath.row]
        
        item.configure(with: room.name, colorSettings: graphicManager.colorsSettings)
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeProgramPresenter: AppkeeProgramInteractorOutput {

}

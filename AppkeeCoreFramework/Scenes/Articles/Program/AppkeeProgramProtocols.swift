//
//  AppkeeProgramProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 09/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit


// ======== Coordinator ======== //

//protocol AppkeeProgramCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeProgramCoordinatorInput: class {
    func navigate(to route: AppkeeProgram.Route)
}


// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeProgramInteractorInput {
    // func perform(_ request: Program.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeProgramInteractorOutput: class {
    // func present(_ response: Program.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeProgramPresenterInput {
    var numberOfSections: Int { get }
    func numberOfItems(in section: Int) -> Int
    
    var numberOfSectionsForDays: Int { get }
    func numberOfItemsForDays(in section: Int) -> Int
    
    var numberOfSectionsForRooms: Int { get }
    func numberOfItemsForRooms(in section: Int) -> Int
    
    func viewCreated()
    func handle(_ action: AppkeeProgram.Action)
    
    func typyOfCell(indexPath: IndexPath) -> AppkeeContentProgram.ContentType
    
    func configure(_ item: AppkeeProgramTableViewCell, at indexPath: IndexPath)
    func configure(_ item: AppkeeProgramBreakTableViewCell, at indexPath: IndexPath)
    func configure(day item: AppkeeProgramCollectionViewCell, at indexPath: IndexPath)
    func configure(room item: AppkeeProgramCollectionViewCell, at indexPath: IndexPath)

    func textSizeForDay(at indexPath: IndexPath) -> CGFloat
    func textSizeForRoom(at indexPath: IndexPath) -> CGFloat
}

// PRESENTER -> VIEW
protocol AppkeeProgramPresenterOutput: class {
    // func display(_ displayModel: Program.DisplayData.Work)
    func setupUI(colorSettings: AppkeeColorSettings)
    
    func updateIndexes(dayIndex: Int, roomIndex: Int)
}

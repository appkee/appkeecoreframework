//
//  AppkeeProgramCollectionViewCell.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 09/03/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import UIKit

protocol AppkeeProgramCollectionViewCellDelegate: class {
    func configure(with title: String, colorSettings: AppkeeColorSettings?)
}

class AppkeeProgramCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.contentView.layer.cornerRadius = 10.0
    }
}

extension AppkeeProgramCollectionViewCell: AppkeeProgramCollectionViewCellDelegate {
    func configure(with title: String, colorSettings: AppkeeColorSettings?) {
        if let colorSettings = colorSettings {
            titleLabel.textColor = colorSettings.contentText
            
            let selectedBackground = UIView()
            selectedBackground.backgroundColor = colorSettings.header
            selectedBackground.layer.cornerRadius = 10.0
            
            selectedBackgroundView = selectedBackground
            
            titleLabel.highlightedTextColor = colorSettings.headerText
        }
        
        titleLabel.text = title
    }    
}

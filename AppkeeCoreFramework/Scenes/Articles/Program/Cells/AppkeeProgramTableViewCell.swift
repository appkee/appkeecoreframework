//
//  AppkeeProgramTableViewCell.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 09/03/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import UIKit

protocol AppkeeProgramTableViewCellDelegate: class {
    func configure(with title: String, day: String, time: String, room: String, favorite: Bool, backgroundColor: String?, textColor: String?, colorSettings: AppkeeColorSettings?, favouriteHandler: @escaping (Bool) -> Void)
}

class AppkeeProgramTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.layer.cornerRadius = 10.0
            containerView.layer.shadowColor = UIColor.gray.cgColor
            containerView.layer.shadowOpacity = 1
            containerView.layer.shadowOffset = .zero
            containerView.layer.shadowRadius = 1.0
        }
    }
    
    @IBOutlet weak var dayLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var roomLabel: UILabel!
    
    @IBOutlet weak var favouriteButton: UIButton!
    
    var favourite: Bool = false {
        didSet {
            if favourite {
                favouriteButton.tintColor = UIColor("#FFCC00")
                let image = UIImage(named: "favouritesOn", in: Bundle(for: AppkeeProgramTableViewCell.self), compatibleWith: nil)
                favouriteButton.setImage( image, for: .normal)
            } else {
                favouriteButton.tintColor = .black
                let image = UIImage(named: "favouritesOff", in: Bundle(for: AppkeeProgramTableViewCell.self), compatibleWith: nil)
                favouriteButton.setImage( image, for: .normal)
            }
        }
    }
    
    var favouriteHandler: (Bool) -> Void = { _ in }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func favouriteButtonTap(_ sender: Any) {
        favourite = !favourite
        
        favouriteHandler(favourite)
    }
}

extension AppkeeProgramTableViewCell: AppkeeProgramTableViewCellDelegate {
    func configure(with title: String, day: String, time: String, room: String, favorite: Bool, backgroundColor: String?, textColor: String?, colorSettings: AppkeeColorSettings?, favouriteHandler: @escaping (Bool) -> Void) {
        if let colorSettings = colorSettings {
            dayLabel.textColor = colorSettings.contentText
            timeLabel.textColor = colorSettings.contentText
            titleLabel.textColor = colorSettings.contentText
            roomLabel.textColor = colorSettings.contentText
        }
        
        if let backgroundColor = backgroundColor {
            containerView.backgroundColor = UIColor(backgroundColor)
        }
        
        if let textColor = textColor {
            let color = UIColor(textColor)
            
            dayLabel.textColor = color
            timeLabel.textColor = color
            titleLabel.textColor = color
            roomLabel.textColor = color
        }
        
        dayLabel.text = day
        timeLabel.text = time
        titleLabel.text = title
        roomLabel.text = room
        
        self.favouriteHandler = favouriteHandler
        self.favourite = favorite
        
//        favoriteImageView.image =
        
        
    }
}

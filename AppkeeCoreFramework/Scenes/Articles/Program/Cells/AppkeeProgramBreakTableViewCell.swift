//
//  AppkeeProgramBreakTableViewCell.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/03/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import UIKit

protocol AppkeeProgramBreakTableViewCellCellDelegate: class {
    func configure(with title: String, time: String, backgroundColor: String?, textColor: String?, colorSettings: AppkeeColorSettings?)
}

class AppkeeProgramBreakTableViewCell: UITableViewCell {
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.layer.cornerRadius = 10.0
            containerView.layer.shadowColor = UIColor.gray.cgColor
            containerView.layer.shadowOpacity = 1
            containerView.layer.shadowOffset = .zero
            containerView.layer.shadowRadius = 1.0
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


extension AppkeeProgramBreakTableViewCell: AppkeeProgramBreakTableViewCellCellDelegate {
    func configure(with title: String, time: String, backgroundColor: String?, textColor: String?, colorSettings: AppkeeColorSettings?) {
        if let colorSettings = colorSettings {
            timeLabel.textColor = colorSettings.contentText
            titleLabel.textColor = colorSettings.contentText
        }
        
        timeLabel.text = time
        titleLabel.text = title
        
//        favoriteImageView.image =
        
        containerView.backgroundColor = .white
        
        if let backgroundColor = backgroundColor {
            containerView.backgroundColor = UIColor(backgroundColor)
        }
        
        if let textColor = textColor {
            let color = UIColor(textColor)
            
            timeLabel.textColor = color
            titleLabel.textColor = color        
        }
    }
}

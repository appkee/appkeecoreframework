//
//  AppkeeProgramInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 09/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeProgramInteractor {
    // MARK: - Properties
    weak var output: AppkeeProgramInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeProgramInteractor: AppkeeProgramInteractorInput {
}

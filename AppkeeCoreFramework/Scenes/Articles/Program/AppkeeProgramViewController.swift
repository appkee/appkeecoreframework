//
//  AppkeeProgramViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 09/03/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeProgramViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var dayCollectionView: UICollectionView! {
        didSet {
            dayCollectionView.register(AppkeeProgramCollectionViewCell.nib, forCellWithReuseIdentifier: AppkeeProgramCollectionViewCell.reuseId)
        }
    }
    
    @IBOutlet weak var roomCollectionView: UICollectionView! {
        didSet {
            roomCollectionView.register(AppkeeProgramCollectionViewCell.nib, forCellWithReuseIdentifier: AppkeeProgramCollectionViewCell.reuseId)
        }
    }
    
    @IBOutlet weak var programTableView: UITableView! {
        didSet {
            programTableView.register(AppkeeProgramTableViewCell.nib, forCellReuseIdentifier: AppkeeProgramTableViewCell.reuseId)
            programTableView.register(AppkeeProgramBreakTableViewCell.nib, forCellReuseIdentifier: AppkeeProgramBreakTableViewCell.reuseId)
            
            programTableView.tableFooterView = UIView()
        }
    }
    
    // MARK: - Properties
    private var presenter: AppkeeProgramPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeProgramPresenterInput) -> AppkeeProgramViewController {
        let name = "\(AppkeeProgramViewController.self)"
        let bundle = Bundle(for: AppkeeArticlesViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeProgramViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
    }

    // MARK: - Callbacks -

}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeProgramViewController: AppkeeProgramPresenterOutput {
    func setupUI(colorSettings: AppkeeColorSettings) {
        self.view.backgroundColor = colorSettings.content
    }
    
    func updateIndexes(dayIndex: Int, roomIndex: Int) {
        dayCollectionView.selectItem(at: IndexPath(row: dayIndex, section: 0), animated: true, scrollPosition: .left)
        roomCollectionView.selectItem(at: IndexPath(row: roomIndex, section: 0), animated: true, scrollPosition: .left)

        roomCollectionView.reloadData()
        programTableView.reloadData()
    }
}

// table view

extension AppkeeProgramViewController: UICollectionViewDelegateFlowLayout {
     func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (collectionView == dayCollectionView) {
//            let width = (collectionView.frame.size.width / CGFloat(presenter.numberOfItemsForDays(in: 0))) - 16.0
//            return CGSize(width: width, height: 40)

            let width = presenter.textSizeForDay(at: indexPath) + 24

            return CGSize(width: width, height: 40)
        }
        if (collectionView == roomCollectionView) {
//            let width = (roomCollectionView.frame.size.width / CGFloat(presenter.numberOfItemsForRooms(in: 0))) - 16.0
//            return CGSize(width: width, height: 40)

            let width = presenter.textSizeForRoom(at: indexPath) + 24

            return CGSize(width: width, height: 40)
        }

        return CGSize(width: 60, height: 40)
    }
}

extension AppkeeProgramViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  presenter.numberOfItems(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch presenter.typyOfCell(indexPath: indexPath) {
        case .detail:
            let cell = tableView.dequeueReusableCell(withIdentifier: AppkeeProgramTableViewCell.reuseId, for: indexPath) as! AppkeeProgramTableViewCell
            presenter.configure(cell, at: indexPath)
            
            return cell
        case .pause, .empty:
            let cell = tableView.dequeueReusableCell(withIdentifier: AppkeeProgramBreakTableViewCell.reuseId, for: indexPath) as! AppkeeProgramBreakTableViewCell
            presenter.configure(cell, at: indexPath)
            
            return cell
        }
    }
}

extension AppkeeProgramViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.handle(.rowSelected(indexPath.row))
    }
}

// collection view

extension AppkeeProgramViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if (collectionView == dayCollectionView) {
            return presenter.numberOfSectionsForDays
        } else if (collectionView == roomCollectionView) {
            return presenter.numberOfSectionsForRooms
        }
        
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView == dayCollectionView) {
            return presenter.numberOfItemsForDays(in: section)
        }
        if (collectionView == roomCollectionView) {
            return presenter.numberOfItemsForRooms(in: section)
        }
        
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AppkeeProgramCollectionViewCell.reuseId, for: indexPath) as! AppkeeProgramCollectionViewCell
        
        if (collectionView == dayCollectionView) {
            presenter.configure(day: cell, at: indexPath)
        }
        if (collectionView == roomCollectionView) {
            presenter.configure(room: cell, at: indexPath)
        }
                
        return cell
    }
}

extension AppkeeProgramViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView == dayCollectionView) {
            presenter.handle(.updateDay(indexPath.row))
        }
        if (collectionView == roomCollectionView) {
            presenter.handle(.updateRoom(indexPath.row))
        }
    }
}

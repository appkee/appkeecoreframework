//
//  AppkeeProgramCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 10/03/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit
import EasyTipView

class AppkeeProgramCoordinator: AppkeeCoordinator {
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
    private let section: AppkeeProgramSection
    private let banners: [AppkeeAdvertisement]?
    private let title: String?
    private let email: String?
//    weak var delegate: GalleryCoordinatorDelegate?
    
    var delegate: AppkeeSideMenuCoordinatorDelegate?

    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, section: AppkeeProgramSection, banners: [AppkeeAdvertisement]?, email: String? = nil, title: String? = nil) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.section = section
        self.banners = banners
        self.title = title
        self.email = email
    }

    func start(root: Bool = false) {
        let interactor = AppkeeProgramInteractor()
        let presenter = AppkeeProgramPresenter(interactor: interactor, coordinator: self, graphicManager: dependencies.graphicManager, program: section)
        let vc = AppkeeProgramViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        var menuItems: [UIBarButtonItem] = []
        if dependencies.configManager.appMenu {
            let image = UIImage(named: "dots", in: Bundle(for: AppkeeProgramCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(debugMenu(_:))))
        }

        if true  {
            let image = UIImage(named: "search", in: Bundle(for: AppkeeProgramCoordinator.self), compatibleWith: nil)
            let button = UIBarButtonItem(image: image, style: .done, target: self, action: #selector(search(_:)))
            menuItems.append(button)

            if UserDefaults.searchTooltip < 2 {
                var preferences = EasyTipView.Preferences()
                preferences.drawing.font = .systemFont(ofSize: 14.0)
                if let colorsSettings = dependencies.graphicManager.colorsSettings {
                    preferences.drawing.foregroundColor = colorsSettings.header
                    preferences.drawing.backgroundColor = colorsSettings.headerText
                }
                preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.top

                let view = UIView(frame: CGRect(x: vc.view.frame.width - 60, y: 1, width: 60, height: 1))
                vc.view.addSubview(view)

                EasyTipView.show(forView: view,
                                 withinSuperview: vc.view,
                                 text: translations .search.AUTHORS,
                preferences: preferences,
                delegate: self)

                UserDefaults.searchTooltip = UserDefaults.searchTooltip + 1
            }
        }

        vc.navigationItem.rightBarButtonItems = menuItems
        vc.title = title
        
        let rootController = AppkeeBannerViewController(graphicManager: dependencies.graphicManager)
        rootController.setViewController(vc)
        rootController.delegate = self

        if let banners = self.banners {
            rootController.setBanners(banners: banners)
        }

        if root {
            let image = UIImage(named: "hamburger", in: Bundle(for: AppkeeProgramCoordinator.self), compatibleWith: nil)
            rootController.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  image, style: .done, target: self, action: #selector(menu(_:)))

            navigationController.setViewControllers([rootController], animated: false)

            return
        }

        navigationController.pushViewController(rootController, animated: true)
    }

    @objc func menu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openMenu()
    }
    
    @objc func debugMenu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openDebugMenu()
    }

    @objc func search(_ barButtonItem: UIBarButtonItem) {
        let coordinator = ApppkeeProgramSearchCoordinator(navigationController: self.navigationController, dependencies: dependencies, section: section)
        childrens.append(coordinator)
        coordinator.start()
    }
}

// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeProgramCoordinator: AppkeeProgramCoordinatorInput {
    func navigate(to route: AppkeeProgram.Route) {
        switch route {
        case let .openAdvertisementLink(link):
            if let url = URL(string: link) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        case let .openDeeplink(params):
            delegate?.openDeeplink(params: params)
        case .detail(content: let content):
            let coordinator = AppkeeProgramDetailCoordinator(navigationController: self.navigationController, dependencies: dependencies, content: content)
            childrens.append(coordinator)
            coordinator.start()
        }
    }
}

extension AppkeeProgramCoordinator: AppkeeBannerViewControllerDelegate {
    func openLink(link: String) {
        self.navigate(to: .openAdvertisementLink(link: link))
    }

    func openDeeplink(params: String) {
        self.navigate(to: .openDeeplink(params: params))
    }
}

extension AppkeeProgramCoordinator: EasyTipViewDelegate {
    func easyTipViewDidTap(_ tipView: EasyTipView) {

    }

    func easyTipViewDidDismiss(_ tipView: EasyTipView) {

    }
}

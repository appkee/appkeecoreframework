//
//  SeniorPassIZSBranchDetailProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/3/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol SeniorPassIZSBranchDetailCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeSeniorPassIZSBranchDetailCoordinatorInput: class {
    func navigate(to route: AppkeeSeniorPassIZSBranchDetail.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeSeniorPassIZSBranchDetailInteractorInput {
    // func perform(_ request: SeniorPassIZSBranchDetail.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeSeniorPassIZSBranchDetailInteractorOutput: class {
    // func present(_ response: SeniorPassIZSBranchDetail.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeSeniorPassIZSBranchDetailPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeeSeniorPassIZSBranchDetail.Action)
}

// PRESENTER -> VIEW
protocol AppkeeSeniorPassIZSBranchDetailPresenterOutput: class {
    // func display(_ displayModel: SeniorPassIZSBranchDetail.DisplayData.Work)

    func display(_ displayModel: AppkeeSeniorPassIZSBranchDetail.DisplayData.Name)
    func display(_ displayModel: AppkeeSeniorPassIZSBranchDetail.DisplayData.Address)
    func display(_ displayModel: AppkeeSeniorPassIZSBranchDetail.DisplayData.Telephone)
    func display(_ displayModel: AppkeeSeniorPassIZSBranchDetail.DisplayData.Email)
    func display(_ displayModel: AppkeeSeniorPassIZSBranchDetail.DisplayData.Navigation)

    func setupUI(colorSettings: AppkeeColorSettings)
}

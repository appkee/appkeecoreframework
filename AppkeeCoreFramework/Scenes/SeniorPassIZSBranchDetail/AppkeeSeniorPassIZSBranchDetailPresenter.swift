//
//  SeniorPassIZSBranchDetailPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/3/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeSeniorPassIZSBranchDetailPresenter {
    // MARK: - Properties
    private let graphicManager: AppkeeGraphicManager

    private let branch: AppkeeSeniorPassIZSBranch

    let interactor: AppkeeSeniorPassIZSBranchDetailInteractorInput
    weak var coordinator: AppkeeSeniorPassIZSBranchDetailCoordinatorInput?
    weak var output: AppkeeSeniorPassIZSBranchDetailPresenterOutput?

    // MARK: - Init
    init(interactor: AppkeeSeniorPassIZSBranchDetailInteractorInput, coordinator: AppkeeSeniorPassIZSBranchDetailCoordinatorInput, graphicManager: AppkeeGraphicManager, branch: AppkeeSeniorPassIZSBranch) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
        self.branch = branch
    }
}

// MARK: - User Events -

extension AppkeeSeniorPassIZSBranchDetailPresenter: AppkeeSeniorPassIZSBranchDetailPresenterInput {
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }

        output?.display(AppkeeSeniorPassIZSBranchDetail.DisplayData.Name(name: branch.name))

        var address = ""
        if !branch.address.isEmpty {
            address = branch.address
        }

        if !address.isEmpty {
            address += ", \(branch.psc)"
        } else {
            address = String(branch.psc)
        }

        if !branch.city.isEmpty {
            if !address.isEmpty {
                address += " " + branch.city
            } else {
                address = branch.city
            }
        }

        output?.display(AppkeeSeniorPassIZSBranchDetail.DisplayData.Address(address: address))

        output?.display(AppkeeSeniorPassIZSBranchDetail.DisplayData.Telephone(tel: branch.phone))
        output?.display(AppkeeSeniorPassIZSBranchDetail.DisplayData.Email(email: branch.email))
        output?.display(AppkeeSeniorPassIZSBranchDetail.DisplayData.Navigation())
    }

    func handle(_ action: AppkeeSeniorPassIZSBranchDetail.Action) {
        switch action {
        case .navigate:
            coordinator?.navigate(to: .openGPS(lat: String(branch.lat), lon: String(branch.lon)))
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeSeniorPassIZSBranchDetailPresenter: AppkeeSeniorPassIZSBranchDetailInteractorOutput {

}

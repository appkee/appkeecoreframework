//
//  SeniorPassIZSBranchDetailModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/3/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeSeniorPassIZSBranchDetail {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case navigate
    }

    enum Route {
        case openGPS(lat: String, lon: String)
    }
}

extension AppkeeSeniorPassIZSBranchDetail.Request {

}

extension AppkeeSeniorPassIZSBranchDetail.Response {

}

extension AppkeeSeniorPassIZSBranchDetail.DisplayData {
    struct Name {
        let name: String
    }

    struct Address {
        let address: String
    }

    struct Telephone {
        let tel: String
    }

    struct Email {
        let email: String
    }

    struct Navigation {

    }
}

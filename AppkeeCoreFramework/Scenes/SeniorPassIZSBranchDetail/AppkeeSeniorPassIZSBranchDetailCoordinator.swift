//
//  SeniorPassIZSBranchDetailCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/3/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeeSeniorPassIZSBranchDetailCoordinator: AppkeeCoordinator {
    /// MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
    private let branch: AppkeeSeniorPassIZSBranch
    private let email: String?
    private let search: Bool
//    weak var delegate: GalleryCoordinatorDelegate?

    var delegate: AppkeeSideMenuCoordinatorDelegate?

    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, branch: AppkeeSeniorPassIZSBranch, email: String?, search: Bool) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.branch = branch
        self.email = email
        self.search = search
    }

    func start(root: Bool = false) {
        let interactor = AppkeeSeniorPassIZSBranchDetailInteractor(repository: dependencies.repository, graphicManager: dependencies.graphicManager)
        let presenter = AppkeeSeniorPassIZSBranchDetailPresenter(interactor: interactor, coordinator: self, graphicManager: dependencies.graphicManager, branch: branch)
        let vc = AppkeeSeniorPassIZSBranchDetailViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        vc.title = branch.type

        navigationController.navigationBar.isHidden = false
        navigationController.pushViewController(vc, animated: true)
    }

    @objc func menu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openMenu()
    }

    @objc func debugMenu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openDebugMenu()
    }

    @objc func photoReporter(_ barButtonItem: UIBarButtonItem) {
        if let email = self.email {
            delegate?.openPhotoReporter(email: email)
        }
    }

    @objc func search(_ barButtonItem: UIBarButtonItem) {
        delegate?.openSearch()
    }
}

// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeSeniorPassIZSBranchDetailCoordinator: AppkeeSeniorPassIZSBranchDetailCoordinatorInput {
    func navigate(to route: AppkeeSeniorPassIZSBranchDetail.Route) {
        switch route {
        case .openGPS(let lat, let lon):
            self.delegate?.openGPS(lat: lat, lon: lon)
        }
    }
}

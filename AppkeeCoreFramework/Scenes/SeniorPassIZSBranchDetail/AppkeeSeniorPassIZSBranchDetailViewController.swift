//
//  SeniorPassIZSBranchDetailViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/3/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeSeniorPassIZSBranchDetailViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var addressTitleLabel: UILabel! {
        didSet {
            addressTitleLabel.text = translations.seniorPass.BRANCH_ADDRESS
        }
    }
    @IBOutlet weak var addressLabel: UILabel!

    @IBOutlet weak var telView: UIView!
    @IBOutlet weak var telTitleLabel: UILabel! {
        didSet {
            telTitleLabel.text = translations.seniorPass.BRANCH_IZS_PHONE
        }
    }
    @IBOutlet weak var telTextView: UITextView! {
        didSet {
            telTextView.textContainerInset = UIEdgeInsets.zero
            telTextView.textContainer.lineFragmentPadding = 0
        }
    }

    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTitleLabel: UILabel! {
        didSet {
            emailTitleLabel.text = translations.seniorPass.BRANCH_IZS_EMAIL
        }
    }

    @IBOutlet weak var emailTextView: UITextView! {
        didSet {
            emailTextView.textContainerInset = UIEdgeInsets.zero
            emailTextView.textContainer.lineFragmentPadding = 0
        }
    }

    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var navigationButton: UIButton! {
        didSet {
            navigationButton.setTitle(translations.seniorPass.BRANCH_NAVIGATE.uppercased(), for: .normal)
        }
    }

    // MARK: - Properties
    private var presenter: AppkeeSeniorPassIZSBranchDetailPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeSeniorPassIZSBranchDetailPresenterInput) -> AppkeeSeniorPassIZSBranchDetailViewController {
        let name = "\(AppkeeSeniorPassIZSBranchDetailViewController.self)"
        let bundle = Bundle(for: AppkeeSeniorPassIZSBranchDetailViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeSeniorPassIZSBranchDetailViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()

        addressView.isHidden = true
        emailView.isHidden = true
        telView.isHidden = true
        navigationView.isHidden = true

        presenter.viewCreated()
    }

    // MARK: - Callbacks -
    @IBAction func navigationButton(_ sender: Any) {
        presenter.handle(.navigate)
    }

}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeSeniorPassIZSBranchDetailViewController: AppkeeSeniorPassIZSBranchDetailPresenterOutput {
    func display(_ displayModel: AppkeeSeniorPassIZSBranchDetail.DisplayData.Name) {
        self.titleLabel.text = displayModel.name
    }
    
    func display(_ displayModel: AppkeeSeniorPassIZSBranchDetail.DisplayData.Address) {
        self.addressLabel.text = displayModel.address
        self.addressView.isHidden = false
    }
    
    func display(_ displayModel: AppkeeSeniorPassIZSBranchDetail.DisplayData.Telephone) {
        self.telTextView.text = displayModel.tel
        self.telView.isHidden = false
    }
    
    func display(_ displayModel: AppkeeSeniorPassIZSBranchDetail.DisplayData.Email) {
        self.emailTextView.text = displayModel.email
        self.emailView.isHidden = false
    }
    
    func display(_ displayModel: AppkeeSeniorPassIZSBranchDetail.DisplayData.Navigation) {
        self.navigationView.isHidden = false
    }
    
    func setupUI(colorSettings: AppkeeColorSettings) {
        self.view.backgroundColor = colorSettings.content

        self.titleLabel.textColor = colorSettings.menu

        self.telTextView.tintColor = colorSettings.loginColor
        self.emailTextView.tintColor = colorSettings.loginColor

        self.navigationButton.setTitleColor(.white, for: .normal)
        self.navigationButton.backgroundColor = colorSettings.loginColor
    }
}

//
//  RSSTableViewCell.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 30/09/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit
import WebKit
import Alamofire
import AlamofireImage

protocol AppkeeRSSTableViewCellDelegate: class {
    func configure(with title: String, description: String?, imageLink: String?, colorSettings: AppkeeColorSettings?)
}

class AppkeeRSSTableViewCell: UITableViewCell {
    
    //MARK: IBOutlets
    @IBOutlet weak var iconStackView: UIStackView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!


    private var webView: WKWebView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension AppkeeRSSTableViewCell: AppkeeRSSTableViewCellDelegate {
    func configure(with title: String, description: String?, imageLink: String?, colorSettings: AppkeeColorSettings?) {
        titleLabel.text = title
        descriptionLabel.text = description

        self.iconStackView.isHidden = true

        if let imageLink = imageLink {
            Alamofire.request(imageLink).responseImage { response in
                self.iconStackView.isHidden = false
                self.iconImageView.image = response.value
            }
        }


        if let colorSettings = colorSettings {            
            titleLabel.textColor = colorSettings.contentText
            descriptionLabel.textColor = colorSettings.contentText
        }
    }
}

//
//  AppkeeRSSViewController.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 30/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeRSSViewController: UIViewController {
    
    // MARK: - Outlets
    private let pullToRefresh: UIRefreshControl = {
        let pullToRefresh = UIRefreshControl()
        pullToRefresh.attributedTitle = NSAttributedString(string: translations.common.TABLE_PULL_TO_REFRESH)
        pullToRefresh.tintColor = .clear
        return pullToRefresh
    }()
    
    private let tableLabel: UILabel = {
         let label = UILabel()
         label.text = translations.rss.NO_DATA
         label.textAlignment = .center
         label.isHidden = true
         return label
    }()
    
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.register(AppkeeRSSTableViewCell.nib, forCellReuseIdentifier: AppkeeRSSTableViewCell.reuseId)
            tableView.estimatedRowHeight = 40
//            tableView.separatorStyle = .none
            tableView.backgroundView = tableLabel
        }
    }

    // MARK: - Properties
    private var presenter: AppkeeRSSPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeRSSPresenterInput) -> AppkeeRSSViewController {
        let name = "\(AppkeeRSSViewController.self)"
        let bundle = Bundle(for: AppkeeRSSViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeRSSViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        
        pullToRefresh.addTarget(self, action: #selector(reloadData), for: .valueChanged)
        tableView.refreshControl = pullToRefresh
        
        presenter.viewCreated()
        showProgress()
    }
    
    @objc func reloadData() {
        pullToRefresh.endRefreshing()
        
        showProgress()
        presenter.handle(.reload)
    }

    // MARK: - Callbacks -    
}

extension AppkeeRSSViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.numberOfItems(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AppkeeRSSTableViewCell.reuseId, for: indexPath) as! AppkeeRSSTableViewCell
        presenter.configure(cell, at: indexPath)
        return cell
    }
}

extension AppkeeRSSViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.handle(.navigate(indexPath.row))
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeRSSViewController: AppkeeRSSPresenterOutput, Alertable, Progressable {
    
    func display(_ displayModel: AppkeeRSS.DisplayData.Reload) {
        hideProgress()
        tableLabel.isHidden = !displayModel.emptyArray
        tableView.reloadData()
    }
    
    func display(_ displayModel: AppkeeRSS.DisplayData.Error) {
        hideProgress()
        tableLabel.isHidden = false
        tableView.reloadData()
       
       showErrorAlert(withMessage: displayModel.message)
    }
    
    func setupUI(colorSettings: AppkeeColorSettings) {
        pullToRefresh.backgroundColor = colorSettings.contentText
        if let attributedTitle = pullToRefresh.attributedTitle {
            let mutableAttributedTitle = NSMutableAttributedString(attributedString: attributedTitle)
            mutableAttributedTitle.addAttribute(.foregroundColor, value: colorSettings.content, range: NSRange(location: 0, length: attributedTitle.length))
            
            pullToRefresh.attributedTitle = mutableAttributedTitle
        }
                
        tableView.separatorColor = colorSettings.header
        tableLabel.textColor = colorSettings.contentText
        
        self.view.backgroundColor = colorSettings.content
    }
}

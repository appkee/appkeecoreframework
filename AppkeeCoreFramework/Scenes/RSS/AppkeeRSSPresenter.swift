//
//  AppkeeRSSPresenter.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 30/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeRSSPresenter {
    // MARK: - Properties
    var rsss: [AppkeeRSSFeedItem] = []
    
    private let section: AppkeeSection
    private let graphicManager: AppkeeGraphicManager
    
    let interactor: AppkeeRSSInteractorInput
    weak var coordinator: AppkeeRSSCoordinatorInput?
    weak var output: AppkeeRSSPresenterOutput?

    // MARK: - Init
    init(interactor: AppkeeRSSInteractorInput, coordinator: AppkeeRSSCoordinatorInput, graphicManager: AppkeeGraphicManager, section: AppkeeSection) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.section = section
        self.graphicManager = graphicManager
    }
}

// MARK: - User Events -

extension AppkeeRSSPresenter: AppkeeRSSPresenterInput {
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfItems(in section: Int) -> Int {
        return rsss.count
    }
        
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }
        
        if let rssLink = section.rssLink {
           interactor.perform(AppkeeRSS.Request.Feed(link: rssLink))
        }
    }

    func handle(_ action: AppkeeRSS.Action) {
        switch action {
        case .navigate(let index):
            let rss = rsss[index]
            if let link = rss.link {
                coordinator?.navigate(to: .open(link))
            }
        case .reload:
            if let rssLink = section.rssLink {
               interactor.perform(AppkeeRSS.Request.Feed(link: rssLink))
            }
        }
    }
    
    func configure(_ item: AppkeeRSSTableViewCell, at indexPath: IndexPath) {
        let rssItem = rsss[indexPath.row]

        item.configure(with: rssItem.title ?? "", description: rssItem.itemDescription?.toHtml().string, imageLink: rssItem.image, colorSettings: graphicManager.colorsSettings)
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeRSSPresenter: AppkeeRSSInteractorOutput {
    func present(_ response: AppkeeRSS.Response.Error) {
        rsss = []
        output?.display(AppkeeRSS.DisplayData.Error(message: response.message))
    }
    
    func present(_ response: AppkeeRSS.Response.Feed) {
        rsss = response.rss
        output?.display(AppkeeRSS.DisplayData.Reload(emptyArray: rsss.isEmpty))
    }

}

//
//  AppkeeRSSProtocols.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 30/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol RSSCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeRSSCoordinatorInput: class {
    func navigate(to route: AppkeeRSS.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeRSSInteractorInput {
     func perform(_ request: AppkeeRSS.Request.Feed)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeRSSInteractorOutput: class {
    func present(_ response: AppkeeRSS.Response.Feed)
    func present(_ response: AppkeeRSS.Response.Error)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeRSSPresenterInput {
    var numberOfSections: Int { get }
    func numberOfItems(in section: Int) -> Int
    
    func viewCreated()
    func handle(_ action: AppkeeRSS.Action)
    
    func configure(_ item: AppkeeRSSTableViewCell, at indexPath: IndexPath)
}

// PRESENTER -> VIEW
protocol AppkeeRSSPresenterOutput: class {    
    func display(_ displayModel: AppkeeRSS.DisplayData.Reload)
    func display(_ displayModel: AppkeeRSS.DisplayData.Error)
    
    func setupUI(colorSettings: AppkeeColorSettings)
}

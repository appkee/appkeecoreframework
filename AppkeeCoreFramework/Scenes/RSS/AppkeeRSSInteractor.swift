//
//  AppkeeRSSInteractor.swift
//  AppkeeAppkeeCore
//
//  Created by Radek Zmeskal on 30/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import FirebaseAnalytics

typealias AppkeeRSSFeedItem = RSSItem

class AppkeeRSSInteractor {    
    // MARK: - Properties
    weak var output: AppkeeRSSInteractorOutput?
    private let repository: AppkeeRepository
    private let graphicManager: AppkeeGraphicManager
    
    // MARK: - Init
    init(repository: AppkeeRepository, graphicManager: AppkeeGraphicManager) {
        self.repository = repository
        self.graphicManager = graphicManager
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeRSSInteractor: AppkeeRSSInteractorInput {
    func perform(_ request: AppkeeRSS.Request.Feed) {
        repository.getRSS(link: request.link) { [weak self] response in
            guard let self = self else { return }
            
            switch response.result {
            case .success(let response):                
                self.output?.present(AppkeeRSS.Response.Feed(rss: response.items))
            case .failure(let error):
                self.output?.present(AppkeeRSS.Response.Error(message: error.localizedDescription))
                
                Analytics.logEvent("Lodaing RSS failed", parameters: ["link": request.link, "error": error.localizedDescription])
            }
        }
    }
}

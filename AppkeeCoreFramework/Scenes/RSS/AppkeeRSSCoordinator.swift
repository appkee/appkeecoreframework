//
//  AppkeeRSSCoordinator.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 30/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit
import SafariServices

class AppkeeRSSCoordinator: AppkeeCoordinator {
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
    private let section: AppkeeSection
    private let email: String?
    private let search: Bool
//    weak var delegate: RSSCoordinatorDelegate?
    
    var delegate: AppkeeSideMenuCoordinatorDelegate?

    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, section: AppkeeSection, email: String?, search: Bool) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.section = section
        self.email = email
        self.search = search
    }

    func start(root: Bool = false) {
        let interactor = AppkeeRSSInteractor(repository: dependencies.repository, graphicManager: dependencies.graphicManager)
        let presenter = AppkeeRSSPresenter(interactor: interactor, coordinator: self, graphicManager: dependencies.graphicManager, section: section)
        let vc = AppkeeRSSViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        var menuItems: [UIBarButtonItem] = []
        if dependencies.configManager.appMenu {
            let image = UIImage(named: "dots", in: Bundle(for: AppkeeRSSCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(debugMenu(_:))))
        }

        if let _ = UserDefaults.pageUser {
            let image = UIImage(named: "logoutIcon", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(logoutTap(_:))))
        }

        if search {
            let image = UIImage(named: "search", in: Bundle(for: AppkeeRSSCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(search(_:))))
        }

        if let _ = email {
            let image = UIImage(named: "cameraMenu", in: Bundle(for: AppkeeRSSCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(photoReporter(_:))))
        }

        if UserDefaults.seniorPassLogin != nil || UserDefaults.familyPassLogin != nil {
            let image = UIImage(named: "logoutIcon", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(passLogoutTap(_:))))
        }
        
        vc.navigationItem.rightBarButtonItems = menuItems
        vc.title = section.name
        
        let rootController = AppkeeBannerViewController(graphicManager: dependencies.graphicManager)
        rootController.setViewController(vc)
        rootController.delegate = self

        if let banners = section.banners {
            rootController.setBanners(banners: banners)
        }

        if root {
            let image = UIImage(named: "hamburger", in: Bundle(for: AppkeeRSSCoordinator.self), compatibleWith: nil)
            rootController.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  image, style: .done, target: self, action: #selector(menu(_:)))

            navigationController.setViewControllers([rootController], animated: false)

            return
        }
        
        navigationController.navigationBar.isHidden = false
        navigationController.pushViewController(rootController, animated: true)
    }
    
    @objc func menu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openMenu()
    }
    
    @objc func debugMenu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openDebugMenu()
    }
    
    @objc func photoReporter(_ barButtonItem: UIBarButtonItem) {
        if let email = self.email {
            delegate?.openPhotoReporter(email: email)
        }
    }
    
    @objc func search(_ barButtonItem: UIBarButtonItem) {
        delegate?.openSearch()
    }

    @objc func logoutTap(_ barButtonItem: UIBarButtonItem) {
        delegate?.logout()
    }

    @objc func passLogoutTap(_ barButtonItem: UIBarButtonItem) {
        delegate?.passLogout()
    }
}

// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeRSSCoordinator: AppkeeRSSCoordinatorInput {
    func navigate(to route: AppkeeRSS.Route) {
        switch route {
        case .open(let link):
            if let url = URL(string: link) {
                let vc = SFSafariViewController(url: url, entersReaderIfAvailable: true)
                navigationController.present(vc, animated: true)
            }
        case let .openAdvertisementLink(link):
            if let url = URL(string: link) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        case let .openDeeplink(params):
            delegate?.openDeeplink(params: params)
        }
    }
}

extension AppkeeRSSCoordinator: AppkeeBannerViewControllerDelegate {
    func openLink(link: String) {
        self.navigate(to: .openAdvertisementLink(link: link))
    }

    func openDeeplink(params: String) {
        self.navigate(to: .openDeeplink(params: params))
    }
}

//
//  AppkeeRSSModels.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 30/09/2019.
//  Copyright (c) 2019 Radek Zmeskal. All rights reserved.
//

import Foundation


enum AppkeeRSS {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case navigate(Int)
        case reload
    }

    enum Route {
        case open(String)
        case openAdvertisementLink(link: String)
        case openDeeplink(params: String)
    }
}

extension AppkeeRSS.Request {
    struct Feed {
        let link: String
    }
}

extension AppkeeRSS.Response {
    struct Feed {
        let rss: [AppkeeRSSFeedItem]
    }
    
    struct Error {
        let message: String
    }
}

extension AppkeeRSS.DisplayData {
    struct Reload {
        let emptyArray: Bool
    }
    
    struct Error {
        let message: String
    }
}

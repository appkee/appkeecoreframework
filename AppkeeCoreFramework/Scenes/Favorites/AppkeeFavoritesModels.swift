//
//  AppkeeFavoritesModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 1/24/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

struct AppkeeFavorite {
    let aticleId: Int
    let name: String
    let texts: [AppkeeContent]
    let images: [AppkeeContent]
    let videos: [AppkeeContent]
    let hideImages: Int
    let type: AppkeeArticleContentType
    let advertisement: AppkeeAdvertisement?
}

enum AppkeeFavorites {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case navigate(Int)
        case reload
    }

    enum Route {
        case open(AppkeePageProps)
        case openAdvertisementLink(link: String)
        case openDeeplink(params: String)
    }
}

extension AppkeeFavorites.Request {

}

extension AppkeeFavorites.Response {

}

extension AppkeeFavorites.DisplayData {
    
}

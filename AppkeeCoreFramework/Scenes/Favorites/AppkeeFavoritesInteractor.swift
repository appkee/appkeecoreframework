//
//  AppkeeFavoritesInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 1/24/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeFavoritesInteractor {
    // MARK: - Properties
    weak var output: AppkeeFavoritesInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeFavoritesInteractor: AppkeeFavoritesInteractorInput {
}

//
//  AppkeeFavoritesPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 1/24/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeFavoritesPresenter {
    // MARK: - Properties
    private let sections: [AppkeeSection]
    private let graphicManager: AppkeeGraphicManager
    private let favoritesType: AppkeeAppDescription.FavoriteType

    let interactor: AppkeeFavoritesInteractorInput
    weak var coordinator: AppkeeFavoritesCoordinatorInput?
    weak var output: AppkeeFavoritesPresenterOutput?

    var favorites: [AppkeeFavorite] = []

    // MARK: - Init
    init(interactor: AppkeeFavoritesInteractorInput, coordinator: AppkeeFavoritesCoordinatorInput, graphicManager: AppkeeGraphicManager, sections: [AppkeeSection], favoritesType: AppkeeAppDescription.FavoriteType) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.sections = sections
        self.graphicManager = graphicManager
        self.favoritesType = favoritesType

        self.getFavorites()
    }
}

// MARK: - User Events -

extension AppkeeFavoritesPresenter: AppkeeFavoritesPresenterInput {
    var numberOfSections: Int {
        return 1
    }

    func numberOfItems(in section: Int) -> Int {
        return favorites.count
    }

    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            var noData: String
            switch self.favoritesType {
            case .article:
                noData = translations.favorites.ARTICLE_NO_DATA
            case .recipe:
                noData = translations.favorites.RECIPE_NO_DATA
            }

            output?.setupUI(colorSettings: colorsSettings, noData: noData)
        }
    }

    func handle(_ action: AppkeeFavorites.Action) {
        switch action {
        case .navigate(let index):
            let article = favorites[index]

            self.openArticle(article: article)
        case .reload:
            self.getFavorites()
        }
    }

    func configure(_ item: AppkeeArticleTableViewCellDelegate, at indexPath: IndexPath) {
        let article = favorites[indexPath.row]

        let image = article.images.first
        let text = article.texts.first

        let icon = graphicManager.getScaledImage(name: image?.content)

        item.configure(with: article.type, hideImages: article.hideImages, icon: icon, title: article.name, description: text?.content, colorSettings: graphicManager.colorsSettings)
    }

    private func openArticle(article: AppkeeFavorite) {
        switch article.type {
        case .page:
//            let props = AppkeePageProps(articleId: article.aticleId, title: article.name, texts: article.texts, images: article.images, videos: article.videos, favorites: true, favoritesType: self.favoritesType, advertisement: article.advertisement)

            let props = AppkeePageProps(articleId: article.aticleId, title: article.name, texts: article.texts, images: article.images, videos: article.videos, favorites: true, favoritesType: self.favoritesType, banners: nil)

            coordinator?.navigate(to: .open(props))
        default:
            break
        }
    }

    func getFavorites() {
        let favoritesId = UserDefaults.favorites()
        let favotitesSections = sections.filter { $0.favorites }

        var favorites: [AppkeeFavorite] = []

        for section in favotitesSections {
            for article in section.articles ?? [] {
                for id in favoritesId {
                    if article.id == id {
                        guard let article = section.articles?.first(where: { $0.id == id }) else {
                            continue
                        }
                        let texts = section.texts?.filter({ $0.articleId == id }) ?? []
                        let images = section.images?.filter({ $0.articleId == id }) ?? []
                        let videos = section.videos?.filter({ $0.articleId == id }) ?? []

                        let favorite = AppkeeFavorite(aticleId: article.id, name: article.name ?? "", texts: texts, images: images, videos: videos, hideImages: section.hideImages, type: article.type, advertisement: section.advertisement)

                        favorites.append(favorite)
                    }
                }
            }
        }

        self.favorites = favorites

        self.output?.reload()
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeFavoritesPresenter: AppkeeFavoritesInteractorOutput {

}

extension AppkeeFavoritesPresenter: AppkeeAdvertisementViewDelegate {
    func close() {
//        if let advertisement = section.advertisement {
//            UserDefaults.addAdvertisement(advertisementId: advertisement.id)
//            output?.removeAdvertisement()
//        }
    }

    func open() {
//        if let advertisement = section.advertisement, let link = advertisement.url {
//            coordinator?.navigate(to: AppkeeFavorites.Route.openAdvertisementLink(link: link))
//        }
    }
}

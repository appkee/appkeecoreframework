//
//  AppkeeFavoritesProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 1/24/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeeFavoritesCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeFavoritesCoordinatorInput: class {
    func navigate(to route: AppkeeFavorites.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeFavoritesInteractorInput {
    // func perform(_ request: AppkeeFavorites.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeFavoritesInteractorOutput: class {
    // func present(_ response: AppkeeFavorites.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeFavoritesPresenterInput {
    var numberOfSections: Int { get }
    func numberOfItems(in section: Int) -> Int

    func viewCreated()
    func handle(_ action: AppkeeFavorites.Action)

    func configure(_ item: AppkeeArticleTableViewCellDelegate, at indexPath: IndexPath)
}

// PRESENTER -> VIEW
protocol AppkeeFavoritesPresenterOutput: class {
    func setupUI(colorSettings: AppkeeColorSettings, noData: String)
    func reload()
}

//
//  AppkeeFavoritesViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 1/24/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeFavoritesViewController: UIViewController {
    // MARK: - Outlets
    private let tableLabel: UILabel = {
         let label = UILabel()
         label.textAlignment = .center
         label.isHidden = true
         return label
    }()
    
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.register(AppkeeArticleTableViewCell.nib, forCellReuseIdentifier: AppkeeArticleTableViewCell.reuseId)
            tableView.estimatedRowHeight = 30
            tableView.estimatedSectionHeaderHeight = 30
            tableView.separatorStyle = .singleLine
            tableView.tableFooterView = UIView()
            tableView.rowHeight = UITableView.automaticDimension
            tableView.backgroundView = tableLabel
        }
    }

    // MARK: - Properties
    private var presenter: AppkeeFavoritesPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeFavoritesPresenterInput) -> AppkeeFavoritesViewController {
        let name = "\(AppkeeFavoritesViewController.self)"
        let bundle = Bundle(for: AppkeeFavoritesViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeFavoritesViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.presenter.handle(.reload)

//        if let indexPath = tableView.indexPathForSelectedRow {
//            tableView.deselectRow(at: indexPath, animated: true)
//        }
    }
    
    // MARK: - Callbacks -


}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeFavoritesViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableLabel.isHidden = presenter.numberOfItems(in: section) > 0
        return presenter.numberOfItems(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AppkeeArticleTableViewCell.reuseId, for: indexPath) as! AppkeeArticleTableViewCell
        presenter.configure(cell, at: indexPath)

        return cell
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if let _ = tableView.headerView(forSection: section) {
            return UITableView.automaticDimension
        } else {
            return 1
        }
    }
}

extension AppkeeFavoritesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.handle(.navigate(indexPath.row))
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeFavoritesViewController: AppkeeFavoritesPresenterOutput {
    func setupUI(colorSettings: AppkeeColorSettings, noData: String) {
        self.tableView.separatorColor = colorSettings.header
        self.view.backgroundColor = colorSettings.content

        self.tableLabel.text = noData
    }

    func reload() {
        self.tableView.reloadData()
    }
}

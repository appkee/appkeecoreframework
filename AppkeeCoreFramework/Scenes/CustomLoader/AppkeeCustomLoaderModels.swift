//
//  AppkeeCustomLoaderModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 7/14/20.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeCustomLoader {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case login
        case signIn
    }

    enum Route {
        case login
        case signIn
    }
}

extension AppkeeCustomLoader.Request {

}

extension AppkeeCustomLoader.Response {

}

extension AppkeeCustomLoader.DisplayData {
    
}

//
//  AppkeeCustomLoaderViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 7/14/20.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import UIKit
import AVKit

class AppkeeCustomLoaderViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var loginButton: UIButton! {
        didSet {
            loginButton.setTitle(translations.login.LOGIN, for: .normal)
            loginButton.setTitleColor(.white, for: .normal)
            loginButton.backgroundColor = AppkeeConstants.loginColor
            loginButton.layer.cornerRadius = 8
        }
    }
    
    @IBOutlet weak var signInButton: UIButton! {
        didSet {
            signInButton.setTitle(translations.login.SIGNIN, for: .normal)
            signInButton.setTitleColor(.white, for: .normal)
            signInButton.backgroundColor = AppkeeConstants.loginColor
            signInButton.layer.cornerRadius = 8
        }
    }
    
    @IBOutlet weak var playerView: UIView!
    
    // MARK: - Properties
    private var presenter: AppkeeCustomLoaderPresenterInput!
    
    var playerLooper: AVPlayerLooper? = nil

    // MARK: - Init
    class func instantiate(with presenter: AppkeeCustomLoaderPresenterInput) -> AppkeeCustomLoaderViewController {
        let name = "\(AppkeeCustomLoaderViewController.self)"
        let bundle = Bundle(for: AppkeeCustomLoaderViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)               
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeCustomLoaderViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let url = Bundle.main.url(forResource: "login_bg", withExtension: "mp4") {
            let playerItem = AVPlayerItem(url: url)
            let player = AVQueuePlayer(playerItem: playerItem)
            let playerLayer = AVPlayerLayer(player: player)

            playerLooper = AVPlayerLooper(player: player, templateItem: playerItem)

            playerLayer.frame = self.playerView.bounds
            playerLayer.videoGravity = AVLayerVideoGravity.resizeAspect
            playerLayer.videoGravity = .resizeAspectFill;
            
            self.playerView.layer.addSublayer(playerLayer)
            
            player.play()
        }
    }
    
    // MARK: - Actions -
    
    @IBAction func loginButtonTap(_ sender: Any) {
        presenter.handle(.login)
    }
    
    @IBAction func signInButtonTap(_ sender: Any) {
        presenter.handle(.signIn)
    }
    
    // MARK: - Callbacks -

}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeCustomLoaderViewController: AppkeeCustomLoaderPresenterOutput {

}

//
//  AppkeeCustomLoaderProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 7/14/20.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeeCustomLoaderCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeCustomLoaderCoordinatorInput: class {
    func navigate(to route: AppkeeCustomLoader.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeCustomLoaderInteractorInput {
    // func perform(_ request: AppkeeCustomLoader.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeCustomLoaderInteractorOutput: class {
    // func present(_ response: AppkeeCustomLoader.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeCustomLoaderPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeeCustomLoader.Action)
}

// PRESENTER -> VIEW
protocol AppkeeCustomLoaderPresenterOutput: class {
    // func display(_ displayModel: AppkeeCustomLoader.DisplayData.Work)
}

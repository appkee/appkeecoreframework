//
//  AppkeeCustomLoaderPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 7/14/20.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeCustomLoaderPresenter {
    // MARK: - Properties
    let interactor: AppkeeCustomLoaderInteractorInput
    weak var coordinator: AppkeeCustomLoaderCoordinatorInput?
    weak var output: AppkeeCustomLoaderPresenterOutput?

    // MARK: - Init
    init(interactor: AppkeeCustomLoaderInteractorInput, coordinator: AppkeeCustomLoaderCoordinatorInput) {
        self.interactor = interactor
        self.coordinator = coordinator
    }
}

// MARK: - User Events -

extension AppkeeCustomLoaderPresenter: AppkeeCustomLoaderPresenterInput {
    func viewCreated() {

    }

    func handle(_ action: AppkeeCustomLoader.Action) {
        switch action {
        case .login:
            coordinator?.navigate(to: .login)
        case .signIn:
            coordinator?.navigate(to: .signIn)
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeCustomLoaderPresenter: AppkeeCustomLoaderInteractorOutput {

}

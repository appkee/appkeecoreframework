//
//  AppkeeCustomLoaderInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 7/14/20.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeCustomLoaderInteractor {
    // MARK: - Properties
    weak var output: AppkeeCustomLoaderInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeCustomLoaderInteractor: AppkeeCustomLoaderInteractorInput {
}

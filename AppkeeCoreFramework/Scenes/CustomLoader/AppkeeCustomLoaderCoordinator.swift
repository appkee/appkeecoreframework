//
//  AppkeeCustomLoaderCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 7/14/20.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class AppkeeCustomLoaderCoordinator: AppkeeCoordinator {
    // MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies    
//    weak var delegate: AppkeeCustomLoaderCoordinatorDelegate?
    
    private let window: UIWindow

    weak var delegate: AppkeeCoordinatorDelegate?

    // MARK: - Init
     init(window: UIWindow, dependencies: AppkeeFullDependencies) {
           self.dependencies = dependencies
           self.window = window
           
           self.navigationController = UINavigationController()
           self.navigationController.view.backgroundColor = .white
           
           self.navigationController.navigationBar.isHidden = true
    }
        
    func start(root: Bool = false) {
        let interactor = AppkeeCustomLoaderInteractor()
        let presenter = AppkeeCustomLoaderPresenter(interactor: interactor, coordinator: self)
        let vc = AppkeeCustomLoaderViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc
        
        navigationController.setViewControllers([vc], animated: false)
        
        window.rootViewController = navigationController
    }        
}

// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeCustomLoaderCoordinator: AppkeeCustomLoaderCoordinatorInput {

    func navigate(to route: AppkeeCustomLoader.Route) {
        switch route {
        case .login, .signIn:
            delegate?.navigate(to: .menu)
        }
    }
}

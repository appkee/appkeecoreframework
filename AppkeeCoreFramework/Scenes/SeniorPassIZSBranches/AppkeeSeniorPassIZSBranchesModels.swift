//
//  AppkeeSeniorPassIZSBranchesModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/3/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeSeniorPassIZSBranches {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case navigate(Int)
    }

    enum Route {
        case open(AppkeeSeniorPassIZSBranch)
    }
}

extension AppkeeSeniorPassIZSBranches.Request {

}

extension AppkeeSeniorPassIZSBranches.Response {

}

extension AppkeeSeniorPassIZSBranches.DisplayData {
    
}

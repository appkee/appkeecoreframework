//
//  AppkeeSeniorPassIZSBranchesProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/3/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeeSeniorPassIZSBranchesCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeSeniorPassIZSBranchesCoordinatorInput: class {
    func navigate(to route: AppkeeSeniorPassIZSBranches.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeSeniorPassIZSBranchesInteractorInput {
    // func perform(_ request: AppkeeSeniorPassIZSBranches.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeSeniorPassIZSBranchesInteractorOutput: class {
    // func present(_ response: AppkeeSeniorPassIZSBranches.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeSeniorPassIZSBranchesPresenterInput {
    var numberOfSections: Int { get }
    func numberOfItems(in section: Int) -> Int
    
    func viewCreated()
    func handle(_ action: AppkeeSeniorPassIZSBranches.Action)

    func configure(_ item: AppkeeSeniorPassIZSBranchTableViewCellDelegate, at indexPath: IndexPath)
}

// PRESENTER -> VIEW
protocol AppkeeSeniorPassIZSBranchesPresenterOutput: class {
    // func display(_ displayModel: AppkeeSeniorPassIZSBranches.DisplayData.Work)

    func setupUI(colorSettings: AppkeeColorSettings)
}

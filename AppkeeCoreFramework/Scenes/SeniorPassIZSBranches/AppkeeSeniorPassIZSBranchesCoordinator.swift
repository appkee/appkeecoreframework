//
//  AppkeeSeniorPassIZSBranchesCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/3/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit


class AppkeeSeniorPassIZSBranchesCoordinator: AppkeeCoordinator {
    /// MARK: - Properties
    let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
    private let results: [AppkeeSeniorPassIZSBranch]
    private let email: String?
    private let search: Bool
//    weak var delegate: GalleryCoordinatorDelegate?

    var delegate: AppkeeSideMenuCoordinatorDelegate?

    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, results: [AppkeeSeniorPassIZSBranch], email: String?, search: Bool) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.results = results
        self.email = email
        self.search = search
    }

    func start(root: Bool = false) {
        let interactor = AppkeeSeniorPassIZSBranchesInteractor(repository: dependencies.repository, graphicManager: dependencies.graphicManager)
        let presenter = AppkeeSeniorPassIZSBranchesPresenter(interactor: interactor, coordinator: self, graphicManager: dependencies.graphicManager, results: results)
        let vc = AppkeeSeniorPassIZSBranchesViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

//        var menuItems: [UIBarButtonItem] = []
//        if dependencies.configManager.appMenu {
//            let image = UIImage(named: "dots", in: Bundle(for: AppkeeGalleryCoordinator.self), compatibleWith: nil)
//            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(debugMenu(_:))))
//        }
//        if search {
//            let image = UIImage(named: "search", in: Bundle(for: AppkeeGalleryCoordinator.self), compatibleWith: nil)
//            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(search(_:))))
//        }
//        if let _ = email {
//            let image = UIImage(named: "cameraMenu", in: Bundle(for: AppkeePageCoordinator.self), compatibleWith: nil)
//            menuItems.append(UIBarButtonItem(image: image, style: .done, target: self, action: #selector(photoReporter(_:))))
//        }
//        vc.navigationItem.rightBarButtonItems = menuItems

        vc.title = translations.seniorPass.BRANCH_IZS_TITLE

//        let rootController = AppkeeBannerViewController(graphicManager: dependencies.graphicManager)
//        rootController.setViewController(vc)
//        rootController.delegate = self

//        if let banners = section.banners {
//            rootController.setBanners(banners: banners)
//        }

//        if root {
//            let image = UIImage(named: "hamburger", in: Bundle(for: AppkeeGalleryCoordinator.self), compatibleWith: nil)
//            rootController.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  image, style: .done, target: self, action: #selector(menu(_:)))
//
//            navigationController.setViewControllers([rootController], animated: false)
//
//            return
//        }

        navigationController.navigationBar.isHidden = false
        navigationController.pushViewController(vc, animated: true)
    }

    @objc func menu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openMenu()
    }

    @objc func debugMenu(_ barButtonItem: UIBarButtonItem) {
        delegate?.openDebugMenu()
    }

    @objc func photoReporter(_ barButtonItem: UIBarButtonItem) {
        if let email = self.email {
            delegate?.openPhotoReporter(email: email)
        }
    }

    @objc func search(_ barButtonItem: UIBarButtonItem) {
        delegate?.openSearch()
    }
}

// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeSeniorPassIZSBranchesCoordinator: AppkeeSeniorPassIZSBranchesCoordinatorInput {
    func navigate(to route: AppkeeSeniorPassIZSBranches.Route) {
        switch route {
        case .open(let branch):
            let coordinator = AppkeeSeniorPassIZSBranchDetailCoordinator(navigationController: self.navigationController, dependencies: dependencies, branch: branch, email: email, search: search)
            childrens.append(coordinator)
            coordinator.delegate = delegate
            coordinator.start()
        }
    }

}

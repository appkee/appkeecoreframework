//
//  AppkeeSeniorPassIZSBranchTableViewCell.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/3/21.
//  Copyright © 2021 Radek Zmeskal. All rights reserved.
//

import UIKit

protocol AppkeeSeniorPassIZSBranchTableViewCellDelegate: class {
    func configure(with branch: AppkeeSeniorPassIZSBranch, colorSettings: AppkeeColorSettings?)
}

class AppkeeSeniorPassIZSBranchTableViewCell: UITableViewCell {

    @IBOutlet weak var containerLabel: UIView! {
        didSet {
            containerLabel.layer.cornerRadius = 8.0
        }
    }

    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var separatorView: UIView!

    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var addressTitleLabel: UILabel! {
        didSet {
            addressTitleLabel.text = translations.seniorPass.BRANCH_ADDRESS
        }
    }
    @IBOutlet weak var addressLabel: UILabel!

    @IBOutlet weak var distanceView: UIView!
    @IBOutlet weak var distanceTitleLabel: UILabel! {
        didSet {
            distanceTitleLabel.text = translations.seniorPass.BRANCH_IZS_DISTANCE
        }
    }

    @IBOutlet weak var distanceLabel: UILabel! {
        didSet {
            distanceLabel.text = ""
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension AppkeeSeniorPassIZSBranchTableViewCell: AppkeeSeniorPassIZSBranchTableViewCellDelegate {
    func configure(with branch: AppkeeSeniorPassIZSBranch, colorSettings: AppkeeColorSettings?) {
        nameLabel.text = branch.type + "\n" + branch.name
        addressLabel.text = branch.address

        if let distance = branch.distance {
            distanceLabel.text = String(format: translations.seniorPass.BRANCH_DISTANCE, distance/1000.0)
        } else {
            distanceView.isHidden = true
        }

        if let colorSettings = colorSettings {
            nameLabel.textColor = colorSettings.menu
        }
    }
}

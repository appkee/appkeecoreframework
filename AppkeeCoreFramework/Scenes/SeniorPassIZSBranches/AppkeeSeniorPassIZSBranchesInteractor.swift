//
//  AppkeeSeniorPassIZSBranchesInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/3/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeSeniorPassIZSBranchesInteractor {
    // MARK: - Properties
    weak var output: AppkeeSeniorPassIZSBranchesInteractorOutput?
    private let repository: AppkeeRepository
    private let graphicManager: AppkeeGraphicManager

    // MARK: - Init
    init(repository: AppkeeRepository, graphicManager: AppkeeGraphicManager) {
        self.repository = repository
        self.graphicManager = graphicManager
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeSeniorPassIZSBranchesInteractor: AppkeeSeniorPassIZSBranchesInteractorInput {
}

//
//  AppkeeSeniorPassIZSBranchesPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/3/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import Foundation


class AppkeeSeniorPassIZSBranchesPresenter {
    // MARK: - Properties
    private let graphicManager: AppkeeGraphicManager

    private let results: [AppkeeSeniorPassIZSBranch]

    let interactor: AppkeeSeniorPassIZSBranchesInteractorInput
    weak var coordinator: AppkeeSeniorPassIZSBranchesCoordinatorInput?
    weak var output: AppkeeSeniorPassIZSBranchesPresenterOutput?

    // MARK: - Init
    init(interactor: AppkeeSeniorPassIZSBranchesInteractorInput, coordinator: AppkeeSeniorPassIZSBranchesCoordinatorInput, graphicManager: AppkeeGraphicManager, results: [AppkeeSeniorPassIZSBranch]) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
        self.results = results
    }
}

// MARK: - User Events -

extension AppkeeSeniorPassIZSBranchesPresenter: AppkeeSeniorPassIZSBranchesPresenterInput {
    func configure(_ item: AppkeeSeniorPassIZSBranchTableViewCellDelegate, at indexPath: IndexPath) {
        let branch = self.results[indexPath.row]
        item.configure(with: branch, colorSettings: graphicManager.colorsSettings)
    }

    var numberOfSections: Int {
        return 1
    }

    func numberOfItems(in section: Int) -> Int {
        return self.results.count
    }

    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }
    }

    func handle(_ action: AppkeeSeniorPassIZSBranches.Action) {
        switch action {
        case .navigate(let index):
            let branch = results[index]
            coordinator?.navigate(to: .open(branch))
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeSeniorPassIZSBranchesPresenter: AppkeeSeniorPassIZSBranchesInteractorOutput {

}

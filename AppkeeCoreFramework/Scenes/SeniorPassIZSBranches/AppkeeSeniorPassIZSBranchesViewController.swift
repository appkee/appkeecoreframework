//
//  AppkeeSeniorPassIZSBranchesViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 11/3/21.
//  Copyright (c) 2021 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeSeniorPassIZSBranchesViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(AppkeeSeniorPassIZSBranchTableViewCell.nib, forCellReuseIdentifier: AppkeeSeniorPassIZSBranchTableViewCell.reuseId)
            tableView.estimatedRowHeight = 30
            tableView.separatorStyle = .none
            tableView.tableFooterView = UIView()
            tableView.rowHeight = UITableView.automaticDimension
        }
    }

    // MARK: - Properties
    private var presenter: AppkeeSeniorPassIZSBranchesPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeSeniorPassIZSBranchesPresenterInput) -> AppkeeSeniorPassIZSBranchesViewController {
        let name = "\(AppkeeSeniorPassIZSBranchesViewController.self)"
        let bundle = Bundle(for: AppkeeSeniorPassIZSBranchesViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeSeniorPassIZSBranchesViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
    }

    // MARK: - Callbacks -

}

extension AppkeeSeniorPassIZSBranchesViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.numberOfItems(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AppkeeSeniorPassIZSBranchTableViewCell.reuseId, for: indexPath) as! AppkeeSeniorPassIZSBranchTableViewCell
        presenter.configure(cell, at: indexPath)

        cell.selectionStyle = .none

        return cell
    }
}

extension AppkeeSeniorPassIZSBranchesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.handle(.navigate(indexPath.row))
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeSeniorPassIZSBranchesViewController: AppkeeSeniorPassIZSBranchesPresenterOutput {
    func setupUI(colorSettings: AppkeeColorSettings) {

        self.view.backgroundColor = colorSettings.content
    }
}

//
//  AppkeeLoginDialogInteractor.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 5/1/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeLoginDialogInteractor {
    // MARK: - Properties
    weak var output: AppkeeLoginDialogInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension AppkeeLoginDialogInteractor: AppkeeLoginDialogInteractorInput {
}

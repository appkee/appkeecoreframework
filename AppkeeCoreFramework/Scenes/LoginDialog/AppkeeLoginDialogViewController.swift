//
//  AppkeeLoginDialogViewController.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 5/1/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import UIKit

class AppkeeLoginDialogViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.text = translations.loginPage.DIALOG
        }
    }

    @IBOutlet weak var loginButton: UIButton! {
        didSet {
            loginButton.setTitle(translations.login.LOGIN.uppercased(), for: .normal)
        }
    }

    @IBOutlet weak var registerButton: UIButton! {
        didSet {
            registerButton.setTitle(translations.login.SIGNIN.uppercased(), for: .normal)
        }
    }

    @IBOutlet weak var cancelButton: UIButton! {
        didSet {
            cancelButton.setTitle(translations.common.CANCEL, for: .normal)
        }
    }

    // MARK: - Properties
    private var presenter: AppkeeLoginDialogPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: AppkeeLoginDialogPresenterInput) -> AppkeeLoginDialogViewController {
        let name = "\(AppkeeLoginDialogViewController.self)"
        let bundle = Bundle(for: AppkeeLoginDialogViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: bundle)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! AppkeeLoginDialogViewController
        vc.presenter = presenter
        return vc
    }

    @IBAction func loginButtonTap(_ sender: Any) {
        presenter.handle(.login)
    }

    @IBAction func registerButtonTap(_ sender: Any) {
        presenter.handle(.register)
    }


    @IBAction func closeButtonTap(_ sender: Any) {
        self.presenter.handle(.close)
    }

    @objc func handleTap(recognizer: UITapGestureRecognizer) {
        self.presenter.handle(.close)
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()

        view.isOpaque = false
        view.backgroundColor = .clear

        let recognizer = UITapGestureRecognizer(target: self,
                                                action: #selector(handleTap(recognizer:)))
        self.view.addGestureRecognizer(recognizer)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.navigationBar.isHidden = true
    }

    // MARK: - Callbacks -

}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension AppkeeLoginDialogViewController: AppkeeLoginDialogPresenterOutput {
    func setupUI(colorSettings: AppkeeColorSettings) {
        self.view.backgroundColor = colorSettings.content

        self.loginButton.backgroundColor = colorSettings.loginColor
        self.registerButton.backgroundColor = colorSettings.registerColor

        self.loginButton.setTitleColor(.white, for: .normal)
        self.registerButton.setTitleColor(.white, for: .normal)

        self.titleLabel.textColor = colorSettings.contentText

        self.cancelButton.setTitleColor(colorSettings.loginColor, for: .normal)
    }
}

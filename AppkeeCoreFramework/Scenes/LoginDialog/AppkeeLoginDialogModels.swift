//
//  AppkeeLoginDialogModels.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 5/1/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

enum AppkeeLoginDialog {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case login
        case register
        case close
    }

    enum Route {
        case login
        case register
        case close
    }
}

extension AppkeeLoginDialog.Request {

}

extension AppkeeLoginDialog.Response {

}

extension AppkeeLoginDialog.DisplayData {
    
}

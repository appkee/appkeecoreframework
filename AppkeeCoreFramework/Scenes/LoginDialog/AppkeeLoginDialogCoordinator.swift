//
//  AppkeeLoginDialogCoordinator.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 5/1/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

protocol AppkeeLoginDialogCoordinatorDelegate {
    func logged()
}

class AppkeeLoginDialogCoordinator: AppkeeCoordinator {
    // MARK: - Properties
    private let navigationController: UINavigationController
    // NOTE: This array is used to retain child coordinators. Don't forget to
    // remove them when the coordinator is done.
    var childrens: [AppkeeCoordinator] = []
    var dependencies: AppkeeFullDependencies
    let appDescription: AppkeeAppDescription?
    //    weak var delegate: AppkeeFavoritesCoordinatorDelegate?

    var delegate: AppkeeSideMenuCoordinatorDelegate?
//    var delegate: AppkeeLoginDialogCoordinatorDelegate?

    private let childrenNavController = UINavigationController()
    private let section: AppkeeSection

    // MARK: - Init
    init(navigationController: UINavigationController, dependencies: AppkeeFullDependencies, appDescription: AppkeeAppDescription?, section: AppkeeSection) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.appDescription = appDescription
        self.section = section
    }

    func start(root: Bool = false) {
        let interactor = AppkeeLoginDialogInteractor()
        let presenter = AppkeeLoginDialogPresenter(interactor: interactor, coordinator: self, graphicManager: dependencies.graphicManager)
        let vc = AppkeeLoginDialogViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc

        vc.title = ""

        vc.modalPresentationStyle = .overCurrentContext

        childrenNavController.setViewControllers([vc], animated: false)
        childrenNavController.navigationBar.isHidden = true

        navigationController.topViewController?.present(childrenNavController, animated: true, completion: nil)
    }

    func showLogin() {
        let coordinator = AppkeeLoginRegisterLoginCoordinator(navigationController: childrenNavController,
                                          dependencies: dependencies)
        childrens.append(coordinator)
        coordinator.start(root: false)
        coordinator.loginDialogDelegate = self
    }

    func showRegister() {
        let coordinator = AppkeeLoginRegisterRegisterCoordinator(navigationController: childrenNavController,
                                                                 dependencies: dependencies,
                                                                 style: .dialog)
        childrens.append(coordinator)
        coordinator.start(root: false)
        coordinator.delegate = self.delegate
    }
}
// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension AppkeeLoginDialogCoordinator: AppkeeLoginDialogCoordinatorInput {
    func navigate(to route: AppkeeLoginDialog.Route) {
        switch route {
        case .login:
            showLogin()
        case .register:
            showRegister()
        case .close:
            navigationController.dismiss(animated: true)
        }
    }
}

extension AppkeeLoginDialogCoordinator: AppkeeLoginDialogCoordinatorDelegate {
    func logged() {
        self.delegate?.reload()
    }
}

//
//  AppkeeLoginDialogPresenter.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 5/1/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation

class AppkeeLoginDialogPresenter {
    // MARK: - Properties
    private let graphicManager: AppkeeGraphicManager

    let interactor: AppkeeLoginDialogInteractorInput
    weak var coordinator: AppkeeLoginDialogCoordinatorInput?
    weak var output: AppkeeLoginDialogPresenterOutput?

    // MARK: - Init
    init(interactor: AppkeeLoginDialogInteractorInput, coordinator: AppkeeLoginDialogCoordinatorInput, graphicManager: AppkeeGraphicManager) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.graphicManager = graphicManager
    }
}

// MARK: - User Events -

extension AppkeeLoginDialogPresenter: AppkeeLoginDialogPresenterInput {
    func viewCreated() {
        if let colorsSettings = graphicManager.colorsSettings {
            output?.setupUI(colorSettings: colorsSettings)
        }
    }

    func handle(_ action: AppkeeLoginDialog.Action) {
        switch action {
        case .login:
            coordinator?.navigate(to: .login)
        case .register:
            coordinator?.navigate(to: .register)
        case .close:
            coordinator?.navigate(to: .close)
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension AppkeeLoginDialogPresenter: AppkeeLoginDialogInteractorOutput {

}

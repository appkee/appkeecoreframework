//
//  AppkeeLoginDialogProtocols.swift
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 5/1/22.
//  Copyright (c) 2022 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol AppkeeLoginDialogCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol AppkeeLoginDialogCoordinatorInput: class {
    func navigate(to route: AppkeeLoginDialog.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol AppkeeLoginDialogInteractorInput {
    // func perform(_ request: AppkeeLoginDialog.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol AppkeeLoginDialogInteractorOutput: class {
    // func present(_ response: AppkeeLoginDialog.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol AppkeeLoginDialogPresenterInput {
    func viewCreated()
    func handle(_ action: AppkeeLoginDialog.Action)
}

// PRESENTER -> VIEW
protocol AppkeeLoginDialogPresenterOutput: class {
    // func display(_ displayModel: AppkeeLoginDialog.DisplayData.Work)

    func setupUI(colorSettings: AppkeeColorSettings)
}

//
//  AppkeeCoreFramework.h
//  AppkeeCoreFramework
//
//  Created by Radek Zmeskal on 01/03/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for AppkeeCoreFramework.
FOUNDATION_EXPORT double AppkeeCoreFrameworkVersionNumber;

//! Project version string for AppkeeCoreFramework.
FOUNDATION_EXPORT const unsigned char AppkeeCoreFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AppkeeCoreFramework/PublicHeader.h>


